import gql from 'graphql-tag';

export const ADD_TRANSPORT_PROFILE = gql`
mutation AddTransportProfile( 
    $destinationType: String,
    $nameOfHandoverPerson: String,
    $nicOfHandoverPerson: String,
    $numberOfHandoverPerson: String,
    $relationshipOfHandoverPerson: String,
    $hospitalName: String,
    $transportMethod: String,
    $slbfeOffices: JSON,
    $departedOnTheirOwn: String,
    $thirdPartyDetails: JSON,
    $departureLocation: String,
    $destinationDistrict: String,
    $destinationLocation: String,
    $expectedDepartureTime: String,
    $expectedArrivalTime: String,
    $actualDepartureTime: String,
    $actualArrivalTime: String,
    $taxiFare: String,
    $locationOfPayment: String,
    $migrantId: String!){
        addTransportProfile( 
            destinationType:$destinationType,
            nameOfHandoverPerson:$nameOfHandoverPerson,
            nicOfHandoverPerson:$nicOfHandoverPerson,
            numberOfHandoverPerson:$numberOfHandoverPerson,
            relationshipOfHandoverPerson: $relationshipOfHandoverPerson,
            hospitalName: $hospitalName,
            transportMethod:$transportMethod,
            slbfeOffices:$slbfeOffices,
            departedOnTheirOwn:$departedOnTheirOwn,
            thirdPartyDetails:$thirdPartyDetails,
            departureLocation:$departureLocation,
            destinationDistrict:$destinationDistrict,
            destinationLocation:$destinationLocation,
            expectedDepartureTime:$expectedDepartureTime,
            expectedArrivalTime:$expectedArrivalTime,
            actualDepartureTime:$actualDepartureTime,
            actualArrivalTime:$actualArrivalTime,
            taxiFare:$taxiFare,
            locationOfPayment:$locationOfPayment,
            migrantId:$migrantId
        ){
            id
            destinationType
            nameOfHandoverPerson
            nicOfHandoverPerson
            numberOfHandoverPerson
            relationshipOfHandoverPerson
            hospitalName
            transportMethod
            slbfeOffices
            departedOnTheirOwn
            thirdPartyDetails
            departureLocation
            destinationDistrict
            destinationLocation
            expectedDepartureTime
            expectedArrivalTime
            actualDepartureTime
            actualArrivalTime
            taxiFare
            locationOfPayment
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const EDIT_TRANSPORT_PROFILE = gql`
mutation EditTransportProfile( 
        $id: String!,
        $destinationType: String,
        $nameOfHandoverPerson: String,
        $nicOfHandoverPerson: String,
        $numberOfHandoverPerson: String,
        $relationshipOfHandoverPerson: String,
        $hospitalName: String,
        $transportMethod: String,
        $slbfeOffices: JSON,
        $departedOnTheirOwn: String,
        $thirdPartyDetails: JSON,
        $departureLocation: String,
        $destinationDistrict: String,
        $destinationLocation: String,
        $expectedDepartureTime: String,
        $expectedArrivalTime: String,
        $actualDepartureTime: String,
        $actualArrivalTime: String,
        $taxiFare: String,
        $locationOfPayment: String,
    ){
        editTransportProfile( 
            id:$id,
            destinationType:$destinationType,
            nameOfHandoverPerson:$nameOfHandoverPerson,
            nicOfHandoverPerson:$nicOfHandoverPerson,
            numberOfHandoverPerson:$numberOfHandoverPerson,
            relationshipOfHandoverPerson: $relationshipOfHandoverPerson,
            transportMethod:$transportMethod,
            slbfeOffices:$slbfeOffices,
            departedOnTheirOwn:$departedOnTheirOwn,
            thirdPartyDetails:$thirdPartyDetails,
            departureLocation:$departureLocation,
            destinationDistrict:$destinationDistrict,
            destinationLocation:$destinationLocation,
            expectedDepartureTime:$expectedDepartureTime,
            expectedArrivalTime:$expectedArrivalTime,
            actualDepartureTime:$actualDepartureTime,
            actualArrivalTime:$actualArrivalTime,
            taxiFare:$taxiFare,
            locationOfPayment:$locationOfPayment,
        ){
            id
            destinationType
            nameOfHandoverPerson
            nicOfHandoverPerson
            numberOfHandoverPerson
            relationshipOfHandoverPerson
            hospitalName
            transportMethod
            slbfeOffices
            departedOnTheirOwn
            thirdPartyDetails
            departureLocation
            destinationDistrict
            destinationLocation
            expectedDepartureTime
            expectedArrivalTime
            actualDepartureTime
            actualArrivalTime
            taxiFare
            locationOfPayment
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const GET_SINGLE_TRANSPORT_PROFILE = gql`
query GetSingleTransportProfile( $migrantId: String!){
    getSingleTransportProfile( migrantId:$migrantId){
            id
            destinationType
            nameOfHandoverPerson
            nicOfHandoverPerson
            numberOfHandoverPerson
            relationshipOfHandoverPerson
            hospitalName
            transportMethod
            slbfeOffices
            departedOnTheirOwn
            thirdPartyDetails
            departureLocation
            destinationDistrict
            destinationLocation
            expectedDepartureTime
            expectedArrivalTime
            actualDepartureTime
            actualArrivalTime
            taxiFare
            locationOfPayment
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const GET_TRANSPORT_HISTORY_PROFILES = gql`
query GetTransportHistoryProfiles( $migrantId: String!){
    getTransportHistoryProfiles( migrantId:$migrantId){
        id
        destinationType
        nameOfHandoverPerson
        nicOfHandoverPerson
        numberOfHandoverPerson
        relationshipOfHandoverPerson
        hospitalName
        transportMethod
        slbfeOffices
        departedOnTheirOwn
        thirdPartyDetails
        departureLocation
        destinationDistrict
        destinationLocation
        expectedDepartureTime
        expectedArrivalTime
        actualDepartureTime
        actualArrivalTime
        taxiFare
        locationOfPayment
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`