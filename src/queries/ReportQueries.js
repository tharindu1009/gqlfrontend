import gql from 'graphql-tag';

export const GET_SINGLE_USER = gql`
query searchMigrants( $id: String!){
    searchMigrants(id:$id){
            id
            name
            email
            phoneNumber
            userRole
            notifications
            createdAt
            updatedAt
            isActive
            createdBy
    }
}
`

export const MIGRANT_SICKNESS_REPORT = gql`
query MigrantSicknessReport( $sicknessName: String, $from: String, $to: String){
    migrantSicknessReport( sicknessName:$sicknessName,from:$from,to:$to)
}
`

export const MIGRANT_REFERREL_REPORT = gql`
query MigrantReferrelReport($referrelType: String,$from: String, $to: String){
    migrantReferrelReport(referrelType: $referrelType,from:$from,to:$to)
}
`

export const MIGRANT_MEDICAL_TREATMENT_REPORT = gql`
query MigrantMedicalTreatmentReport( $medicalTreatmentCategory: String, $from: String, $to: String){
    migrantMedicalTreatmentReport( medicalTreatmentCategory:$medicalTreatmentCategory,from:$from,to:$to)
}
`

export const MIGRANT_FOLLOWUP_REPORT = gql`
query MigrantFollowupReport( $nic: String,$passport: String, $from: String, $to: String){
    migrantFollowupReport( nic:$nic,passport:$passport,from:$from,to:$to)
}
`

export const RETURNED_REASON_REPORT = gql`
query ReturnedReasonReport( $returnedReason: String, $from: String, $to: String){
    returnedReasonReport( returnedReason:$returnedReason,from:$from,to:$to)
}
`