import gql from 'graphql-tag';

export const GET_SINGLE_USER = gql`
query searchMigrants( $id: String!){
    searchMigrants(id:$id){
            id
            name
            email
            phoneNumber
            userRole
            notifications
            createdAt
            updatedAt
            isActive
            createdBy
    }
}
`

export const SEARCH_MIGRANTS = gql`
query SearchMigrants( $searchTerm: String ,$skip: Int,$first: Int){
    searchMigrants( searchTerm:$searchTerm,skip:$skip,first:$first){
        migrants{
            id
            fullName
            address
            telNumber
            mobileNumber
            nic
            passport
            registeredSLBFE
            registrationNumber
            maritalStatus
            gender
            isActive
            createdAt
            createdBy
        }
        count
    }
}
`


export const GET_MIGRANT_BASIC_HISTORY_PRIFILES = gql`
query GetMigrantBasicHistoryProfiles( $migrantId: String!){
    getMigrantBasicHistoryProfiles( migrantId:$migrantId){
        id
        fullName
        address
        telNumber
        mobileNumber
        nic
        passport
        registeredSLBFE
        registrationNumber
        maritalStatus
        gender
        conditionOfTheMigrant
        closeupStatus
        closeupAction
        recievedToSahanaPiyasa
        releasedFromSahanaPiyasa
        migrantId
        createdAt
        updatedAt
        isActive
        createdBy
    }
}
`

