import gql from 'graphql-tag';

export const ADD_FOLLOWUP_PROFILE= gql`
mutation AddFollowupProfile( 
    $followupAction: String,
    $followupDate: String,
    $executeOfficer: String,
    $followupStatus: String,
    $followupNote: String,
    $migrantId: String!){
        addFollowupProfile( 
            followupAction:$followupAction,
            followupDate:$followupDate,
            executeOfficer:$executeOfficer,
            followupStatus:$followupStatus,
            followupNote:$followupNote,
            migrantId:$migrantId
        ){
            id
            followupAction
            followupDate
            executeOfficer
            followupStatus
            followupNote
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const EDIT_FOLLOWUP_PROFILE = gql`
mutation EditFollowupProfile( 
    $id: String!,
    $followupAction: String,
    $followupDate: String,
    $executeOfficer: String,
    $followupStatus: String,
    $followupNote: String,
    ){
        editFollowupProfile( 
            id:$id,
            followupAction:$followupAction,
            followupDate:$followupDate,
            executeOfficer:$executeOfficer,
            followupStatus:$followupStatus,
            followupNote:$followupNote,
        ){
            id
            followupAction
            followupDate
            executeOfficer
            followupStatus
            followupNote
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const GET_SINGLE_FOLLOWUP_PROFILE = gql`
query GetSingleFollowupProfile( $migrantId: String!){
    getSingleFollowupProfile( migrantId:$migrantId){
            id
            followupAction
            followupDate
            executeOfficer
            followupStatus
            followupNote
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const GET_FOLLOWUP_ACTION_HISTORIES = gql`
query GetFollowupActionHistories( $migrantId: String!){
    getFollowupActionHistories( migrantId:$migrantId){
        id
        followupAction
        followupDate
        executeOfficer
        followupStatus
        followupNote
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`