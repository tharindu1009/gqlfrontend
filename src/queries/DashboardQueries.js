import gql from 'graphql-tag';

export const GET_SYSTEM_USER_COUNT = gql`
query GetSystemUserCount{
    getSystemUserCount
}
`
export const GET_MIGRANT_COUNT = gql`
query GetMigrantCount{
    getMigrantCount
}
`

export const GET_FOREIGN_MISSION_ONBOARDING_COUNT = gql`
query GetForeignMissionOnboaringCount{
    getForeignMissionOnboaringCount
}
`

export const GET_MEDICAL_TREATMENT_COUNT = gql`
query GetMedicalTreatmentCount{
    getMedicalTreatmentCount
}
`