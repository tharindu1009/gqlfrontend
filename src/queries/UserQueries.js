import gql from 'graphql-tag';


export const LOGIN = gql`
mutation Login( $userName: String!, $password: String!){
        login( userName:$userName, password: $password ){
            token
            user{
                id
                name
                email
                phoneNumber
                userName
                password
                userRole
                isActive
            }
        }
}
`


export const ADD_NEW_USER = gql`
mutation AddNewUser($name: String!,$email:String!,$phoneNumber:String,$userRole:UserRole!,
    $organization: String,
    $districtId: String,
    $district: String,
    $department: String,
    $isActive:Boolean,$userName: String!,$password: String!) {
  signup(name:$name,email:$email,phoneNumber:$phoneNumber,userRole:$userRole,
        organization:$organization,
        districtId:$districtId,
        district:$district,
        department:$department,
        isActive:$isActive,userName: $userName,password:$password)
    {
      token
      user{
        id
        name
        email
        phoneNumber
        userRole
        organization
        districtId
        district
        department
        notifications
        createdAt
        updatedAt
        isActive
        createdBy
      }
    }
  }
`


export const EDIT_USER = gql`
mutation EditUser( 
    $id: String!,
    $name: String!,
    $phoneNumber: String,
    $organization: String,
    $districtId: String,
    $district: String,
    $department: String,
    $isActive: Boolean
    ){
        editUser( 
            id:$id,
            name:$name,
            phoneNumber:$phoneNumber,
            organization:$organization,
            districtId:$districtId,
            district:$district,
            department:$department,
            isActive:$isActive
        ){
            id
            name
            email
            phoneNumber
            userRole
            organization
            districtId
            district
            department
            notifications
            createdAt
            updatedAt
            isActive
            createdBy
    }
}
`
export const GET_SINGLE_USER = gql`
query GetSingleUser( $id: String!){
    getSingleUser(id:$id){
            id
            name
            email
            phoneNumber
            userRole
            organization
            districtId
            district
            department
            notifications
            createdAt
            updatedAt
            isActive
            createdBy
    }
}
`

export const GET_ALL_USERS = gql`
query GetAllUsers( $skip: Int,$first: Int){
    getAllUsers( skip:$skip,first:$first){
        users{
            id
            name
            email
            phoneNumber
            userRole
            organization
            districtId
            district
            department
            notifications
            isActive
            createdAt
            createdBy
        }
        count
    }
}
`
