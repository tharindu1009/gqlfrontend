import gql from 'graphql-tag';

export const ADD_MIGRANT_GENERAL_PROFILE = gql`
mutation AddMigrantGeneralProfile( $fullName: String!, $address: String, $telNumber: String ,$mobileNumber: String, $nic: String!, $passport:String,$tempPassport:String, $registeredSLBFE: Boolean, $registrationNumber: String, $maritalStatus:MaritalStatus,$gender:Gender,
        $conditionOfTheMigrant: String,
        $recievedToSahanaPiyasa: String,
        $releasedFromSahanaPiyasa: String,
        $isActive:Boolean){
            addMigrantGeneralProfile( fullName:$fullName, address: $address ,telNumber: $telNumber,mobileNumber:$mobileNumber ,nic:$nic, passport:$passport,tempPassport:$tempPassport, registeredSLBFE:$registeredSLBFE, registrationNumber:$registrationNumber, maritalStatus:$maritalStatus,gender:$gender,
                conditionOfTheMigrant: $conditionOfTheMigrant,
                recievedToSahanaPiyasa: $recievedToSahanaPiyasa,
                releasedFromSahanaPiyasa: $releasedFromSahanaPiyasa,
                ,isActive:$isActive){
                    id
                    fullName
                    address
                    telNumber
                    mobileNumber
                    nic
                    passport
                    tempPassport
                    registeredSLBFE
                    registrationNumber
                    maritalStatus
                    gender
                    conditionOfTheMigrant
                    closeupStatus
                    closeupAction
                    recievedToSahanaPiyasa
                    releasedFromSahanaPiyasa
                    isActive
            }
}
`

export const EDIT_MIGRANT_GENERAL_PROFILE = gql`
mutation EditMigrantGeneralProfile( $id: String!,$fullName: String, $address: String, $telNumber: String ,$mobileNumber: String, $maritalStatus:MaritalStatus,$gender:Gender,
    $conditionOfTheMigrant: String,
    $recievedToSahanaPiyasa: String,
    $releasedFromSahanaPiyasa: String,
    $isActive:Boolean){
        editMigrantGeneralProfile( id:$id,fullName:$fullName, address: $address ,telNumber: $telNumber,mobileNumber:$mobileNumber , maritalStatus:$maritalStatus,gender:$gender,
            conditionOfTheMigrant: $conditionOfTheMigrant,
            recievedToSahanaPiyasa: $recievedToSahanaPiyasa,
            releasedFromSahanaPiyasa: $releasedFromSahanaPiyasa,
            isActive:$isActive){
                fullName
                address
                telNumber
                mobileNumber
                nic
                passport
                tempPassport
                registeredSLBFE
                registrationNumber
                maritalStatus
                gender
                conditionOfTheMigrant
                closeupStatus
                closeupAction
                recievedToSahanaPiyasa
                releasedFromSahanaPiyasa
                isActive
            }
}
`

export const GET_SINGLE_MIGRANT_GENERAL_PROFILE = gql`
query GetSingleMigrantGeneralProfile( $id: String!){
    getSingleMigrantGeneralProfile( id:$id){
        id
        fullName
        address
        telNumber
        mobileNumber
        nic
        passport
        tempPassport
        registeredSLBFE
        registrationNumber
        maritalStatus
        gender
        conditionOfTheMigrant
        closeupStatus
        closeupAction
        recievedToSahanaPiyasa
        releasedFromSahanaPiyasa
        isActive
        createdAt
        createdBy
    }
}
`

export const GET_SINGLE_MIGRANT_TRAVEL_PROFILE = gql`
query GetSingleMigrantTravelProfile( $migrantId: String!){
    getSingleMigrantTravelProfile( migrantId:$migrantId){
        id
        numOfTimesMigrated
        travelType
        arrivalCountry
        arrivalDate
        contractPeriod
        returnedReason
        custodyOfSLBFE
        officialStatement
        confirmOfStatement
        comments
        flightNumber
        timeOfFlight
        lastOccupationCountry
        sourceOfEmployment
        agencyName
        agencyLicenseNo
        profilePriority
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`

export const ADD_MIGRANT_TRAVEL_PROFILE = gql`
mutation AddMigrantTravelProfile( $numOfTimesMigrated: Int, $travelType: String,$arrivalCountry: String,$arrivalDate: String,$contractPeriod: String, $returnedReason: String ,$custodyOfSLBFE: String, $officialStatement: String, $confirmOfStatement:String, $comments: String, $flightNumber: String, $timeOfFlight: String,$lastOccupationCountry:String,
    $sourceOfEmployment: String
    $agencyName: String
    $agencyLicenseNo: String
    $profilePriority:String,
    $migrantId: String!){
        addMigrantTravelProfile( numOfTimesMigrated:$numOfTimesMigrated, travelType: $travelType ,arrivalCountry:$arrivalCountry,arrivalDate:$arrivalDate,contractPeriod:$contractPeriod,returnedReason: $returnedReason,custodyOfSLBFE:$custodyOfSLBFE ,officialStatement:$officialStatement, confirmOfStatement:$confirmOfStatement, comments:$comments, flightNumber:$flightNumber, timeOfFlight:$timeOfFlight,lastOccupationCountry:$lastOccupationCountry,
            sourceOfEmployment:$sourceOfEmployment,
            agencyName:$agencyName,
            agencyLicenseNo:$agencyLicenseNo,
            profilePriority:$profilePriority,
            migrantId:$migrantId){
                numOfTimesMigrated
                travelType
                arrivalCountry
                arrivalDate
                contractPeriod
                returnedReason
                custodyOfSLBFE
                officialStatement
                confirmOfStatement
                comments
                flightNumber
                timeOfFlight
                lastOccupationCountry
                sourceOfEmployment
                agencyName
                agencyLicenseNo
                profilePriority
                migrantId
        }
}
`

export const EDIT_MIGRANT_TRAVEL_PROFILE = gql`
mutation EditMigrantTravelProfile( $id: String!,$numOfTimesMigrated: Int, $travelType: String,$arrivalCountry: String,$arrivalDate: String,$contractPeriod: String, $returnedReason: String ,$custodyOfSLBFE: String, $officialStatement: String, $confirmOfStatement:String, $comments: String, $flightNumber: String, $timeOfFlight: String,$lastOccupationCountry:String,
    $sourceOfEmployment: String
    $agencyName: String
    $agencyLicenseNo: String
    $profilePriority:String){
        editMigrantTravelProfile( id:$id,numOfTimesMigrated:$numOfTimesMigrated, travelType: $travelType ,arrivalCountry:$arrivalCountry,arrivalDate:$arrivalDate,contractPeriod:$contractPeriod,returnedReason: $returnedReason,custodyOfSLBFE:$custodyOfSLBFE ,officialStatement:$officialStatement, confirmOfStatement:$confirmOfStatement, comments:$comments, flightNumber:$flightNumber, timeOfFlight:$timeOfFlight,lastOccupationCountry:$lastOccupationCountry,
            sourceOfEmployment:$sourceOfEmployment,
            agencyName:$agencyName,
            agencyLicenseNo:$agencyLicenseNo,
            profilePriority:$profilePriority){
                numOfTimesMigrated
                travelType
                arrivalCountry
                arrivalDate
                contractPeriod
                returnedReason
                custodyOfSLBFE
                officialStatement
                confirmOfStatement
                comments
                flightNumber
                timeOfFlight
                lastOccupationCountry
                sourceOfEmployment
                agencyName
                agencyLicenseNo
                profilePriority
                migrantId
            }
}
`


export const GET_SINGLE_SCHEDULED_ARRIVAL_PROFILE = gql`
query GetSingleSheduledArrivalProfile( $migrantId: String!){
    getSingleSheduledArrivalProfile( migrantId:$migrantId){
        id
        departureLocation
        scheduledFlightNo
        scheduledFlightDatetime
        transitInformations
        scheduledArrival
        suggestedAction
        suggestedActionNote
        foreignMissionProfilePriority
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`

export const ADD_SCHEDULED_ARRIVAL_PROFILE = gql`
mutation AddSheduledArrivalProfile( $departureLocation: String, $scheduledFlightNo: String, $scheduledFlightDatetime: String ,$transitInformations: JSON, $scheduledArrival: String, $suggestedAction:String,$suggestedActionNote:String, $foreignMissionProfilePriority: String, $migrantId: String!){
    addSheduledArrivalProfile( departureLocation:$departureLocation, scheduledFlightNo: $scheduledFlightNo ,scheduledFlightDatetime: $scheduledFlightDatetime,transitInformations:$transitInformations ,scheduledArrival:$scheduledArrival, suggestedAction:$suggestedAction,suggestedActionNote:$suggestedActionNote, foreignMissionProfilePriority:$foreignMissionProfilePriority, migrantId:$migrantId){
        departureLocation
        scheduledFlightNo
        scheduledFlightDatetime
        transitInformations
        scheduledArrival
        suggestedAction
        suggestedActionNote
        foreignMissionProfilePriority
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`

export const EDIT_SCHEDULED_ARRIVAL_PROFILE = gql`
mutation EditSheduledArrivalProfile( $id: String!,$departureLocation: String, $scheduledFlightNo: String, $scheduledFlightDatetime: String ,$transitInformations: JSON, $scheduledArrival: String, $suggestedAction:String,$suggestedActionNote:String, $foreignMissionProfilePriority: String){
    editSheduledArrivalProfile( id:$id,departureLocation:$departureLocation, scheduledFlightNo: $scheduledFlightNo ,scheduledFlightDatetime: $scheduledFlightDatetime,transitInformations:$transitInformations ,scheduledArrival:$scheduledArrival, suggestedAction:$suggestedAction,suggestedActionNote:$suggestedActionNote, foreignMissionProfilePriority:$foreignMissionProfilePriority){
        id
        departureLocation
        scheduledFlightNo
        scheduledFlightDatetime
        transitInformations
        scheduledArrival
        suggestedAction
        suggestedActionNote
        foreignMissionProfilePriority
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`

export const GET_MIGRANT_TRAVEL_HISTORY_PROFILES = gql`
query GetMigrantTravelHistoryProfiles( $migrantId: String!){
    getMigrantTravelHistoryProfiles( migrantId:$migrantId){
        id
        numOfTimesMigrated
        travelType
        arrivalCountry
        arrivalDate
        contractPeriod
        returnedReason
        custodyOfSLBFE
        officialStatement
        confirmOfStatement
        comments
        flightNumber
        timeOfFlight
        lastOccupationCountry
        profilePriority
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`

export const GET_SCHEDULED_ARRIVAL_HISTORY_PROFILES = gql`
query GetSheduledArrivalHistoryProfiles( $migrantId: String!){
    getSheduledArrivalHistoryProfiles( migrantId:$migrantId){
        id
        departureLocation
        scheduledFlightNo
        scheduledFlightDatetime
        transitInformations
        scheduledArrival
        suggestedAction
        suggestedActionNote
        foreignMissionProfilePriority
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`

