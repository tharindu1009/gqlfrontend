import gql from 'graphql-tag';

export const ADD_BASIC_MEDICAL_PROFILE = gql`
mutation AddBasicMedicalProfile( 
    $ethnicity: JSON,
    $haveChildren: String,
    $numOfChildren:Int,
    $sexualOrientation: String,
    $familyHistory: JSON,
    $otherMajorConditions: String,
    $isCurrentlyTreatedMedicalCondition:String,
    $currentMedicalCondition:String,
    $allergies: String,
    $emergencyContact: JSON,
    $migrantId: String!){
        addBasicMedicalProfile( 
            ethnicity:$ethnicity ,
            haveChildren:$haveChildren,
            numOfChildren:$numOfChildren,
            sexualOrientation:$sexualOrientation,
            familyHistory:$familyHistory,
            otherMajorConditions:$otherMajorConditions,
            isCurrentlyTreatedMedicalCondition:$isCurrentlyTreatedMedicalCondition,
            currentMedicalCondition:$currentMedicalCondition,
            allergies:$allergies,
            emergencyContact: $emergencyContact,
            migrantId:$migrantId
        ){
            id
            ethnicity
            haveChildren
            numOfChildren
            sexualOrientation
            familyHistory
            otherMajorConditions
            isCurrentlyTreatedMedicalCondition
            currentMedicalCondition
            allergies
            emergencyContact
            closeupStatus
            closeupAction
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const EDIT_BASIC_MEDICAL_PROFILE = gql`
mutation EditBasicMedicalProfile( 
    $id: String!,
    $ethnicity: JSON,
    $haveChildren: String,
    $numOfChildren:Int,
    $sexualOrientation: String,
    $familyHistory: JSON,
    $otherMajorConditions: String,
    $isCurrentlyTreatedMedicalCondition:String,
    $currentMedicalCondition:String,
    $allergies: String,
    $emergencyContact: JSON){
        editBasicMedicalProfile( 
            id:$id,
            ethnicity:$ethnicity ,
            haveChildren:$haveChildren,
            numOfChildren:$numOfChildren,
            sexualOrientation:$sexualOrientation,
            familyHistory:$familyHistory,
            otherMajorConditions:$otherMajorConditions,
            isCurrentlyTreatedMedicalCondition:$isCurrentlyTreatedMedicalCondition,
            currentMedicalCondition:$currentMedicalCondition,
            allergies:$allergies,
            emergencyContact: $emergencyContact,
        ){
            id
            ethnicity
            haveChildren
            numOfChildren
            sexualOrientation
            familyHistory
            otherMajorConditions
            isCurrentlyTreatedMedicalCondition
            currentMedicalCondition
            allergies
            emergencyContact
            closeupStatus
            closeupAction
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const GET_SINGLE_BASIC_MEDICAL_PROFILE = gql`
query GetSingleBasicMedicalProfile( $migrantId: String!){
    getSingleBasicMedicalProfile( migrantId:$migrantId){
        id
        ethnicity
        haveChildren
        numOfChildren
        sexualOrientation
        familyHistory
        otherMajorConditions
        isCurrentlyTreatedMedicalCondition
        currentMedicalCondition
        allergies
        emergencyContact
        closeupStatus
        closeupAction
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`


export const ADD_SEXUAL_RISK_PROFILE = gql`
mutation AddSexualRiskProfile( 
    $isSmoke: String,
    $timesOfSmoke: String,
    $isUseAlchohol: String ,
    $capacityOfAlchohol: String,
    $isPartnerUseDrugs: String,
    $capacityOfDrugs: String,
    $isEverInjectedDrugs: String,
    $isLikeHelpDrugProblems: String,
    $isLikeDiscussPhysicalAbuse: String,
    $hasThreatnedRelationship:String,
    $migrantId: String!){
        addSexualRiskProfile( 
            isSmoke:$isSmoke,
            timesOfSmoke: $timesOfSmoke ,
            isUseAlchohol: $isUseAlchohol,
            capacityOfAlchohol:$capacityOfAlchohol ,
            isPartnerUseDrugs:$isPartnerUseDrugs,
            capacityOfDrugs:$capacityOfDrugs,
            isEverInjectedDrugs:$isEverInjectedDrugs,
            isLikeHelpDrugProblems:$isLikeHelpDrugProblems,
            isLikeDiscussPhysicalAbuse:$isLikeDiscussPhysicalAbuse,
            hasThreatnedRelationship:$hasThreatnedRelationship,
            migrantId:$migrantId
        ){
            id
            isSmoke
            timesOfSmoke
            isUseAlchohol
            capacityOfAlchohol
            isPartnerUseDrugs
            capacityOfDrugs
            isEverInjectedDrugs
            isLikeHelpDrugProblems
            isLikeDiscussPhysicalAbuse
            hasThreatnedRelationship 
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const EDIT_SEXUAL_RISK_PROFILE = gql`
mutation EditSexualRiskProfile( 
    $id: String!,
    $isSmoke: String,
    $timesOfSmoke: String,
    $isUseAlchohol: String ,
    $capacityOfAlchohol: String,
    $isPartnerUseDrugs: String,
    $capacityOfDrugs: String,
    $isEverInjectedDrugs: String,
    $isLikeHelpDrugProblems: String,
    $isLikeDiscussPhysicalAbuse: String,
    $hasThreatnedRelationship:String,
    ){
        editSexualRiskProfile( 
            id:$id,
            isSmoke:$isSmoke,
            timesOfSmoke: $timesOfSmoke ,
            isUseAlchohol: $isUseAlchohol,
            capacityOfAlchohol:$capacityOfAlchohol ,
            isPartnerUseDrugs:$isPartnerUseDrugs,
            capacityOfDrugs:$capacityOfDrugs,
            isEverInjectedDrugs:$isEverInjectedDrugs,
            isLikeHelpDrugProblems:$isLikeHelpDrugProblems,
            isLikeDiscussPhysicalAbuse:$isLikeDiscussPhysicalAbuse,
            hasThreatnedRelationship:$hasThreatnedRelationship,
        ){
            id
            isSmoke
            timesOfSmoke
            isUseAlchohol
            capacityOfAlchohol
            isPartnerUseDrugs
            capacityOfDrugs
            isEverInjectedDrugs
            isLikeHelpDrugProblems
            isLikeDiscussPhysicalAbuse
            hasThreatnedRelationship 
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const GET_SINGLE_SEXUAL_RISK_PROFILE = gql`
query GetSingleSexualRiskProfile( $migrantId: String!){
    getSingleSexualRiskProfile( migrantId:$migrantId){
        id
        isSmoke
        timesOfSmoke
        isUseAlchohol
        capacityOfAlchohol
        isPartnerUseDrugs
        capacityOfDrugs
        isEverInjectedDrugs
        isLikeHelpDrugProblems
        isLikeDiscussPhysicalAbuse
        hasThreatnedRelationship 
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`

export const ADD_MEDICAL_TREATMENT_PROFILE = gql`
mutation AddMedicalTreatmentProfile( 
    $natureOfIllness: String
    $medicalTreatmentCategory: String
    $medicalTreatmentRecords: JSON
    $migrantId: String!){
        addMedicalTreatmentProfile( 
            natureOfIllness:$natureOfIllness,
            medicalTreatmentCategory:$medicalTreatmentCategory,
            medicalTreatmentRecords: $medicalTreatmentRecords,
            migrantId:$migrantId
        ){
            id
            natureOfIllness
            medicalTreatmentCategory
            medicalTreatmentRecords
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const EDIT_MEDICAL_TREATMENT_PROFILE = gql`
mutation EditMedicalTreatmentProfile( 
    $id: String!,
    $natureOfIllness: String,
    $medicalTreatmentCategory: String,
    $medicalTreatmentRecords: JSON,
    ){
        editMedicalTreatmentProfile( 
            id:$id,
            natureOfIllness:$natureOfIllness,
            medicalTreatmentCategory:$medicalTreatmentCategory,
            medicalTreatmentRecords:$medicalTreatmentRecords ,
        ){
            id
            natureOfIllness
            medicalTreatmentCategory
            medicalTreatmentRecords
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const GET_SINGLE_MEDICAL_TREATMENT_PROFILE = gql`
query GetSingleMedicalTreatmentProfile( $migrantId: String!){
    getSingleMedicalTreatmentProfile( migrantId:$migrantId){
        id
        natureOfIllness
        medicalTreatmentCategory
        medicalTreatmentRecords
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`

export const ADD_COUNSELING_PROFILE = gql`
mutation AddCounselingProfile( 
    $counsellerInformations: JSON,
    $counsellingSymptoms: JSON,
    $suicidalThoughts: String,
    $suicidalThoughtsDetail: String,
    $mentalHealthConditions: String,
    $mentalHealthConditionsDetail: String,
    $mentalHealthMedications: String,
    $mentalHealthMedicationsDetail: String,
    $abuses: String ,
    $abusesDetail: String,
    $migrantId: String!){
        addCounselingProfile( 
            counsellerInformations:$counsellerInformations,
            counsellingSymptoms:$counsellingSymptoms,
            suicidalThoughts:$suicidalThoughts,
            suicidalThoughtsDetail:$suicidalThoughtsDetail,
            mentalHealthConditions:$mentalHealthConditions,
            mentalHealthConditionsDetail:$mentalHealthConditionsDetail,
            mentalHealthMedications:$mentalHealthMedications,
            mentalHealthMedicationsDetail:$mentalHealthMedicationsDetail,
            abuses:$abuses,
            abusesDetail:$abusesDetail,
            migrantId:$migrantId
        ){
            id
            counsellerInformations
            counsellingSymptoms
            suicidalThoughts
            suicidalThoughtsDetail
            mentalHealthConditions
            mentalHealthConditionsDetail
            mentalHealthMedications
            mentalHealthMedicationsDetail
            abuses 
            abusesDetail
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const EDIT_COUNSELING_PROFILE = gql`
mutation EditCounselingProfile( 
    $id: String!,
    $counsellerInformations: JSON,
    $counsellingSymptoms: JSON,
    $suicidalThoughts: String,
    $suicidalThoughtsDetail: String,
    $mentalHealthConditions: String,
    $mentalHealthConditionsDetail: String,
    $mentalHealthMedications: String,
    $mentalHealthMedicationsDetail: String,
    $abuses: String ,
    $abusesDetail: String,
    ){
        editCounselingProfile( 
            id:$id,
            counsellerInformations:$counsellerInformations,
            counsellingSymptoms:$counsellingSymptoms,
            suicidalThoughts:$suicidalThoughts,
            suicidalThoughtsDetail:$suicidalThoughtsDetail,
            mentalHealthConditions:$mentalHealthConditions,
            mentalHealthConditionsDetail:$mentalHealthConditionsDetail,
            mentalHealthMedications:$mentalHealthMedications,
            mentalHealthMedicationsDetail:$mentalHealthMedicationsDetail,
            abuses:$abuses,
            abusesDetail:$abusesDetail,
        ){
            id
            counsellerInformations
            counsellingSymptoms
            suicidalThoughts
            suicidalThoughtsDetail
            mentalHealthConditions
            mentalHealthConditionsDetail
            mentalHealthMedications
            mentalHealthMedicationsDetail
            abuses 
            abusesDetail
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const GET_SINGLE_COUNSELING_PROFILE = gql`
query GetSingleCounselingProfile( $migrantId: String!){
    getSingleCounselingProfile( migrantId:$migrantId){
            id
            counsellerInformations
            counsellingSymptoms
            suicidalThoughts
            suicidalThoughtsDetail
            mentalHealthConditions
            mentalHealthConditionsDetail
            mentalHealthMedications
            mentalHealthMedicationsDetail
            abuses 
            abusesDetail
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const ADD_MEDICAL_PAST_TREATMENT_PROFILE = gql`
mutation AddMedicalPastTreatmentProfile( 
    $pastNatureOfIllness: String
    $medicalPastTreatmentCategory: String
    $medicalPastTreatmentRecords: JSON,
    $migrantId: String!){
        addMedicalPastTreatmentProfile( 
            pastNatureOfIllness:$pastNatureOfIllness,
            medicalPastTreatmentCategory:$medicalPastTreatmentCategory,
            medicalPastTreatmentRecords:$medicalPastTreatmentRecords,
            migrantId:$migrantId
        ){
            id
            pastNatureOfIllness
            medicalPastTreatmentCategory
            medicalPastTreatmentRecords
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const EDIT_MEDICAL_PAST_TREATMENT_PROFILE = gql`
mutation EditMedicalPastTreatmentProfile( 
    $id: String!,
    $pastNatureOfIllness: String
    $medicalPastTreatmentCategory: String
    $medicalPastTreatmentRecords: JSON,
    ){
        editMedicalPastTreatmentProfile( 
            id:$id,
            pastNatureOfIllness:$pastNatureOfIllness,
            medicalPastTreatmentCategory:$medicalPastTreatmentCategory,
            medicalPastTreatmentRecords:$medicalPastTreatmentRecords,
        ){
            id
            pastNatureOfIllness
            medicalPastTreatmentCategory
            medicalPastTreatmentRecords
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const GET_SINGLE_MEDICAL_PAST_TREATMENT_PROFILE = gql`
query GetSingleMedicalPastTreatmentProfile( $migrantId: String!){
    getSingleMedicalPastTreatmentProfile( migrantId:$migrantId){
            id
            pastNatureOfIllness
            medicalPastTreatmentCategory
            medicalPastTreatmentRecords
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const ADD_COUNSELING_PAST_PROFILE = gql`
mutation AddCounselingPastProfile( 
    $hadPreviousCounselling: String,
    $lastDateOfCounselling: String,
    $counsellingDetail: String,
    $migrantId: String!){
        addCounselingPastProfile( 
            hadPreviousCounselling:$hadPreviousCounselling,
            lastDateOfCounselling:$lastDateOfCounselling,
            counsellingDetail:$counsellingDetail,
            migrantId:$migrantId
        ){
            id
            hadPreviousCounselling
            lastDateOfCounselling
            counsellingDetail
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const EDIT_COUNSELING_PAST_PROFILE = gql`
mutation EditCounselingPastProfile( 
    $id: String!,
    $hadPreviousCounselling: String,
    $lastDateOfCounselling: String,
    $counsellingDetail: String,
    ){
        editCounselingPastProfile( 
            id:$id,
            hadPreviousCounselling:$hadPreviousCounselling,
            lastDateOfCounselling:$lastDateOfCounselling,
            counsellingDetail:$counsellingDetail,
        ){
            id
            hadPreviousCounselling
            lastDateOfCounselling
            counsellingDetail
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const GET_SINGLE_COUNSILING_PAST_PROFILE = gql`
query GetSingleCounselingPastProfile( $migrantId: String!){
    getSingleCounselingPastProfile( migrantId:$migrantId){
            id
            hadPreviousCounselling
            lastDateOfCounselling
            counsellingDetail
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const ADD_MEDICAL_REFERREL_INFORMATION= gql`
mutation AddMedicalReferrelInformation( 
    $isReferrel: String,
    $referrelType: String,
    $hospitalName: String,
    $hospitalType: String,
    $referrelInformations: JSON,
    $migrantId: String!){
        addMedicalReferrelInformation( 
            isReferrel: $isReferrel,
            referrelType:$referrelType,
            hospitalName:$hospitalName,
            hospitalType:$hospitalType,
            referrelInformations:$referrelInformations,
            migrantId:$migrantId
        ){
            id
            isReferrel
            referrelType
            hospitalName
            hospitalType
            referrelInformations
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const EDIT_MEDICAL_REFERREL_INFORMATION = gql`
mutation EditMedicalReferrelInformation( 
    $id: String!,
    $isReferrel: String,
    $referrelType: String,
    $hospitalName: String,
    $hospitalType: String,
    $referrelInformations: JSON,
    ){
        editMedicalReferrelInformation( 
            id:$id,
            isReferrel:$isReferrel,
            referrelType:$referrelType,
            hospitalName:$hospitalName,
            hospitalType:$hospitalType,
            referrelInformations:$referrelInformations,
        ){
            id
            isReferrel
            referrelType
            hospitalName
            hospitalType
            referrelInformations
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const GET_SINGLE_MEDICAL_REFERREL_INFORMATION = gql`
query GetSingleMedicalReferrelInformation( $migrantId: String!){
    getSingleMedicalReferrelInformation( migrantId:$migrantId){
        id
        isReferrel
        referrelType
        hospitalName
        hospitalType
        referrelInformations
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`

export const ADD_MEDICAL_QUALIFICATION_PROFILE= gql`
mutation AddMedicalQualificationProfile( 
    $qualificationStatus: String,
    $notes: String,
    $migrantId: String!){
        addMedicalQualificationProfile( 
            qualificationStatus:$qualificationStatus,
            notes:$notes,
            migrantId:$migrantId
        ){
            id
            qualificationStatus
            notes
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const EDIT_MEDICAL_QUALIFICATION_PROFILE = gql`
mutation EditMedicalQualificationProfile( 
    $id: String!,
    $qualificationStatus: String,
    $notes: String,
    ){
        editMedicalQualificationProfile( 
            id:$id,
            qualificationStatus:$qualificationStatus,
            notes:$notes,
        ){
            id
            qualificationStatus
            notes
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const GET_SINGLE_MEDICAL_QUALIFICATION_PROFILE = gql`
query GetSingleMedicalQualificationProfile( $migrantId: String!){
    getSingleMedicalQualificationProfile( migrantId:$migrantId){
        id
        qualificationStatus
        notes
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`

export const GET_BASIC_MEDICAL_HISTORY_PROFILES = gql`
query GetBasicMedicalHistoryProfiles( $migrantId: String!){
    getBasicMedicalHistoryProfiles( migrantId:$migrantId){
        id
        ethnicity
        haveChildren
        numOfChildren
        sexualOrientation
        familyHistory
        otherMajorConditions
        isCurrentlyTreatedMedicalCondition
        currentMedicalCondition
        allergies
        emergencyContact
        closeupStatus
        closeupAction
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`

export const GET_SEXUAL_RISK_HISTORY_PROFILES = gql`
query GetSexualRiskHistoryProfiles( $migrantId: String!){
    getSexualRiskHistoryProfiles( migrantId:$migrantId){
        id
        isSmoke
        timesOfSmoke
        isUseAlchohol
        capacityOfAlchohol
        isPartnerUseDrugs
        capacityOfDrugs
        isEverInjectedDrugs
        isLikeHelpDrugProblems
        isLikeDiscussPhysicalAbuse
        hasThreatnedRelationship 
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`
export const GET_MEDICAL_PAST_TREATMENT_PROFILES = gql`
query GetMedicalPastTreatmentProfiles( $migrantId: String!){
    getMedicalPastTreatmentProfiles( migrantId:$migrantId){
        id
        pastNatureOfIllness
        medicalPastTreatmentCategory
        medicalPastTreatmentRecords
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`

export const GET_COUNSELLING_PAST_RECORD_PROFILES = gql`
query GetCounselingPastRecordProfiles( $migrantId: String!){
    getCounselingPastRecordProfiles( migrantId:$migrantId){
        id
        counsellerInformations
        counsellingSymptoms
        suicidalThoughts
        suicidalThoughtsDetail
        mentalHealthConditions
        mentalHealthConditionsDetail
        mentalHealthMedications
        mentalHealthMedicationsDetail
        abuses 
        abusesDetail
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`
export const GET_MEDICAL_REFERREL_HISTORY_INFORMATIONS = gql`
query GetMedicalReferrelHistoryInformations( $migrantId: String!){
    getMedicalReferrelHistoryInformations( migrantId:$migrantId){
        id
        isReferrel
        referrelType
        hospitalName
        hospitalType
        referrelInformations
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`








