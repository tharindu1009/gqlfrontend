import gql from 'graphql-tag';

export const CLOSE_MIGRANT_OVERALL_PROFILE = gql`
mutation CloseMigrantOverallProfile( 
    $closeupStatus: Boolean
    $closeupAction: String
    $migrantId: String!
    ){
        closeMigrantOverallProfile( 
            closeupStatus:$closeupStatus,
            closeupAction:$closeupAction,
            migrantId:$migrantId,
        ){
                fullName
                address
                telNumber
                mobileNumber
                nic
                passport
                registeredSLBFE
                registrationNumber
                maritalStatus
                gender
                conditionOfTheMigrant
                closeupStatus
                closeupAction
                isActive
    }
}
`

export const CLOSE_MIGRANT_MEDICAL_PROFILE = gql`
mutation CloseMigrantMedicalProfile( 
    $closeupStatus: Boolean
    $closeupAction: String
    $migrantId: String!
    ){
        closeMigrantMedicalProfile( 
            closeupStatus:$closeupStatus,
            closeupAction:$closeupAction,
            migrantId:$migrantId,
        ){
            id
            ethnicity
            haveChildren
            numOfChildren
            sexualOrientation
            familyHistory
            otherMajorConditions
            isCurrentlyTreatedMedicalCondition
            currentMedicalCondition
            allergies
            emergencyContact
            closeupStatus
            closeupAction
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`