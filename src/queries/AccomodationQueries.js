import gql from 'graphql-tag';

export const ADD_ACCOMODATION_DETAIL_PROFILE= gql`
mutation AddAccomodationDetailProfile( 
    $accomodationDetails: JSON,
    $migrantId: String!){
        addAccomodationDetailProfile( 
            accomodationDetails:$accomodationDetails,
            migrantId:$migrantId
        ){
            id
            accomodationDetails
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const EDIT_ACCOMODATION_DETAIL_PROFILE = gql`
mutation EditAccomodationDetailProfile( 
    $id: String!,
    $accomodationDetails: JSON,
    ){
        editAccomodationDetailProfile( 
            id:$id,
            accomodationDetails:$accomodationDetails,
        ){
            id
            accomodationDetails
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const GET_SINGLE_ACCOMODATION_DETAIL_PROFILE = gql`
query GetSingleAccomodationDetailProfile( $migrantId: String!){
    getSingleAccomodationDetailProfile( migrantId:$migrantId){
        id
        accomodationDetails
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`

export const ADD_MEAL_RECORD_PROFILE= gql`
mutation AddMealRecordProfile( 
    $mealRecords: JSON,
    $migrantId: String!){
        addMealRecordProfile( 
            mealRecords:$mealRecords,
            migrantId:$migrantId
        ){
            id
            mealRecords
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const EDIT_MEAL_RECORD_PROFILE = gql`
mutation EditMealRecordProfile( 
    $id: String!,
    $mealRecords: JSON,
    ){
        editMealRecordProfile( 
            id:$id,
            mealRecords:$mealRecords,
        ){
            id
            mealRecords
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const GET_SINGLE_MEAL_RECORD_PROFILE = gql`
query GetSingleMealRecordProfile( $migrantId: String!){
    getSingleMealRecordProfile( migrantId:$migrantId){
        id
        mealRecords
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`

export const GET_ACCOMODATION_DETAIL_HISTORY_PROFILES = gql`
query GetAccomodationDetailHistoryProfiles( $migrantId: String!){
    getAccomodationDetailHistoryProfiles( migrantId:$migrantId){
        id
        accomodationDetails
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`

export const GET_MEAL_RECORD_HISTORY_PROFILES = gql`
query GetMealRecordHistoryProfiles( $migrantId: String!){
    getMealRecordHistoryProfiles( migrantId:$migrantId){
        id
        mealRecords
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`

export const GET_SINGLE_EXPENSES_PROFILE = gql`
query GetSingleExpensesProfile( $migrantId: String!){
    getSingleExpensesProfile( migrantId:$migrantId){
        id
        expenses
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`

export const GET_EXPENSES_RECORD_HISTORY_PROFILES = gql`
query GetExpensesRecordHistoryProfiles( $migrantId: String!){
    getExpensesRecordHistoryProfiles( migrantId:$migrantId){
        id
        expenses
        migrantId
        createdAt
        updatedAt
        createdBy
    }
}
`

export const ADD_EXPENSES_RECORD_PROFILE= gql`
mutation AddExpensesRecordProfile( 
    $expenses: JSON,
    $migrantId: String!){
        addExpensesRecordProfile( 
            expenses:$expenses,
            migrantId:$migrantId
        ){
            id
            expenses
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`

export const EDIT_EXPENSES_RECORD_PROFILE = gql`
mutation EditExpensesRecordProfile( 
    $id: String!,
    $expenses: JSON,
    ){
        editExpensesRecordProfile( 
            id:$id,
            expenses:$expenses,
        ){
            id
            expenses
            migrantId
            createdAt
            updatedAt
            createdBy
    }
}
`