import gql from 'graphql-tag';

export const CHECK_REGISTERED_MIGRANT = gql`
mutation CheckRegisteredMigrant( $searchInNIC: String, $searchInPassport: String){
    checkRegisteredMigrant( searchInNIC:$searchInNIC, searchInPassport: $searchInPassport )
}
`
export const GET_ALL_MIGRANTS = gql`
query GetAllMigrants( $skip: Int,$first: Int){
    getAllMigrants( skip:$skip,first:$first){
        migrants{
            id
            fullName
            address
            telNumber
            mobileNumber
            nic
            passport
            registeredSLBFE
            registrationNumber
            maritalStatus
            gender
            isActive
            createdAt
            createdBy
        }
        count
    }
}
`

export const GET_ALL_SICKNESSES = gql`
query {
    getAllSicknesses{
        id
        sicknessName
        status
        createdAt
        updatedAt
        createdBy
    }
}
`

export const GET_SINGLE_SICKNESS = gql`
query GetSingleSickness( $id: String!){
    getSingleSickness( id:$id){
        id
        sicknessName
        status
        createdAt
        updatedAt
        createdBy
    }
}
`
export const ADD_SICKNESS= gql`
mutation AddSickness( $sicknessName: String, , $status: Boolean){
    addSickness( sicknessName:$sicknessName, status:$status){
            id
            sicknessName
            status
            createdAt
            updatedAt
            createdBy
    }
}
`

export const EDIT_SICKNESS= gql`
mutation EditSickness( $id: String! ,$sicknessName: String, , $status: Boolean){
    editSickness( id:$id, sicknessName:$sicknessName, status:$status){
            id
            sicknessName
            status
            createdAt
            updatedAt
            createdBy
    }
}
`

export const GET_ALL_ACCOMMODATIONS = gql`
query {
    getAllAccommodations{
        id
        accommodation
        status
        createdAt
        updatedAt
        createdBy
    }
}
`

export const GET_SINGLE_ACCOMMODATION = gql`
query GetSingleAccommodation( $id: String!){
    getSingleAccommodation( id:$id){
        id
        accommodation
        status
        createdAt
        updatedAt
        createdBy
    }
}
`
export const ADD_ACCOMMODATION= gql`
mutation AddAccommodation( $accommodation: String , $status: Boolean){
    addAccommodation( accommodation:$accommodation, status:$status){
            id
            accommodation
            status
            createdAt
            updatedAt
            createdBy
    }
}
`

export const EDIT_ACCOMMODATION= gql`
mutation EditAccommodation( $id: String! ,$accommodation: String , $status: Boolean){
    editAccommodation( id:$id, accommodation:$accommodation, status:$status){
            id
            accommodation
            status
            createdAt
            updatedAt
            createdBy
    }
}
`
export const GET_ALL_MEDICAL_TREATMENT_CATEGORIES = gql`
query {
    getAllMedicalTreatmentCategories{
        id
        medicalTreatmentCategory
        status
        createdAt
        updatedAt
        createdBy
    }
}
`

export const GET_ALL_DISTRICTS = gql`
query {
    getAllDistricts{
        id
        district
    }
}
`

export const GET_ALL_RETURN_REASONS = gql`
query {
    getAllReturnReasons{
        id
        reason
    }
}
`

export const GET_ALL_SUGGEST_ACTIONS = gql`
query {
    getAllSuggestActions{
        id
        action
    }
}
`



export const ADD_RETURN_REASON= gql`
mutation AddReturnReason( $reason: String){
    addReturnReason( reason:$reason){
            id
            reason
            createdAt
            updatedAt
            createdBy
    }
}
`

export const EDIT_RETURN_REASON= gql`
mutation EditReturnReason( $id: String! ,$reason: String){
    editReturnReason( id:$id, reason:$reason){
            id
            reason
            createdAt
            updatedAt
            createdBy
    }
}
`
export const GET_SINGLE_RETURN_REASON = gql`
query GetSingleReturnReason( $id: String!){
    getSingleReturnReason( id:$id){
        id
        reason
        createdAt
        updatedAt
        createdBy
    }
}
`



export const GET_ALL_EXPENSE_TYPES = gql`
query {
    getAllExpenseTypes{
        id
        expenceType
        createdAt
        updatedAt
        createdBy
    }
}
`



export const ADD_EXPENSE_TYPE= gql`
mutation AddExpenseType( $expenceType: String){
    addExpenseType( expenceType:$expenceType){
        id
        expenceType
        createdAt
        updatedAt
        createdBy
    }
}
`

export const EDIT_EXPENSE_TYPE= gql`
mutation EditExpenseType( $id: String! ,$expenceType: String){
    editExpenseType( id:$id, expenceType:$expenceType){
        id
        expenceType
        createdAt
        updatedAt
        createdBy
    }
}
`
export const GET_SINGLE_EXPENSE_TYPE = gql`
query GetSingleExpense( $id: String!){
    getSingleExpense( id:$id){
        id
        expenceType
        createdAt
        updatedAt
        createdBy
    }
}
`


