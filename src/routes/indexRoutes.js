import Login from '../screens/login/login';
import Registration from '../screens/registration/registration';
import Home from '../screens/home/home';
import Dashboard from '../screens/dashboard/dashboard';
import ManageUser from '../screens/manageUser/manageUser';
import ManageMigrant from '../screens/manageMigrant/manageMigrant';


const indexRoutes = [
    {
        path: "/login",
        name: "Login",
        icon: "",
        component: Login
    },
    {
        path: "/registration",
        name: "Registration",
        icon: "",
        component: Registration
    },
    {
        path: "/home",
        name: "Home",
        icon: "",
        component: Home
    },
    {
        path: "/dashboard",
        name: "Dashboard",
        icon: "",
        component: Home
    },
    {
        path: "/manageusers",
        name: "Manage Users",
        icon: "",
        component: Home
    },
    {
        path: "/managemigrants",
        name: "Manage Migrants",
        icon: "",
        component: Home
    },
    {
        path: "/migrantactions",
        name: "Migrant Actions",
        icon: "",
        component: Home
    },
    {
        path: "/addnewsystemuser",
        name: "Add New System User",
        icon: "",
        component: Home
    },
    {
        path: "/manageElements",
        name: "Manage Elements",
        icon: "",
        component: Home
    },
    {
        path: "/accommodation",
        name: "Accommodation",
        icon: "",
        component: Home
    },
    {
        path: "/sickness",
        name: "Sickness",
        icon: "",
        component: Home
    },
    {
        path: "/migrantsicknessreport",
        name: "Sickness Report",
        icon: "",
        component: Home
    },
    {
        path: "/migrantreferrelreport",
        name: "Referrel Report",
        icon: "",
        component: Home
    },
    {
        path: "/migrantmedicaltreatmentreport",
        name: "Medical Report",
        icon: "",
        component: Home
    },
    {
        path: "/migrantfollowupreport",
        name: "Followup Report",
        icon: "",
        component: Home
    },
    {
        path: "/addreturnreason",
        name: "Add New Return Reasonz",
        icon: "",
        component: Home
    },
    {
        path: "/editreturnreason",
        name: "Edit Return Reasonz",
        icon: "",
        component: Home
    },
    {
        path: "/returnreasons",
        name: "Return Reasons",
        icon: "",
        component: Home
    },
    {
        path: "/addexpensetype",
        name: "Add Expense Type",
        icon: "",
        component: Home
    },
    {
        path: "/editexpensetype",
        name: "Edit Expense Type",
        icon: "",
        component: Home
    },
    {
        path: "/expensetypes",
        name: "Expense Types",
        icon: "",
        component: Home
    },
    {
        path: "/returnedreasonreport",
        name: "Returned Reason Report",
        icon: "",
        component: Home
    }


    

];

export default indexRoutes;
