import Dashboard from '../screens/dashboard/dashboard';
import ManageUser from '../screens/manageUser/manageUser';
import ManageMigrant from '../screens/manageMigrant/manageMigrant';
import MigrantActions from '../screens/migrantActions/migrantActions';
import AddNewSystemUser from '../screens/manageUser/addNewSystemUser';

import ManageElement from '../screens/manageElements/manageElement';
import ManageAccommodation from '../screens/manageElements/manageAccommodation/manageAccommodation';
import AddNewAccommodation from '../screens/manageElements/manageAccommodation/addNewAccommodation';
import ManageSickness from '../screens/manageElements/manageSickness/manageSickness';
import AddNewSickness from '../screens/manageElements/manageSickness/addNewSickness';

import MigrantSicknessReport from '../screens/reports/migrantSicknessReport/migrantSicknessReport';
import MigrantReferrelReport from '../screens/reports/migrantReferrelReport/migrantReferrelReport';
import MigrantMedicalTreatmentReport from '../screens/reports/migrantMedicalTreatmentReport/migrantMedicalTreatmentReport';
import FollowupReport from '../screens/reports/followupReport/followupReport';
import ManageReturnReason from '../screens/manageElements/manageReturnReason/manageReturnReason';
import AddNewReturnReason from '../screens/manageElements/manageReturnReason/addNewReturnReason';
import ManageExpenseType from '../screens/manageElements/manageExpenseType/manageExpenseType';
import AddExpenseType from '../screens/manageElements/manageExpenseType/addExpenseType';
import ReturnedReasonReport from '../screens/reports/returnedReasonReport/returnedReasonReport';

export const userRoutes = [
    {
        path: "/dashboard",
        name: "Dashboard",
        icon: "ti-home",
        component: Dashboard,
        isVisibleInMenu:false
    },
    {
        path: "/manageusers",
        name: "Manage Users",
        icon: "ti-user",
        component: ManageUser,
        isVisibleInMenu:false
    },
    {
        path: "/managemigrants",
        name: "Manage Migrants",
        icon: "ti-desktop",
        component: ManageMigrant,
        isVisibleInMenu:true
    },
    {
        path: "/migrantactions",
        name: "Migrant Actions",
        icon: "",
        component: MigrantActions,
        isVisibleInMenu:false
    },
    {
        path: "/addnewsystemuser",
        name: "Add New System User",
        icon: "",
        component: AddNewSystemUser
    },
    {
        path: "/manageelements",
        name: "Manage Elements",
        icon: "",
        component: ManageElement
    },
    {
        path: "/accommodation",
        name: "Accommodation",
        icon: "",
        component: AddNewAccommodation
    },
    {
        path: "/sickness",
        name: "Sickness",
        icon: "",
        component: AddNewSickness
    },
    {
        path: "/migrantsicknessreport",
        name: "Sickness Report",
        icon: "",
        component: MigrantSicknessReport
    },
    {
        path: "/migrantreferrelreport",
        name: "Referrel Report",
        icon: "",
        component: MigrantReferrelReport
    },
    {
        path: "/migrantmedicaltreatmentreport",
        name: "Medical Report",
        icon: "",
        component: MigrantMedicalTreatmentReport
    },
    {
        path: "/migrantfollowupreport",
        name: "Followup Report",
        icon: "",
        component: FollowupReport
    },
    {
        path: "/addreturnreason",
        name: "Add New Return Reasonz",
        icon: "",
        component: AddNewReturnReason
    },
    {
        path: "/editreturnreason",
        name: "Edit Return Reasonz",
        icon: "",
        component: AddNewReturnReason
    },
    {
        path: "/returnreasons",
        name: "Return Reasons",
        icon: "",
        component: ManageReturnReason
    },
    {
        path: "/addexpensetype",
        name: "Add Expense Type",
        icon: "",
        component: AddExpenseType
    },
    {
        path: "/editexpensetype",
        name: "Edit Expense Type",
        icon: "",
        component: AddExpenseType
    },
    {
        path: "/expensetypes",
        name: "Expense Types",
        icon: "",
        component: ManageExpenseType
    },
    {
        path: "/returnedreasonreport",
        name: "Returned Reason Report",
        icon: "",
        component: ReturnedReasonReport
    }

];



