import React, { Component } from 'react';
import { Route } from "react-router-dom";

import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { Link } from 'react-router-dom';


class Sidemenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            routes: []
        }
    }

    componentDidMount() {
        var route_list = this.props.sideRoutes;
        console.log(route_list)
        var routes = [];
        route_list.map((prop, i) => {
            if (prop.isVisibleInMenu == true) {
                routes.push({ path: prop.path, icon: prop.icon, name: prop.name })
            }
        });
        this.setState({ routes: routes });

        console.log(routes);
    }

    RedirectToScreen = (url) => {
        this.props.getClickedUrl(url);
    }

    render() {
        const { sideRoutes } = this.props;
        console.log(this.state.routes)

        return (
            <div>
                <div className="left side-menu">
                    <div className="slimscroll-menu" id="remove-scroll">

                        <div id="sidebar-menu">
                            <br /><br />
                            <ul className="metismenu" id="side-menu">
                                <li className="menu-title">Main</li>
                                <li>
                                    <a className="waves-effect" onClick={() => this.RedirectToScreen("/dashboard")}>
                                        <i className="ti-home" ></i> <span> Dashboard </span>
                                    </a>
                                </li>

                                <li className="menu-title">Managements</li>

                                {this.state.routes.map((route, key) => {
                                    return (
                                        <li>
                                            <a onClick={() => this.RedirectToScreen(route.path)} className="waves-effect">
                                                <i className={route.icon}></i> <span> {route.name} </span>
                                            </a>
                                        </li>
                                    );
                                })}

                                {
                                    localStorage.USER_ROLE == "MISSIONOFFICIAL" ? (
                                        null
                                    ) : (
                                            [<li className="menu-title">Elements</li>,

                                            <li>
                                                <a onClick={() => this.RedirectToScreen("/manageelements")} className="waves-effect">
                                                    <i className="ti-layout-accordion-list"></i> <span> Manage Elements </span>
                                                </a>
                                            </li>]
                                    )
                                }

                                <li className="menu-title">Reports</li>

                                <li>
                                    <a onClick={() => this.RedirectToScreen("/migrantsicknessreport")} className="waves-effect">
                                        <i className="ti-bar-chart"></i> <span> Sickness Analysis Report </span>
                                    </a>
                                </li>

                                <li>
                                    <a onClick={() => this.RedirectToScreen("/migrantmedicaltreatmentreport")} className="waves-effect">
                                        <i className="ti-support"></i> <span>  Medi Treatment Report </span>
                                    </a>
                                </li>

                                <li>
                                    <a onClick={() => this.RedirectToScreen("/migrantreferrelreport")} className="waves-effect">
                                        <i className="ti-pie-chart"></i> <span>  Referral Analysis Report </span>
                                    </a>
                                </li>
                                <li>
                                    <a onClick={() => this.RedirectToScreen("/migrantfollowupreport")} className="waves-effect">
                                        <i className="ti-receipt"></i> <span>  Followup Report </span>
                                    </a>
                                </li>
                                <li>
                                    <a onClick={() => this.RedirectToScreen("/returnedreasonreport")} className="waves-effect">
                                        <i className="ti-target"></i> <span>  Returned Reason Report </span>
                                    </a>
                                </li>
                                
                            </ul>

                        </div>
                        <div className="clearfix"></div>

                    </div>

                </div>

            </div>
        );
    }
}

export default withRouter(withApollo(Sidemenu)); 