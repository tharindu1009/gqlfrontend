import React, { Component } from 'react'
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { slbfeauthtoken, USER_ID, EMAIL, NAME, USER_ROLE, USER_NAME } from '../../constants/genarics';


class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,

        }
    }

    componentDidMount() {

    }

    logout = () => {
        setTimeout(() => {
            localStorage.removeItem(slbfeauthtoken);
            localStorage.removeItem(USER_ID);
            localStorage.removeItem(EMAIL);
            localStorage.removeItem(NAME);
            localStorage.removeItem(USER_ROLE);
            localStorage.removeItem(USER_NAME);
            this.props.history.push("/");
        }, 500)

    };


    render() {

        return (
            <div>

                <div className="topbar">

                    <div className="topbar-left">
                        <a href="" className="logo">
                            <div className="LogoContent">
                                <h2>SLBFE</h2>
                            </div>
                            {localStorage.USER_ROLE == "SAHANAPIYASAOFFICIAL" ? (
                                [<h6 style={{ color: "#ced4da" }}>SAHANAPIYASA OFFICIAL</h6>, <br />]
                            ) : (
                                localStorage.USER_ROLE == "MISSIONOFFICIAL" ? (
                                    [<h6 style={{ color: "#ced4da" }}>MISSION OFFICIAL</h6>, <br />]
                                ) : (
                                        [<h6 style={{ color: "#ced4da" }}>{localStorage.USER_ROLE}</h6>, <br />]
                                    )
                                )}


                        </a>
                    </div>

                    <nav className="navbar-custom">
                        <ul className="navbar-right list-inline float-right mb-0">
                            
                            <li className="dropdown notification-list list-inline-item">
                                <div className="dropdown notification-list nav-pro-img">
                                    <a className="dropdown-toggle nav-link arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                        <i className="fas fa-user-circle fa-2x" />
                                    </a>
                                    <div className="dropdown-menu dropdown-menu-right profile-dropdown ">

                                        <a className="dropdown-item" href="#"><i className="mdi mdi-account-circle m-r-5"></i> Profile</a>
                                        <a className="dropdown-item d-block" href="#"><i className="mdi mdi-settings m-r-5"></i> Settings</a>
                                        <div className="dropdown-divider"></div>
                                        <a className="dropdown-item text-danger cursor" onClick={() => this.logout()}><i className="mdi mdi-power text-danger"></i> Logout</a>
                                    </div>
                                </div>
                            </li>

                        </ul>

                        <ul className="list-inline menu-left mb-0">
                            <li className="float-left">
                                <button className="button-menu-mobile open-left waves-effect">
                                    <i className="mdi mdi-menu"></i>
                                </button>
                            </li>

                        </ul>

                    </nav>

                </div>

            </div>
        );
    }
}

export default withRouter(withApollo(Header)); 