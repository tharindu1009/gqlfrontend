import firebase from '@firebase/app';
import '@firebase/firestore';
import '@firebase/auth';
import '@firebase/storage';

const settings = {timestampsInSnapshots: true};

var config = {
    apiKey: "AIzaSyBo7C4jN7kfhIocP84MdW08PrUna43Nhis",
    authDomain: "slbfe-96cc4.firebaseapp.com",
    databaseURL: "https://slbfe-96cc4.firebaseio.com",
    projectId: "slbfe-96cc4",
    storageBucket: "slbfe-96cc4.appspot.com",
    messagingSenderId: "166227506263",
    appId: "1:166227506263:web:b47ce02f9aff4f9d725f12"
};


firebase.initializeApp(config);

//firebase.firestore().settings(settings);

export default firebase;