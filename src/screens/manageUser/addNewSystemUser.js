import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../actions/user-actions'

//sementic ui
import { Dropdown } from 'semantic-ui-react';

// Queries
import { GET_SINGLE_USER, ADD_NEW_USER, EDIT_USER } from '../../queries/UserQueries'
import { GET_ALL_DISTRICTS } from '../../queries/CommonQueries'



// Loader
import Loader from 'react-loader-spinner';

const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class AddNewSystemUser extends Component {
    constructor(props) {
        super(props);

        const query = new URLSearchParams(this.props.location.search);
        const id = query.get('id');

        var userId = "";

        if (id != null || id != undefined) {
            userId = id;
        }

        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        this.state = {
            loading: false,
            userRoles: [
                { text: "Administrator", value: "ADMINISTRATOR", id: 'userRole' },
                { text: "SLBFE Official", value: "SLBFEOFFICIAL", id: 'userRole' },
                { text: "Sahana Piyasa Official", value: "SAHANAPIYASAOFFICIAL", id: 'userRole' },
                { text: "Mission Official", value: "MISSIONOFFICIAL", id: 'userRole' },
                { text: "Senior Management", value: "SENIORMANAGEMENT", id: 'userRole' },
            ],
            userRole: "",
            userId: userId,
            name: "",
            phoneNumber: "",
            email: "",
            organization: "",
            districtId: "",
            district: "",
            department: "",
            userName: "",
            password: "",
            repeatPassword: "",
            isActive: true,
            districts: [],


        }

    }

    componentDidMount() {
        if (this.state.userId != "") {
            this.loadSingleUser();
        }
        this.loadAllDistricts();
    }

    loadAllDistricts() {
        var districts = [];
        this.getAllDistricts().then(result => {
            districts = result.map((district, i) => {
                return { key: district.id, text: district.district, value: district.district, id: 'district' }
            })
            districts.unshift({ key: "", id: "district", text: "Select-District", value: "Select-District", disabled: true });
            this.setState({ districts: districts });
        })
    }

    getAllDistricts = async () => {
        const result = await this.props.client.query({
            query: GET_ALL_DISTRICTS,
            fetchPolicy: 'network-only'
        });
        return result.data.getAllDistricts;
    }

    loadSingleUser() {
        this.getSingleUser().then(result => {
            this.setState({
                name: result.name,
                phoneNumber: result.phoneNumber,
                email: result.email,
                isActive: result.isActive,
                userRole: result.userRole
            });

        })
    }

    getSingleUser = async () => {
        const id = this.state.userId;

        const result = await this.props.client.query({
            query: GET_SINGLE_USER,
            variables: { id },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleUser;
    }


    handleChange = (event) => {
        this.setState({ [event.target.id]: event.target.value });
    }

    handleOnChange = (e, data) => {
        this.setState({ [data.id]: data.value });
    }

    handleOnDistrictChange = (e, data) => {
        this.setState({ [data.id]: data.value });

        const { value } = data;
        const { key, text } = data.options.find(o => o.value === value);

        this.setState({ "districtId": key, [data.id]: text });
    }

    handleSubmit = () => {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        const { userId, name, email, userRole, userName, password, repeatPassword } = this.state;

        if (name == "" || email == "" || userRole == "" || userName == "") {
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "All fields Required."
            });
            return;
        } else {
            if (userId == "" && password == "") {
                this.props.setFormStatus({
                    status: true,
                    title: "Oops!",
                    message: "Password Required."
                });
                return;
            } else if (password != repeatPassword) {
                this.props.setFormStatus({
                    status: true,
                    title: "Oops!",
                    message: "Repeat password is not matching."
                });
                return;
            } else {

                this.setState({ loading: true });
                this.props.setFormStatus({ status: false, title: '', message: '' });
                this.props.setSuccessStatus({ status: false, title: '', message: '' });

                if (this.state.userId == "") {

                    this.addNewUser().then(result => {

                        this.setState({
                            loading: false,
                            form_state: 'default',
                            title: "",
                            message: "",
                            userRole: "",
                            name: "",
                            phoneNumber: "",
                            email: "",
                            organization: "",
                            districtId: "",
                            district: "",
                            department: "",
                            userName: "",
                            password: "",
                            repeatPassword: "",
                            isActive: true,
                        });

                        this.props.setSuccessStatus({
                            status: true,
                            title: "",
                            message:
                                "User Registered Successfully!."
                        });

                    }).catch(error => {
                        console.log(error);
                        if (error) {
                            this.setState({ loading: false });
                            this.props.setFormStatus({
                                status: true,
                                title: "Oops!",
                                message:
                                    "User already Exist."

                            });
                        }
                    });



                } else {
                    this.editUser().then(result => {
                        this.setState({ loading: false })
                        this.loadSingleUser();

                        this.props.setSuccessStatus({
                            status: true,
                            title: "",
                            message:
                                "User edited Successfully!"
                        });

                    }).catch(error => {
                        console.log(error);
                        if (error) {
                            this.setState({ loading: false });
                            this.props.setFormStatus({
                                status: true,
                                title: "Oops!",
                                message:
                                    "There was an error while trying to edit User Profile."

                            });
                        }
                    });
                }
            }
        }


    }

    addNewUser = async () => {
        const {
            name,
            email,
            phoneNumber,
            userRole,
            organization,
            districtId,
            district,
            department,
            userName,
            password
        } = this.state;

        const isActive = JSON.parse(this.state.isActive);

        var date = { name, email, phoneNumber, userRole, organization, districtId, district, department, userName, password, isActive };

        const result = await this.props.client.mutate({
            mutation: ADD_NEW_USER,
            variables: date
        });
        return result.data.signup;
    };

    editUser = async () => {
        const {
            name,
            email,
            organization,
            districtId,
            district,
            department,
            phoneNumber
        } = this.state;

        const id = this.state.userId;
        const isActive = JSON.parse(this.state.isActive);

        var date = { id, name, email, phoneNumber, organization, districtId, district, department, isActive }

        const result = await this.props.client.mutate({
            mutation: EDIT_USER,
            variables: date
        });
        return result.data.editUser;
    };


    render() {

        const { loading } = this.state;
        const { formErrorStatus, formSuccessState } = this.props;

        return (
            <div class="content-page">
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">

                                <div class="col-sm-6">
                                    <h4 class="page-title">Add New System User</h4>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-8">
                                <div class="card">
                                    <div class="card-body">

                                        <h4 class="mt-0 header-title">General User Details</h4><br />

                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Full Name </label>
                                            <div class="col-sm-12">
                                                <input id="name" name="name" value={this.state.name} onChange={this.handleChange} class="form-control" type="text" placeholder="Full Name" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Email </label>
                                            <div class="col-sm-12">
                                                <input id="email" name="email" value={this.state.email} onChange={this.handleChange} class="form-control" type="email" placeholder="Email" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Phone Number </label>
                                            <div class="col-sm-12">
                                                <input id="phoneNumber" name="phoneNumber" value={this.state.phoneNumber} onChange={this.handleChange} class="form-control" type="number" placeholder="Phone Number" />
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Organization </label>
                                            <div class="col-sm-12">
                                                <input id="organization" name="organization" value={this.state.organization} onChange={this.handleChange} class="form-control" type="text" placeholder="Organization" />
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Department </label>
                                            <div class="col-sm-12">
                                                <input id="department" name="department" value={this.state.department} onChange={this.handleChange} class="form-control" type="text" placeholder="Department" />
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> District of Organization  </label>
                                            <div class="col-sm-12">
                                                <Dropdown
                                                    placeholder='Select District'
                                                    fluid
                                                    selection
                                                    search
                                                    id='district'
                                                    name='district'
                                                    value={this.state.district}
                                                    options={this.state.districts}
                                                    onChange={this.handleOnDistrictChange}
                                                />
                                            </div>
                                        </div>

                                        {this.state.userId == "" ? (
                                            <div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> User Role </label>
                                                    <div class="col-sm-12">
                                                        <Dropdown
                                                            placeholder='Select User Role'
                                                            fluid
                                                            selection
                                                            id='userRole'
                                                            name='userRole'
                                                            value={this.state.userRole}
                                                            options={this.state.userRoles}
                                                            onChange={this.handleOnChange}
                                                        />
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> User Name </label>
                                                    <div class="col-sm-12">
                                                        <input id="userName" name="userName" value={this.state.userName} onChange={this.handleChange} class="form-control" type="text" placeholder="User Name" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Password </label>
                                                    <div class="col-sm-12">
                                                        <input id="password" name="password" value={this.state.password} onChange={this.handleChange} class="form-control" type="password" placeholder="Password" />
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Repeat Password </label>
                                                    <div class="col-sm-12">
                                                        <input id="repeatPassword" name="repeatPassword" value={this.state.repeatPassword} onChange={this.handleChange} class="form-control" type="password" placeholder="Repeat Password" />
                                                    </div>
                                                </div>
                                            </div>
                                        ) : (
                                                null
                                            )}


                                        <div class="form-group row">
                                            <div class="col-sm-12" style={{ textAlign: "right" }}>
                                                <br /><button disabled={loading ? (true) : (false)} class="col-sm-2 btn btn-info " onClick={() => this.handleSubmit()} type="button" aria-haspopup="true" aria-expanded="false">
                                                    {loading ? (
                                                        <Loader
                                                            type="Oval"
                                                            color="#2A3F54"
                                                            height={15}
                                                            width={40}
                                                        />
                                                    ) : ("Submit")}

                                                </button>
                                            </div>
                                        </div>

                                        <div>

                                            {
                                                (formErrorStatus.status) ? (

                                                    < div class="ui negative message">
                                                        <div class="header">
                                                            Not Submitted.
                                                    </div>
                                                        <p>{formErrorStatus.message}</p>
                                                    </div>

                                                ) : ((formSuccessState.status) ? (

                                                    < div class="ui success message">
                                                        <div class="header">
                                                            Submitted successfully.
                                                    </div>
                                                        <p>{formSuccessState.message}</p>
                                                    </ div>

                                                ) : (''))
                                            }
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(AddNewSystemUser)));

