import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../actions/user-actions'

// Queries
import { GET_ALL_USERS } from '../../queries/UserQueries'


// Loader
import Loader from 'react-loader-spinner';


const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class ManageUser extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            pagination: {
                skip: 0,
                first: 50
            },
            users: [],
            count: 0,

        }

    }

    componentDidMount() {
        this.loadAllUsers();
    }

    loadAllUsers() {
        this.setState({ loading: true });
        this.getAllUsers(this.state.pagination).then(result => {
            this.setState({ users: result.data.getAllUsers.users, count: result.data.getAllUsers.count, loading: false })
        });
    }

    getAllUsers = async (pagination) => {
        const result = await this.props.client.query({
            query: GET_ALL_USERS,
            variables: pagination,
            fetchPolicy: 'network-only'
        });
        return result;
    };

    newUser = () => {
        this.props.history.push("/addnewsystemuser")
    }

    editUser = (id) => {
        this.props.history.push("/addnewsystemuser?id=" + id)
    }


    render() {

        const { loading,users } = this.state;

        return (
            <div class="content-page">
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">

                                <div class="col-sm-6">
                                    <h4 class="page-title">Manage Users</h4>
                                </div>
                                <div class="col-sm-6">

                                    <div class="float-right d-none d-md-block">
                                        <div class="dropdown">
                                            <button class="btn btn-primary " onClick={() => this.newUser()} type="button" aria-haspopup="true" aria-expanded="false">
                                                <i class="mdi mdi-user mr-2"></i> Add New System User
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">

                                        <h4 class="mt-0 header-title"></h4>
                                        <h5 class="text-muted m-b-30">System User List</h5>

                                        <div class="table-rep-plugin">
                                            <div class="table-responsive mb-0" data-pattern="priority-columns">

                                                {loading ? (
                                                    <div className="ListLoader">
                                                        <Loader
                                                            type="ThreeDots"
                                                            color="#5A738E"
                                                            height={100}
                                                            width={100}
                                                        />
                                                    </div>
                                                ) : (

                                                        <table id="tech-companies-1" class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th> Name </th>
                                                                    <th data-priority="1">Email</th>
                                                                    <th data-priority="2">Phone Number</th>
                                                                    <th data-priority="2">Organization</th>
                                                                    <th data-priority="3">User Role</th>
                                                                    <th data-priority="4"></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            {
                                                                    users.length > 0 ? (
                                                                        users.map((user, index) => (
                                                                            <tr key={index}>
                                                                                <td> {user.name} </td>
                                                                                <td> {user.email} </td>
                                                                                <td> {user.phoneNumber} </td>
                                                                                <td> {user.organization} </td>
                                                                                <td> {user.userRole}</td>
                                                                                <td>
                                                                                    <button class="btn btn-info " onClick={() => this.editUser(user.id)} type="button" aria-haspopup="true" aria-expanded="false">Edit</button>
                                                                                </td>
                                                                            </tr>
                                                                        ))
                                                                    ) : (null)

                                                                }

                                                            </tbody>
                                                        </table>

                                                    )}
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(ManageUser)));

