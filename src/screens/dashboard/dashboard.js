import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../actions/user-actions'

// Queries
import { GET_SYSTEM_USER_COUNT, GET_MIGRANT_COUNT, GET_FOREIGN_MISSION_ONBOARDING_COUNT, GET_MEDICAL_TREATMENT_COUNT } from '../../queries/DashboardQueries'

// Loader
import Loader from 'react-loader-spinner';

const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class Dashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            userCount: 0,
            migrantCount: 0,
            foreignMissionMigrantCount: 0,
            medicalTreatmentCount: 0,
            userLoader: true,
            migrantLoader: true,
            foreignMissionLoader: true,
            medicalLoader: true,
        }

    }

    componentDidMount() {
        this.loadSystemUserCount();
        this.loadMigrantCount();
        this.loadForeignMissionOnboaringCount();
        this.loadMedicalTreatmentCount();
    }


    loadSystemUserCount() {
        this.setState({ userLoader: true });
        this.getSystemUserCount().then(result => {
            this.setState({ userCount: result, userLoader: false });
        })
    }

    getSystemUserCount = async () => {
        const result = await this.props.client.query({
            query: GET_SYSTEM_USER_COUNT,
            fetchPolicy: 'network-only'
        });
        return result.data.getSystemUserCount;
    }

    loadMigrantCount() {
        this.setState({ migrantLoader: true });
        this.getMigrantCount().then(result => {
            this.setState({ migrantCount: result, migrantLoader: false });
        })
    }

    getMigrantCount = async () => {
        const result = await this.props.client.query({
            query: GET_MIGRANT_COUNT,
            fetchPolicy: 'network-only'
        });
        return result.data.getMigrantCount;
    }

    loadForeignMissionOnboaringCount() {
        this.setState({ foreignMissionLoader: true });
        this.getForeignMissionOnboaringCount().then(result => {
            this.setState({ foreignMissionMigrantCount: result, foreignMissionLoader: false });
        })
    }

    getForeignMissionOnboaringCount = async () => {
        const result = await this.props.client.query({
            query: GET_FOREIGN_MISSION_ONBOARDING_COUNT,
            fetchPolicy: 'network-only'
        });
        return result.data.getForeignMissionOnboaringCount;
    }

    loadMedicalTreatmentCount() {
        this.setState({ medicalLoader: true });
        this.getMedicalTreatmentCount().then(result => {
            this.setState({ medicalTreatmentCount: result, medicalLoader: false });
        })
    }

    getMedicalTreatmentCount = async () => {
        const result = await this.props.client.query({
            query: GET_MEDICAL_TREATMENT_COUNT,
            fetchPolicy: 'network-only'
        });
        return result.data.getMedicalTreatmentCount;
    }


    render() {

        return (
            <div class="content-page">
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">

                                <div class="col-sm-6">
                                    <h4 class="page-title">Dashboard</h4><br />
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item active">Welcome to <b>Migrant Worker Welfare Management Platform </b></li>
                                    </ol>

                                </div>

                            </div>
                        </div>
                    

                        <div class="row">
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="assets/images/services-icon/Group_34.png" alt="" />
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50"> System &nbsp; Users</h5><br/>
                                            {this.state.userLoader ? (
                                                <Loader
                                                    type="Oval"
                                                    color="#2A3F54"
                                                    height={30}
                                                    width={30}
                                                />
                                            ) : (
                                                <p class="countLabel"> {this.state.userCount} </p>
                                            )}
                                            

                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a  class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>

                                            <p class="text-white-50 mb-0">Since last month</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="assets/images/services-icon/Group-33.png" alt="" />
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">Migrants</h5><br/>
                                            {this.state.migrantLoader ? (
                                                <Loader
                                                    type="Oval"
                                                    color="#2A3F54"
                                                    height={30}
                                                    width={30}
                                                />
                                            ) : (
                                                <p class="countLabel"> {this.state.migrantCount} </p>
                                            )}
                                            

                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>

                                            <p class="text-white-50 mb-0">Since last month</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="assets/images/services-icon/Group_32.png" style={{width:"90px"}} alt="" />
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">Foreign Mission Onboarding</h5>
                                            {this.state.foreignMissionLoader ? (
                                                <Loader
                                                    type="Oval"
                                                    color="#2A3F54"
                                                    height={30}
                                                    width={30}
                                                />
                                            ) : (
                                                <p class="countLabel"> {this.state.foreignMissionMigrantCount} </p>
                                            )}

                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>

                                            <p class="text-white-50 mb-0">Since last month</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6">
                                <div class="card mini-stat bg-primary text-white">
                                    <div class="card-body">
                                        <div class="mb-4">
                                            <div class="float-left mini-stat-img mr-4">
                                                <img src="assets/images/services-icon/Group_31.png" alt="" />
                                            </div>
                                            <h5 class="font-16 text-uppercase mt-0 text-white-50">Medical Treatments</h5>
                                            {this.state.medicalLoader ? (
                                                <Loader
                                                    type="Oval"
                                                    color="#2A3F54"
                                                    height={30}
                                                    width={30}
                                                />
                                            ) : (
                                                <p class="countLabel"> {this.state.medicalTreatmentCount} </p>
                                            )}
                                        </div>
                                        <div class="pt-2">
                                            <div class="float-right">
                                                <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                                            </div>

                                            <p class="text-white-50 mb-0">Since last month</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(Dashboard)));

