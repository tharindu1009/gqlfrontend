import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../actions/user-actions'

//components
import OnboardActions from './onboardActions/onboardActions';
import MedicalActions from './medicalActions/medicalActions';
import AccommodationActions from './accommodationActions/accommodationActions';
import TransportActions from './transportActions/transportActions';
import FollowupActions from './followupActions/followupActions';
import ClosingActions from './closingActions/closingActions';

//sementic ui
import { Dropdown } from 'semantic-ui-react';
import {
    DateTimeInput
} from 'semantic-ui-calendar-react';


const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class MigrantActions extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,

        }

    }


    render() {

        return (
            <div class="content-page" >

                <div class="content">
                    <div class="container-fluid" >
                        <div >
                            <div class="page-title-box">
                                <div class="row align-items-center">
                                    <div class="col-sm-6">
                                        <h4 class="page-title">Manage Migrant Actions</h4>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-12">

                                    <div class="col-lg-12">


                                        <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#action1" role="tab">
                                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                                    <span class="d-none d-sm-block">Onboarding</span>
                                                </a>
                                            </li>
                                            {
                                                localStorage.USER_ROLE == "MISSIONOFFICIAL" ? (
                                                    null
                                                ) : (
                                                        [<li class="nav-item">
                                                            <a class="nav-link" data-toggle="tab" href="#action2" role="tab">
                                                                <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                                                <span class="d-none d-sm-block">Medical</span>
                                                            </a>
                                                        </li>,
                                                        <li class="nav-item">
                                                            <a class="nav-link" data-toggle="tab" href="#action3" role="tab">
                                                                <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                                                                <span class="d-none d-sm-block">Hospitalities</span>
                                                            </a>
                                                        </li>,
                                                        <li class="nav-item">
                                                            <a class="nav-link" data-toggle="tab" href="#action4" role="tab">
                                                                <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                                                                <span class="d-none d-sm-block">Transport</span>
                                                            </a>
                                                        </li>,
                                                        <li class="nav-item">
                                                            <a class="nav-link" data-toggle="tab" href="#action5" role="tab">
                                                                <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                                                                <span class="d-none d-sm-block">Followup Actions</span>
                                                            </a>
                                                        </li>,
                                                        <li class="nav-item">
                                                            <a class="nav-link" data-toggle="tab" href="#action6" role="tab">
                                                                <span class="d-block d-sm-none"><i class="fas fa-cog"></i></span>
                                                                <span class="d-none d-sm-block">Closing Actions</span>
                                                            </a>
                                                        </li>]
                                                    )
                                            }

                                        </ul>


                                        <div class="tab-content">
                                            <div class="tab-pane active p-3" id="action1" role="tabpanel">
                                                <OnboardActions />
                                            </div>
                                            {
                                                localStorage.USER_ROLE == "MISSIONOFFICIAL" ? (
                                                    null
                                                ) : (
                                                        [
                                                            <div class="tab-pane p-3" id="action2" role="tabpanel">

                                                                <MedicalActions />
                                                            </div>,
                                                            <div class="tab-pane p-3" id="action3" role="tabpanel">

                                                                <AccommodationActions />
                                                            </div>,
                                                            <div class="tab-pane p-3" id="action4" role="tabpanel">

                                                                <TransportActions />
                                                            </div>,
                                                            <div class="tab-pane p-3" id="action5" role="tabpanel">

                                                                <FollowupActions />
                                                            </div>,
                                                            <div class="tab-pane p-3" id="action6" role="tabpanel">

                                                                <ClosingActions />
                                                            </div>
                                                        ]
                                                    )
                                            }

                                        </div>


                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            </div>

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(MigrantActions)));

