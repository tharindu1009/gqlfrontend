import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../../actions/user-actions'
import { formatDate, formatDateTime } from '../../../middleware/index';

//sementic ui
import { Dropdown, Grid, Icon, Message, Input, Accordion, GridRow, GridColumn } from 'semantic-ui-react';
import {
    DateTimeInput
} from 'semantic-ui-calendar-react';

//Spinner
import Loader from 'react-loader-spinner'

import DateTime from 'react-datetime';

//firebase
import { uploadFile, deleteFile } from "../../../firebase/FileServices";

//query
import { CHECK_REGISTERED_MIGRANT, GET_ALL_SUGGEST_ACTIONS, GET_ALL_RETURN_REASONS } from '../../../queries/CommonQueries';
import {
    ADD_MIGRANT_GENERAL_PROFILE, EDIT_MIGRANT_GENERAL_PROFILE, GET_SINGLE_MIGRANT_GENERAL_PROFILE,
    GET_SINGLE_MIGRANT_TRAVEL_PROFILE, ADD_MIGRANT_TRAVEL_PROFILE, EDIT_MIGRANT_TRAVEL_PROFILE,
    GET_SINGLE_SCHEDULED_ARRIVAL_PROFILE, ADD_SCHEDULED_ARRIVAL_PROFILE, EDIT_SCHEDULED_ARRIVAL_PROFILE,
    GET_MIGRANT_TRAVEL_HISTORY_PROFILES, GET_SCHEDULED_ARRIVAL_HISTORY_PROFILES
} from '../../../queries/OnboardingQueries';



const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class OnboardActions extends Component {
    constructor(props) {
        super(props);

        const query = new URLSearchParams(this.props.location.search);
        const id = query.get('id');

        console.log(id)

        var existMigrant = false;
        var migrantId = "";

        if (id != null || id != undefined) {
            migrantId = id;
            existMigrant = true;
        }

        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        this.state = {
            activeIndex: -1,
            migrantId: migrantId,
            existMigrant: existMigrant,
            loading: false,
            meritalStatusList: [
                { key: "SINGLE", text: "Single", value: "SINGLE", id: 'meritalStatus' },
                { key: "MARRIED", text: "Married", value: "MARRIED", id: 'meritalStatus' },
                { key: "DIVORCED", text: "Divorced", value: "DIVORCED", id: 'meritalStatus' },
                { key: "WIDOWED", text: "WIDOWED", value: "Widowed", id: 'meritalStatus' }
            ],
            genderList: [
                { key: "MALE", text: "Male", value: "MALE", id: 'gender' },
                { key: "FEMALE", text: "Female", value: "FEMALE", id: 'gender' },
                { key: "TRANSGENDER", text: "Transgender", value: "TRANSGENDER", id: 'gender' },
            ],
            travelTypes: [
                { key: "ARRIVAL", text: "Arrival", value: "ARRIVAL", id: 'travelType' },
                { key: "DEPARTURE", text: "Departure", value: "DEPARTURE", id: 'travelType' }
            ],
            profilePriorities: [
                { key: "NORMAL", text: "Normal", value: "NORMAL", id: 'profilePriority' },
                { key: "MEDIUM", text: "Medium", value: "MEDIUM", id: 'profilePriority' },
                { key: "HIGH", text: "High", value: "HIGH", id: 'profilePriority' }
            ],
            sourcesOfEmployment: [
                { text: "Through an Agency", value: "Through an Agency", id: 'sourceOfEmployment' },
                { text: "Self Obtained", value: "Self Obtained", id: 'sourceOfEmployment' },
                { text: "Through Illegal Means", value: "Through Illegal Means", id: 'sourceOfEmployment' }
            ],
            conditionsOfMigrant: [
                { text: "Patient", value: "Patient", id: 'conditionOfTheMigrant' },
                { text: "Disabled", value: "Disabled", id: 'conditionOfTheMigrant' },
                { text: "Deported - Illegal Stay", value: "Deported - Illegal Stay", id: 'conditionOfTheMigrant' },
                { text: "Deported - Violation of other laws", value: "Deported - Violation of other laws", id: 'conditionOfTheMigrant' },
                { text: "Diseased", value: "Diseased", id: 'conditionOfTheMigrant' },
                { text: "Pregnant", value: "Pregnant", id: 'conditionOfTheMigrant' },
                { text: "Mother with babies ", value: "Mother with babies", id: 'conditionOfTheMigrant' },
                { text: "Other", value: "Other", id: 'conditionOfTheMigrant' },
            ],
            contractPeriodList: [
                { key: "ONE_YEAR", text: "One Year", value: "ONE_YEAR", id: 'contractPeriod' },
                { key: "TWO_YEARS", text: "Two Years", value: "TWO_YEARS", id: 'contractPeriod' },
                { key: "THREE_YEARS", text: "Three Years", value: "THREE_YEARS", id: 'contractPeriod' },
                { key: "MORE_THAN_THREE_YEARS", text: "More than Three Years", value: "MORE_THAN_THREE_YEARS", id: 'contractPeriod' }
            ],
            transitInformations: [{ transitArrival: "", transitDeparture: "" }],
            searchInNIC: "",
            searchInPassport: "",
            hasRegisteredMigrantResponseMessage: { status: false, label: "" },
            fullName: "",
            address: "",
            telNumber: "",
            mobileNumber: "",
            nic: "",
            passport: "",
            tempPassport: "",
            registeredSLBFE: false,
            registrationNumber: "",
            maritalStatus: "SINGLE",
            gender: "MALE",
            closeupStatus: "false",
            closeupAction: "",
            recievedToSahanaPiyasa: new Date(),
            releasedFromSahanaPiyasa: "",
            conditionOfTheMigrant: "",
            isActive: true,
            hasGeneralProfileMessage: false,
            existTravelProfile: false,
            numOfTimesMigrated: 0,
            travelType: "",
            arrivalCountry: "",
            arrivalDate: "",
            contractPeriod: "",
            returnedReason: "",
            custodyOfSLBFE: "",
            OfficialStatement: "",
            confirmOfStatement: "",
            comments: "",
            flightNumber: "",
            timeOfFlight: "",
            lastOccupationCountry: "",
            sourceOfEmployment: "",
            agencyName: "",
            agencyLicenseNo: "",
            profilePriority: "",
            travelProfileId: "",
            hasTravelProfileMessage: false,
            existSheduledArrivalProfile: false,
            sheduledArrivalProfileId: "",
            departureLocation: "",
            scheduledFlightNo: "",
            scheduledFlightDatetime: "",
            scheduledArrival: "",
            suggestedAction: "",
            foreignMissionProfilePriority: "",
            hasSheduledArrivalProfileMessage: false,
            migrantTravelHistoryProfiles: [],
            sheduledArrivalHistoryProfiles: [],
            generalDetailDisplay: false,
            suggestedActionsList: [],
            returnReasons: [],

        }

    }

    componentDidMount() {
        if (this.state.existMigrant) {
            this.loadSingleMigrantGeneralProfile();
            this.loadSingleMigrantTravelProfile();
            this.loadSingleSheduledArrivalProfile();
            this.loadMigrantTravelHistoryProfiles();
            this.loadSheduledArrivalHistoryProfiles();
        }

        this.loadAllSuggestActions();
        this.loadAllReturnReasons();
    }

    loadAllSuggestActions() {
        var suggestedActionsList = [];
        this.getAllSuggestActions().then(result => {
            suggestedActionsList = result.map((suggestedAction, i) => {
                return { text: suggestedAction.action, value: suggestedAction.action, id: 'suggestedAction' }
            })

            this.setState({ suggestedActionsList: suggestedActionsList });
        })
    }

    getAllSuggestActions = async () => {
        const result = await this.props.client.query({
            query: GET_ALL_SUGGEST_ACTIONS,
            fetchPolicy: 'network-only'
        });
        return result.data.getAllSuggestActions;
    }

    loadAllReturnReasons() {
        var returnReasons = [];
        this.getAllReturnReasons().then(result => {
            returnReasons = result.map((returnReason, i) => {
                return { text: returnReason.reason, value: returnReason.reason, id: 'returnedReason' }
            })

            this.setState({ returnReasons: returnReasons });
        })
    }

    getAllReturnReasons = async () => {
        const result = await this.props.client.query({
            query: GET_ALL_RETURN_REASONS,
            fetchPolicy: 'network-only'
        });
        return result.data.getAllReturnReasons;
    }

    handleChange = (event) => {
        this.setState({ [event.target.id]: event.target.value });
    }

    handleOnChange = (e, data) => {
        this.setState({ [data.id]: data.value });
    }

    handleRadioOnChange = (e) => {
        if (e.target.checked) {
            var name = e.target.name;
            var value = e.target.value;
            console.log(name)
            console.log(value)
            this.setState({ [name]: value });
        }
    }

    setGender(event) {
        console.log(event.target.value);
    }

    handleDate = (date, id) => {
        console.log(id)
        console.log(date._d)
        var value = formatDateTime(date._d);
        this.setState({ [id]: value });
    };

    handleSahanaDate = (date, id) => {
        console.log(id)
        console.log(date._d)
        var value = date._d;
        this.setState({ [id]: value });
    };

    getValidDates = function (currentDate) {
        var yesterday = DateTime.moment().subtract(1, 'day');
        return currentDate.isAfter(yesterday);
    }

    handleGeneralProfileSubmit() {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });
        this.setState({ hasGeneralProfileMessage: false })

        const { fullName, nic, passport, existMigrant } = this.state;


        if (fullName == "" || nic == "") {
            this.setState({ hasGeneralProfileMessage: true })
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "Name, NIC and Passport Required."
            });
            return;
        } else {

            this.setState({ loading: true });
            this.props.setFormStatus({ status: false, title: '', message: '' });
            this.props.setSuccessStatus({ status: false, title: '', message: '' });

            if (existMigrant) {

                this.editMigrantGeneralProfile().then(result => {
                    this.setState({ hasGeneralProfileMessage: true, loading: false })
                    this.loadSingleMigrantGeneralProfile();

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "General Profile edited Successfully!"
                    });


                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasGeneralProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to edit General Profile."

                        });
                    }
                });

            } else {
                this.addMigrantGeneralProfile().then(result => {

                    this.setState({
                        loading: false,
                        form_state: 'default',
                        title: "",
                        message: "",
                        fullName: "",
                        address: "",
                        telNumber: "",
                        mobileNumber: "",
                        nic: "",
                        passport: "",
                        tempPassport: "",
                        registeredSLBFE: false,
                        registrationNumber: "",
                        maritalStatus: "SINGLE",
                        gender: "MALE",
                        conditionOfTheMigrant: "",
                        hasGeneralProfileMessage: true,
                        migrantId: result.id
                    });

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "General Profile submitted Successfully!."
                    });

                    this.props.history.push("/migrantactions?id=" + result.id);

                    window.location.reload();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasGeneralProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to submit General Profile. NIC or Password Already Exist"

                        });
                    }
                });
            }
        }


    }

    handleTravelProfileSubmit() {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });
        this.setState({
            hasTravelProfileMessage: false, hasSheduledArrivalProfileMessage: false,
            hasMedicalProfileMessage: false, hasSexualRiskProfileMessage: false, hasMedicalTreatmentProfileMessage: false, hasCounselingProfileMessage: false,
            hasMedicalTreatmentPastProfileMessage: false, hasCounselingPastProfileMessage: false, hasMedicalReferrelInformationMessage: false,
            hasMedicalQualificationProfileMessage: false, hasAccomodationDetailProfileMessage: false, hasMealRecordProfileMessage: false, hasTransportProfileMessage: false, hasFollowupProfileMessage: false
        })

        const { migrantId } = this.state;

        if (migrantId == "" || migrantId == undefined || migrantId == null) {
            this.setState({ hasGeneralProfileMessage: true })
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "Please submit migrant general details before submit travel details."
            });
            return;
        } else {

            this.setState({ loading: true });
            this.props.setFormStatus({ status: false, title: '', message: '' });
            this.props.setSuccessStatus({ status: false, title: '', message: '' });

            console.log(this.state.existTravelProfile)

            if (this.state.existTravelProfile) {

                this.editMigrantTravelProfile().then(result => {
                    this.setState({ hasTravelProfileMessage: true, loading: false })
                    this.loadSingleMigrantTravelProfile();

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Travel Profile edited Successfully!"
                    });

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasTravelProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to edit Travel Profile."

                        });
                    }
                });

            } else {
                this.addMigrantTravelProfile().then(result => {

                    this.setState({
                        loading: false,
                        form_state: 'default',
                        title: "",
                        message: "",
                        numOfTimesMigrated: 0,
                        travelType: "",
                        arrivalCountry: "",
                        arrivalDate: "",
                        contractPeriod: "",
                        returnedReason: "",
                        custodyOfSLBFE: "",
                        OfficialStatement: [],
                        confirmOfStatement: "",
                        comments: "",
                        flightNumber: "",
                        timeOfFlight: "",
                        lastOccupationCountry: "",
                        profilePriority: "",
                        hasTravelProfileMessage: true
                    });

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Travel Profile submitted Successfully!."
                    });
                    this.loadSingleMigrantTravelProfile();
                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasTravelProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to submit Travel Profile."
                        });
                    }
                });
            }
        }


    }

    handleSheduledArrivalProfileSubmit() {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });
        this.setState({
            hasTravelProfileMessage: false, hasSheduledArrivalProfileMessage: false,
            hasMedicalProfileMessage: false, hasSexualRiskProfileMessage: false, hasMedicalTreatmentProfileMessage: false, hasCounselingProfileMessage: false,
            hasMedicalTreatmentPastProfileMessage: false, hasCounselingPastProfileMessage: false, hasMedicalReferrelInformationMessage: false,
            hasMedicalQualificationProfileMessage: false, hasAccomodationDetailProfileMessage: false, hasMealRecordProfileMessage: false, hasTransportProfileMessage: false, hasFollowupProfileMessage: false
        })

        const { migrantId } = this.state;

        if (migrantId == "" || migrantId == undefined || migrantId == null) {
            this.setState({ hasSheduledArrivalProfileMessage: true })
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "Please submit migrant general details before submit Sheduled arrival Profile details."
            });
            return;
        } else {

            this.setState({ loading: true });
            this.props.setFormStatus({ status: false, title: '', message: '' });
            this.props.setSuccessStatus({ status: false, title: '', message: '' });

            if (this.state.existSheduledArrivalProfile) {

                this.editSheduledArrivalProfile().then(result => {
                    this.setState({ hasSheduledArrivalProfileMessage: true, loading: false })
                    this.loadSingleSheduledArrivalProfile();

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Sheduled arrival Profile edited Successfully!"
                    });

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasSheduledArrivalProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to edit Sheduled arrival Profile."

                        });
                    }
                });

            } else {
                this.addSheduledArrivalProfile().then(result => {

                    this.setState({
                        loading: false,
                        form_state: 'default',
                        title: "",
                        message: "",
                        departureLocation: "",
                        scheduledFlightNo: "",
                        scheduledFlightDatetime: "",
                        transitInformation: [],
                        scheduledArrival: "",
                        suggestedActions: "",
                        foreignMissionProfilePriority: "",
                        hasSheduledArrivalProfileMessage: true
                    });

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Sheduled arrival Profile submitted Successfully!."
                    });
                    this.loadSingleSheduledArrivalProfile();
                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasSheduledArrivalProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to submit Sheduled arrival Profile."

                        });
                    }
                });
            }
        }


    }

    addMigrantTravelProfile = async () => {
        const {
            travelType,
            arrivalCountry,
            arrivalDate,
            contractPeriod,
            returnedReason,
            custodyOfSLBFE,
            officialStatement,
            comments,
            flightNumber,
            timeOfFlight,
            lastOccupationCountry,
            sourceOfEmployment,
            agencyName,
            agencyLicenseNo,
            profilePriority,
            migrantId } = this.state;


        var confirmOfStatement = this.state.confirmOfStatement;
        const numOfTimesMigrated = parseInt(this.state.numOfTimesMigrated);

        const result = await this.props.client.mutate({
            mutation: ADD_MIGRANT_TRAVEL_PROFILE,
            variables: {
                numOfTimesMigrated,
                travelType,
                arrivalCountry,
                arrivalDate,
                contractPeriod,
                returnedReason,
                custodyOfSLBFE,
                officialStatement,
                confirmOfStatement,
                comments,
                flightNumber,
                timeOfFlight,
                lastOccupationCountry,
                sourceOfEmployment,
                agencyName,
                agencyLicenseNo,
                profilePriority,
                migrantId
            }
        });
        return result.data.addMigrantTravelProfile;
    };

    editMigrantTravelProfile = async () => {
        const {
            travelType,
            arrivalCountry,
            arrivalDate,
            contractPeriod,
            returnedReason,
            custodyOfSLBFE,
            officialStatement,
            comments,
            flightNumber,
            timeOfFlight,
            lastOccupationCountry,
            sourceOfEmployment,
            agencyName,
            agencyLicenseNo,
            profilePriority,
        } = this.state;


        var confirmOfStatement = this.state.confirmOfStatement;


        const numOfTimesMigrated = parseInt(this.state.numOfTimesMigrated);
        const id = this.state.travelProfileId;

        const result = await this.props.client.mutate({
            mutation: EDIT_MIGRANT_TRAVEL_PROFILE,
            variables: {
                id,
                numOfTimesMigrated,
                travelType,
                arrivalCountry,
                arrivalDate,
                contractPeriod,
                returnedReason,
                custodyOfSLBFE,
                officialStatement,
                confirmOfStatement,
                comments,
                flightNumber,
                timeOfFlight,
                lastOccupationCountry,
                sourceOfEmployment,
                agencyName,
                agencyLicenseNo,
                profilePriority,
            }
        });
        return result.data.editMigrantTravelProfile;
    };

    addMigrantGeneralProfile = async () => {
        const {
            fullName, address, telNumber, mobileNumber, nic, passport, tempPassport, registeredSLBFE, registrationNumber, maritalStatus, gender,
            conditionOfTheMigrant, recievedToSahanaPiyasa, isActive } = this.state;

        const result = await this.props.client.mutate({
            mutation: ADD_MIGRANT_GENERAL_PROFILE,
            variables: {
                fullName, address, telNumber, mobileNumber, nic, passport, tempPassport, registeredSLBFE, registrationNumber, maritalStatus, gender,
                conditionOfTheMigrant, recievedToSahanaPiyasa, isActive
            }
        });
        return result.data.addMigrantGeneralProfile;
    };

    editMigrantGeneralProfile = async () => {
        const { fullName, address, telNumber, mobileNumber, maritalStatus, gender,
            conditionOfTheMigrant, recievedToSahanaPiyasa, releasedFromSahanaPiyasa, isActive } = this.state;
        const id = this.state.migrantId;


        console.log(id)

        const result = await this.props.client.mutate({
            mutation: EDIT_MIGRANT_GENERAL_PROFILE,
            variables: {
                id, fullName, address, telNumber, mobileNumber, maritalStatus, gender,
                conditionOfTheMigrant, recievedToSahanaPiyasa, releasedFromSahanaPiyasa, isActive
            }
        });

        return result.data.editMigrantGeneralProfile;
    };

    addSheduledArrivalProfile = async () => {
        const {
            departureLocation,
            scheduledFlightNo,
            scheduledFlightDatetime,
            transitInformations,
            scheduledArrival,
            suggestedActions,
            foreignMissionProfilePriority,
            migrantId } = this.state;

        console.log(scheduledFlightDatetime)
        console.log(scheduledArrival)

        const result = await this.props.client.mutate({
            mutation: ADD_SCHEDULED_ARRIVAL_PROFILE,
            variables: {
                departureLocation,
                scheduledFlightNo,
                scheduledFlightDatetime,
                transitInformations,
                scheduledArrival,
                suggestedActions,
                foreignMissionProfilePriority,
                migrantId
            }
        });
        return result.data.addSheduledArrivalProfile;
    };

    editSheduledArrivalProfile = async () => {
        const { departureLocation,
            scheduledFlightNo,
            scheduledFlightDatetime,
            transitInformations,
            scheduledArrival,
            suggestedActions,
            foreignMissionProfilePriority
        } = this.state;
        const id = this.state.sheduledArrivalProfileId;

        const result = await this.props.client.mutate({
            mutation: EDIT_SCHEDULED_ARRIVAL_PROFILE,
            variables: {
                id,
                departureLocation,
                scheduledFlightNo,
                scheduledFlightDatetime,
                transitInformations,
                scheduledArrival,
                suggestedActions,
                foreignMissionProfilePriority
            }
        });
        return result.data.editSheduledArrivalProfile;
    };

    loadSingleMigrantGeneralProfile() {
        this.getSingleMigrantGeneralProfile().then(result => {
            console.log(result)
            this.setState({
                searchInNIC: result.nic,
                searchInPassport: result.passport,
                fullName: result.fullName,
                address: result.address,
                telNumber: result.telNumber,
                mobileNumber: result.mobileNumber,
                nic: result.nic,
                passport: result.passport,
                tempPassport: result.tempPassport,
                registeredSLBFE: result.registeredSLBFE,
                registrationNumber: result.registrationNumber,
                maritalStatus: result.maritalStatus,
                migrantId: result.id,
                gender: result.gender,
                closeupStatus: result.closeupStatus.toString(),
                closeupAction: result.closeupAction,
                recievedToSahanaPiyasa: result.recievedToSahanaPiyasa,
                releasedFromSahanaPiyasa: result.releasedFromSahanaPiyasa,
                conditionOfTheMigrant: result.conditionOfTheMigrant,
                generalDetailDisplay: true
            });
        })
    }

    getSingleMigrantGeneralProfile = async () => {
        const id = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_SINGLE_MIGRANT_GENERAL_PROFILE,
            variables: { id },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleMigrantGeneralProfile;
    }

    loadSingleMigrantTravelProfile() {
        this.getSingleMigrantTravelProfile().then(result => {

            if (result.length > 0) {
                this.setState({
                    travelProfileId: result[0].id,
                    numOfTimesMigrated: result[0].numOfTimesMigrated,
                    travelType: result[0].travelType,
                    arrivalCountry: result[0].arrivalCountry,
                    arrivalDate: result[0].arrivalDate,
                    contractPeriod: result[0].contractPeriod,
                    returnedReason: result[0].returnedReason,
                    custodyOfSLBFE: result[0].custodyOfSLBFE,
                    officialStatement: result[0].officialStatement,
                    confirmOfStatement: result[0].confirmOfStatement,
                    comments: result[0].comments,
                    flightNumber: result[0].flightNumber,
                    timeOfFlight: result[0].timeOfFlight,
                    lastOccupationCountry: result[0].lastOccupationCountry,
                    sourceOfEmployment: result[0].sourceOfEmployment,
                    agencyName: result[0].agencyName,
                    agencyLicenseNo: result[0].agencyLicenseNo,
                    profilePriority: result[0].profilePriority,
                    existTravelProfile: true
                });
            }
        })
    }

    getSingleMigrantTravelProfile = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_SINGLE_MIGRANT_TRAVEL_PROFILE,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleMigrantTravelProfile;
    }

    loadMigrantTravelHistoryProfiles() {
        this.getMigrantTravelHistoryProfiles().then(result => {

            if (result.length > 0) {
                this.setState({
                    migrantTravelHistoryProfiles: result,
                });
            }
        })
    }

    getMigrantTravelHistoryProfiles = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_MIGRANT_TRAVEL_HISTORY_PROFILES,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getMigrantTravelHistoryProfiles;
    }

    loadSingleSheduledArrivalProfile() {
        this.getSingleSheduledArrivalProfile().then(result => {

            if (result.length > 0) {
                this.setState({
                    sheduledArrivalProfileId: result[0].id,
                    departureLocation: result[0].departureLocation,
                    scheduledFlightNo: result[0].scheduledFlightNo,
                    scheduledFlightDatetime: result[0].scheduledFlightDatetime,
                    transitInformations: result[0].transitInformations,
                    scheduledArrival: result[0].scheduledArrival,
                    suggestedActions: result[0].suggestedActions,
                    foreignMissionProfilePriority: result[0].foreignMissionProfilePriority,
                    existSheduledArrivalProfile: true
                });
            }
        })
    }

    getSingleSheduledArrivalProfile = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_SINGLE_SCHEDULED_ARRIVAL_PROFILE,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleSheduledArrivalProfile;
    }

    loadSheduledArrivalHistoryProfiles() {
        this.getSheduledArrivalHistoryProfiles().then(result => {
            console.log(result)
            if (result.length > 0) {
                this.setState({
                    sheduledArrivalHistoryProfiles: result,
                });
            }
        })
    }

    getSheduledArrivalHistoryProfiles = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_SCHEDULED_ARRIVAL_HISTORY_PROFILES,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getSheduledArrivalHistoryProfiles;
    }

    _createTransitInformation() {
        return this.state.transitInformations.map((el, i) => (
            <div key={i}>
                <br />

                {this.state.transitInformations.length <= 1 ? (
                    null
                ) : (
                        <div style={{ textAlign: "right", cursor: "pointer" }}>
                            <i className="fas fa-times-circle fa-2x" onClick={this.removeTransitInformation.bind(this, i)}></i>
                        </div>
                    )}

                {i + 1 + " ) "}
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label"> Transit arrival </label>
                    <div class="col-sm-12">
                        <input id="transitArrival" name="transitArrival" class="form-control" type="text" placeholder="Transit arrival" value={el.transitArrival || ''} onChange={this.handleTransitInformationChange.bind(this, i)} />
                    </div>
                </div>

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label"> Transit departure </label>
                    <div class="col-sm-12">
                        <input id="transitDeparture" name="transitDeparture" class="form-control" type="text" placeholder="Transit departure" value={el.transitDeparture || ''} onChange={this.handleTransitInformationChange.bind(this, i)} />
                    </div>
                </div>

                <hr />

            </div >

        ))

    }

    handleTransitInformationChange(i, e) {
        const { name, value } = e.target;
        let transitInformations = [...this.state.transitInformations];
        transitInformations[i] = { ...transitInformations[i], [name]: value };
        this.setState({ transitInformations });
    }

    _addMoreTransitInformation() {
        let transitInformations = this.state.transitInformations;
        transitInformations.push({ transitArrival: "", transitDeparture: "" });
        this.setState(transitInformations)
    }

    removeTransitInformation(i) {
        let transitInformations = this.state.transitInformations;
        transitInformations.splice(i, 1);
        this.setState({ transitInformations });
    }

    migrantIsRegistered = () => {

        const { searchInNIC, searchInPassport } = this.state;

        if (searchInNIC != "" || searchInPassport != "") {

            this.checkRegisteredMigrant().then(result => {
                if (result.data.checkRegisteredMigrant.status) {
                    this.setState({ registeredSLBFE: true, generalDetailDisplay: true });
                } else {
                    this.setState({ registeredSLBFE: false, loading: false, hasRegisteredMigrantResponseMessage: { status: true, label: "WARNING" }, generalDetailDisplay: true });
                    this.props.setFormStatus({ status: true, title: 'Oops!', message: 'Not matching Migrant for these credentials. Please enter profile details manually' });
                }

            }).catch(error => {
                if (error) {
                    console.log(error);
                    this.setState({ loading: false, hasRegisteredMigrantResponseMessage: { status: true, label: "DANGER" } });
                    this.props.setFormStatus({ status: true, title: 'Oops!', message: 'Occured Error in access Service!' });
                }
                this.setState({ loading: false, generalDetailDisplay: true });
            });

        } else {
            this.setState({ loading: false, hasRegisteredMigrantResponseMessage: { status: true, label: "DANGER" } });
            this.props.setFormStatus({ status: true, title: 'Oops!', message: ' NIC or Passport Required.' });
        }

    }

    checkRegisteredMigrant = async () => {
        const { searchInNIC, searchInPassport } = this.state;
        console.log(this.props.client)
        const result = this.props.client.mutate(
            {
                mutation: CHECK_REGISTERED_MIGRANT,
                variables: { searchInNIC, searchInPassport },
            })

        return result;
    }


    handleClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = this.state
        const newIndex = activeIndex === index ? -1 : index

        this.setState({ activeIndex: newIndex })
    }

    render() {

        const { activeIndex, loading, hasRegisteredMigrantResponseMessage, hasGeneralProfileMessage, hasTravelProfileMessage, hasSheduledArrivalProfileMessage } = this.state;
        const { formErrorStatus, formSuccessState } = this.props;

        console.log(this.state.closeupStatus)
        return (
            <div >
                <br />

                <div class="col-lg-10">

                    <Accordion fluid styled style={{ "box-shadow": "none" }}>
                        <Accordion.Title
                            className="card-header darkHeader"
                            active={activeIndex === 0}
                            index={0}
                            onClick={this.handleClick}
                        >

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="headerAlign">
                                        <Icon name='dropdown headerRightAlign' />
                                        <div class="">
                                            <div className="accordianLeftTitleText">
                                                <h4 class="mt-0 header-title" > General Profile Details </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </Accordion.Title>
                        <Accordion.Content
                            active={activeIndex === 0}
                        >

                            <br />
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Search for Existing Profile </label>
                                    <div class="col-sm-2">
                                        <input class="form-control" type="text" value={this.state.searchInNIC} id="searchInNIC" name="searchInNIC" placeholder="NIC" onChange={this.handleChange} />
                                    </div>
                                    <div class="col-sm-2">
                                        <input class="form-control" type="text" maxLength="8" value={this.state.searchInPassport} id="searchInPassport" name="searchInPassport" placeholder="Passport" onChange={this.handleChange} />
                                    </div>
                                    <button disabled={loading ? (true) : (false)} class="col-sm-2 btn btn-info " onClick={() => this.migrantIsRegistered()} type="button" aria-haspopup="true" aria-expanded="false">
                                        {loading ? (
                                            <Loader
                                                type="Oval"
                                                color="#2A3F54"
                                                height={15}
                                                width={40}
                                            />
                                        ) : ("Search Profile")}

                                    </button>

                                </div>

                                <div className="col-lg-6">
                                    {(formErrorStatus.status) ? (
                                        hasRegisteredMigrantResponseMessage.status ? (
                                            hasRegisteredMigrantResponseMessage.label == "DANGER" ? (
                                                <div class="ui negative message" >
                                                    <p>{formErrorStatus.message}</p>
                                                </div>
                                            ) : (
                                                    <div class="ui warning message" >
                                                        <p>{formErrorStatus.message}</p>
                                                    </div>
                                                )
                                        ) : (
                                                null
                                            )

                                    ) : (null)}
                                </div>

                                <hr />

                            </div>
                            <div class="row">

                                <div class="col-lg-8" >

                                    {this.state.generalDetailDisplay == true ? (
                                        <div class="card">
                                            <div class="card-body">

                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Full Name </label>
                                                    <div class="col-sm-12">
                                                        <input id="fullName" name="fullName" value={this.state.fullName} onChange={this.handleChange} class="form-control" type="text" placeholder="Full Name" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Current Address </label>
                                                    <div class="col-sm-12">
                                                        <textarea id="address" name="address" value={this.state.address} onChange={this.handleChange} class="form-control" rows="3" placeholder="Address"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Tel Number </label>
                                                    <div class="col-sm-12">
                                                        <input id="telNumber"
                                                            maxLength="10" name="telNumber" value={this.state.telNumber} onChange={this.handleChange} class="form-control" type="number" placeholder="Tel Number" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Mobile Number </label>
                                                    <div class="col-sm-12">
                                                        <input id="mobileNumber" maxLength="10" name="mobileNumber" value={this.state.mobileNumber} onChange={this.handleChange} class="form-control" type="number" placeholder="Mobile Number" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> National ID Card Number  </label>
                                                    <div class="col-sm-12">
                                                        <input id="nic" name="nic" class="form-control" value={this.state.nic} onChange={this.handleChange} type="text" placeholder="National ID Card Number" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Passport Number  </label>
                                                    <div class="col-sm-12">
                                                        <input id="passport" maxLength="8" name="passport" value={this.state.passport} onChange={this.handleChange} class="form-control" type="text" placeholder="Passport Number" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Temporary Passport Number  </label>
                                                    <div class="col-sm-12">
                                                        <input id="tempPassport" maxLength="8" name="tempPassport" value={this.state.tempPassport} onChange={this.handleChange} class="form-control" type="text" placeholder="Temporary Passport Number" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Have you registered previously at the SLBFE   </label>
                                                    <div class="col-sm-12">
                                                        <input type="checkbox" id="switch7" switch="info" checked={this.state.registeredSLBFE} />
                                                        <label for="switch7" data-on-label="Yes" data-off-label="No"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> SLBFE Registration Number  </label>
                                                    <div class="col-sm-12">
                                                        <input id="registrationNumber" name="registrationNumber" value={this.state.registrationNumber} onChange={this.handleChange} class="form-control" type="text" placeholder="SLBFE Registration Number" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Marital Status  </label>
                                                    <div class="col-sm-12">
                                                        <Dropdown
                                                            placeholder='Select Marital Status'
                                                            fluid
                                                            selection
                                                            id='maritalStatus'
                                                            name='maritalStatus'
                                                            value={this.state.maritalStatus}
                                                            options={this.state.meritalStatusList}
                                                            onChange={this.handleOnChange}
                                                        />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Gender  </label>
                                                    <div class="col-sm-12">
                                                        <Dropdown
                                                            placeholder='Select Gender'
                                                            fluid
                                                            selection
                                                            id='gender'
                                                            name='gender'
                                                            value={this.state.gender}
                                                            options={this.state.genderList}
                                                            onChange={this.handleOnChange}
                                                        />
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Condition Of the Migrant  </label>
                                                    <div class="col-sm-12">
                                                        <Dropdown
                                                            placeholder='Select Condition Of Migrant'
                                                            fluid
                                                            selection
                                                            id='conditionOfTheMigrant'
                                                            name='conditionOfTheMigrant'
                                                            value={this.state.conditionOfTheMigrant}
                                                            options={this.state.conditionsOfMigrant}
                                                            onChange={this.handleOnChange}
                                                        />
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Recieved To Sahana Piyasa  </label>
                                                    <div class="col-sm-12">
                                                        <DateTime
                                                            className="DateTime"
                                                            id="recievedToSahanaPiyasa"
                                                            name="recievedToSahanaPiyasa"
                                                            defaultValue={new Date()}
                                                            dateFormat="YYYY-MM-DD"
                                                            timeFormat="HH:mm"
                                                            value={
                                                                this.state.recievedToSahanaPiyasa != undefined && this.state.recievedToSahanaPiyasa != null && this.state.recievedToSahanaPiyasa != "" ? (
                                                                    formatDateTime(this.state.recievedToSahanaPiyasa)
                                                                ) : ("")
                                                            }
                                                            onChange={(e) => this.handleSahanaDate(e, "recievedToSahanaPiyasa")}
                                                        />
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Released From Sahana Piyasa (Enter this after leaving migrant )  </label>
                                                    <div class="col-sm-12">
                                                        <DateTime
                                                            className="DateTime"
                                                            id="releasedFromSahanaPiyasa"
                                                            name="releasedFromSahanaPiyasa"

                                                            dateFormat="YYYY-MM-DD"
                                                            timeFormat="HH:mm"
                                                            value={
                                                                this.state.releasedFromSahanaPiyasa != undefined && this.state.releasedFromSahanaPiyasa != null && this.state.releasedFromSahanaPiyasa != "" ? (
                                                                    formatDateTime(this.state.releasedFromSahanaPiyasa)
                                                                ) : ("")
                                                            }
                                                            onChange={(e) => this.handleSahanaDate(e, "releasedFromSahanaPiyasa")}
                                                        />
                                                    </div>
                                                </div>


                                                <div class="form-group row">
                                                    <div class="col-sm-12" style={{ textAlign: "right" }}>
                                                        <br /><button disabled={loading ? (true) : (false)} onClick={() => this.handleGeneralProfileSubmit()} class="col-sm-2 btn btn-info " type="button" aria-haspopup="true" aria-expanded="false">
                                                            {loading ? (
                                                                <Loader
                                                                    type="Oval"
                                                                    color="#2A3F54"
                                                                    height={15}
                                                                    width={40}
                                                                />
                                                            ) : ("Submit")}
                                                        </button>
                                                    </div>
                                                </div>

                                                <div>

                                                    {
                                                        (formErrorStatus.status) ? (
                                                            hasGeneralProfileMessage ? (
                                                                < div class="ui negative message">
                                                                    <div class="header">
                                                                        Not Submitted.
                                                    </div>
                                                                    <p>{formErrorStatus.message}</p>
                                                                </div>
                                                            ) : (
                                                                    null
                                                                )

                                                        ) : ((formSuccessState.status) ? (
                                                            hasGeneralProfileMessage ? (
                                                                < div class="ui success message">
                                                                    <div class="header">
                                                                        Submitted successfully.
                                                    </div>
                                                                    <p>{formSuccessState.message}</p>
                                                                </ div>
                                                            ) : (
                                                                    null
                                                                )

                                                        ) : (''))
                                                    }
                                                </div>

                                            </div>
                                        </div>


                                    ) : (
                                            null
                                        )}


                                </div>


                            </div>

                        </Accordion.Content>

                        <Accordion.Title
                            className="card-header darkHeader"
                            active={activeIndex === 1}
                            index={1}
                            onClick={this.handleClick}
                        >
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="headerAlign">
                                        <Icon name='dropdown headerRightAlign' />
                                        <div class="">
                                            <div className="accordianLeftTitleText">
                                                <h4 class="mt-0 header-title" > Travel Profile </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="headerRightAlign">
                                        <div className="accordianRightTitleText">
                                            {this.state.migrantTravelHistoryProfiles.length > 0 ? (
                                                <h4> ( {this.state.migrantTravelHistoryProfiles.length} )  previous {this.state.migrantTravelHistoryProfiles.length == 1 ? ("record") : ("records")} available. </h4>
                                            ) : (null)}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Accordion.Title>

                        <Accordion.Content active={activeIndex === 1}>
                            <div class="col-lg-12">
                                <div class="card HistoryContainer">
                                    <h3 className="HistoryHeading">Hisory Details</h3>

                                    {this.state.migrantTravelHistoryProfiles.length > 0 ? (
                                        this.state.migrantTravelHistoryProfiles.map((profile, index) => {
                                            return [<div className="HistoryGrayHeader"> {index + 1 + ") " + "Created Date : " + formatDate(profile.createdAt)} </div>
                                                , <hr />, <div className="HistoryContainerInner">
                                                <Grid>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Travel Type : </strong> {profile.travelType}  </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Last country of Employment : </strong> {profile.lastOccupationCountry}  </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    {profile.travelType == "ARRIVAL" ? (
                                                        <GridRow>
                                                            <GridColumn width={8}>
                                                                <label> <strong> Arrival Country : </strong> {profile.arrivalCountry}  </label>
                                                            </GridColumn>

                                                            <GridColumn width={8}>
                                                                <label> <strong> Arrival Date : </strong>

                                                                    {profile.arrivalDate != undefined && formatDate(new Date(profile.arrivalDate)) != "1970-01-01" ? (
                                                                        formatDateTime(profile.arrivalDate)
                                                                    ) : ('')
                                                                    }

                                                                </label>
                                                            </GridColumn>


                                                        </GridRow>
                                                    ) : (null)}

                                                    <GridRow>
                                                        <GridColumn width={12}>
                                                            <label> <strong> Contract Period : </strong> {profile.contractPeriod}  </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Returned reason : </strong> {profile.returnedReason}  </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Bindings with the SLBFE : </strong> {profile.custodyOfSLBFE}  </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Official statement by migrant worker : </strong> {profile.officialStatement}  </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Other Comments : </strong> {profile.comments}  </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Flight Number : </strong> {profile.flightNumber}  </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Time Of Flight : </strong> {profile.timeOfFlight}  </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Source Of Employment : </strong> {profile.sourceOfEmployment}  </label>
                                                        </GridColumn>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Agency License No : </strong> {profile.agencyLicenseNo}  </label>
                                                        </GridColumn>
                                                    </GridRow>


                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Confirm Of Statement : </strong> {profile.confirmOfStatement}  </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Profile Priority : </strong> {profile.profilePriority}  </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                </Grid>

                                            </div>, <hr />]
                                        })
                                    ) : (<h3 style={{ color: "#6c757d" }}>Not any History Records</h3>)}

                                </div>

                            </div>

                            <div class="col-lg-8">
                                <div class="card">
                                    <div class="card-body">

                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Number of times migrated for work </label>
                                            <div class="col-sm-12">
                                                <input id="numOfTimesMigrated" name="numOfTimesMigrated" value={this.state.numOfTimesMigrated} onChange={this.handleChange} class="form-control" type="number" placeholder=" Number of times migrated" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Travel Type </label>
                                            <div class="col-sm-12">
                                                <Dropdown
                                                    placeholder='Select Travel Type'
                                                    fluid
                                                    selection
                                                    id='travelType'
                                                    name='travelType'
                                                    value={this.state.travelType}
                                                    options={this.state.travelTypes}
                                                    onChange={this.handleOnChange}
                                                />
                                            </div>
                                        </div>


                                        {this.state.travelType == "ARRIVAL" ? (
                                            <div>

                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Arrival Country </label>
                                                    <div class="col-sm-12">
                                                        <input id="arrivalCountry" name="arrivalCountry" value={this.state.arrivalCountry} onChange={this.handleChange} class="form-control" type="text" placeholder="Arrival Country" />
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Arrival Date </label>
                                                    <div class="col-sm-12">
                                                        <DateTime
                                                            className="DateTime"
                                                            id="arrivalDate"
                                                            name="arrivalDate"
                                                            defaultValue={new Date()}
                                                            dateFormat="YYYY-MM-DD"
                                                            timeFormat="HH:mm"
                                                            value={
                                                                this.state.arrivalDate != undefined && this.state.arrivalDate != null && this.state.arrivalDate != "" ? (
                                                                    formatDateTime(this.state.arrivalDate)
                                                                ) : ("")
                                                            }
                                                            onChange={(e) => this.handleSahanaDate(e, "arrivalDate")}
                                                        />
                                                    </div>
                                                </div>

                                            </div>

                                        ) : (null)}

                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Contract Period  </label>
                                            <div class="col-sm-12">
                                                <Dropdown
                                                    placeholder='Select Contract Period'
                                                    fluid
                                                    selection
                                                    id='contractPeriod'
                                                    name='contractPeriod'
                                                    value={this.state.contractPeriod}
                                                    options={this.state.contractPeriodList}
                                                    onChange={this.handleOnChange}
                                                />
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Returned reason </label>
                                            <div class="col-sm-12">

                                                <Dropdown
                                                    placeholder='Select Destination Type'
                                                    fluid
                                                    selection
                                                    id='returnedReason'
                                                    name='returnedReason'
                                                    options={this.state.returnReasons}
                                                    value={this.state.returnedReason}
                                                    onChange={this.handleOnChange}
                                                />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Bindings with the SLBFE </label>
                                            <div class="col-sm-12">
                                                <input id="custodyOfSLBFE" name="custodyOfSLBFE" value={this.state.custodyOfSLBFE} onChange={this.handleChange} class="form-control" type="text" placeholder="Details of any personal possesions " />
                                            </div>
                                        </div>
                                        <br />

                                        <div class="form-group row">
                                            <label class="control-label col-md-12 col-sm-12 " for="number" > Official statement by migrant worker <br /> (including a brief on overall situation and problems faced) </label>
                                            <div class="col-sm-12">
                                                <textarea id="officialStatement" name="officialStatement" value={this.state.officialStatement} onChange={this.handleChange} class="form-control" rows="6" placeholder="Official Statement"></textarea>
                                            </div>
                                        </div>

                                        <br />

                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <label class="d-block mb-3"> Confirm that the statement provided by the worker </label>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="confirmStatmentYes" name="confirmOfStatement" checked={this.state.confirmOfStatement == "true" ? true : false} value={true} onChange={this.handleRadioOnChange} class="custom-control-input" />
                                                    <label class="custom-control-label" for="confirmStatmentYes">Yes</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="confirmStatmentNo" name="confirmOfStatement" checked={this.state.confirmOfStatement == "false" ? true : false} value={false} onChange={this.handleRadioOnChange} class="custom-control-input" />
                                                    <label class="custom-control-label" for="confirmStatmentNo">No</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Other Comments </label>
                                            <div class="col-sm-12">
                                                <textarea id="comments" name="comments" value={this.state.comments} onChange={this.handleChange} class="form-control" rows="3" placeholder="Other Comments"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Flight Number  </label>
                                            <div class="col-sm-12">
                                                <input id="flightNumber" name="flightNumber" value={this.state.flightNumber} onChange={this.handleChange} class="form-control" type="text" placeholder="Flight Number" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Time Of Flight  </label>
                                            <div class="col-sm-12">
                                                <DateTime
                                                    className="DateTime"
                                                    id="timeOfFlight"
                                                    name="timeOfFlight"
                                                    defaultValue={new Date()}
                                                    isValidDate={this.getValidDates}
                                                    dateFormat="YYYY-MM-DD"
                                                    timeFormat="HH:mm"
                                                    value={this.state.timeOfFlight}
                                                    onChange={(e) => this.handleDate(e, "timeOfFlight")}
                                                />
                                            </div>

                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Last country of Employment   </label>
                                            <div class="col-sm-12">
                                                <input id="lastOccupationCountry" name="lastOccupationCountry" value={this.state.lastOccupationCountry} onChange={this.handleChange} class="form-control" type="text" placeholder="Last Country of Occupation" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Source Of Employment  </label>
                                            <div class="col-sm-12">
                                                <Dropdown
                                                    placeholder='Select Source Of Employment'
                                                    fluid
                                                    selection
                                                    id='sourceOfEmployment'
                                                    name='sourceOfEmployment'
                                                    value={this.state.sourceOfEmployment}
                                                    options={this.state.sourcesOfEmployment}
                                                    onChange={this.handleOnChange}
                                                />
                                            </div>
                                        </div>

                                        {
                                            this.state.sourceOfEmployment == "Through an Agency" ? (
                                                <div>
                                                    {/* <div class="form-group row">
                                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Agency Name  </label>
                                                        <div class="col-sm-12">
                                                            <input id="agencyName" name="agencyName" value={this.state.agencyName} onChange={this.handleChange} class="form-control" type="text" placeholder="Agency Name" />
                                                        </div>
                                                    </div> */}

                                                    <div class="form-group row">
                                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Agency License No  </label>
                                                        <div class="col-sm-12">
                                                            <input id="agencyLicenseNo" name="agencyLicenseNo" value={this.state.agencyLicenseNo} onChange={this.handleChange} class="form-control" type="text" placeholder="Agency License No" />
                                                        </div>
                                                    </div>
                                                </div>
                                            ) : (
                                                    null
                                                )
                                        }
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Profile Priority </label>
                                            <div class="col-sm-12">
                                                <Dropdown
                                                    placeholder='Select Profile Priority'
                                                    fluid
                                                    selection
                                                    id='profilePriority'
                                                    name='profilePriority'
                                                    options={this.state.profilePriorities}
                                                    value={this.state.profilePriority}
                                                    onChange={this.handleOnChange}
                                                />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12" style={{ textAlign: "right" }}>
                                                <br /><button disabled={loading ? (true) : (false)} onClick={() => this.handleTravelProfileSubmit()} class="col-sm-2 btn btn-info " type="button" aria-haspopup="true" aria-expanded="false">
                                                    {loading ? (
                                                        <Loader
                                                            type="Oval"
                                                            color="#2A3F54"
                                                            height={15}
                                                            width={40}
                                                        />
                                                    ) : ("Submit")}
                                                </button>
                                            </div>
                                        </div>

                                        <div>

                                            {
                                                (formErrorStatus.status) ? (
                                                    hasTravelProfileMessage ? (
                                                        < div class="ui negative message">
                                                            <div class="header">
                                                                Not Submitted.
                                                        </div>
                                                            <p>{formErrorStatus.message}</p>
                                                        </div>
                                                    ) : (
                                                            null
                                                        )

                                                ) : ((formSuccessState.status) ? (
                                                    hasTravelProfileMessage ? (
                                                        < div class="ui success message">
                                                            <div class="header">
                                                                Submitted successfully.
                                                        </div>
                                                            <p>{formSuccessState.message}</p>
                                                        </ div>
                                                    ) : (
                                                            null
                                                        )

                                                ) : (''))
                                            }
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </Accordion.Content>


                        {localStorage.USER_ROLE == "MISSIONOFFICIAL" || localStorage.USER_ROLE == "ADMINISTRATOR" ? (
                            <div>
                                <Accordion.Title
                                    className="card-header darkHeader"
                                    active={activeIndex === 2}
                                    index={2}
                                    onClick={this.handleClick}
                                >
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="headerAlign">
                                                <Icon name='dropdown headerRightAlign' />
                                                <div class="">
                                                    <div className="accordianLeftTitleText">
                                                        <h4 class="mt-0 header-title" > Foreign Mission Onboarding Profile </h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="headerRightAlign">
                                                <div className="accordianRightTitleText">
                                                    {this.state.sheduledArrivalHistoryProfiles.length > 0 ? (
                                                        <h4> ( {this.state.sheduledArrivalHistoryProfiles.length} )  previous {this.state.sheduledArrivalHistoryProfiles.length == 1 ? ("record") : ("records")} available. </h4>
                                                    ) : (null)}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Accordion.Title>

                                <Accordion.Content active={activeIndex === 2}>
                                    <div class="row">

                                        <div class="col-lg-12">
                                            <div class="card HistoryContainer">
                                                <h3 className="HistoryHeading">Hisory Details</h3>

                                                {this.state.sheduledArrivalHistoryProfiles.length > 0 ? (
                                                    this.state.sheduledArrivalHistoryProfiles.map((profile, index) => {
                                                        return [<div className="HistoryGrayHeader"> {index + 1 + ") " + "Created Date : " + formatDate(profile.createdAt)} </div>
                                                            , <hr />, <div className="HistoryContainerInner">
                                                            <Grid>

                                                                <GridRow>
                                                                    <GridColumn width={8}>
                                                                        <label> <strong> Departure Location : </strong> {profile.departureLocation}  </label>
                                                                    </GridColumn>

                                                                    <GridColumn width={8}>
                                                                        <label> <strong> Scheduled Arrival : </strong> {profile.scheduledArrival}  </label>
                                                                    </GridColumn>
                                                                </GridRow>

                                                                <GridRow>
                                                                    <GridColumn width={8}>
                                                                        <label> <strong> Scheduled Flight No : </strong> {profile.scheduledFlightNo}  </label>
                                                                    </GridColumn>

                                                                    <GridColumn width={8}>
                                                                        <label> <strong> Scheduled Flight Datetime : </strong> {profile.scheduledFlightDatetime}  </label>
                                                                    </GridColumn>
                                                                </GridRow>

                                                                <GridRow>
                                                                    <GridColumn width={8}>
                                                                        <label> <strong> Suggested Action : </strong> {profile.suggestedAction}  </label>
                                                                    </GridColumn>

                                                                    <GridColumn width={8}>
                                                                        <label> <strong> Suggested Action Note : </strong> {profile.suggestedActionNote}  </label>
                                                                    </GridColumn>
                                                                </GridRow>

                                                                <GridRow>
                                                                    <GridColumn width={16}>
                                                                        <label> <strong> Foreign Mission Profile Priority : </strong> {profile.foreignMissionProfilePriority}  </label>
                                                                    </GridColumn>
                                                                </GridRow>

                                                                <GridRow>
                                                                    <div className="HistoryMultipleContainer">
                                                                        <Grid>
                                                                            <GridRow>
                                                                                <h3> Transit Informations </h3>
                                                                            </GridRow>
                                                                            {profile.transitInformations.length > 0 ? (
                                                                                profile.transitInformations.map((transitInformation, index) => {
                                                                                    return [<GridRow>

                                                                                        <GridColumn width={6}>
                                                                                            {index + 1 + ") "} <label> <strong> Transit Arrival : </strong> {transitInformation.transitArrival}  </label>
                                                                                            <hr />
                                                                                        </GridColumn>
                                                                                        <GridColumn width={9}>
                                                                                            <label> <strong> Transit Departure : </strong> {transitInformation.transitDeparture}  </label>
                                                                                            <hr />
                                                                                        </GridColumn>

                                                                                    </GridRow>]

                                                                                })
                                                                            ) : (null)}
                                                                        </Grid>
                                                                    </div>
                                                                </GridRow>

                                                            </Grid>

                                                        </div>, <hr />]
                                                    })
                                                ) : (<h3 style={{ color: "#6c757d" }}>Not any History Records</h3>)}

                                            </div>

                                        </div>

                                        <div class="col-lg-8" >

                                            <div class="card">
                                                <div class="card-body">

                                                    <div class="form-group row">
                                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Departure Location </label>
                                                        <div class="col-sm-12">
                                                            <input id="departureLocation" name="departureLocation" value={this.state.departureLocation} onChange={this.handleChange} class="form-control" type="text" placeholder="Departure Location" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Scheduled Flight No  </label>
                                                        <div class="col-sm-12">
                                                            <input id="scheduledFlightNo" name="scheduledFlightNo" value={this.state.scheduledFlightNo} onChange={this.handleChange} class="form-control" type="text" placeholder="Scheduled Flight No" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Sheduled Flight Datetime  </label>
                                                        <div class="col-sm-12">
                                                            <DateTime
                                                                className="DateTime"
                                                                id="scheduledFlightDatetime"
                                                                name="scheduledFlightDatetime"
                                                                defaultValue={new Date()}
                                                                isValidDate={this.getValidDates}
                                                                dateFormat="YYYY-MM-DD"
                                                                timeFormat="HH:mm"
                                                                value={this.state.scheduledFlightDatetime}
                                                                onChange={(e) => this.handleDate(e, "scheduledFlightDatetime")}
                                                            />
                                                        </div>
                                                    </div>

                                                    <div class="card">
                                                        <div class="card-body" >

                                                            <div class="card-header" style={{ backgroundColor: "#e6e8eb" }}>
                                                                <h4 class="mt-0 header-title" > Transit informations </h4>
                                                            </div><br />


                                                            {this._createTransitInformation()}
                                                            <div style={{ textAlign: "right" }}><button className="btn btn-secondary" onClick={this._addMoreTransitInformation.bind(this)} type="button">
                                                                Add More <i className="fas fa-plus-circle" />
                                                            </button>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Sheduled arrival </label>
                                                        <div class="col-sm-12">
                                                            <DateTime
                                                                className="DateTime"
                                                                id="scheduledArrival"
                                                                name="scheduledArrival"
                                                                defaultValue={new Date()}
                                                                isValidDate={this.getValidDates}
                                                                dateFormat="YYYY-MM-DD"
                                                                timeFormat="HH:mm"
                                                                value={this.state.scheduledArrival}
                                                                onChange={(e) => this.handleDate(e, "scheduledArrival")}
                                                            />
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Suggested Actions on Arrival  </label>
                                                        <div class="col-sm-12">
                                                            <Dropdown
                                                                placeholder='Select Suggested Action'
                                                                fluid
                                                                selection
                                                                id='suggestedAction'
                                                                name='suggestedAction'
                                                                options={this.state.suggestedActionsList}
                                                                value={this.state.suggestedAction}
                                                                onChange={this.handleOnChange}
                                                            />
                                                        </div>
                                                    </div>

                                                    {this.state.suggestedAction == "Other" ? (
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Suggested Action Note : </label>
                                                            <div class="col-sm-12">
                                                                <textarea id="suggestedActionNote" name="suggestedActionNote" value={this.state.suggestedActionNote} onChange={this.handleChange} class="form-control" rows="3" placeholder="Suggested Action Note"></textarea>
                                                            </div>
                                                        </div>
                                                    ) : (
                                                            null
                                                        )}

                                                    <div class="form-group row">
                                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Profile Priority </label>
                                                        <div class="col-sm-12">
                                                            <Dropdown
                                                                placeholder='Select Profile Priority'
                                                                fluid
                                                                selection
                                                                id='foreignMissionProfilePriority'
                                                                name='foreignMissionProfilePriority'
                                                                value={this.state.foreignMissionProfilePriority}
                                                                options={this.state.profilePriorities}
                                                                onChange={this.handleOnChange}
                                                            />
                                                        </div>
                                                    </div>


                                                    <div class="form-group row">
                                                        <div class="col-sm-12" style={{ textAlign: "right" }}>
                                                            <br /><button disabled={loading ? (true) : (false)} onClick={() => this.handleSheduledArrivalProfileSubmit()} class="col-sm-2 btn btn-info " type="button" aria-haspopup="true" aria-expanded="false">
                                                                {loading ? (
                                                                    <Loader
                                                                        type="Oval"
                                                                        color="#2A3F54"
                                                                        height={15}
                                                                        width={40}
                                                                    />
                                                                ) : ("Submit")}
                                                            </button>
                                                        </div>
                                                    </div>

                                                    <div>

                                                        {
                                                            (formErrorStatus.status) ? (
                                                                hasSheduledArrivalProfileMessage ? (
                                                                    < div class="ui negative message">
                                                                        <div class="header">
                                                                            Not Submitted.
                                                            </div>
                                                                        <p>{formErrorStatus.message}</p>
                                                                    </div>
                                                                ) : (
                                                                        null
                                                                    )

                                                            ) : ((formSuccessState.status) ? (
                                                                hasSheduledArrivalProfileMessage ? (
                                                                    < div class="ui success message">
                                                                        <div class="header">
                                                                            Submitted successfully.
                                                            </div>
                                                                        <p>{formSuccessState.message}</p>
                                                                    </ div>
                                                                ) : (
                                                                        null
                                                                    )

                                                            ) : (''))
                                                        }
                                                    </div>

                                                </div>
                                            </div>


                                        </div>

                                    </div>

                                </Accordion.Content>

                            </div>
                        ) : (
                                null
                            )}
                    </Accordion>
                </div >
                <hr />
                <br />

            </div >

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(OnboardActions)));

