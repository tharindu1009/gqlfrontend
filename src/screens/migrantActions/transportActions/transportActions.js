import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../../actions/user-actions'
import { formatDate, formatDateTime } from '../../../middleware/index';

//sementic ui
import { Dropdown, Grid, Icon, Message, Input, Accordion, GridRow, GridColumn } from 'semantic-ui-react';
import {
    DateTimeInput
} from 'semantic-ui-calendar-react';

//Spinner
import Loader from 'react-loader-spinner'

import DateTime from 'react-datetime';

//query
import {
    GET_SINGLE_TRANSPORT_PROFILE, ADD_TRANSPORT_PROFILE, EDIT_TRANSPORT_PROFILE, GET_TRANSPORT_HISTORY_PROFILES
} from '../../../queries/TransportQueries';

import {
    GET_ALL_DISTRICTS,
} from '../../../queries/CommonQueries';



const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class TransportActions extends Component {
    constructor(props) {
        super(props);

        const query = new URLSearchParams(this.props.location.search);
        const id = query.get('id');

        var existMigrant = false;
        var migrantId = "";

        if (id != null || id != undefined) {
            migrantId = id;
            existMigrant = true;
        }

        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        this.state = {
            activeIndex: -1,
            migrantId: migrantId,
            existMigrant: existMigrant,
            loading: false,
            destinationTypes: [
                { text: "Home", value: "Home ", id: 'destinationType' },
                { text: "Friend's Home", value: "Friend's Home", id: 'destinationType' },
                { text: "Relative's Home", value: "Relative's Home", id: 'destinationType' },
                { text: "Hospital", value: "Hospital", id: 'destinationType' },
            ],
            relationships: [
                { text: "Father", value: "Father", id: 'relationship' },
                { text: "Mother", value: "Mother", id: 'relationship' },
                { text: "Child", value: "Child", id: 'relationship' },
                { text: "Neighbour", value: "Neighbour", id: 'relationship' },
                { text: "Cusin", value: "Cusin", id: 'relationship' },
                { text: "Other", value: "Other", id: 'relationship' },
            ],
            transportMethods: [
                { text: "Ambulence", value: "Ambulence", id: 'transportMethod' },
                { text: "Office Vehicle", value: "Office Vehicle", id: 'transportMethod' },
                { text: "Bus Fare", value: "Bus Fare", id: 'transportMethod' },
                { text: "Taxi Fare", value: "Taxi Fare", id: 'transportMethod' }
            ],
            locationsOfPayment: [
                { text: "Sahana Piyasa", value: "Sahana Piyasa", id: 'locationOfPayment' },
                { text: "Airport", value: "Airport", id: 'locationOfPayment' },
            ],
            medicalTreatmentRecords: [{ prescribedMedication: "", dosage: "", frequency: "", dateStarted: "", comments: "" }],
            destinationType: "",
            transportMethod: "",
            departedOnTheirOwn: "",
            locationOfPayment: "",
            slbfeOffices: [{ officerName: "", officerMobileNo: "" }],
            thirdPartyDetails: [{ thirdPartyPersonName: "", thirdPartyPersonNIC: "", thirdPartyPersonMobileNum: "" }],

            hasTransportProfileMessage: false,
            existTransportProfile: false,
            transportProfileId: "",

            nameOfHandoverPerson: "",
            nicOfHandoverPerson: "",
            numberOfHandoverPerson: "",
            relationshipOfHandoverPerson: "",
            hospitalName: "",
            departureLocation: "",
            destinationDistrict: "",
            destinationLocation: "",
            expectedDepartureTime: "",
            expectedArrivalTime: "",
            actualDepartureTime: "",
            actualArrivalTime: "",
            taxiFare: "",

            districts: [],
            transportHistoryDisplayProfiles: [],

        }

    }

    componentDidMount() {
        if (this.state.existMigrant) {
            this.loadSingleTransportProfile();
            this.loadTransportHistoryProfiles();
        }

        this.loadAllDistricts();
    }

    loadTransportHistoryProfiles() {
        this.getTransportHistoryProfiles().then(result => {

            console.log(result)

            if (result.length > 0) {
                this.setState({
                    transportHistoryDisplayProfiles: result
                });
            }
        })
    }

    getTransportHistoryProfiles = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_TRANSPORT_HISTORY_PROFILES,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getTransportHistoryProfiles;
    }

    loadAllDistricts() {
        var districts = [];
        this.getAllDistricts().then(result => {
            districts = result.map((district, i) => {
                return { text: district.district, value: district.district, id: 'destinationDistrict' }
            })

            this.setState({ districts: districts });
        })
    }

    getAllDistricts = async () => {
        const result = await this.props.client.query({
            query: GET_ALL_DISTRICTS,
            fetchPolicy: 'network-only'
        });
        return result.data.getAllDistricts;
    }

    loadSingleTransportProfile() {
        this.getSingleTransportProfile().then(result => {

            if (result.length > 0) {
                this.setState({
                    transportProfileId: result[0].id,
                    destinationType: result[0].destinationType,
                    nameOfHandoverPerson: result[0].nameOfHandoverPerson,
                    nicOfHandoverPerson: result[0].nicOfHandoverPerson,
                    numberOfHandoverPerson: result[0].numberOfHandoverPerson,
                    relationshipOfHandoverPerson: result[0].relationshipOfHandoverPerson,
                    hospitalName: result[0].hospitalName,
                    transportMethod: result[0].transportMethod,
                    slbfeOffices: result[0].slbfeOffices,
                    departedOnTheirOwn: result[0].departedOnTheirOwn,
                    thirdPartyDetails: result[0].thirdPartyDetails,
                    departureLocation: result[0].departureLocation,
                    destinationDistrict: result[0].destinationDistrict,
                    destinationLocation: result[0].destinationLocation,
                    expectedDepartureTime: result[0].expectedDepartureTime,
                    expectedArrivalTime: result[0].expectedArrivalTime,
                    actualDepartureTime: result[0].actualDepartureTime,
                    actualArrivalTime: result[0].actualArrivalTime,
                    taxiFare: result[0].taxiFare,
                    locationOfPayment: result[0].locationOfPayment,
                    existTransportProfile: true
                });
            }
        })
    }

    getSingleTransportProfile = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_SINGLE_TRANSPORT_PROFILE,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleTransportProfile;
    }

    handleTransportProfileSubmit() {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        this.setState({
            hasTravelProfileMessage: false, hasSheduledArrivalProfileMessage: false,
            hasMedicalProfileMessage: false, hasSexualRiskProfileMessage: false, hasMedicalTreatmentProfileMessage: false, hasCounselingProfileMessage: false,
            hasMedicalTreatmentPastProfileMessage: false, hasCounselingPastProfileMessage: false, hasMedicalReferrelInformationMessage: false,
            hasMedicalQualificationProfileMessage: false, hasAccomodationDetailProfileMessage: false, hasMealRecordProfileMessage: false, hasTransportProfileMessage: false, hasFollowupProfileMessage: false
        })

        const { migrantId } = this.state;

        if (migrantId == "" || migrantId == undefined || migrantId == null) {
            this.setState({ hasTransportProfileMessage: true })
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "Please submit migrant general details before submit Transport Profile Details."
            });
            return;
        } else {

            this.setState({ loading: true });
            this.props.setFormStatus({ status: false, title: '', message: '' });
            this.props.setSuccessStatus({ status: false, title: '', message: '' });

            if (this.state.existTransportProfile) {

                this.editTransportProfile().then(result => {
                    this.setState({ hasTransportProfileMessage: true, loading: false })

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Transport Profile Details edited Successfully!"
                    });

                    this.loadSingleTransportProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasTransportProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to edit Transport Profile Details."

                        });
                    }
                });

            } else {
                this.addTransportProfile().then(result => {

                    this.setState({
                        loading: false,
                        form_state: 'default',
                        title: "",
                        message: "",
                        hasTransportProfileMessage: true
                    });

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Transport Profile Details submitted Successfully!."
                    });
                    this.loadSingleTransportProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasTransportProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to submit Transport Profile Details."

                        });
                    }
                });
            }
        }


    }

    addTransportProfile = async () => {
        const {
            destinationType,
            nameOfHandoverPerson,
            nicOfHandoverPerson,
            numberOfHandoverPerson,
            relationshipOfHandoverPerson,
            hospitalName,
            transportMethod,
            slbfeOffices,
            thirdPartyDetails,
            departureLocation,
            destinationDistrict,
            destinationLocation,
            expectedDepartureTime,
            expectedArrivalTime,
            actualDepartureTime,
            actualArrivalTime,
            taxiFare,
            locationOfPayment,
            migrantId } = this.state;

        const departedOnTheirOwn = this.state.departedOnTheirOwn;

        const result = await this.props.client.mutate({
            mutation: ADD_TRANSPORT_PROFILE,
            variables: {
                destinationType,
                nameOfHandoverPerson,
                nicOfHandoverPerson,
                numberOfHandoverPerson,
                relationshipOfHandoverPerson,
                hospitalName,
                transportMethod,
                slbfeOffices,
                departedOnTheirOwn,
                thirdPartyDetails,
                departureLocation,
                destinationDistrict,
                destinationLocation,
                expectedDepartureTime,
                expectedArrivalTime,
                actualDepartureTime,
                actualArrivalTime,
                taxiFare,
                locationOfPayment,
                migrantId
            }
        });
        return result.data.addTransportProfile;
    };

    editTransportProfile = async () => {
        const {
            destinationType,
            nameOfHandoverPerson,
            nicOfHandoverPerson,
            numberOfHandoverPerson,
            relationshipOfHandoverPerson,
            hospitalName,
            transportMethod,
            slbfeOffices,
            thirdPartyDetails,
            departureLocation,
            destinationDistrict,
            destinationLocation,
            expectedDepartureTime,
            expectedArrivalTime,
            actualDepartureTime,
            actualArrivalTime,
            taxiFare,
            locationOfPayment,
        } = this.state;

        const id = this.state.transportProfileId;
        const departedOnTheirOwn = this.state.departedOnTheirOwn;

        const result = await this.props.client.mutate({
            mutation: EDIT_TRANSPORT_PROFILE,
            variables: {
                id,
                destinationType,
                nameOfHandoverPerson,
                nicOfHandoverPerson,
                numberOfHandoverPerson,
                relationshipOfHandoverPerson,
                hospitalName,
                transportMethod,
                slbfeOffices,
                departedOnTheirOwn,
                thirdPartyDetails,
                departureLocation,
                destinationDistrict,
                destinationLocation,
                expectedDepartureTime,
                expectedArrivalTime,
                actualDepartureTime,
                actualArrivalTime,
                taxiFare,
                locationOfPayment,
            }
        });
        return result.data.editTransportProfile;
    };

    handleChange = (event) => {
        this.setState({ [event.target.id]: event.target.value });
    }

    handleOnChange = (e, data) => {
        this.setState({ [data.id]: data.value });
    }

    handleRadioOnChange = (e) => {

        if (e.target.checked) {
            console.log(e.target.name)
            console.log(e.target.value)
            var name = e.target.name;
            var value = e.target.value;

            this.setState({ [name]: value });
        }
    }

    handleDate = (date, id) => {
        var value = formatDateTime(date._d);
        this.setState({ [id]: value });
    };

    getValidDates = function (currentDate) {
        var yesterday = DateTime.moment().subtract(1, 'day');
        return currentDate.isAfter(yesterday);
    }

    _createSLBFEOfficerDetails() {
        return this.state.slbfeOffices.map((el, i) => (
            <div key={i}>
                <br />

                {this.state.slbfeOffices.length <= 1 ? (
                    null
                ) : (
                        <div style={{ textAlign: "right", cursor: "pointer" }}>
                            <i className="fas fa-times-circle fa-2x" onClick={this.removeSLBFEOfficer.bind(this, i)}></i>
                        </div>
                    )}


                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label"> Office Full Name </label>
                    <div class="col-sm-12">
                        <input id="officerName" name="officerName" class="form-control" type="text" placeholder="Office Full Name" value={el.officerName || ''} onChange={this.handleSLBFEOfficerChange.bind(this, i)} />
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label"> Officer Number </label>
                    <div class="col-sm-12">
                        <input id="officerNumber" name="officerNumber" class="form-control" type="text" placeholder="Officer Number" value={el.officerNumber || ''} onChange={this.handleSLBFEOfficerChange.bind(this, i)} />
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label"> Mobile No </label>
                    <div class="col-sm-12">
                        <input id="officerMobileNo" name="officerMobileNo" class="form-control" type="number" placeholder="Mobile No" value={el.officerMobileNo || ''} onChange={this.handleSLBFEOfficerChange.bind(this, i)} />
                    </div>
                </div>

                <hr />

            </div >

        ))

    }


    _createThirdPartyDetails() {
        return this.state.thirdPartyDetails.map((el, i) => (
            <div key={i}>
                <br />

                {this.state.thirdPartyDetails.length <= 1 ? (
                    null
                ) : (
                        <div style={{ textAlign: "right", cursor: "pointer" }}>
                            <i className="fas fa-times-circle fa-2x" onClick={this.removeThirdPartyDetail.bind(this, i)}></i>
                        </div>
                    )}

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label"> Name </label>
                    <div class="col-sm-12">
                        <input id="thirdPartyPersonName" name="thirdPartyPersonName" class="form-control" type="text" placeholder="Name" value={el.thirdPartyPersonName || ''} onChange={this.handleThirdPartyDetailChange.bind(this, i)} />
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label"> NIC </label>
                    <div class="col-sm-12">
                        <input id="thirdPartyPersonNIC" name="thirdPartyPersonNIC" class="form-control" type="text" placeholder="NIC" value={el.thirdPartyPersonNIC || ''} onChange={this.handleThirdPartyDetailChange.bind(this, i)} />
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label"> Mobile Number </label>
                    <div class="col-sm-12">
                        <input id="thirdPartyPersonMobileNum" name="thirdPartyPersonMobileNum" class="form-control" type="number" placeholder="Mobile Number" value={el.thirdPartyPersonMobileNum || ''} onChange={this.handleThirdPartyDetailChange.bind(this, i)} />
                    </div>
                </div>

                <hr />

            </div >

        ))

    }

    handleSLBFEOfficerChange(i, e) {
        const { name, value } = e.target;
        let slbfeOffices = [...this.state.slbfeOffices];
        slbfeOffices[i] = { ...slbfeOffices[i], [name]: value };
        this.setState({ slbfeOffices });
    }

    handleThirdPartyDetailChange(i, e) {
        const { name, value } = e.target;
        let thirdPartyDetails = [...this.state.thirdPartyDetails];
        thirdPartyDetails[i] = { ...thirdPartyDetails[i], [name]: value };
        this.setState({ thirdPartyDetails });
    }

    _addMoreSLBFEOfficer() {
        let slbfeOffices = this.state.slbfeOffices;
        slbfeOffices.push({ officerName: "", officerMobileNo: "" });
        this.setState(slbfeOffices)
    }

    _addMoreThirdPartyDetail() {
        let thirdPartyDetails = this.state.thirdPartyDetails;
        thirdPartyDetails.push({ thirdPartyPersonName: "", thirdPartyPersonNIC: "", thirdPartyPersonMobileNum: "" });
        this.setState(thirdPartyDetails)
    }

    removeSLBFEOfficer(i) {
        let slbfeOffices = this.state.slbfeOffices;
        slbfeOffices.splice(i, 1);
        this.setState({ slbfeOffices });
    }

    removeThirdPartyDetail(i) {
        let thirdPartyDetails = this.state.thirdPartyDetails;
        thirdPartyDetails.splice(i, 1);
        this.setState({ thirdPartyDetails });
    }

    handleClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = this.state
        const newIndex = activeIndex === index ? -1 : index

        this.setState({ activeIndex: newIndex })
    }

    render() {
        const { activeIndex, loading, hasTransportProfileMessage } = this.state;

        const { formErrorStatus, formSuccessState } = this.props;

        return (
            <div>
                <br />

                <div class="col-lg-10">
                    <Accordion fluid styled style={{ "box-shadow": "none" }}>
                        <Accordion.Title
                            className="card-header darkHeader"
                            active={activeIndex === 13}
                            index={13}
                            onClick={this.handleClick}
                        >
                            <div class="row">
                               
                                <div class="col-lg-6">
                                    <div class="headerAlign">
                                        <Icon name='dropdown headerRightAlign' />
                                        <div class="">
                                            <div className="accordianLeftTitleText">
                                                <h4 class="mt-0 header-title" > Transport Profile </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="headerRightAlign">
                                        <div className="accordianRightTitleText">
                                            {this.state.transportHistoryDisplayProfiles.length > 0 ? (
                                                <h4> ( {this.state.transportHistoryDisplayProfiles.length} )  previous {this.state.transportHistoryDisplayProfiles.length == 1 ? ("record") : ("records")} available. </h4>
                                            ) : (null)}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </Accordion.Title>

                        <Accordion.Content active={activeIndex === 13}>


                            <div class="col-lg-12">
                                <div class="card HistoryContainer">
                                    <h3 className="HistoryHeading">Hisory Details</h3>

                                    {this.state.transportHistoryDisplayProfiles.length > 0 ? (
                                        this.state.transportHistoryDisplayProfiles.map((profile, index) => {
                                            return [<div className="HistoryGrayHeader"> {index + 1 + ") " + "Created Date : " + formatDate(profile.createdAt)} </div>
                                                , <hr />, <div className="HistoryContainerInner">
                                                <Grid>

                                                    <GridRow>
                                                        <GridColumn width={16}>
                                                            <label> <strong> Destination Type : </strong>
                                                                {profile.destinationType}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    {profile.destinationType != "Hospital" ? (
                                                        [

                                                            <GridRow>
                                                                <GridColumn>
                                                                    <div class="card-header">
                                                                        <h3 class="mt-0 header-title" > Handover Person Details </h3>
                                                                    </div>
                                                                </GridColumn>
                                                            </GridRow>, <br />,

                                                            <GridRow>
                                                                <GridColumn width={8}>
                                                                    <label> <strong> Name  : </strong>
                                                                        {profile.nameOfHandoverPerson}
                                                                    </label>
                                                                </GridColumn>

                                                                <GridColumn width={8}>
                                                                    <label> <strong> NIC  : </strong> {profile.nicOfHandoverPerson}  </label>
                                                                </GridColumn>

                                                            </GridRow>,

                                                            <GridRow>
                                                                <GridColumn width={8}>
                                                                    <label> <strong> Contact Number : </strong>
                                                                        {profile.numberOfHandoverPerson}
                                                                    </label>
                                                                </GridColumn>

                                                                <GridColumn width={8}>
                                                                    <label> <strong> Relationship : </strong> {profile.relationshipOfHandoverPerson}  </label>
                                                                </GridColumn>

                                                            </GridRow>]
                                                    ) : (
                                                            < GridRow >
                                                                <GridColumn width={16}>
                                                                    <label> <strong> Hospital Name : </strong>
                                                                        {profile.hospitalName}
                                                                    </label>
                                                                </GridColumn>
                                                            </GridRow>
                                                        )}


                                                    <GridRow>
                                                        <GridColumn width={16}>
                                                            <label> <strong> Transport Method : </strong>
                                                                {profile.transportMethod}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    <GridRow>
                                                        <div className="HistoryMultipleContainer">
                                                            <Grid>
                                                                <GridRow>
                                                                    <GridColumn>
                                                                        <div class="card-header">
                                                                            <h3 class="mt-0 header-title" > SLBFE Officer Details </h3>
                                                                        </div>
                                                                    </GridColumn>
                                                                </GridRow>
                                                                {profile.slbfeOffices.length > 0 ? (
                                                                    profile.slbfeOffices.map((slbfeOffice, index) => {
                                                                        return [<GridRow>
                                                                            {index + 1 + ") "}
                                                                            <GridColumn width={6}>
                                                                                <label> <strong> Officer Name : </strong> {slbfeOffice.officerName}  </label>
                                                                                <hr />
                                                                            </GridColumn>
                                                                            <GridColumn width={9}>
                                                                                <label> <strong> Officer Contact Number : </strong> {slbfeOffice.officerMobileNo}  </label>
                                                                                <hr />
                                                                            </GridColumn>

                                                                            <GridColumn width={9}>
                                                                                <label> <strong> Officer Number : </strong> {slbfeOffice.officerNumber}  </label>
                                                                                <hr />
                                                                            </GridColumn>

                                                                        </GridRow>]

                                                                    })
                                                                ) : (null)}
                                                            </Grid>
                                                        </div>
                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={16}>
                                                            <label> <strong> Departed On Their Own: </strong>
                                                                {profile.departedOnTheirOwn == "true" ? ("Yes") : ("No")}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    <GridRow>
                                                        <div className="HistoryMultipleContainer">
                                                            <Grid>
                                                                <GridRow>
                                                                    <GridColumn>
                                                                        <div class="card-header">
                                                                            <h3 class="mt-0 header-title" > ThirdParty Person Details </h3>
                                                                        </div>
                                                                    </GridColumn>
                                                                </GridRow>
                                                                {profile.thirdPartyDetails.length > 0 ? (
                                                                    profile.thirdPartyDetails.map((thirdPartyDetail, index) => {
                                                                        return [<GridRow>
                                                                            {index + 1 + ") "}
                                                                            <GridColumn width={6}>
                                                                                <label> <strong> Name : </strong> {thirdPartyDetail.thirdPartyPersonName}  </label>
                                                                                <hr />
                                                                            </GridColumn>
                                                                            <GridColumn width={9}>
                                                                                <label> <strong> NIC : </strong> {thirdPartyDetail.thirdPartyPersonNIC}  </label>
                                                                                <hr />
                                                                            </GridColumn>

                                                                            <GridColumn width={9}>
                                                                                <label> <strong> Mobile Number : </strong> {thirdPartyDetail.thirdPartyPersonMobileNum}  </label>
                                                                                <hr />
                                                                            </GridColumn>

                                                                        </GridRow>]

                                                                    })
                                                                ) : (null)}
                                                            </Grid>
                                                        </div>
                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={16}>
                                                            <label> <strong> Departure Location: </strong>
                                                                {profile.departureLocation}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Destination District: </strong>
                                                                {profile.destinationDistrict}
                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Destination Location: </strong>
                                                                {profile.destinationLocation}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Expected Departure Time: </strong>
                                                                {formatDateTime(profile.expectedDepartureTime)}
                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Expected Arrival Time: </strong>
                                                                {formatDateTime(profile.expectedArrivalTime)}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Actual Departure Time: </strong>
                                                                {formatDateTime(profile.actualDepartureTime)}
                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Actual Arrival Time: </strong>
                                                                {formatDateTime(profile.actualArrivalTime)}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Taxi Fare: </strong>
                                                                {profile.taxiFare}
                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Location Of Payment: </strong>
                                                                {profile.locationOfPayment}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                </Grid>

                                            </div>, <hr />]
                                        })
                                    ) : (<h3 style={{ color: "#6c757d" }}>Not any History Records</h3>)}

                                </div>

                            </div>


                            <div class="col-lg-12" >

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Destination Type  </label>
                                    <div class="col-sm-12">
                                        <Dropdown
                                            placeholder='Select Destination Type'
                                            fluid
                                            selection
                                            id='destinationType'
                                            name='destinationType'
                                            options={this.state.destinationTypes}
                                            value={this.state.destinationType}
                                            onChange={this.handleOnChange}
                                        />
                                    </div>
                                </div>

                                {this.state.destinationType != "Hospital" && this.state.destinationType != "" ? (
                                    <div>
                                        <div class="card">
                                            <div class="card-body" >

                                                <h5 class="mt-0 header-title" > Handover Person Details </h5>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Name </label>
                                                    <div class="col-sm-12">
                                                        <input id="nameOfHandoverPerson" name="nameOfHandoverPerson" value={this.state.nameOfHandoverPerson} onChange={this.handleChange} class="form-control" type="text" placeholder="Handover Person Name" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> NIC </label>
                                                    <div class="col-sm-12">
                                                        <input id="nicOfHandoverPerson" name="nicOfHandoverPerson" value={this.state.nicOfHandoverPerson} onChange={this.handleChange} class="form-control" type="text" placeholder="Handover Person NIC" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Mobile Number </label>
                                                    <div class="col-sm-12">
                                                        <input id="numberOfHandoverPerson" name="numberOfHandoverPerson" value={this.state.numberOfHandoverPerson} onChange={this.handleChange} class="form-control" type="number" placeholder="Handover Person Mobile Number" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Relationship </label>
                                                    <div class="col-sm-12">
                                                        <Dropdown
                                                            placeholder='Select Relationship'
                                                            fluid
                                                            selection
                                                            id='relationshipOfHandoverPerson'
                                                            name='relationshipOfHandoverPerson'
                                                            value={this.state.relationshipOfHandoverPerson}
                                                            options={this.state.relationships}
                                                            onChange={this.handleOnChange}
                                                        />
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                ) : (null)}

                                {this.state.destinationType == "Hospital" ? (
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Hospital Name </label>
                                        <div class="col-sm-12">
                                            <input id="hospitalName" name="hospitalName" value={this.state.hospitalName} onChange={this.handleChange} class="form-control" type="text" placeholder="Hospital Name" />
                                        </div>
                                    </div>
                                ) : (null)}

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Transport Method  </label>
                                    <div class="col-sm-12">
                                        <Dropdown
                                            placeholder='Select Transport Method'
                                            fluid
                                            selection
                                            id='transportMethod'
                                            name='transportMethod'
                                            options={this.state.transportMethods}
                                            value={this.state.transportMethod}
                                            onChange={this.handleOnChange}
                                        />
                                    </div>
                                </div>

                                {this.state.transportMethod == "Ambulence" || this.state.transportMethod == "Office Vehicle" ? (
                                    <div>
                                        <div class="card">
                                            <div class="card-body" >

                                                <div class="card-header" style={{ backgroundColor: "#e6e8eb" }}>
                                                    <h5 class="mt-0 header-title" > Accompanying SLBFE Officer details </h5>
                                                </div><br />
                                                <div >
                                                    {this._createSLBFEOfficerDetails()}
                                                    <div style={{ textAlign: "right" }}><button className="btn btn-secondary" onClick={this._addMoreSLBFEOfficer.bind(this)} type="button">
                                                        Add More <i className="fas fa-plus-circle" />
                                                    </button>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                ) : (null)}

                                <br />

                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="d-block mb-3"> Departed On his/her Own ?  </label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="departedOnTheirOwnYes" name="departedOnTheirOwn" checked={this.state.departedOnTheirOwn == "true" ? true : false} class="custom-control-input" value="true" onChange={this.handleRadioOnChange} />
                                            <label class="custom-control-label" for="departedOnTheirOwnYes">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="departedOnTheirOwnNo" name="departedOnTheirOwn" checked={this.state.departedOnTheirOwn == "false" ? true : false} class="custom-control-input" value="false" onChange={this.handleRadioOnChange} />
                                            <label class="custom-control-label" for="departedOnTheirOwnNo">No</label>
                                        </div>
                                    </div>
                                </div>

                                {this.state.departedOnTheirOwn == "false" ? (
                                    <div>

                                        <div class="card">
                                            <div class="card-body" >

                                                <div class="card-header" style={{ backgroundColor: "#e6e8eb" }}>
                                                    <h5 class="mt-0 header-title" > Accompanying third party information </h5>
                                                </div><br />
                                                <div >
                                                    {this._createThirdPartyDetails()}
                                                    <div style={{ textAlign: "right" }}><button className="btn btn-secondary" onClick={this._addMoreThirdPartyDetail.bind(this)} type="button">
                                                        Add More <i className="fas fa-plus-circle" />
                                                    </button>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                ) : (
                                        null
                                    )}

                                <br />

                                <div>

                                    <h5 class="mt-0 header-title" > Journey details </h5>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Departure location </label>
                                        <div class="col-sm-12">
                                            <input id="departureLocation" name="departureLocation" value={this.state.departureLocation} onChange={this.handleChange} class="form-control" type="text" placeholder="Departure location" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Destination District  </label>
                                        <div class="col-sm-12">
                                            <Dropdown
                                                placeholder='Select District'
                                                fluid
                                                selection
                                                search
                                                id='destinationDistrict'
                                                name='destinationDistrict'
                                                value={this.state.destinationDistrict}
                                                options={this.state.districts}
                                                onChange={this.handleOnChange}
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Destination location </label>
                                        <div class="col-sm-12">
                                            <input id="destinationLocation" name="destinationLocation" value={this.state.destinationLocation} onChange={this.handleChange} class="form-control" type="text" placeholder="Destination location" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Expected departure time  </label>
                                        <div class="col-sm-12">
                                            <DateTime
                                                className="DateTime"
                                                id="expectedDepartureTime"
                                                name="expectedDepartureTime"
                                                defaultValue={new Date()}
                                                dateFormat="YYYY-MM-DD"
                                                timeFormat="HH:mm"
                                                value={this.state.expectedDepartureTime}
                                                onChange={(e) => this.handleDate(e, "expectedDepartureTime")}
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Expected arrival time  </label>
                                        <div class="col-sm-12">
                                            <DateTime
                                                className="DateTime"
                                                id="expectedArrivalTime"
                                                name="expectedArrivalTime"
                                                defaultValue={new Date()}
                                                dateFormat="YYYY-MM-DD"
                                                timeFormat="HH:mm"
                                                value={this.state.expectedArrivalTime}
                                                onChange={(e) => this.handleDate(e, "expectedArrivalTime")}
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Actual departure time  </label>
                                        <div class="col-sm-12">
                                            <DateTime
                                                className="DateTime"
                                                id="actualDepartureTime"
                                                name="actualDepartureTime"
                                                defaultValue={new Date()}
                                                dateFormat="YYYY-MM-DD"
                                                timeFormat="HH:mm"
                                                value={this.state.actualDepartureTime}
                                                onChange={(e) => this.handleDate(e, "actualDepartureTime")}
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Actual arrival time  </label>
                                        <div class="col-sm-12">
                                            <DateTime
                                                className="DateTime"
                                                id="actualArrivalTime"
                                                name="actualArrivalTime"
                                                defaultValue={new Date()}
                                                dateFormat="YYYY-MM-DD"
                                                timeFormat="HH:mm"
                                                value={this.state.actualArrivalTime}
                                                onChange={(e) => this.handleDate(e, "actualArrivalTime")}
                                            />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Bus fare / Taxi fare  </label>
                                        <div class="col-sm-12">
                                            <input id="taxiFare" name="taxiFare" class="form-control" type="text" value={this.state.taxiFare} onChange={this.handleChange} placeholder="Bus fare / Taxi fare" />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Location of payment  </label>
                                        <div class="col-sm-12">
                                            <Dropdown
                                                placeholder='Select Location of payment'
                                                fluid
                                                selection
                                                id='locationOfPayment'
                                                name='locationOfPayment'
                                                options={this.state.locationsOfPayment}
                                                value={this.state.locationOfPayment}
                                                onChange={this.handleOnChange}
                                            />
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12" style={{ textAlign: "right" }}>
                                        <br /><button disabled={loading ? (true) : (false)} onClick={() => this.handleTransportProfileSubmit()} class="col-sm-2 btn btn-info " type="button" aria-haspopup="true" aria-expanded="false">
                                            {loading ? (
                                                <Loader
                                                    type="Oval"
                                                    color="#2A3F54"
                                                    height={15}
                                                    width={40}
                                                />
                                            ) : ("Submit")}
                                        </button>
                                    </div>
                                </div>

                                <div>

                                    {
                                        (formErrorStatus.status) ? (
                                            hasTransportProfileMessage ? (
                                                < div class="ui negative message">
                                                    <div class="header">
                                                        Not Submitted.
                                                    </div>
                                                    <p>{formErrorStatus.message}</p>
                                                </div>
                                            ) : (
                                                    null
                                                )

                                        ) : ((formSuccessState.status) ? (
                                            hasTransportProfileMessage ? (
                                                < div class="ui success message">
                                                    <div class="header">
                                                        Submitted successfully.
                                                    </div>
                                                    <p>{formSuccessState.message}</p>
                                                </ div>
                                            ) : (
                                                    null
                                                )

                                        ) : (''))
                                    }
                                </div>


                            </div>

                        </Accordion.Content>
                    </Accordion>
                </div>


                <hr />

            </div >

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(TransportActions)));

