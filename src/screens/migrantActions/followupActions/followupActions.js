import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../../actions/user-actions'
import { formatDate, formatDateTime } from '../../../middleware/index';


import { DateRange } from 'react-date-range';

//sementic ui
import { Dropdown, Grid, Icon, Message, Input, Accordion, GridRow, GridColumn } from 'semantic-ui-react';
import {
    DateTimeInput
} from 'semantic-ui-calendar-react';

//Spinner
import Loader from 'react-loader-spinner'

import DateTime from 'react-datetime';

//query
import {
    GET_SINGLE_FOLLOWUP_PROFILE, ADD_FOLLOWUP_PROFILE, EDIT_FOLLOWUP_PROFILE,GET_FOLLOWUP_ACTION_HISTORIES
} from '../../../queries/FollowupQueries';


const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class FollowupActions extends Component {
    constructor(props) {
        super(props);

        const query = new URLSearchParams(this.props.location.search);
        const id = query.get('id');

        var existMigrant = false;
        var migrantId = "";

        if (id != null || id != undefined) {
            migrantId = id;
            existMigrant = true;
        }

        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });


        this.state = {
            activeIndex: -1,
            migrantId: migrantId,
            existMigrant: existMigrant,
            loading: false,
            profilePriorities: [
                { key: "GENERAL", text: "General", value: "GENERAL", id: 'profilePriority' },
                { key: "MEDIUM", text: "Medium", value: "MEDIUM", id: 'profilePriority' },
                { key: "HIGH", text: "High", value: "HIGH", id: 'profilePriority' }
            ],
            profilePriority: "GENERAL",
            followupStatusList: [
                { text: "UNDERGOING", value: "UNDERGOING", id: 'followupStatus' },
                { text: "COMPLETED", value: "COMPLETED", id: 'followupStatus' }
            ],
            followupActions: [{ id: "", followupAction: "", followupDate: "", executeOfficer: "", followupStatus: "", followupNote: "" }],
            followupAction: "",
            followupDate: "",
            executeOfficer: "",
            followupStatus: "",
            followupNote: "",
            hasFollowupProfileMessage: false,
            existFollowupProfile: false,
            followupProfileId: "",

            followupActionDisplayHistories: [],
        }

    }

    componentDidMount() {
        if (this.state.existMigrant) {
            this.loadSingleFollowupProfile();
            this.loadFollowupActionHistories();
        }
    }

    loadFollowupActionHistories() {
        this.getFollowupActionHistories().then(result => {
            
            console.log(result)

            if (result.length > 0) {
                this.setState({
                    followupActionDisplayHistories: result
                });
            }
        })
    }

    getFollowupActionHistories = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_FOLLOWUP_ACTION_HISTORIES,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getFollowupActionHistories;
    }

    loadSingleFollowupProfile() {
        this.getSingleFollowupProfile().then(result => {

            console.log(result);

            if (result.length > 0) {

                var followupActions = [];

                result.map((action, i) => (
                    followupActions.push({ id: action.id, followupAction: action.followupAction, followupDate: action.followupDate, executeOfficer: action.executeOfficer, followupStatus: action.followupStatus, followupNote: action.followupNote })
                ))

                this.setState({
                    followupActions: followupActions,
                    existFollowupProfile: true
                });
            }
        })
    }

    getSingleFollowupProfile = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_SINGLE_FOLLOWUP_PROFILE,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleFollowupProfile;
    }

    handleFollowupDetailProfileSubmit() {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        this.setState({
            hasTravelProfileMessage: false, hasSheduledArrivalProfileMessage: false,
            hasMedicalProfileMessage: false, hasSexualRiskProfileMessage: false, hasMedicalTreatmentProfileMessage: false, hasCounselingProfileMessage: false,
            hasMedicalTreatmentPastProfileMessage: false, hasCounselingPastProfileMessage: false, hasMedicalReferrelInformationMessage: false,
            hasMedicalQualificationProfileMessage: false, hasAccomodationDetailProfileMessage: false, hasMealRecordProfileMessage: false, hasTransportProfileMessage: false, hasFollowupProfileMessage: false
        })

        const { migrantId } = this.state;

        if (migrantId == "" || migrantId == undefined || migrantId == null) {
            this.setState({ hasFollowupProfileMessage: true })
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "Please submit migrant general details before submit Followup Details."
            });
            return;
        } else {

            this.setState({ loading: true });
            this.props.setFormStatus({ status: false, title: '', message: '' });
            this.props.setSuccessStatus({ status: false, title: '', message: '' });

            if (this.state.existFollowupProfile) {

                this.state.followupActions.map((action, i) => (

                    this.editFollowupProfile(action).then(result => {
                        this.setState({ hasFollowupProfileMessage: true, loading: false })

                        this.props.setSuccessStatus({
                            status: true,
                            title: "",
                            message:
                                "Followup Details edited Successfully!"
                        });

                        this.loadSingleFollowupProfile();

                    }).catch(error => {
                        console.log(error);
                        if (error) {
                            this.setState({ loading: false, hasFollowupProfileMessage: true });
                            this.props.setFormStatus({
                                status: true,
                                title: "Oops!",
                                message:
                                    "There was an error while trying to edit Followup Details."

                            });
                        }
                    })

                ))

            } else {

                this.state.followupActions.map((action, i) => (
                    this.addFollowupProfile(action).then(result => {

                        this.setState({
                            loading: false,
                            form_state: 'default',
                            title: "",
                            message: "",
                            hasFollowupProfileMessage: true
                        });

                        this.props.setSuccessStatus({
                            status: true,
                            title: "",
                            message:
                                "Followup Details submitted Successfully!."
                        });
                        this.loadSingleFollowupProfile();

                    }).catch(error => {
                        console.log(error);
                        if (error) {
                            this.setState({ loading: false, hasFollowupProfileMessage: true });
                            this.props.setFormStatus({
                                status: true,
                                title: "Oops!",
                                message:
                                    "There was an error while trying to submit Followup Details."

                            });
                        }
                    })
                ));

            }
        }


    }

    addFollowupProfile = async (action) => {

        var followupDate = action.followupDate;
        var followupStatus = action.followupStatus;
        var followupNote = action.followupNote;
        var followupAction = action.followupAction;
        var executeOfficer = action.executeOfficer;

        const {
            migrantId } = this.state;

        const result = await this.props.client.mutate({
            mutation: ADD_FOLLOWUP_PROFILE,
            variables: {
                followupAction,
                followupDate,
                executeOfficer,
                followupStatus,
                followupNote,
                migrantId
            }
        });
        return result.data.addFollowupProfile;
    };

    editFollowupProfile = async (action) => {

        var id = action.id;
        var followupDate = action.followupDate;
        var followupStatus = action.followupStatus;
        var followupNote = action.followupNote;
        var followupAction = action.followupAction;
        var executeOfficer = action.executeOfficer;

        const result = await this.props.client.mutate({
            mutation: EDIT_FOLLOWUP_PROFILE,
            variables: {
                id,
                followupAction,
                followupDate,
                executeOfficer,
                followupStatus,
                followupNote,
            }
        });
        return result.data.editFollowupProfile;
    };

    _createFollowupAction() {
        return this.state.followupActions.map((el, i) => (
            <div key={i}>
                <br />
                {i + 1 + " ) "}
                {this.state.followupActions.length <= 1 ? (
                    null
                ) : (
                        <div style={{ textAlign: "right", cursor: "pointer" }}>
                            <i className="fas fa-times-circle fa-2x" onClick={this.removeFollowupAction.bind(this, i)}></i>
                        </div>
                    )}

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label">  Followup Date  </label>
                    <div class="col-sm-12">
                        <DateTime
                            className="DateTime"
                            id="followupDate"
                            name="followupDate"
                            defaultValue={formatDate(new Date())}
                            dateFormat="YYYY-MM-DD"
                            timeFormat="HH:mm"
                            value={el.followupDate || ''}
                            onChange={this.handleFollowupActionDateChange.bind(this, i)}
                        />
                    </div>
                </div>

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label"> Followup Action </label>
                    <div class="col-sm-12">
                        <textarea id="followupAction" name="followupAction" class="form-control" rows="3" placeholder="Followup Action" value={el.followupAction || ''} onChange={this.handleFollowupActionChange.bind(this, i)}></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label"> Executed Officer </label>
                    <div class="col-sm-12">
                        <input id="executeOfficer" name="executeOfficer" class="form-control" placeholder="Executed Officer" value={el.executeOfficer || ''} onChange={this.handleFollowupActionChange.bind(this, i)} />
                    </div>
                </div>

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label"> Followup Status </label>
                    <div class="col-sm-12">
                        <Dropdown
                            placeholder='Followup Status'
                            fluid
                            selection
                            id='followupStatus'
                            name='followupStatus'
                            options={this.state.followupStatusList}
                            value={el.followupStatus || ''}
                            onChange={this.handleFollowupOnChange.bind(this, i)}
                        />
                    </div>
                </div>

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label"> Followup Notes </label>
                    <div class="col-sm-12">
                        <textarea id="followupNote" name="followupNote" class="form-control" rows="3" placeholder="Followup Notes" value={el.followupNote || ''} onChange={this.handleFollowupActionChange.bind(this, i)}></textarea>
                    </div>
                </div>

                <hr />

            </div >

        ))

    }

    handleFollowupActionChange(i, e) {
        const { name, value } = e.target;
        let followupActions = [...this.state.followupActions];
        followupActions[i] = { ...followupActions[i], [name]: value };
        this.setState({ followupActions });
    }

    handleFollowupActionDateChange(i, e) {
        var name = "followupDate";
        var value = formatDate(e);
        let followupActions = [...this.state.followupActions];
        followupActions[i] = { ...followupActions[i], [name]: value };
        this.setState({ followupActions });
    }

    handleFollowupOnChange = (i, e, data) => {
        console.log(data.value);
        const name = data.id;
        const value = data.value;

        let followupActions = [...this.state.followupActions];
        followupActions[i] = { ...followupActions[i], [name]: value };
        this.setState({ followupActions });
    }

    _addMoreFollowupAction() {
        let followupActions = this.state.followupActions;
        followupActions.push({ id: "", followupAction: "", followupDate: "", executeOfficer: "", followupStatus: "", followupNote: "" });
        this.setState(followupActions)
    }

    removeFollowupAction(i) {
        let followupActions = this.state.followupActions;
        followupActions.splice(i, 1);
        this.setState({ followupActions });
    }

    handleDate = (date) => {
        console.log(date._d)
    };

    getValidDates = function (currentDate) {
        var yesterday = DateTime.moment().subtract(1, 'day');
        return currentDate.isAfter(yesterday);
    }

    handleClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = this.state
        const newIndex = activeIndex === index ? -1 : index

        this.setState({ activeIndex: newIndex })
    }

    render() {
        const { activeIndex, loading, hasFollowupProfileMessage } = this.state;
        const { formErrorStatus, formSuccessState } = this.props;


        console.log(this.state.followupActions)

        return (
            <div>
                <br />

                <div class="col-lg-10">
                    <Accordion fluid styled style={{ "box-shadow": "none" }}>
                        <Accordion.Title
                            className="card-header darkHeader"
                            active={activeIndex === 14}
                            index={14}
                            onClick={this.handleClick}
                        >
                            <div class="row">
                                
                                <div class="col-lg-6">
                                    <div class="headerAlign">
                                        <Icon name='dropdown headerRightAlign' />
                                        <div class="">
                                            <div className="accordianLeftTitleText">
                                                <h4 class="mt-0 header-title" > Followup Actions </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="headerRightAlign">
                                        <div className="accordianRightTitleText">
                                            {this.state.followupActionDisplayHistories.length > 0 ? (
                                                <h4> ( {this.state.followupActionDisplayHistories.length} )  previous {this.state.followupActionDisplayHistories.length == 1 ? ("record") : ("records")} available. </h4>
                                            ) : (null)}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </Accordion.Title>

                        <Accordion.Content active={activeIndex === 14}>
                            <br />
                            <div class="col-lg-12">
                                <div class="card HistoryContainer">
                                    <h3 className="HistoryHeading">Hisory Details</h3>

                                    {this.state.followupActionDisplayHistories.length > 0 ? (
                                        this.state.followupActionDisplayHistories.map((profile, index) => {
                                            return [<div className="HistoryGrayHeader"> {index + 1 + ") " + "Created Date : " + formatDate(profile.createdAt)} </div>
                                                , <hr />, <div className="HistoryContainerInner">
                                                <Grid>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Followup Action : </strong>{ profile.followupAction }

                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Followup Date : </strong> {profile.followupDate}  </label>
                                                        </GridColumn>

                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Executed Officer : </strong>{ profile.executeOfficer }

                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Followup Status : </strong> {profile.followupStatus}  </label>
                                                        </GridColumn>

                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={16}>
                                                            <label> <strong> Followup Notes : </strong>{ profile.followupNote }

                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>


                                                </Grid>

                                            </div>, <hr />]
                                        })
                                    ) : (<h3 style={{ color: "#6c757d" }}>Not any History Records</h3>)}

                                </div>

                            </div>


                            <div class="col-lg-12">

                                {this._createFollowupAction()}
                                <div style={{ textAlign: "right" }}><button className="btn btn-secondary" onClick={this._addMoreFollowupAction.bind(this)} type="button">
                                    Add More <i className="fas fa-plus-circle" />
                                </button>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12" style={{ textAlign: "right" }}>
                                        <br /><button disabled={loading ? (true) : (false)} onClick={() => this.handleFollowupDetailProfileSubmit()} class="col-sm-2 btn btn-info " type="button" aria-haspopup="true" aria-expanded="false">
                                            {loading ? (
                                                <Loader
                                                    type="Oval"
                                                    color="#2A3F54"
                                                    height={15}
                                                    width={40}
                                                />
                                            ) : ("Submit")}
                                        </button>
                                    </div>
                                </div>

                                <div>

                                    {
                                        (formErrorStatus.status) ? (
                                            hasFollowupProfileMessage ? (
                                                < div class="ui negative message">
                                                    <div class="header">
                                                        Not Submitted.
                                                    </div>
                                                    <p>{formErrorStatus.message}</p>
                                                </div>
                                            ) : (
                                                    null
                                                )

                                        ) : ((formSuccessState.status) ? (
                                            hasFollowupProfileMessage ? (
                                                < div class="ui success message">
                                                    <div class="header">
                                                        Submitted successfully.
                                                    </div>
                                                    <p>{formSuccessState.message}</p>
                                                </ div>
                                            ) : (
                                                    null
                                                )

                                        ) : (''))
                                    }
                                </div>

                            </div>

                        </Accordion.Content>
                    </Accordion>
                </div>

                <hr />

            </div>

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(FollowupActions)));

