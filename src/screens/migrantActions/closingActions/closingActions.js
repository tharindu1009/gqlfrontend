import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../../actions/user-actions'
import { formatDate, formatDateTime } from '../../../middleware/index';


import { DateRange } from 'react-date-range';

//sementic ui
import { Dropdown, Grid, Icon, Message, Input, Accordion, GridRow, GridColumn, Modal, Header, Button } from 'semantic-ui-react';
import {
    DateTimeInput
} from 'semantic-ui-calendar-react';

//Spinner
import Loader from 'react-loader-spinner'

import DateTime from 'react-datetime';

import {
    CLOSE_MIGRANT_OVERALL_PROFILE
} from '../../../queries/ClosingQueries';

import {
    GET_SINGLE_MIGRANT_GENERAL_PROFILE,
} from '../../../queries/OnboardingQueries';

import {
    GET_MIGRANT_BASIC_HISTORY_PRIFILES,
} from '../../../queries/MigrantQueries';



const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

var moment = require('moment');

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class ClosingActions extends Component {
    constructor(props) {
        super(props);

        const query = new URLSearchParams(this.props.location.search);
        const id = query.get('id');

        var existMigrant = false;
        var migrantId = "";

        if (id != null || id != undefined) {
            migrantId = id;
            existMigrant = true;
        }

        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });


        this.state = {
            activeIndex: -1,
            migrantId: migrantId,
            existMigrant: existMigrant,
            loading: false,
            isMigrantClosed: "false",
            closeupStatus: "false",
            closeupAction: "",
            hasClosingMessage: false,
            open: false,

            migrantHistories: [],
        }

    }

    show = () => this.setState({ open: true })
    close = () => this.setState({ open: false })

    componentDidMount() {

        if (this.state.existMigrant) {
            this.loadSingleMigrantGeneralProfile();
            this.loadMigrantBasicHistoryProfiles();
        }

    }

    loadMigrantBasicHistoryProfiles() {
        this.getMigrantBasicHistoryProfiles().then(result => {

            console.log(result)

            if (result.length > 0) {
                this.setState({
                    migrantHistories: result
                });
            }
        })
    }

    getMigrantBasicHistoryProfiles = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_MIGRANT_BASIC_HISTORY_PRIFILES,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getMigrantBasicHistoryProfiles;
    }

    loadSingleMigrantGeneralProfile() {
        this.getSingleMigrantGeneralProfile().then(result => {
            console.log(result)
            this.setState({
                isMigrantClosed: result.closeupStatus.toString(),
                closeupStatus: result.closeupStatus.toString(),
                closeupAction: result.closeupAction
            });
        })
    }

    getSingleMigrantGeneralProfile = async () => {
        const id = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_SINGLE_MIGRANT_GENERAL_PROFILE,
            variables: { id },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleMigrantGeneralProfile;
    }

    handleCloseupSubmit() {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        this.setState({
            hasTravelProfileMessage: false, hasSheduledArrivalProfileMessage: false,
            hasMedicalProfileMessage: false, hasSexualRiskProfileMessage: false, hasMedicalTreatmentProfileMessage: false, hasCounselingProfileMessage: false,
            hasMedicalTreatmentPastProfileMessage: false, hasCounselingPastProfileMessage: false, hasMedicalReferrelInformationMessage: false,
            hasMedicalQualificationProfileMessage: false, hasAccomodationDetailProfileMessage: false, hasMealRecordProfileMessage: false,
            hasTransportProfileMessage: false, hasFollowupProfileMessage: false, hasClosingMessage: false
        })

        const { migrantId } = this.state;

        if (migrantId == "" || migrantId == undefined || migrantId == null) {
            this.setState({ hasClosingMessage: true })
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "Please submit migrant general details before submit Closing Action."
            });
            return;
        } else {
            this.setState({ open: true });
        }


    }

    confirmCloseAction() {
        this.setState({ loading: true });
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        this.closeMigrantOverallProfile().then(result => {
            this.setState({ hasClosingMessage: true, loading: false, open: false })

            this.props.setSuccessStatus({
                status: true,
                title: "",
                message:
                    "Closing Action Executed Successfully!"
            });

            this.loadSingleMigrantGeneralProfile();

        }).catch(error => {
            console.log(error);
            if (error) {
                this.setState({ loading: false, hasClosingMessage: true, open: false });
                this.props.setFormStatus({
                    status: true,
                    title: "Oops!",
                    message:
                        "There was an error while trying to execute closing action."

                });
            }
        });
    }


    closeMigrantOverallProfile = async () => {
        const {
            closeupAction,
            migrantId
        } = this.state;

        var closeupStatus = JSON.parse(this.state.closeupStatus);

        const result = await this.props.client.mutate({
            mutation: CLOSE_MIGRANT_OVERALL_PROFILE,
            variables: {
                closeupStatus,
                closeupAction,
                migrantId,
            }
        });
        return result.data.closeMigrantOverallProfile;
    };


    handleClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = this.state
        const newIndex = activeIndex === index ? -1 : index

        this.setState({ activeIndex: newIndex })
    }

    handleRadioOnChange = (e) => {
        if (e.target.checked) {
            var name = e.target.name;
            var value = e.target.value;
            console.log(name)
            console.log(value)
            this.setState({ [name]: value });
        }
    }

    handleChange = (event) => {
        this.setState({ [event.target.id]: event.target.value });
    }


    render() {

        const { activeIndex, loading, hasClosingMessage } = this.state;

        const { formErrorStatus, formSuccessState } = this.props;

        return (
            <div>
                <br />


                <div class="col-lg-10">
                    <Accordion fluid styled style={{ "box-shadow": "none" }} >
                        <Accordion.Title
                            className="card-header darkHeader"
                            active={activeIndex === 15}
                            index={15}
                            onClick={this.handleClick}
                        >
                            <div class="row">


                                <div class="col-lg-12">
                                    <div class="headerAlign">
                                        <Icon name='dropdown headerRightAlign' />
                                        <div class="">
                                            <div className="accordianLeftTitleText">
                                                <h4 class="mt-0 header-title" > Closing Action </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </Accordion.Title>

                        <Accordion.Content active>
                            <br />

                            <div class="col-lg-12">
                                <div class="card HistoryContainer">
                                    <h3 className="HistoryHeading">Hisory Details</h3>

                                    {this.state.migrantHistories.length > 0 ? (
                                        this.state.migrantHistories.map((profile, index) => {
                                            return [<div className="HistoryGrayHeader"> {index + 1 + ") " + "Created Date : " + formatDate(profile.createdAt)} </div>
                                                , <hr />, <div className="HistoryContainerInner">
                                                <Grid>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> File Closing Status : </strong> {profile.closeupStatus == true ? ("Yes") : ("No")}  </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Closing Action : </strong> {profile.closeupAction}  </label>
                                                        </GridColumn>

                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Recieved To Sahana Piyasa : </strong>  
                                                            {profile.recievedToSahanaPiyasa != undefined && formatDate(new Date(profile.recievedToSahanaPiyasa)) != "1970-01-01" ? (
                                                                    formatDateTime(profile.recievedToSahanaPiyasa)
                                                                ) : ('')
                                                                }
                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Released From Sahana Piyasa : </strong>

                                                                {profile.releasedFromSahanaPiyasa != undefined && formatDate(new Date(profile.releasedFromSahanaPiyasa)) != "1970-01-01" ? (
                                                                    formatDateTime(profile.releasedFromSahanaPiyasa)
                                                                ) : ('')
                                                                }
                                                            </label>
                                                        </GridColumn>

                                                    </GridRow>

                                                </Grid>

                                            </div>, <hr />]
                                        })
                                    ) : (<h3 style={{ color: "#6c757d" }}>Not any History Records</h3>)}

                                </div>

                            </div>


                            <div class="col-lg-12" >

                                <Modal className="ModalDisplay" size="mini" dimmer="blurring" open={this.state.open} onClose={this.close}>
                                    <Modal.Content>
                                        <Modal.Description>
                                            <Header> Closing File </Header>
                                            <p>
                                                Do you want to confirm this closing action?
                                            </p>

                                        </Modal.Description>
                                    </Modal.Content>
                                    <Modal.Actions>
                                        <Button basic color='blue' onClick={() => this.confirmCloseAction()}>
                                            {loading ? (
                                                <Loader
                                                    type="Oval"
                                                    color="#2A3F54"
                                                    height={15}
                                                    width={40}
                                                />
                                            ) : ("Yes")}
                                        </Button>
                                        <Button basic onClick={this.close}>
                                            No
                                        </Button>
                                    </Modal.Actions>
                                </Modal>


                                <div >

                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label class="d-block mb-3"> Close File </label>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="closeupStatusYes" name="closeupStatus" checked={this.state.closeupStatus == "true" ? true : false} value="true" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                                <label class="custom-control-label" for="closeupStatusYes">Yes</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input
                                                    // disabled={this.state.isMigrantClosed == "true" ? true : false} 
                                                    type="radio" id="closeupStatusNo" name="closeupStatus" checked={this.state.closeupStatus == "false" ? true : false} value="false" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                                <label class="custom-control-label" for="closeupStatusNo">No</label>
                                            </div>
                                        </div>
                                    </div>


                                    {
                                        this.state.closeupStatus == "true" ? (
                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-sm-12 col-form-label"> Close File Comment  </label>
                                                <div class="col-sm-12">
                                                    <input id="closeupAction" name="closeupAction" value={this.state.closeupAction} onChange={this.handleChange} class="form-control" type="text" placeholder="Comment" />
                                                </div>
                                            </div>
                                        ) : (
                                                null
                                            )
                                    }

                                    <div class="form-group row">
                                        <div class="col-sm-12" style={{ textAlign: "right" }}>
                                            <br /><button disabled={loading ? (true) : (false)} onClick={() => this.handleCloseupSubmit()} class="col-sm-2 btn btn-info " type="button" aria-haspopup="true" aria-expanded="false">
                                                {loading ? (
                                                    <Loader
                                                        type="Oval"
                                                        color="#2A3F54"
                                                        height={15}
                                                        width={40}
                                                    />
                                                ) : ("Submit")}
                                            </button>
                                        </div>
                                    </div>

                                    <div>

                                        {
                                            (formErrorStatus.status) ? (
                                                hasClosingMessage ? (
                                                    < div class="ui negative message">
                                                        <div class="header">
                                                            Not Executed.
                                                    </div>
                                                        <p>{formErrorStatus.message}</p>
                                                    </div>
                                                ) : (
                                                        null
                                                    )

                                            ) : ((formSuccessState.status) ? (
                                                hasClosingMessage ? (
                                                    < div class="ui success message">
                                                        <div class="header">
                                                            Executed successfully.
                                                    </div>
                                                        <p>{formSuccessState.message}</p>
                                                    </ div>
                                                ) : (
                                                        null
                                                    )

                                            ) : (''))
                                        }
                                    </div>



                                </div>

                            </div>

                        </Accordion.Content>

                    </Accordion>


                </div>

                <hr />
            </div>

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(ClosingActions)));

