import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../../actions/user-actions'
import { formatDate, formatDateTime } from '../../../middleware/index';


import { DateRange } from 'react-date-range';

//sementic ui
import { Dropdown, Grid, Icon, Message, Input, Accordion, GridRow, GridColumn } from 'semantic-ui-react';
import {
    DateTimeInput
} from 'semantic-ui-calendar-react';

//Spinner
import Loader from 'react-loader-spinner'

import DateTime from 'react-datetime';

//query
import {
    GET_SINGLE_ACCOMODATION_DETAIL_PROFILE, ADD_ACCOMODATION_DETAIL_PROFILE, EDIT_ACCOMODATION_DETAIL_PROFILE,
    GET_SINGLE_MEAL_RECORD_PROFILE, ADD_MEAL_RECORD_PROFILE, EDIT_MEAL_RECORD_PROFILE, GET_ACCOMODATION_DETAIL_HISTORY_PROFILES,
    GET_MEAL_RECORD_HISTORY_PROFILES,GET_SINGLE_EXPENSES_PROFILE,GET_EXPENSES_RECORD_HISTORY_PROFILES,ADD_EXPENSES_RECORD_PROFILE,EDIT_EXPENSES_RECORD_PROFILE
} from '../../../queries/AccomodationQueries';

import {
    GET_ALL_ACCOMMODATIONS,GET_ALL_EXPENSE_TYPES
} from '../../../queries/CommonQueries';

const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

var moment = require('moment');

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class AccommodationActions extends Component {
    constructor(props) {
        super(props);

        const query = new URLSearchParams(this.props.location.search);
        const id = query.get('id');

        var existMigrant = false;
        var migrantId = "";

        if (id != null || id != undefined) {
            migrantId = id;
            existMigrant = true;
        }

        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });


        this.state = {
            activeIndex: -1,
            migrantId: migrantId,
            existMigrant: existMigrant,
            loading: false,
            accommodationTypes: [
            ],
            meals: [
                { text: "Breakfast", value: "Breakfast", id: 'meal' },
                { text: "Lunch", value: "Lunch", id: 'meal' },
                { text: "Dinner", value: "Dinner", id: 'meal' },
                { text: "Snack", value: "Snack", id: 'meal' },
            ],
            accomodationDetails: [
                { accommodationType: "", dateRange: {}, notes: "" }
            ],
            mealRecords: [
                { meal: "", cost: "", date: "" }
            ],

            hasAccomodationDetailProfileMessage: false,
            existAccomodationDetailProfile: false,
            accomodationDetailProfileId: "",

            hasMealRecordProfileMessage: false,
            existMealRecordProfile: false,
            mealRecordProfileId: "",

            rangePicker: {
                startDate: moment(),
                endDate: moment().add(7, "days")
            },

            accomodationDetailDisplayHistoryProfiles: [],
            mealRecordDisplayHistoryProfiles: [],
            expensesRecordDisplayHistoryProfiles: [],

            expenses:[
                { expenseTypeId: "", expenseType: "", expense: "" }
            ],
            expenseTypes:[],
            expenseType:"",

            hasExpensesRecordProfileMessage: false,
            existExpensesRecordProfile: false,
            expensesRecordProfileId: "",
        }

    }

    componentDidMount() {

        if (this.state.existMigrant) {
            this.loadSingleAccomodationDetailProfile();
            this.loadSingleMealRecordProfile();
            this.loadAccomodationDetailHistoryProfiles();
            this.loadMealRecordHistoryProfiles();

            this.loadSingleExpensesProfile();
            this.loadExpensesRecordHistoryProfiles();
        }

        this.loadAllAccommodations();
        console.log("--------------")
        this.loadAllExpenseTypes();
        console.log("22222222222222")
    }

    loadExpensesRecordHistoryProfiles() {
        this.getExpensesRecordHistoryProfiles().then(result => {

            if (result.length > 0) {
                this.setState({
                    expensesRecordDisplayHistoryProfiles: result
                });
            }
        })
    }

    getExpensesRecordHistoryProfiles = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_EXPENSES_RECORD_HISTORY_PROFILES,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getExpensesRecordHistoryProfiles;
    }

    loadSingleExpensesProfile() {
        this.getSingleExpensesProfile().then(result => {

            console.log(result)

            if (result.length > 0) {
                this.setState({
                    expensesRecordProfileId: result[0].id,
                    expenses: result[0].expenses,
                    existExpensesRecordProfile: true
                });
            }
        })
    }

    getSingleExpensesProfile = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_SINGLE_EXPENSES_PROFILE,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleExpensesProfile;
    }

    loadAllExpenseTypes() {

        console.log("55555555555555")
        
        this.getAllExpenseTypes().then(result => {
            console.log("6666666666666666")
            console.log(result)

            var expenseTypes = [];

            var expenseTypes = result.map((expenseType, index) => {
                return { key: expenseType.id,text: expenseType.expenceType, value: expenseType.expenceType, id: 'expenseType' }
            })

            this.setState({ expenseTypes: expenseTypes })

            
        }).catch(error => {
            console.log(error);
           
        });

    }

    getAllExpenseTypes = async () => {
        console.log("6666666666666666")
        const result = await this.props.client.query({
            query: GET_ALL_EXPENSE_TYPES,
            fetchPolicy: 'network-only'
        });
        console.log(result)
        return result.data.getAllExpenseTypes;
    }

    loadMealRecordHistoryProfiles() {
        this.getMealRecordHistoryProfiles().then(result => {

            if (result.length > 0) {
                this.setState({
                    mealRecordDisplayHistoryProfiles: result
                });
            }
        })
    }

    getMealRecordHistoryProfiles = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_MEAL_RECORD_HISTORY_PROFILES,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getMealRecordHistoryProfiles;
    }

    loadAccomodationDetailHistoryProfiles() {
        this.getAccomodationDetailHistoryProfiles().then(result => {

            if (result.length > 0) {
                this.setState({
                    accomodationDetailDisplayHistoryProfiles: result
                });
            }
        })
    }

    getAccomodationDetailHistoryProfiles = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_ACCOMODATION_DETAIL_HISTORY_PROFILES,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getAccomodationDetailHistoryProfiles;
    }

    loadAllAccommodations() {
        this.getAllAccommodations().then(result => {

            var accommodationTypes = [];

            var accommodationTypes = result.data.getAllAccommodations.map((accommodation, index) => {
                return { text: accommodation.accommodation, value: accommodation.accommodation, id: 'accommodationType' }
            })

            this.setState({ accommodationTypes: accommodationTypes })
        });
    }

    getAllAccommodations = async () => {
        const result = await this.props.client.query({
            query: GET_ALL_ACCOMMODATIONS,
            fetchPolicy: 'network-only'
        });
        return result;
    };

    loadSingleAccomodationDetailProfile() {
        this.getSingleAccomodationDetailProfile().then(result => {

            if (result.length > 0) {
                this.setState({
                    accomodationDetailProfileId: result[0].id,
                    accomodationDetails: result[0].accomodationDetails,
                    existAccomodationDetailProfile: true
                });
            }
        })
    }

    getSingleAccomodationDetailProfile = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_SINGLE_ACCOMODATION_DETAIL_PROFILE,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleAccomodationDetailProfile;
    }

    loadSingleMealRecordProfile() {
        this.getSingleMealRecordProfile().then(result => {

            console.log(result)

            if (result.length > 0) {
                this.setState({
                    mealRecordProfileId: result[0].id,
                    mealRecords: result[0].mealRecords,
                    existMealRecordProfile: true
                });
            }
        })
    }

    getSingleMealRecordProfile = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_SINGLE_MEAL_RECORD_PROFILE,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleMealRecordProfile;
    }

    handleAccomodationDetailProfileSubmit() {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        this.setState({
            hasTravelProfileMessage: false, hasSheduledArrivalProfileMessage: false,
            hasMedicalProfileMessage: false, hasSexualRiskProfileMessage: false, hasMedicalTreatmentProfileMessage: false, hasCounselingProfileMessage: false,
            hasMedicalTreatmentPastProfileMessage: false, hasCounselingPastProfileMessage: false, hasMedicalReferrelInformationMessage: false,
            hasMedicalQualificationProfileMessage: false, hasAccomodationDetailProfileMessage: false, hasMealRecordProfileMessage: false, hasTransportProfileMessage: false, hasFollowupProfileMessage: false,
            hasExpensesRecordProfileMessage: false
        })

        const { migrantId } = this.state;

        if (migrantId == "" || migrantId == undefined || migrantId == null) {
            this.setState({ hasAccomodationDetailProfileMessage: true })
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "Please submit migrant general details before submit Accomodation Details."
            });
            return;
        } else {

            this.setState({ loading: true });
            this.props.setFormStatus({ status: false, title: '', message: '' });
            this.props.setSuccessStatus({ status: false, title: '', message: '' });

            if (this.state.existAccomodationDetailProfile) {

                this.editAccomodationDetailProfile().then(result => {
                    this.setState({ hasAccomodationDetailProfileMessage: true, loading: false })

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Accomodation Details edited Successfully!"
                    });

                    this.loadSingleAccomodationDetailProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasAccomodationDetailProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to edit Accomodation Details."

                        });
                    }
                });

            } else {
                this.addAccomodationDetailProfile().then(result => {

                    this.setState({
                        loading: false,
                        form_state: 'default',
                        title: "",
                        message: "",
                        hasAccomodationDetailProfileMessage: true
                    });

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Accomodation Details submitted Successfully!."
                    });
                    this.loadSingleAccomodationDetailProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasAccomodationDetailProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to submit Accomodation Details."

                        });
                    }
                });
            }
        }


    }

    addAccomodationDetailProfile = async () => {
        const {
            accomodationDetails,
            migrantId } = this.state;

        const result = await this.props.client.mutate({
            mutation: ADD_ACCOMODATION_DETAIL_PROFILE,
            variables: {
                accomodationDetails,
                migrantId
            }
        });
        return result.data.addAccomodationDetailProfile;
    };

    editAccomodationDetailProfile = async () => {
        const {
            accomodationDetails,
        } = this.state;

        const id = this.state.accomodationDetailProfileId;

        const result = await this.props.client.mutate({
            mutation: EDIT_ACCOMODATION_DETAIL_PROFILE,
            variables: {
                id,
                accomodationDetails,
            }
        });
        return result.data.editAccomodationDetailProfile;
    };


    handleMealRecordProfileSubmit() {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        this.setState({
            hasTravelProfileMessage: false, hasSheduledArrivalProfileMessage: false,
            hasMedicalProfileMessage: false, hasSexualRiskProfileMessage: false, hasMedicalTreatmentProfileMessage: false, hasCounselingProfileMessage: false,
            hasMedicalTreatmentPastProfileMessage: false, hasCounselingPastProfileMessage: false, hasMedicalReferrelInformationMessage: false,
            hasMedicalQualificationProfileMessage: false, hasAccomodationDetailProfileMessage: false, hasMealRecordProfileMessage: false, hasTransportProfileMessage: false, hasFollowupProfileMessage: false,
            hasExpensesRecordProfileMessage:false
        })


        const { migrantId } = this.state;

        if (migrantId == "" || migrantId == undefined || migrantId == null) {
            this.setState({ hasMealRecordProfileMessage: true })
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "Please submit migrant general details before submit Meal Profile Details."
            });
            return;
        } else {

            this.setState({ loading: true });
            this.props.setFormStatus({ status: false, title: '', message: '' });
            this.props.setSuccessStatus({ status: false, title: '', message: '' });

            if (this.state.existMealRecordProfile) {

                this.editMealRecordProfile().then(result => {
                    this.setState({ hasMealRecordProfileMessage: true, loading: false })

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Meal Profile Details edited Successfully!"
                    });

                    this.loadSingleMealRecordProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasMealRecordProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to edit Meal Profile Details."

                        });
                    }
                });

            } else {
                this.addMealRecordProfile().then(result => {

                    this.setState({
                        loading: false,
                        form_state: 'default',
                        title: "",
                        message: "",
                        hasMealRecordProfileMessage: true
                    });

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Meal Profile Details submitted Successfully!."
                    });
                    this.loadSingleMealRecordProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasMealRecordProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to submit Meal Profile Details."

                        });
                    }
                });
            }
        }


    }

    addMealRecordProfile = async () => {
        const {
            mealRecords,
            migrantId } = this.state;

        const result = await this.props.client.mutate({
            mutation: ADD_MEAL_RECORD_PROFILE,
            variables: {
                mealRecords,
                migrantId
            }
        });
        return result.data.addMealRecordProfile;
    };

    editMealRecordProfile = async () => {
        const {
            mealRecords,
        } = this.state;

        const id = this.state.mealRecordProfileId;

        const result = await this.props.client.mutate({
            mutation: EDIT_MEAL_RECORD_PROFILE,
            variables: {
                id,
                mealRecords,
            }
        });
        return result.data.editMealRecordProfile;
    };

    _createAccomodationDetails() {
        return this.state.accomodationDetails.map((el, i) => (
            <div key={i}>
                <br />
                {i + 1 + " ) "}

                {this.state.accomodationDetails.length <= 1 ? (
                    null
                ) : (
                        <div style={{ textAlign: "right", cursor: "pointer" }}>
                            <i className="fas fa-times-circle fa-2x" onClick={this.removeAccomodationDetail.bind(this, i)}></i>
                        </div>
                    )}

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label"> Type of Accommodation provided  </label>
                    <div class="col-sm-12">
                        <Dropdown
                            placeholder='Accommodation Type'
                            fluid
                            selection
                            id='accommodationType'
                            name='accommodationType'
                            options={this.state.accommodationTypes}
                            value={el.accommodationType || ''}
                            onChange={this.handleAccomodationDetailOnChange.bind(this, i)}
                        />
                    </div>
                </div>

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label"> Date Range </label>
                    <div class="col-sm-12">
                        {/* <div >
                            <input type="text" class="form-control" name="start" />
                            <input type="text" class="form-control" name="end" />
                        </div>
                        <DateRange
                            showSelectionPreview={true}
                            moveRangeOnFirstSelection={false}
                            format="YYYY-MM-DD"
                            onChange={this.handleAccomodationDetailDateChange.bind(this, i)}
                        /> */}

                        <div class="input-daterange input-group" id="date-range">
                            <input
                                type="text"
                                class="form-control"
                                name="start"
                                readOnly
                                value={el.dateRange.startDate ? formatDate(el.dateRange.startDate) : ("")}
                            />

                            <input
                                type="text"
                                class="form-control"
                                name="end"
                                readOnly
                                value={el.dateRange.endDate ? formatDate(el.dateRange.endDate) : ("")}
                            />
                        </div>

                        <div>
                            <DateRange
                                className={'PreviewArea'}
                                showSelectionPreview={true}
                                moveRangeOnFirstSelection={false}
                                // ranges= {[{"startDate":new Date(),"endDate":new Date("2020-01-25"),key: 'selection'}]}
                                startDate={moment(el.dateRange.startDate)}
                                endDate={moment(el.dateRange.endDate)}
                                disableDaysBeforeToday={true}
                                direction="horizontal"
                                format="YYYY-MM-DD"
                                onChange={this.handleAccomodationDetailDateChange.bind(this, i)}
                            />
                        </div>
                    </div>

                </div>

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label">  Notes </label>
                    <div class="col-sm-12">
                        <input id="notes" name="notes" class="form-control" type="text" value={el.notes || ''} placeholder="Notes" onChange={this.handleAccomodationDetailChange.bind(this, i)} />
                    </div>
                </div>

                <hr />

            </div >

        ))

    }

    handleSelect(date) {
        console.log(date); // Momentjs object
    }


    _createMealsRecords() {
        return this.state.mealRecords.map((el, i) => (
            <div key={i}>
                <br />
                {i + 1 + " ) "}
                {this.state.mealRecords.length <= 1 ? (
                    null
                ) : (
                        <div style={{ textAlign: "right", cursor: "pointer" }}>
                            <i className="fas fa-times-circle fa-2x" onClick={this.removeMealsRecord.bind(this, i)}></i>
                        </div>
                    )}

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label"> Type of Meal </label>
                    <div class="col-sm-12">
                        <Dropdown
                            placeholder='Meal Type'
                            fluid
                            selection
                            id='meal'
                            name='meal'
                            options={this.state.meals}
                            value={el.meal || ''}
                            onChange={this.handleMealsRecordOnChange.bind(this, i)}
                        />
                    </div>
                </div>

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label">  Cost </label>
                    <div class="col-sm-12">
                        <input id="cost" name="cost" class="form-control" type="text" value={el.cost || ''} placeholder="Cost" onChange={this.handleMealsRecordChange.bind(this, i)} />
                    </div>
                </div>

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label">  Date </label>
                    <div class="col-sm-12">

                        <DateTime
                            className="DateTime"
                            id="date"
                            name="date"
                            defaultValue={new Date()}
                            dateFormat="YYYY-MM-DD"
                            timeFormat="HH:mm"
                            value={el.date || ''}
                            onChange={this.handleMealsRecordDateChange.bind(this, i)}
                        />

                    </div>
                </div>

                <hr />

            </div >

        ))

    }

    handleAccomodationDetailChange(i, e) {
        const { name, value } = e.target;
        let accomodationDetails = [...this.state.accomodationDetails];
        accomodationDetails[i] = { ...accomodationDetails[i], [name]: value };
        this.setState({ accomodationDetails });
    }

    handleAccomodationDetailOnChange = (i, e, data) => {
        console.log(data.value);
        const name = data.id;
        const value = data.value;

        let accomodationDetails = [...this.state.accomodationDetails];
        accomodationDetails[i] = { ...accomodationDetails[i], [name]: value };
        this.setState({ accomodationDetails });
    }

    handleAccomodationDetailDateChange(i, e) {
        var name = "dateRange";

        console.log(e)

        var startDate = e.startDate._d;
        var endDate = e.endDate._d;

        var dateRange = { startDate: startDate, endDate: endDate };

        let accomodationDetails = [...this.state.accomodationDetails];
        accomodationDetails[i] = { ...accomodationDetails[i], [name]: dateRange };
        this.setState({ accomodationDetails });
    }



    handleMealsRecordChange(i, e) {
        const { name, value } = e.target;
        let mealRecords = [...this.state.mealRecords];
        mealRecords[i] = { ...mealRecords[i], [name]: value };
        this.setState({ mealRecords });
    }

    handleMealsRecordDateChange(i, e) {
        var name = "date";
        var value = formatDateTime(e);
        let mealRecords = [...this.state.mealRecords];
        mealRecords[i] = { ...mealRecords[i], [name]: value };
        this.setState({ mealRecords });
    }

    handleMealsRecordOnChange = (i, e, data) => {
        console.log(data.value);
        const name = data.id;
        const value = data.value;

        let mealRecords = [...this.state.mealRecords];
        mealRecords[i] = { ...mealRecords[i], [name]: value };
        this.setState({ mealRecords });
    }

    _addMoreAccomodationDetail() {
        let accomodationDetails = this.state.accomodationDetails;
        accomodationDetails.push({ accommodationType: "", dateRange: {}, notes: "" });
        this.setState(accomodationDetails)
    }

    _addMoreMealsRecord() {
        let mealRecords = this.state.mealRecords;
        mealRecords.push({ meal: "", cost: "", date: "" });
        this.setState(mealRecords)
    }

    removeAccomodationDetail(i) {
        let accomodationDetails = this.state.accomodationDetails;
        accomodationDetails.splice(i, 1);
        this.setState({ accomodationDetails });
    }

    removeMealsRecord(i) {
        let mealRecords = this.state.mealRecords;
        mealRecords.splice(i, 1);
        this.setState({ mealRecords });
    }

    handleClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = this.state
        const newIndex = activeIndex === index ? -1 : index

        this.setState({ activeIndex: newIndex })
    }


    _createExpensesRecords() {
        return this.state.expenses.map((el, i) => (
            <div key={i}>
                <br />
                {i + 1 + " ) "}
                {this.state.expenses.length <= 1 ? (
                    null
                ) : (
                        <div style={{ textAlign: "right", cursor: "pointer" }}>
                            <i className="fas fa-times-circle fa-2x" onClick={this.removeExpensesRecord.bind(this, i)}></i>
                        </div>
                    )}

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label"> Type of Expense </label>
                    <div class="col-sm-12">
                        <Dropdown
                            placeholder='Expense Type'
                            fluid
                            selection
                            id='expenseType'
                            name='expenseType'
                            options={this.state.expenseTypes}
                            value={el.expenseType || ''}
                            onChange={this.handleExpenseRecordOnChange.bind(this, i)}
                        />
                    </div>
                </div>

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label">  Expense </label>
                    <div class="col-sm-12">
                        <input id="expense" name="expense" class="form-control" type="text" value={el.expense || ''} placeholder="Expense" onChange={this.handleExpenseRecordChange.bind(this, i)} />
                    </div>
                </div>

                <hr />

            </div >

        ))

    }

    handleExpenseRecordChange(i, e) {
        const { name, value } = e.target;
        let expenses = [...this.state.expenses];
        expenses[i] = { ...expenses[i], [name]: value };
        this.setState({ expenses });
    }

    handleExpenseRecordOnChange = (i, e, data) => {
    
        const { value } = data;
        const { key, text } = data.options.find(o => o.value === value);

        let expenses = [...this.state.expenses];
        expenses[i] = { ...expenses[i], "expenseTypeId": key, "expenseType": text };
        this.setState({ expenses });
    }

    _addMoreExpensesRecord() {
        let expenses = this.state.expenses;
        expenses.push({ expenseTypeId: "", expenseType: "", expense: "" });
        this.setState(expenses)
    }

    removeExpensesRecord(i) {
        let expenses = this.state.expenses;
        expenses.splice(i, 1);
        this.setState({ expenses });
    }

    handleExpenseRecordProfileSubmit() {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        this.setState({
            hasTravelProfileMessage: false, hasSheduledArrivalProfileMessage: false,
            hasMedicalProfileMessage: false, hasSexualRiskProfileMessage: false, hasMedicalTreatmentProfileMessage: false, hasCounselingProfileMessage: false,
            hasMedicalTreatmentPastProfileMessage: false, hasCounselingPastProfileMessage: false, hasMedicalReferrelInformationMessage: false,
            hasMedicalQualificationProfileMessage: false, hasAccomodationDetailProfileMessage: false, hasMealRecordProfileMessage: false, hasTransportProfileMessage: false, hasFollowupProfileMessage: false,
            hasExpensesRecordProfileMessage:false
        })


        const { migrantId } = this.state;

        if (migrantId == "" || migrantId == undefined || migrantId == null) {
            this.setState({ hasExpensesRecordProfileMessage: true })
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "Please submit migrant general details before submit Expenses Profile Details."
            });
            return;
        } else {

            this.setState({ loading: true });
            this.props.setFormStatus({ status: false, title: '', message: '' });
            this.props.setSuccessStatus({ status: false, title: '', message: '' });

            if (this.state.existExpensesRecordProfile) {

                this.editExpensesRecordProfile().then(result => {
                    this.setState({ hasExpensesRecordProfileMessage: true, loading: false })

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Expenses Profile Details edited Successfully!"
                    });

                    this.loadSingleExpensesProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasExpensesRecordProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to edit Expense Profile Details."

                        });
                    }
                });

            } else {
                this.addExpensesRecordProfile().then(result => {

                    this.setState({
                        loading: false,
                        form_state: 'default',
                        title: "",
                        message: "",
                        hasExpensesRecordProfileMessage: true
                    });

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Expense Profile Details submitted Successfully!."
                    });
                    this.loadSingleExpensesProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasExpensesRecordProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to submit Expense Profile Details."

                        });
                    }
                });
            }
        }


    }

    addExpensesRecordProfile = async () => {
        const {
            expenses,
            migrantId } = this.state;

        const result = await this.props.client.mutate({
            mutation: ADD_EXPENSES_RECORD_PROFILE,
            variables: {
                expenses,
                migrantId
            }
        });
        return result.data.addExpensesRecordProfile;
    };

    editExpensesRecordProfile = async () => {
        const {
            expenses,
        } = this.state;

        const id = this.state.expensesRecordProfileId;

        const result = await this.props.client.mutate({
            mutation: EDIT_EXPENSES_RECORD_PROFILE,
            variables: {
                id,
                expenses,
            }
        });
        return result.data.editExpensesRecordProfile;
    };

    addMealRecordProfile = async () => {
        const {
            mealRecords,
            migrantId } = this.state;

        const result = await this.props.client.mutate({
            mutation: ADD_MEAL_RECORD_PROFILE,
            variables: {
                mealRecords,
                migrantId
            }
        });
        return result.data.addMealRecordProfile;
    };

    editMealRecordProfile = async () => {
        const {
            mealRecords,
        } = this.state;

        const id = this.state.mealRecordProfileId;

        const result = await this.props.client.mutate({
            mutation: EDIT_MEAL_RECORD_PROFILE,
            variables: {
                id,
                mealRecords,
            }
        });
        return result.data.editMealRecordProfile;
    };

    render() {

        const { activeIndex, loading, hasAccomodationDetailProfileMessage, hasMealRecordProfileMessage ,hasExpensesRecordProfileMessage} = this.state;

        const { formErrorStatus, formSuccessState } = this.props;

        console.log(this.state.expenses)

        return (
            <div>
                <br />

                <div class="col-lg-10">
                    <Accordion fluid styled style={{ "box-shadow": "none" }}>
                        <Accordion.Title
                            className="card-header darkHeader"
                            active={activeIndex === 11}
                            index={11}
                            onClick={this.handleClick}
                        >
                            <div class="row">

                                <div class="col-lg-6">
                                    <div class="headerAlign">
                                        <Icon name='dropdown headerRightAlign' />
                                        <div class="">
                                            <div className="accordianLeftTitleText">
                                                <h4 class="mt-0 header-title" > Accomodation Info </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="headerRightAlign">
                                        <div className="accordianRightTitleText">
                                            {this.state.accomodationDetailDisplayHistoryProfiles.length > 0 ? (
                                                <h4> ( {this.state.accomodationDetailDisplayHistoryProfiles.length} )  previous {this.state.accomodationDetailDisplayHistoryProfiles.length == 1 ? ("record") : ("records")} available. </h4>
                                            ) : (null)}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </Accordion.Title>

                        <Accordion.Content active={activeIndex === 11}>
                            <br />

                            <div class="col-lg-12">
                                <div class="card HistoryContainer">
                                    <h3 className="HistoryHeading">Hisory Details</h3>

                                    {this.state.accomodationDetailDisplayHistoryProfiles.length > 0 ? (
                                        this.state.accomodationDetailDisplayHistoryProfiles.map((profile, index) => {
                                            return [<div className="HistoryGrayHeader"> {index + 1 + ") " + "Created Date : " + formatDate(profile.createdAt)} </div>
                                                , <hr />, <div className="HistoryContainerInner">
                                                <Grid>

                                                    <GridRow>
                                                        <div className="HistoryMultipleContainer">
                                                            <Grid>
                                                                <GridRow>
                                                                    <h3> Accommodation Details </h3>
                                                                </GridRow>
                                                                {profile.accomodationDetails.length > 0 ? (
                                                                    profile.accomodationDetails.map((accomodationDetail, index) => {
                                                                        return [<GridRow>
                                                                            {index + 1 + ") "}
                                                                            <GridColumn width={4}>
                                                                                <label> <strong> Accommodation Type : </strong> {accomodationDetail.accommodationType}  </label>
                                                                                
                                                                            </GridColumn>
                                                                            <GridColumn width={6}>
                                                                                <label> <strong> Date Range : </strong><br /><br />

                                                                                    {
                                                                                        accomodationDetail.dateRange != undefined ? (
                                                                                            <div>    <strong> Start Date :</strong> {accomodationDetail.dateRange.startDate != undefined ? (formatDate(accomodationDetail.dateRange.startDate)) : (null)} <br />
                                                                                                <strong> End Date :</strong> {accomodationDetail.dateRange.endDate != undefined ? (formatDate(accomodationDetail.dateRange.endDate)) : (null)}
                                                                                            </div>
                                                                                        ) : (null)
                                                                                    }

                                                                                </label>
                                                                                
                                                                            </GridColumn>
                                                                        </GridRow>,

                                                                        <GridRow>
                                                                            <GridColumn width={9}>
                                                                                <label> <strong> Other Notes : </strong> {accomodationDetail.notes}  </label>
                                                                                <hr />
                                                                            </GridColumn>

                                                                        </GridRow>]

                                                                    })
                                                                ) : (null)}
                                                            </Grid>
                                                        </div>
                                                    </GridRow>


                                                </Grid>

                                            </div>]
                                        })
                                    ) : (<h3 style={{ color: "#6c757d" }}>Not any History Records</h3>)}

                                </div>

                            </div>


                            <div class="col-lg-12" >

                                <div >
                                    {this._createAccomodationDetails()}
                                    <div style={{ textAlign: "right" }}><button className="btn btn-secondary" onClick={this._addMoreAccomodationDetail.bind(this)} type="button">
                                        Add More Record <i className="fas fa-plus-circle" />
                                    </button>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-12" style={{ textAlign: "right" }}>
                                            <br /><button disabled={loading ? (true) : (false)} onClick={() => this.handleAccomodationDetailProfileSubmit()} class="col-sm-2 btn btn-info " type="button" aria-haspopup="true" aria-expanded="false">
                                                {loading ? (
                                                    <Loader
                                                        type="Oval"
                                                        color="#2A3F54"
                                                        height={15}
                                                        width={40}
                                                    />
                                                ) : ("Submit")}
                                            </button>
                                        </div>
                                    </div>

                                    <div>

                                        {
                                            (formErrorStatus.status) ? (
                                                hasAccomodationDetailProfileMessage ? (
                                                    < div class="ui negative message">
                                                        <div class="header">
                                                            Not Submitted.
                                                    </div>
                                                        <p>{formErrorStatus.message}</p>
                                                    </div>
                                                ) : (
                                                        null
                                                    )

                                            ) : ((formSuccessState.status) ? (
                                                hasAccomodationDetailProfileMessage ? (
                                                    < div class="ui success message">
                                                        <div class="header">
                                                            Submitted successfully.
                                                    </div>
                                                        <p>{formSuccessState.message}</p>
                                                    </ div>
                                                ) : (
                                                        null
                                                    )

                                            ) : (''))
                                        }
                                    </div>



                                </div>

                            </div>

                        </Accordion.Content>

                        <Accordion.Title
                            className="card-header darkHeader"
                            active={activeIndex === 12}
                            index={12}
                            onClick={this.handleClick}
                        >
                            <div class="row">

                                <div class="col-lg-6">
                                    <div class="headerAlign">
                                        <Icon name='dropdown headerRightAlign' />
                                        <div class="">
                                            <div className="accordianLeftTitleText">
                                                <h4 class="mt-0 header-title" > Meals Info </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="headerRightAlign">
                                        <div className="accordianRightTitleText">
                                            {this.state.mealRecordDisplayHistoryProfiles.length > 0 ? (
                                                <h4> ( {this.state.mealRecordDisplayHistoryProfiles.length} )  previous {this.state.mealRecordDisplayHistoryProfiles.length == 1 ? ("record") : ("records")} available. </h4>
                                            ) : (null)}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </Accordion.Title>

                        <Accordion.Content active={activeIndex === 12}>
                            <br />
                            <div class="col-lg-12">
                                <div class="card HistoryContainer">
                                    <h3 className="HistoryHeading">Hisory Details</h3>

                                    {this.state.mealRecordDisplayHistoryProfiles.length > 0 ? (
                                        this.state.mealRecordDisplayHistoryProfiles.map((profile, index) => {
                                            return [<div className="HistoryGrayHeader"> {index + 1 + ") " + "Created Date : " + formatDate(profile.createdAt)} </div>
                                                , <hr />, <div className="HistoryContainerInner">
                                                <Grid>

                                                    <GridRow>
                                                        <div className="HistoryMultipleContainer">
                                                            <Grid>
                                                                <GridRow>
                                                                    <h3> Meal Details </h3>
                                                                </GridRow>
                                                                {profile.mealRecords.length > 0 ? (
                                                                    profile.mealRecords.map((mealRecord, index) => {
                                                                        return [<GridRow>
                                                                            {index + 1 + ") "}
                                                                            <GridColumn width={6}>
                                                                                <label> <strong> Meal : </strong> {mealRecord.meal}  </label>
                                                                                <hr />
                                                                            </GridColumn>
                                                                            <GridColumn width={6}>
                                                                                <label> <strong> Cost : </strong> {mealRecord.cost}  </label>
                                                                                <hr />
                                                                            </GridColumn>
                                                                            <GridColumn width={16}>
                                                                                <label> <strong> Date : </strong> {formatDateTime(mealRecord.date)}  </label>
                                                                                <hr />
                                                                            </GridColumn>

                                                                        </GridRow>]

                                                                    })
                                                                ) : (null)}
                                                            </Grid>
                                                        </div>
                                                    </GridRow>


                                                </Grid>

                                            </div>, <hr />]
                                        })
                                    ) : (<h3 style={{ color: "#6c757d" }}>Not any History Records</h3>)}

                                </div>

                            </div>


                            <div class="col-lg-12" >

                                <div >
                                    {this._createMealsRecords()}
                                    <div style={{ textAlign: "right" }}><button className="btn btn-secondary" onClick={this._addMoreMealsRecord.bind(this)} type="button">
                                        Add More Record <i className="fas fa-plus-circle" />
                                    </button>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-12" style={{ textAlign: "right" }}>
                                            <br /><button disabled={loading ? (true) : (false)} onClick={() => this.handleMealRecordProfileSubmit()} class="col-sm-2 btn btn-info " type="button" aria-haspopup="true" aria-expanded="false">
                                                {loading ? (
                                                    <Loader
                                                        type="Oval"
                                                        color="#2A3F54"
                                                        height={15}
                                                        width={40}
                                                    />
                                                ) : ("Submit")}
                                            </button>
                                        </div>
                                    </div>

                                    <div>

                                        {
                                            (formErrorStatus.status) ? (
                                                hasMealRecordProfileMessage ? (
                                                    < div class="ui negative message">
                                                        <div class="header">
                                                            Not Submitted.
                                                    </div>
                                                        <p>{formErrorStatus.message}</p>
                                                    </div>
                                                ) : (
                                                        null
                                                    )

                                            ) : ((formSuccessState.status) ? (
                                                hasMealRecordProfileMessage ? (
                                                    < div class="ui success message">
                                                        <div class="header">
                                                            Submitted successfully.
                                                        </div>
                                                        <p>{formSuccessState.message}</p>
                                                    </ div>
                                                ) : (
                                                        null
                                                    )

                                            ) : (''))
                                        }
                                    </div>
                                </div>

                            </div>

                        </Accordion.Content>
                    
                    
                        <Accordion.Title
                            className="card-header darkHeader"
                            active={activeIndex === 20}
                            index={20}
                            onClick={this.handleClick}
                        >
                            <div class="row">

                                <div class="col-lg-6">
                                    <div class="headerAlign">
                                        <Icon name='dropdown headerRightAlign' />
                                        <div class="">
                                            <div className="accordianLeftTitleText">
                                                <h4 class="mt-0 header-title" > Expenses Info </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="headerRightAlign">
                                        <div className="accordianRightTitleText">
                                            {this.state.expensesRecordDisplayHistoryProfiles.length > 0 ? (
                                                <h4> ( {this.state.expensesRecordDisplayHistoryProfiles.length} )  previous {this.state.expensesRecordDisplayHistoryProfiles.length == 1 ? ("record") : ("records")} available. </h4>
                                            ) : (null)}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </Accordion.Title>

                        <Accordion.Content active={activeIndex === 20}>
                            <br />
                            <div class="col-lg-12">
                                <div class="card HistoryContainer">
                                    <h3 className="HistoryHeading">Hisory Details</h3>

                                    {this.state.expensesRecordDisplayHistoryProfiles.length > 0 ? (
                                        this.state.expensesRecordDisplayHistoryProfiles.map((profile, index) => {
                                            return [<div className="HistoryGrayHeader"> {index + 1 + ") " + "Created Date : " + formatDate(profile.createdAt)} </div>
                                                , <hr />, <div className="HistoryContainerInner">
                                                <Grid>

                                                    <GridRow>
                                                        <div className="HistoryMultipleContainer">
                                                            <Grid>
                                                                <GridRow>
                                                                    <h3> Expenses Details </h3>
                                                                </GridRow>
                                                                {profile.expenses.length > 0 ? (
                                                                    profile.expenses.map((expense, index) => {
                                                                        return [<GridRow>
                                                                            {index + 1 + ") "}
                                                                            <GridColumn width={6}>
                                                                                <label> <strong> Expense Type : </strong> {expense.expenseType}  </label>
                                                                                <hr />
                                                                            </GridColumn>
                                                                            <GridColumn width={6}>
                                                                                <label> <strong> Expense : </strong> {expense.expense}  </label>
                                                                                <hr />
                                                                            </GridColumn>
                                                                           

                                                                        </GridRow>]

                                                                    })
                                                                ) : (null)}
                                                            </Grid>
                                                        </div>
                                                    </GridRow>


                                                </Grid>

                                            </div>, <hr />]
                                        })
                                    ) : (<h3 style={{ color: "#6c757d" }}>Not any History Records</h3>)}

                                </div>

                            </div>


                            <div class="col-lg-12" >

                                <div >
                                    {this._createExpensesRecords()}
                                    <div style={{ textAlign: "right" }}><button className="btn btn-secondary" onClick={this._addMoreExpensesRecord.bind(this)} type="button">
                                        Add More <i className="fas fa-plus-circle" />
                                    </button>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-sm-12" style={{ textAlign: "right" }}>
                                            <br /><button disabled={loading ? (true) : (false)} onClick={() => this.handleExpenseRecordProfileSubmit()} class="col-sm-2 btn btn-info " type="button" aria-haspopup="true" aria-expanded="false">
                                                {loading ? (
                                                    <Loader
                                                        type="Oval"
                                                        color="#2A3F54"
                                                        height={15}
                                                        width={40}
                                                    />
                                                ) : ("Submit")}
                                            </button>
                                        </div>
                                    </div>

                                    <div>

                                        {
                                            (formErrorStatus.status) ? (
                                                hasExpensesRecordProfileMessage ? (
                                                    < div class="ui negative message">
                                                        <div class="header">
                                                            Not Submitted.
                                                    </div>
                                                        <p>{formErrorStatus.message}</p>
                                                    </div>
                                                ) : (
                                                        null
                                                    )

                                            ) : ((formSuccessState.status) ? (
                                                hasExpensesRecordProfileMessage ? (
                                                    < div class="ui success message">
                                                        <div class="header">
                                                            Submitted successfully.
                                                        </div>
                                                        <p>{formSuccessState.message}</p>
                                                    </ div>
                                                ) : (
                                                        null
                                                    )

                                            ) : (''))
                                        }
                                    </div>
                                </div>

                            </div>

                        </Accordion.Content>
                    
                    </Accordion>
                </div>

                <hr />
            </div>

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(AccommodationActions)));

