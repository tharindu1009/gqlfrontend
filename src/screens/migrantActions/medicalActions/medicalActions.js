import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../../actions/user-actions'
import { formatDate, formatDateTime } from '../../../middleware/index';

//sementic ui
import { Dropdown, Grid, Icon, Message, Input, Accordion, GridRow, GridColumn, Checkbox, Modal, Button, Header } from 'semantic-ui-react';
import {
    DateTimeInput
} from 'semantic-ui-calendar-react';

//Spinner
import Loader from 'react-loader-spinner'

import DateTime from 'react-datetime';

//query
import {
    GET_SINGLE_BASIC_MEDICAL_PROFILE, ADD_BASIC_MEDICAL_PROFILE, EDIT_BASIC_MEDICAL_PROFILE,
    GET_SINGLE_SEXUAL_RISK_PROFILE, ADD_SEXUAL_RISK_PROFILE, EDIT_SEXUAL_RISK_PROFILE,
    GET_SINGLE_MEDICAL_TREATMENT_PROFILE, ADD_MEDICAL_TREATMENT_PROFILE, EDIT_MEDICAL_TREATMENT_PROFILE,
    GET_SINGLE_COUNSELING_PROFILE, ADD_COUNSELING_PROFILE, EDIT_COUNSELING_PROFILE,
    GET_SINGLE_MEDICAL_PAST_TREATMENT_PROFILE, ADD_MEDICAL_PAST_TREATMENT_PROFILE, EDIT_MEDICAL_PAST_TREATMENT_PROFILE,
    GET_SINGLE_COUNSILING_PAST_PROFILE, ADD_COUNSELING_PAST_PROFILE, EDIT_COUNSELING_PAST_PROFILE,
    GET_SINGLE_MEDICAL_REFERREL_INFORMATION, ADD_MEDICAL_REFERREL_INFORMATION, EDIT_MEDICAL_REFERREL_INFORMATION,
    GET_SINGLE_MEDICAL_QUALIFICATION_PROFILE, ADD_MEDICAL_QUALIFICATION_PROFILE, EDIT_MEDICAL_QUALIFICATION_PROFILE,
    GET_BASIC_MEDICAL_HISTORY_PROFILES, GET_SEXUAL_RISK_HISTORY_PROFILES, GET_MEDICAL_PAST_TREATMENT_PROFILES,
    GET_COUNSELLING_PAST_RECORD_PROFILES, GET_MEDICAL_REFERREL_HISTORY_INFORMATIONS
} from '../../../queries/MedicalQueries';

import { CLOSE_MIGRANT_MEDICAL_PROFILE } from '../../../queries/ClosingQueries';

import {
    GET_ALL_SICKNESSES, GET_ALL_MEDICAL_TREATMENT_CATEGORIES
} from '../../../queries/CommonQueries';

const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class MedicalActions extends Component {
    constructor(props) {
        super(props);

        const query = new URLSearchParams(this.props.location.search);
        const id = query.get('id');

        console.log(id)

        var existMigrant = false;
        var migrantId = "";

        if (id != null || id != undefined) {
            migrantId = id;
            existMigrant = true;
        }

        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        this.state = {
            activeIndex: -1,
            migrantId: migrantId,
            existMigrant: existMigrant,
            loading: false,
            relationships: [
                { text: "Father", value: "Father", id: 'relationship' },
                { text: "Mother", value: "Mother", id: 'relationship' },
                { text: "Child", value: "Child", id: 'relationship' },
                { text: "Neighbour", value: "Neighbour", id: 'relationship' },
                { text: "Cusin", value: "Cusin", id: 'relationship' },
                { text: "Husband", value: "Husband", id: 'relationship' },
                { text: "Wife", value: "Wife", id: 'relationship' },
                { text: "Other", value: "Other", id: 'relationship' },
            ],
            ethnicities: [
                { text: "Sinhala", value: "Sinhala", id: 'ethnicity' },
                { text: "Tamil", value: "Tamil", id: 'ethnicity' },
                { text: "Muslim", value: "Muslim", id: 'ethnicity' },
                { text: "Other", value: "Other", id: 'ethnicity' },
            ],
            sexualOrientations: [
                { text: "Heterosexual", value: "Heterosexual", id: 'sexualOrientation' },
                { text: "Homosexual", value: "Homosexual", id: 'sexualOrientation' },
                { text: "Bisexual", value: "Bisexual", id: 'sexualOrientation' },
            ],
            familyHistories: [
                { text: "Diabetes", value: "Diabetes", id: 'familyHistory' },
                { text: "Cancer", value: "Cancer", id: 'familyHistory' },
                { text: "High Blood Pressure", value: "High Blood Pressure", id: 'familyHistory' },
                { text: "Alzheimers", value: "Alzheimers", id: 'familyHistory' },
                { text: "Heart Attack", value: "Heart Attack", id: 'familyHistory' },
                { text: "Mental Illness", value: "Mental Illness", id: 'familyHistory' },
                { text: "Tuberculosis", value: "Tuberculosis", id: 'familyHistory' },
                { text: "Epilepsy", value: "Epilepsy", id: 'familyHistory' },
            ],
            medicalTreatmentRecords: [{ physicianName: "", physicianAddress: "", physicianPhone: "", prescribionRecords: [{ prescribedMedication: "", dosage: "", frequency: "", dateStarted: "", comments: "" }] }],
            medicalPastTreatmentRecords: [{ physicianName: "", physicianAddress: "", physicianPhone: "", prescribionRecords: [{ prescribedMedication: "", dosage: "", frequency: "", dateStarted: "", comments: "" }] }],
            counselingInformations: [],
            qualificationStatuses: [
                { text: "Recommended", value: "RECOMMENDED", id: 'qualificationStatus' },
                { text: "Not Recommended", value: "NOTRECOMMENDED", id: 'qualificationStatus' }
            ],
            referrelInformations: [{ referalName: "", email: "", phoneNumber: "", comments: "" }],

            medicalTreatmentCategories: [],
            ethnicity: [],
            haveChildren: "",
            numOfChildren: 0,
            sexualOrientation: "",
            familyHistory: [],
            otherMajorConditions: "",
            isCurrentlyTreatedMedicalCondition: "",
            currentMedicalCondition: "",
            allergies: "",
            isMigrantMedicalClosed: "false",
            closeupStatus: "false",
            closeupAction: "",
            emergencyContact: [{ emergencyContactPersonName: "", emergencyContactPersonNum: "", relationship: "" }],
            hasMedicalProfileMessage: false,
            existMedicalProfile: false,
            medicalProfileId: "",

            hasSexualRiskProfileMessage: false,
            existSexualRiskProfile: false,
            sexualRiskProfileId: "",
            isSmoke: "",
            timesOfSmoke: "",
            isUseAlchohol: "",
            capacityOfAlchohol: "",
            isPartnerUseDrugs: "",
            capacityOfDrugs: "",
            isEverInjectedDrugs: "",
            isLikeHelpDrugProblems: "",
            isLikeDiscussPhysicalAbuse: "",
            hasThreatnedRelationship: "",

            hasMedicalTreatmentProfileMessage: false,
            existMedicalTreatmentProfile: false,
            medicalTreatmentProfileId: "",
            natureOfIllness: "",
            medicalTreatmentCategory: "",
            physicianName: "",
            physicianAddress: "",
            physicianPhone: "",
            isPrescribedMedicine: "",
            illnessList: [],
            hasCounselingProfileMessage: false,
            existCounselingProfile: false,
            counselingProfileId: "",
            counsellerInformations: [{ counsellerName: "", counsellerAddress: "", counsellerPhone: "", }],
            counsellingSymptoms: [],
            suicidalThoughts: "",
            suicidalThoughtsDetail: "",
            mentalHealthConditions: "",
            mentalHealthConditionsDetail: "",
            mentalHealthMedications: "",
            mentalHealthMedicationsDetail: "",
            abuses: "",
            abusesDetail: "",

            pastNatureOfIllness: "",
            medicalPastTreatmentCategory: "",
            hasMedicalTreatmentPastProfileMessage: false,
            existMedicalTreatmentPastProfile: false,
            medicalTreatmentPastProfileId: "",

            hasCounselingPastProfileMessage: false,
            existCounselingPastProfile: false,
            counselingPastProfileId: "",
            hadPreviousCounselling: "",
            lastDateOfCounselling: "",
            counsellingDetail: "",

            hasMedicalReferrelInformationMessage: false,
            existMedicalReferrelInformation: false,
            medicalReferrelInformationId: "",
            isReferrel: "",
            referrelType: "",
            hospitalName: "",
            hospitalType: "",
            hospitalTypes: [
                { text: "Government", value: "GOVERNMENT", id: 'hospitalType' },
                { text: "Private", value: "PRIVATE", id: 'hospitalType' }
            ],

            hasMedicalQualificationProfileMessage: false,
            existMedicalQualificationProfile: false,
            medicalQualificationProfileId: "",
            qualificationStatus: "",
            notes: "",

            basicMedicalHistoryProfiles: [],
            sexualRiskHistoryProfiles: [],
            medicalTreatmentHistoryProfiles: [],
            medicalTreatmentDisplayHistoryProfiles: [],
            counselingPastRecordDisplayProfiles: [],
            medicalReferrelHistoryDisplayInformations: [],

            hasClosingMessage: false,
            open: false,
        }

    }

    show = () => this.setState({ open: true })
    close = () => this.setState({ open: false })

    componentDidMount() {
        if (this.state.existMigrant) {
            this.loadSingleBasicMedicalProfile();
            this.loadSingleSexualRiskProfile();

            this.loadSingleMedicalTreatmentProfile();
            this.loadSingleCounselingProfile();

            // this.loadSingleMedicalPastTreatmentProfile();
            this.loadSingleCounselingPastProfile();

            this.loadSingleMedicalReferrelInformation();
            this.loadSingleMedicalQualificationProfile();

            this.loadBasicMedicalHistoryProfiles();
            this.loadSexualRiskHistoryProfiles();

            this.loadMedicalPastTreatmentProfiles();
            this.loadCounselingPastRecordProfiles();
            this.loadMedicalReferrelHistoryInformations();

        }

        this.loadAllSicknesses();
        this.loadAllMedicalTreatmentCategories();

    }

    handleCloseupSubmit() {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        this.setState({
            hasTravelProfileMessage: false, hasSheduledArrivalProfileMessage: false,
            hasMedicalProfileMessage: false, hasSexualRiskProfileMessage: false, hasMedicalTreatmentProfileMessage: false, hasCounselingProfileMessage: false,
            hasMedicalTreatmentPastProfileMessage: false, hasCounselingPastProfileMessage: false, hasMedicalReferrelInformationMessage: false,
            hasMedicalQualificationProfileMessage: false, hasAccomodationDetailProfileMessage: false, hasMealRecordProfileMessage: false,
            hasTransportProfileMessage: false, hasFollowupProfileMessage: false, hasClosingMessage: false
        })

        const { migrantId } = this.state;

        if (migrantId == "" || migrantId == undefined || migrantId == null) {
            this.setState({ hasClosingMessage: true })
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "Please submit migrant general details before submit Closing Action."
            });
            return;
        } else {
            this.setState({ open: true });
        }


    }

    confirmCloseAction() {
        this.setState({ loading: true });
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        this.closeMigrantMedicalProfile().then(result => {
            this.setState({ hasClosingMessage: true, loading: false, open: false })

            this.props.setSuccessStatus({
                status: true,
                title: "",
                message:
                    "Closing Action Executed Successfully!"
            });

            this.loadSingleBasicMedicalProfile();

        }).catch(error => {
            console.log(error);
            if (error) {
                this.setState({ loading: false, hasClosingMessage: true, open: false });
                this.props.setFormStatus({
                    status: true,
                    title: "Oops!",
                    message:
                        "There was an error while trying to execute closing action."

                });
            }
        });
    }


    closeMigrantMedicalProfile = async () => {
        const {
            closeupAction,
            migrantId
        } = this.state;

        var closeupStatus = JSON.parse(this.state.closeupStatus);


        const result = await this.props.client.mutate({
            mutation: CLOSE_MIGRANT_MEDICAL_PROFILE,
            variables: {
                closeupStatus,
                closeupAction,
                migrantId,
            }
        });
        return result.data.closeMigrantMedicalProfile;
    };

    loadMedicalReferrelHistoryInformations() {
        this.getMedicalReferrelHistoryInformations().then(result => {

            console.log(result)

            if (result.length > 0) {
                this.setState({
                    medicalReferrelHistoryDisplayInformations: result,
                });
            }
        })
    }

    getMedicalReferrelHistoryInformations = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_MEDICAL_REFERREL_HISTORY_INFORMATIONS,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getMedicalReferrelHistoryInformations;
    }


    loadCounselingPastRecordProfiles() {
        this.getCounselingPastRecordProfiles().then(result => {

            console.log(result)

            if (result.length > 0) {
                this.setState({
                    counselingPastRecordDisplayProfiles: result,
                });
            }
        })
    }

    getCounselingPastRecordProfiles = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_COUNSELLING_PAST_RECORD_PROFILES,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getCounselingPastRecordProfiles;
    }


    loadMedicalPastTreatmentProfiles() {
        this.getMedicalPastTreatmentProfiles().then(result => {

            console.log(result)
            var medicalTreatmentDisplayHistoryProfiles = [];

            if (result.length > 0) {

                medicalTreatmentDisplayHistoryProfiles = result.map((detail, index) => {
                    return { pastNatureOfIllness: detail.pastNatureOfIllness, medicalPastTreatmentCategory: detail.medicalPastTreatmentCategory, medicalPastTreatmentRecords: detail.medicalPastTreatmentRecords, createdAt: detail.createdAt }
                })

                this.setState({ medicalTreatmentDisplayHistoryProfiles: medicalTreatmentDisplayHistoryProfiles });

            }
        })
    }

    getMedicalPastTreatmentProfiles = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_MEDICAL_PAST_TREATMENT_PROFILES,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getMedicalPastTreatmentProfiles;
    }

    loadBasicMedicalHistoryProfiles() {
        this.getBasicMedicalHistoryProfiles().then(result => {

            if (result.length > 0) {
                this.setState({
                    basicMedicalHistoryProfiles: result,
                });
            }
        })
    }

    getBasicMedicalHistoryProfiles = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_BASIC_MEDICAL_HISTORY_PROFILES,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getBasicMedicalHistoryProfiles;
    }

    loadSexualRiskHistoryProfiles() {
        this.getSexualRiskHistoryProfiles().then(result => {

            if (result.length > 0) {
                this.setState({
                    sexualRiskHistoryProfiles: result,
                });
            }
        })
    }

    getSexualRiskHistoryProfiles = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_SEXUAL_RISK_HISTORY_PROFILES,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getSexualRiskHistoryProfiles;
    }


    loadAllMedicalTreatmentCategories() {
        this.getAllMedicalTreatmentCategories().then(result => {

            var medicalTreatmentCategories = [];

            var medicalTreatmentCategories = result.data.getAllMedicalTreatmentCategories.map((treat, index) => {
                return { text: treat.medicalTreatmentCategory, value: treat.medicalTreatmentCategory, id: 'medicalTreatmentCategory' }
            })

            this.setState({ medicalTreatmentCategories: medicalTreatmentCategories })
        });
    }

    getAllMedicalTreatmentCategories = async () => {
        const result = await this.props.client.query({
            query: GET_ALL_MEDICAL_TREATMENT_CATEGORIES,
            fetchPolicy: 'network-only'
        });
        return result;
    };

    loadAllSicknesses() {
        this.getAllSicknesses().then(result => {

            var illnessList = [];

            var illnessList = result.data.getAllSicknesses.map((sick, index) => {
                return { text: sick.sicknessName, value: sick.sicknessName, id: 'natureOfIllness' }
            })

            this.setState({ illnessList: illnessList })
        });
    }

    getAllSicknesses = async () => {
        const result = await this.props.client.query({
            query: GET_ALL_SICKNESSES,
            fetchPolicy: 'network-only'
        });
        return result;
    };


    handleChange = (event) => {
        this.setState({ [event.target.id]: event.target.value });
    }

    handleOnChange = (e, data) => {
        this.setState({ [data.id]: data.value });
    }

    loadSingleBasicMedicalProfile() {
        this.getSingleBasicMedicalProfile().then(result => {

            console.log(result)

            if (result.length > 0) {
                this.setState({
                    medicalProfileId: result[0].id,
                    ethnicity: result[0].ethnicity,
                    haveChildren: result[0].haveChildren,
                    numOfChildren: result[0].numOfChildren,
                    sexualOrientation: result[0].sexualOrientation,
                    familyHistory: result[0].familyHistory,
                    otherMajorConditions: result[0].otherMajorConditions,
                    isCurrentlyTreatedMedicalCondition: result[0].isCurrentlyTreatedMedicalCondition,
                    currentMedicalCondition: result[0].currentMedicalCondition,
                    allergies: result[0].allergies,
                    isMigrantMedicalClosed: result[0].closeupStatus.toString(),
                    closeupStatus: result[0].closeupStatus.toString(),
                    closeupAction: result[0].closeupAction,
                    emergencyContact: result[0].emergencyContact,
                    existMedicalProfile: true
                });
            }
        })
    }

    getSingleBasicMedicalProfile = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_SINGLE_BASIC_MEDICAL_PROFILE,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleBasicMedicalProfile;
    }

    loadSingleSexualRiskProfile() {
        this.getSingleSexualRiskProfile().then(result => {

            console.log(result)

            if (result.length > 0) {
                this.setState({
                    sexualRiskProfileId: result[0].id,
                    isSmoke: result[0].isSmoke,
                    timesOfSmoke: result[0].timesOfSmoke,
                    isUseAlchohol: result[0].isUseAlchohol,
                    capacityOfAlchohol: result[0].capacityOfAlchohol,
                    isPartnerUseDrugs: result[0].isPartnerUseDrugs,
                    capacityOfDrugs: result[0].capacityOfDrugs,
                    isEverInjectedDrugs: result[0].isEverInjectedDrugs,
                    isLikeHelpDrugProblems: result[0].isLikeHelpDrugProblems,
                    isLikeDiscussPhysicalAbuse: result[0].isLikeDiscussPhysicalAbuse,
                    hasThreatnedRelationship: result[0].hasThreatnedRelationship,
                    existSexualRiskProfile: true
                });
            }
        })
    }

    getSingleSexualRiskProfile = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_SINGLE_SEXUAL_RISK_PROFILE,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleSexualRiskProfile;
    }

    loadSingleMedicalTreatmentProfile() {
        this.getSingleMedicalTreatmentProfile().then(result => {

            console.log(result)

            if (result.length > 0) {
                this.setState({
                    medicalTreatmentProfileId: result[0].id,
                    natureOfIllness: result[0].natureOfIllness,
                    medicalTreatmentCategory: result[0].medicalTreatmentCategory,
                    medicalTreatmentRecords: result[0].medicalTreatmentRecords,
                    existMedicalTreatmentProfile: true
                });
            }
        })
    }

    getSingleMedicalTreatmentProfile = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_SINGLE_MEDICAL_TREATMENT_PROFILE,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleMedicalTreatmentProfile;
    }

    loadSingleCounselingProfile() {
        this.getSingleCounselingProfile().then(result => {

            console.log(result)

            if (result.length > 0) {
                this.setState({
                    counselingProfileId: result[0].id,
                    counsellerInformations: result[0].counsellerInformations,
                    counsellingSymptoms: result[0].counsellingSymptoms,
                    suicidalThoughts: result[0].suicidalThoughts,
                    suicidalThoughtsDetail: result[0].suicidalThoughtsDetail,
                    mentalHealthConditions: result[0].mentalHealthConditions,
                    mentalHealthConditionsDetail: result[0].mentalHealthConditionsDetail,
                    mentalHealthMedications: result[0].mentalHealthMedications,
                    mentalHealthMedicationsDetail: result[0].mentalHealthMedicationsDetail,
                    abuses: result[0].abuses,
                    abusesDetail: result[0].abusesDetail,
                    existCounselingProfile: true
                });
            }
        })
    }

    getSingleCounselingProfile = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_SINGLE_COUNSELING_PROFILE,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleCounselingProfile;
    }

    loadSingleMedicalPastTreatmentProfile() {
        this.getSingleMedicalPastTreatmentProfile().then(result => {

            console.log(result)

            if (result.length > 0) {
                this.setState({
                    medicalTreatmentPastProfileId: result[0].id,
                    pastNatureOfIllness: result[0].pastNatureOfIllness,
                    medicalPastTreatmentCategory: result[0].medicalPastTreatmentCategory,
                    medicalPastTreatmentRecords: result[0].medicalPastTreatmentRecords,
                    existMedicalTreatmentPastProfile: true
                });
            }
        })
    }

    getSingleMedicalPastTreatmentProfile = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_SINGLE_MEDICAL_PAST_TREATMENT_PROFILE,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleMedicalPastTreatmentProfile;
    }

    loadSingleCounselingPastProfile() {
        this.getSingleCounselingPastProfile().then(result => {

            console.log(result)

            if (result.length > 0) {
                this.setState({
                    counselingPastProfileId: result[0].id,
                    hadPreviousCounselling: result[0].hadPreviousCounselling,
                    lastDateOfCounselling: result[0].lastDateOfCounselling,
                    counsellingDetail: result[0].counsellingDetail,
                    existCounselingPastProfile: true
                });
            }
        })
    }

    getSingleCounselingPastProfile = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_SINGLE_COUNSILING_PAST_PROFILE,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleCounselingPastProfile;
    }

    loadSingleMedicalReferrelInformation() {
        this.getSingleMedicalReferrelInformation().then(result => {

            console.log(result)

            if (result.length > 0) {
                this.setState({
                    medicalReferrelInformationId: result[0].id,
                    referrelInformations: result[0].referrelInformations,
                    isReferrel: result[0].isReferrel,
                    referrelType: result[0].referrelType,
                    hospitalName: result[0].hospitalName,
                    hospitalType: result[0].hospitalType,
                    existMedicalReferrelInformation: true
                });
            }
        })
    }

    getSingleMedicalReferrelInformation = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_SINGLE_MEDICAL_REFERREL_INFORMATION,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleMedicalReferrelInformation;
    }

    loadSingleMedicalQualificationProfile() {
        this.getSingleMedicalQualificationProfile().then(result => {

            console.log(result)

            if (result.length > 0) {
                this.setState({
                    medicalQualificationProfileId: result[0].id,
                    qualificationStatus: result[0].qualificationStatus,
                    notes: result[0].notes,
                    existMedicalQualificationProfile: true
                });
            }
        })
    }

    getSingleMedicalQualificationProfile = async () => {
        const migrantId = this.state.migrantId;

        const result = await this.props.client.query({
            query: GET_SINGLE_MEDICAL_QUALIFICATION_PROFILE,
            variables: { migrantId },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleMedicalQualificationProfile;
    }


    _createEmergencyContact() {
        return this.state.emergencyContact.map((el, i) => (
            <div key={i}>
                <br />
                {i + 1 + " ) "}
                {this.state.emergencyContact.length <= 1 ? (
                    null
                ) : (
                        <div style={{ textAlign: "right", cursor: "pointer" }}>
                            <i className="fas fa-times-circle fa-2x" onClick={this.removeEmergencyContact.bind(this, i)}></i>
                        </div>
                    )}

                <div class="col-lg-12" >

                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-12 col-form-label"> Emergency Contact Person Name </label>
                        <div class="col-sm-12">
                            <input id="emergencyContactPersonName" name="emergencyContactPersonName" value={el.emergencyContactPersonName || ''} onChange={this.handleEmergencyContactChange.bind(this, i)} class="form-control" type="text" placeholder="Emergency Contact Person Name" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-12 col-form-label"> Emergency Contact Person Number </label>
                        <div class="col-sm-12">
                            <input id="emergencyContactPersonNum" name="emergencyContactPersonNum" value={el.emergencyContactPersonNum || ''} onChange={this.handleEmergencyContactChange.bind(this, i)} class="form-control" type="number" placeholder="Emergency Contact Person Number" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-12 col-form-label"> Relationship  </label>
                        <div class="col-sm-12">
                            <Dropdown
                                placeholder='Select Relationship'
                                fluid
                                selection
                                id='relationship'
                                name='relationship'
                                options={this.state.relationships}
                                value={el.relationship || ''}
                                onChange={this.handleEmergencyContactOnChange.bind(this, i)}
                            />

                        </div>
                    </div>
                </div>

                <hr />

            </div >

        ))

    }

    handleRelationshipOnChange = (i, e, data) => {
        console.log(data.value);
        const name = data.id;
        const value = data.value;

        let emergencyContact = [...this.state.emergencyContact];
        emergencyContact[i] = { ...emergencyContact[i], [name]: value };
        this.setState({ emergencyContact });
    }

    handleEmergencyContactChange(i, e) {
        const { name, value } = e.target;
        let emergencyContact = [...this.state.emergencyContact];
        emergencyContact[i] = { ...emergencyContact[i], [name]: value };
        this.setState({ emergencyContact });
    }

    handleEmergencyContactOnChange = (i, e, data) => {
        console.log(data.value);
        const name = data.id;
        const value = data.value;

        let emergencyContact = [...this.state.emergencyContact];
        emergencyContact[i] = { ...emergencyContact[i], [name]: value };
        this.setState({ emergencyContact });
    }

    addMoreEmergencyContact() {
        let emergencyContact = this.state.emergencyContact;
        emergencyContact.push({ emergencyContactPersonName: "", emergencyContactPersonNum: "", relationship: "" });
        this.setState(emergencyContact)
    }

    removeEmergencyContact(i) {
        let emergencyContact = this.state.emergencyContact;
        emergencyContact.splice(i, 1);
        this.setState({ emergencyContact });
    }


    handleMedicalProfileSubmit() {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });
        this.setState({
            hasTravelProfileMessage: false, hasSheduledArrivalProfileMessage: false,
            hasMedicalProfileMessage: false, hasSexualRiskProfileMessage: false, hasMedicalTreatmentProfileMessage: false, hasCounselingProfileMessage: false,
            hasMedicalTreatmentPastProfileMessage: false, hasCounselingPastProfileMessage: false, hasMedicalReferrelInformationMessage: false,
            hasMedicalQualificationProfileMessage: false, hasAccomodationDetailProfileMessage: false, hasMealRecordProfileMessage: false, hasTransportProfileMessage: false, hasFollowupProfileMessage: false
        })

        const { migrantId } = this.state;

        if (migrantId == "" || migrantId == undefined || migrantId == null) {
            this.setState({ hasMedicalProfileMessage: true })
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "Please submit migrant general details before submit travel details."
            });
            return;
        } else {

            this.setState({ loading: true });
            this.props.setFormStatus({ status: false, title: '', message: '' });
            this.props.setSuccessStatus({ status: false, title: '', message: '' });

            console.log(this.state.existMedicalProfile)

            if (this.state.existMedicalProfile) {

                this.editBasicMedicalProfile().then(result => {
                    this.setState({ hasMedicalProfileMessage: true, loading: false })

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Medical Profile edited Successfully!"
                    });

                    this.loadSingleBasicMedicalProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasMedicalProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to edit Medical Profile."

                        });
                    }
                });

            } else {
                this.addBasicMedicalProfile().then(result => {

                    this.setState({
                        loading: false,
                        form_state: 'default',
                        title: "",
                        message: "",
                        ethnicity: "",
                        haveChildren: false,
                        numOfChildren: 0,
                        sexualOrientation: "",
                        familyHistory: [],
                        otherMajorConditions: "",
                        isCurrentlyTreatedMedicalCondition: false,
                        currentMedicalCondition: "",
                        allergies: "",
                        closeupStatus: "false",
                        closeupAction: "",
                        hasMedicalProfileMessage: true
                    });

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Medical Profile submitted Successfully!."
                    });
                    this.loadSingleBasicMedicalProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasMedicalProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to submit Medical Profile."

                        });
                    }
                });
            }
        }


    }

    addBasicMedicalProfile = async () => {
        const {
            emergencyContact,
            ethnicity,
            sexualOrientation,
            familyHistory,
            otherMajorConditions,
            currentMedicalCondition,
            allergies,
            migrantId } = this.state;

        const numOfChildren = parseInt(this.state.numOfChildren);
        var haveChildren = this.state.haveChildren;
        var isCurrentlyTreatedMedicalCondition = this.state.isCurrentlyTreatedMedicalCondition;

        const result = await this.props.client.mutate({
            mutation: ADD_BASIC_MEDICAL_PROFILE,
            variables: {
                emergencyContact,
                ethnicity,
                haveChildren,
                numOfChildren,
                sexualOrientation,
                familyHistory,
                otherMajorConditions,
                isCurrentlyTreatedMedicalCondition,
                currentMedicalCondition,
                allergies,
                migrantId
            }
        });
        return result.data.addBasicMedicalProfile;
    };

    editBasicMedicalProfile = async () => {
        const {
            emergencyContact,
            ethnicity,
            sexualOrientation,
            familyHistory,
            otherMajorConditions,
            currentMedicalCondition,
            allergies,

        } = this.state;

        const numOfChildren = parseInt(this.state.numOfChildren);
        var haveChildren = this.state.haveChildren;
        var isCurrentlyTreatedMedicalCondition = this.state.isCurrentlyTreatedMedicalCondition;
        const id = this.state.medicalProfileId;

        const result = await this.props.client.mutate({
            mutation: EDIT_BASIC_MEDICAL_PROFILE,
            variables: {
                id,
                emergencyContact,
                ethnicity,
                haveChildren,
                numOfChildren,
                sexualOrientation,
                familyHistory,
                otherMajorConditions,
                isCurrentlyTreatedMedicalCondition,
                currentMedicalCondition,
                allergies,
            }
        });
        return result.data.editBasicMedicalProfile;
    };

    handleSexualRiskProfileSubmit() {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });
        this.setState({
            hasTravelProfileMessage: false, hasSheduledArrivalProfileMessage: false,
            hasMedicalProfileMessage: false, hasSexualRiskProfileMessage: false, hasMedicalTreatmentProfileMessage: false, hasCounselingProfileMessage: false,
            hasMedicalTreatmentPastProfileMessage: false, hasCounselingPastProfileMessage: false, hasMedicalReferrelInformationMessage: false,
            hasMedicalQualificationProfileMessage: false, hasAccomodationDetailProfileMessage: false, hasMealRecordProfileMessage: false, hasTransportProfileMessage: false, hasFollowupProfileMessage: false
        })

        const { migrantId } = this.state;

        if (migrantId == "" || migrantId == undefined || migrantId == null) {
            this.setState({ hasSexualRiskProfileMessage: true })
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "Please submit migrant general details before submit Sexual risk details."
            });
            return;
        } else {

            this.setState({ loading: true });
            this.props.setFormStatus({ status: false, title: '', message: '' });
            this.props.setSuccessStatus({ status: false, title: '', message: '' });

            if (this.state.existSexualRiskProfile) {

                this.editSexualRiskProfile().then(result => {
                    this.setState({ hasSexualRiskProfileMessage: true, loading: false })

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Sexual Risk Profile edited Successfully!"
                    });

                    this.loadSingleSexualRiskProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasSexualRiskProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to edit Sexual Risk Profile."

                        });
                    }
                });

            } else {
                this.addSexualRiskProfile().then(result => {

                    this.setState({
                        loading: false,
                        form_state: 'default',
                        title: "",
                        message: "",
                        isSmoke: false,
                        timesOfSmoke: "",
                        isUseAlchohol: false,
                        capacityOfAlchohol: "",
                        isPartnerUseDrugs: false,
                        capacityOfDrugs: "",
                        isEverInjectedDrugs: false,
                        isLikeHelpDrugProblems: false,
                        isLikeDiscussPhysicalAbuse: false,
                        hasThreatnedRelationship: false,
                        hasSexualRiskProfileMessage: true
                    });

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Sexual Risk submitted Successfully!."
                    });
                    this.loadSingleSexualRiskProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasSexualRiskProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to submit Sexual Risk Profile."

                        });
                    }
                });
            }
        }


    }

    addSexualRiskProfile = async () => {
        const {
            timesOfSmoke,
            capacityOfAlchohol,
            capacityOfDrugs,
            migrantId } = this.state;

        var isSmoke = this.state.isSmoke;
        var isUseAlchohol = this.state.isUseAlchohol;
        var isPartnerUseDrugs = this.state.isPartnerUseDrugs;
        var isEverInjectedDrugs = this.state.isEverInjectedDrugs;
        var isLikeHelpDrugProblems = this.state.isLikeHelpDrugProblems;
        var isLikeDiscussPhysicalAbuse = this.state.isLikeDiscussPhysicalAbuse;
        var hasThreatnedRelationship = this.state.hasThreatnedRelationship;


        const result = await this.props.client.mutate({
            mutation: ADD_SEXUAL_RISK_PROFILE,
            variables: {
                isSmoke,
                timesOfSmoke,
                isUseAlchohol,
                capacityOfAlchohol,
                isPartnerUseDrugs,
                capacityOfDrugs,
                isEverInjectedDrugs,
                isLikeHelpDrugProblems,
                isLikeDiscussPhysicalAbuse,
                hasThreatnedRelationship,
                migrantId
            }
        });
        return result.data.addSexualRiskProfile;
    };

    editSexualRiskProfile = async () => {
        const {
            timesOfSmoke,
            capacityOfAlchohol,
            capacityOfDrugs,
        } = this.state;

        var isSmoke = this.state.isSmoke;
        var isUseAlchohol = this.state.isUseAlchohol;
        var isPartnerUseDrugs = this.state.isPartnerUseDrugs;
        var isEverInjectedDrugs = this.state.isEverInjectedDrugs;
        var isLikeHelpDrugProblems = this.state.isLikeHelpDrugProblems;
        var isLikeDiscussPhysicalAbuse = this.state.isLikeDiscussPhysicalAbuse;
        var hasThreatnedRelationship = this.state.hasThreatnedRelationship;
        const id = this.state.sexualRiskProfileId;

        const result = await this.props.client.mutate({
            mutation: EDIT_SEXUAL_RISK_PROFILE,
            variables: {
                id,
                isSmoke,
                timesOfSmoke,
                isUseAlchohol,
                capacityOfAlchohol,
                isPartnerUseDrugs,
                capacityOfDrugs,
                isEverInjectedDrugs,
                isLikeHelpDrugProblems,
                isLikeDiscussPhysicalAbuse,
                hasThreatnedRelationship,
            }
        });
        return result.data.editSexualRiskProfile;
    };

    handleMedicalTreatmentProfileSubmit() {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });
        this.setState({
            hasTravelProfileMessage: false, hasSheduledArrivalProfileMessage: false,
            hasMedicalProfileMessage: false, hasSexualRiskProfileMessage: false, hasMedicalTreatmentProfileMessage: false, hasCounselingProfileMessage: false,
            hasMedicalTreatmentPastProfileMessage: false, hasCounselingPastProfileMessage: false, hasMedicalReferrelInformationMessage: false,
            hasMedicalQualificationProfileMessage: false, hasAccomodationDetailProfileMessage: false, hasMealRecordProfileMessage: false, hasTransportProfileMessage: false, hasFollowupProfileMessage: false
        })

        const { migrantId } = this.state;

        if (migrantId == "" || migrantId == undefined || migrantId == null) {
            this.setState({ hasMedicalTreatmentProfileMessage: true })
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "Please submit migrant general details before submit Medical treatment details."
            });
            return;
        } else {

            this.setState({ loading: true });
            this.props.setFormStatus({ status: false, title: '', message: '' });
            this.props.setSuccessStatus({ status: false, title: '', message: '' });

            if (this.state.existMedicalTreatmentProfile) {

                this.editMedicalTreatmentProfile().then(result => {
                    this.setState({ hasMedicalTreatmentProfileMessage: true, loading: false })

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Medical treatment Profile edited Successfully!"
                    });

                    this.loadSingleMedicalTreatmentProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasMedicalTreatmentProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to edit Medical treatment Profile."

                        });
                    }
                });

            } else {
                this.addMedicalTreatmentProfile().then(result => {

                    this.setState({
                        loading: false,
                        form_state: 'default',
                        title: "",
                        message: "",
                        natureOfIllness: "",
                        medicalTreatmentCategory: "",
                        hasMedicalTreatmentProfileMessage: true
                    });

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Medical treatment submitted Successfully!."
                    });
                    this.loadSingleMedicalTreatmentProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasMedicalTreatmentProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to submit Medical treatment Profile."

                        });
                    }
                });
            }
        }


    }

    addMedicalTreatmentProfile = async () => {
        const {
            natureOfIllness,
            medicalTreatmentCategory,
            medicalTreatmentRecords,
            migrantId } = this.state;

        console.log(natureOfIllness)
        console.log(medicalTreatmentCategory)
        console.log(medicalTreatmentRecords)

        const result = await this.props.client.mutate({
            mutation: ADD_MEDICAL_TREATMENT_PROFILE,
            variables: {
                natureOfIllness,
                medicalTreatmentCategory,
                medicalTreatmentRecords,
                migrantId
            }
        });
        return result.data.addMedicalTreatmentProfile;
    };

    editMedicalTreatmentProfile = async () => {
        const {
            natureOfIllness,
            medicalTreatmentCategory,
            medicalTreatmentRecords,
        } = this.state;

        const id = this.state.medicalTreatmentProfileId;

        const result = await this.props.client.mutate({
            mutation: EDIT_MEDICAL_TREATMENT_PROFILE,
            variables: {
                id,
                natureOfIllness,
                medicalTreatmentCategory,
                medicalTreatmentRecords,
            }
        });
        return result.data.editMedicalTreatmentProfile;
    };

    handleCounsilingProfileSubmit() {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });
        this.setState({
            hasTravelProfileMessage: false, hasSheduledArrivalProfileMessage: false,
            hasMedicalProfileMessage: false, hasSexualRiskProfileMessage: false, hasMedicalTreatmentProfileMessage: false, hasCounselingProfileMessage: false,
            hasMedicalTreatmentPastProfileMessage: false, hasCounselingPastProfileMessage: false, hasMedicalReferrelInformationMessage: false,
            hasMedicalQualificationProfileMessage: false, hasAccomodationDetailProfileMessage: false, hasMealRecordProfileMessage: false, hasTransportProfileMessage: false, hasFollowupProfileMessage: false
        })

        const { migrantId } = this.state;

        if (migrantId == "" || migrantId == undefined || migrantId == null) {
            this.setState({ hasCounselingProfileMessage: true })
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "Please submit migrant general details before submit Counseling details."
            });
            return;
        } else {

            this.setState({ loading: true });
            this.props.setFormStatus({ status: false, title: '', message: '' });
            this.props.setSuccessStatus({ status: false, title: '', message: '' });

            if (this.state.existCounselingProfile) {

                this.editCounselingProfile().then(result => {
                    this.setState({ hasCounselingProfileMessage: true, loading: false })

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Counseling Profile edited Successfully!"
                    });

                    this.loadSingleCounselingProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasCounselingProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to edit Counseling Profile."

                        });
                    }
                });

            } else {
                this.addCounselingProfile().then(result => {

                    this.setState({
                        loading: false,
                        form_state: 'default',
                        title: "",
                        message: "",
                        suicidalThoughts: "",
                        suicidalThoughtsDetail: "",
                        mentalHealthConditions: "",
                        mentalHealthConditionsDetail: "",
                        mentalHealthMedications: "",
                        mentalHealthMedicationsDetail: "",
                        abuses: "",
                        abusesDetail: "",
                        hasCounselingProfileMessage: true
                    });

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Counseling submitted Successfully!."
                    });
                    this.loadSingleCounselingProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasCounselingProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to submit Counseling Profile."

                        });
                    }
                });
            }
        }


    }

    addCounselingProfile = async () => {
        const {
            counsellerInformations,
            counsellingSymptoms,
            suicidalThoughts,
            suicidalThoughtsDetail,
            mentalHealthConditions,
            mentalHealthConditionsDetail,
            mentalHealthMedications,
            mentalHealthMedicationsDetail,
            abuses,
            abusesDetail,
            migrantId } = this.state;

        const result = await this.props.client.mutate({
            mutation: ADD_COUNSELING_PROFILE,
            variables: {
                counsellerInformations,
                counsellingSymptoms,
                suicidalThoughts,
                suicidalThoughtsDetail,
                mentalHealthConditions,
                mentalHealthConditionsDetail,
                mentalHealthMedications,
                mentalHealthMedicationsDetail,
                abuses,
                abusesDetail,
                migrantId
            }
        });
        return result.data.addCounselingProfile;
    };

    editCounselingProfile = async () => {
        const {
            counsellerInformations,
            counsellingSymptoms,
            suicidalThoughts,
            suicidalThoughtsDetail,
            mentalHealthConditions,
            mentalHealthConditionsDetail,
            mentalHealthMedications,
            mentalHealthMedicationsDetail,
            abuses,
            abusesDetail,
        } = this.state;

        const id = this.state.counselingProfileId;

        const result = await this.props.client.mutate({
            mutation: EDIT_COUNSELING_PROFILE,
            variables: {
                id,
                counsellerInformations,
                counsellingSymptoms,
                suicidalThoughts,
                suicidalThoughtsDetail,
                mentalHealthConditions,
                mentalHealthConditionsDetail,
                mentalHealthMedications,
                mentalHealthMedicationsDetail,
                abuses,
                abusesDetail,
            }
        });
        return result.data.editCounselingProfile;
    };

    handleMedicalTreatmentPastProfileSubmit() {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });
        this.setState({
            hasTravelProfileMessage: false, hasSheduledArrivalProfileMessage: false,
            hasMedicalProfileMessage: false, hasSexualRiskProfileMessage: false, hasMedicalTreatmentProfileMessage: false, hasCounselingProfileMessage: false,
            hasMedicalTreatmentPastProfileMessage: false, hasCounselingPastProfileMessage: false, hasMedicalReferrelInformationMessage: false,
            hasMedicalQualificationProfileMessage: false, hasAccomodationDetailProfileMessage: false, hasMealRecordProfileMessage: false, hasTransportProfileMessage: false, hasFollowupProfileMessage: false
        })

        const { migrantId } = this.state;

        if (migrantId == "" || migrantId == undefined || migrantId == null) {
            this.setState({ hasMedicalTreatmentPastProfileMessage: true })
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "Please submit migrant general details before submit Medical Past treatment details."
            });
            return;
        } else {

            this.setState({ loading: true });
            this.props.setFormStatus({ status: false, title: '', message: '' });
            this.props.setSuccessStatus({ status: false, title: '', message: '' });

            if (this.state.existMedicalTreatmentPastProfile) {

                this.editMedicalPastTreatmentProfile().then(result => {
                    this.setState({ hasMedicalTreatmentPastProfileMessage: true, loading: false })

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Medical Past treatment Profile edited Successfully!"
                    });

                    this.loadSingleMedicalPastTreatmentProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasMedicalTreatmentPastProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to edit Medical treatment Past Profile."

                        });
                    }
                });

            } else {
                this.addMedicalPastTreatmentProfile().then(result => {

                    this.setState({
                        loading: false,
                        form_state: 'default',
                        title: "",
                        message: "",
                        pastNatureOfIllness: "",
                        medicalPastTreatmentCategory: "",
                        hasMedicalTreatmentPastProfileMessage: true
                    });

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Medical Past treatment submitted Successfully!."
                    });
                    this.loadSingleMedicalPastTreatmentProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasMedicalTreatmentPastProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to submit Medical treatment Past Profile."

                        });
                    }
                });
            }
        }

    }

    addMedicalPastTreatmentProfile = async () => {
        const {
            pastNatureOfIllness,
            medicalPastTreatmentCategory,
            medicalPastTreatmentRecords,
            migrantId } = this.state;

        const result = await this.props.client.mutate({
            mutation: ADD_MEDICAL_PAST_TREATMENT_PROFILE,
            variables: {
                pastNatureOfIllness,
                medicalPastTreatmentCategory,
                medicalPastTreatmentRecords,
                migrantId
            }
        });
        return result.data.addMedicalPastTreatmentProfile;
    };

    editMedicalPastTreatmentProfile = async () => {
        const {
            pastNatureOfIllness,
            medicalPastTreatmentCategory,
            medicalPastTreatmentRecords
        } = this.state;

        const id = this.state.medicalTreatmentPastProfileId;

        const result = await this.props.client.mutate({
            mutation: EDIT_MEDICAL_PAST_TREATMENT_PROFILE,
            variables: {
                id,
                pastNatureOfIllness,
                medicalPastTreatmentCategory,
                medicalPastTreatmentRecords
            }
        });
        return result.data.editMedicalPastTreatmentProfile;
    };

    handleCounsilingPastProfileSubmit() {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });
        this.setState({
            hasTravelProfileMessage: false, hasSheduledArrivalProfileMessage: false,
            hasMedicalProfileMessage: false, hasSexualRiskProfileMessage: false, hasMedicalTreatmentProfileMessage: false, hasCounselingProfileMessage: false,
            hasMedicalTreatmentPastProfileMessage: false, hasCounselingPastProfileMessage: false, hasMedicalReferrelInformationMessage: false,
            hasMedicalQualificationProfileMessage: false, hasAccomodationDetailProfileMessage: false, hasMealRecordProfileMessage: false, hasTransportProfileMessage: false, hasFollowupProfileMessage: false
        })

        const { migrantId } = this.state;

        if (migrantId == "" || migrantId == undefined || migrantId == null) {
            this.setState({ hasCounselingPastProfileMessage: true })
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "Please submit migrant general details before submit Counseling Past details."
            });
            return;
        } else {

            this.setState({ loading: true });
            this.props.setFormStatus({ status: false, title: '', message: '' });
            this.props.setSuccessStatus({ status: false, title: '', message: '' });

            if (this.state.existCounselingPastProfile) {

                this.editCounselingPastProfile().then(result => {
                    this.setState({ hasCounselingPastProfileMessage: true, loading: false })

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Counseling Past Profile edited Successfully!"
                    });

                    this.loadSingleCounselingPastProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasCounselingPastProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to edit Counseling Past Profile."

                        });
                    }
                });

            } else {
                this.addCounselingPastProfile().then(result => {

                    this.setState({
                        loading: false,
                        form_state: 'default',
                        title: "",
                        message: "",
                        hadPreviousCounselling: "",
                        lastDateOfCounselling: "",
                        counsellingDetail: "",
                        hasCounselingPastProfileMessage: true
                    });

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Past Counseling details submitted Successfully!."
                    });
                    this.loadSingleCounselingPastProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasCounselingPastProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to submit Past Counseling Profile."

                        });
                    }
                });
            }
        }

    }

    addCounselingPastProfile = async () => {
        const {
            hadPreviousCounselling,
            lastDateOfCounselling,
            counsellingDetail,
            migrantId } = this.state;

        const result = await this.props.client.mutate({
            mutation: ADD_COUNSELING_PAST_PROFILE,
            variables: {
                hadPreviousCounselling,
                lastDateOfCounselling,
                counsellingDetail,
                migrantId
            }
        });
        return result.data.addCounselingPastProfile;
    };

    editCounselingPastProfile = async () => {
        const {
            hadPreviousCounselling,
            lastDateOfCounselling,
            counsellingDetail,
        } = this.state;

        const id = this.state.counselingPastProfileId;

        const result = await this.props.client.mutate({
            mutation: EDIT_COUNSELING_PAST_PROFILE,
            variables: {
                id,
                hadPreviousCounselling,
                lastDateOfCounselling,
                counsellingDetail,
            }
        });
        return result.data.editCounselingPastProfile;
    };

    handleMedicalReferrelInformationSubmit() {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });
        this.setState({
            hasTravelProfileMessage: false, hasSheduledArrivalProfileMessage: false,
            hasMedicalProfileMessage: false, hasSexualRiskProfileMessage: false, hasMedicalTreatmentProfileMessage: false, hasCounselingProfileMessage: false,
            hasMedicalTreatmentPastProfileMessage: false, hasCounselingPastProfileMessage: false, hasMedicalReferrelInformationMessage: false,
            hasMedicalQualificationProfileMessage: false, hasAccomodationDetailProfileMessage: false, hasMealRecordProfileMessage: false, hasTransportProfileMessage: false, hasFollowupProfileMessage: false
        })

        const { migrantId } = this.state;

        if (migrantId == "" || migrantId == undefined || migrantId == null) {
            this.setState({ hasMedicalReferrelInformationMessage: true })
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "Please submit migrant general details before submit Medical Referrel informations."
            });
            return;
        } else {

            this.setState({ loading: true });
            this.props.setFormStatus({ status: false, title: '', message: '' });
            this.props.setSuccessStatus({ status: false, title: '', message: '' });

            if (this.state.existMedicalReferrelInformation) {

                this.editMedicalReferrelInformation().then(result => {
                    this.setState({ hasMedicalReferrelInformationMessage: true, loading: false })

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Medical Referrel informations edited Successfully!"
                    });

                    this.loadSingleMedicalReferrelInformation();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasMedicalReferrelInformationMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to edit Medical Referrel informations."

                        });
                    }
                });

            } else {
                this.addMedicalReferrelInformation().then(result => {

                    this.setState({
                        loading: false,
                        form_state: 'default',
                        title: "",
                        message: "",
                        isReferrel: "false",
                        referrelType: "",
                        hospitalName: "",
                        hospitalType: "",
                        hasMedicalReferrelInformationMessage: true
                    });

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Medical Referrel informations submitted Successfully!."
                    });
                    this.loadSingleMedicalReferrelInformation();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasMedicalReferrelInformationMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to submit Medical Referrel informations."

                        });
                    }
                });
            }
        }

    }


    addMedicalReferrelInformation = async () => {
        const {
            referrelInformations,
            referrelType,
            hospitalName,
            hospitalType,
            migrantId } = this.state;

        const isReferrel = this.state.isReferrel;

        const result = await this.props.client.mutate({
            mutation: ADD_MEDICAL_REFERREL_INFORMATION,
            variables: {
                isReferrel,
                referrelType,
                hospitalName,
                hospitalType,
                referrelInformations,
                migrantId
            }
        });
        return result.data.addMedicalReferrelInformation;
    };

    editMedicalReferrelInformation = async () => {
        const {
            referrelType,
            hospitalName,
            hospitalType,
            referrelInformations,
        } = this.state;

        const id = this.state.medicalReferrelInformationId;
        const isReferrel = this.state.isReferrel;

        const result = await this.props.client.mutate({
            mutation: EDIT_MEDICAL_REFERREL_INFORMATION,
            variables: {
                id,
                isReferrel,
                referrelType,
                hospitalName,
                hospitalType,
                referrelInformations,
            }
        });
        return result.data.editMedicalReferrelInformation;
    };


    handleMedicalQualificationProfileSubmit() {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });
        this.setState({
            hasTravelProfileMessage: false, hasSheduledArrivalProfileMessage: false,
            hasMedicalProfileMessage: false, hasSexualRiskProfileMessage: false, hasMedicalTreatmentProfileMessage: false, hasCounselingProfileMessage: false,
            hasMedicalTreatmentPastProfileMessage: false, hasCounselingPastProfileMessage: false, hasMedicalReferrelInformationMessage: false,
            hasMedicalQualificationProfileMessage: false, hasAccomodationDetailProfileMessage: false, hasMealRecordProfileMessage: false, hasTransportProfileMessage: false, hasFollowupProfileMessage: false
        })

        const { migrantId } = this.state;

        if (migrantId == "" || migrantId == undefined || migrantId == null) {
            this.setState({ hasMedicalQualificationProfileMessage: true })
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "Please submit migrant general details before submit Medical Qualification Profile."
            });
            return;
        } else {

            this.setState({ loading: true });
            this.props.setFormStatus({ status: false, title: '', message: '' });
            this.props.setSuccessStatus({ status: false, title: '', message: '' });

            if (this.state.existMedicalQualificationProfile) {

                this.editMedicalQualificationProfile().then(result => {
                    this.setState({ hasMedicalQualificationProfileMessage: true, loading: false })

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Medical Qualification Profile edited Successfully!"
                    });

                    this.loadSingleMedicalQualificationProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasMedicalQualificationProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to edit Medical Qualification Profile."

                        });
                    }
                });

            } else {
                this.addMedicalQualificationProfile().then(result => {

                    this.setState({
                        loading: false,
                        form_state: 'default',
                        title: "",
                        message: "",
                        qualificationStatus: "",
                        notes: "",
                        hasMedicalQualificationProfileMessage: true
                    });

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Medical Qualification Profile submitted Successfully!."
                    });
                    this.loadSingleMedicalQualificationProfile();

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false, hasMedicalQualificationProfileMessage: true });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to submit Medical Qualification Profile."

                        });
                    }
                });
            }
        }

    }

    addMedicalQualificationProfile = async () => {
        const {
            qualificationStatus,
            notes,
            migrantId } = this.state;

        const result = await this.props.client.mutate({
            mutation: ADD_MEDICAL_QUALIFICATION_PROFILE,
            variables: {
                qualificationStatus,
                notes,
                migrantId
            }
        });
        return result.data.addMedicalQualificationProfile;
    };

    editMedicalQualificationProfile = async () => {
        const {
            qualificationStatus,
            notes,
        } = this.state;

        const id = this.state.medicalQualificationProfileId;

        const result = await this.props.client.mutate({
            mutation: EDIT_MEDICAL_QUALIFICATION_PROFILE,
            variables: {
                id,
                qualificationStatus,
                notes,
            }
        });
        return result.data.editMedicalQualificationProfile;
    };

    _createTreatmentRecord() {
        return this.state.medicalTreatmentRecords.map((el, i) => (
            <div key={i}>
                <br />

                {this.state.medicalTreatmentRecords.length <= 1 ? (
                    null
                ) : (
                        <div style={{ textAlign: "right", cursor: "pointer" }}>
                            <i className="fas fa-times-circle fa-2x" onClick={this.removeTreatmentRecord.bind(this, i)}></i>
                        </div>
                    )}

                <div class="card-header accordianSubTitle">
                    <h4 class="mt-0 header-title" > General Physician Info </h4>
                </div><br />
                {i + 1 + " ) "}
                <div >
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-12 col-form-label">  Name </label>
                        <div class="col-sm-12">
                            <input id="physicianName" name="physicianName" value={el.physicianName || ''} onChange={this.handleTreatmentRecordChange.bind(this, i)} class="form-control" type="text" placeholder="Name" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-12 col-form-label"> Address </label>
                        <div class="col-sm-12">
                            <textarea id="physicianAddress" name="physicianAddress" value={el.physicianAddress || ''} onChange={this.handleTreatmentRecordChange.bind(this, i)} class="form-control" rows="3" placeholder="Address"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-12 col-form-label"> Phone Number </label>
                        <div class="col-sm-12">
                            <input id="physicianPhone" name="physicianPhone" value={el.physicianPhone || ''} onChange={this.handleTreatmentRecordChange.bind(this, i)} class="form-control" type="number" placeholder="Phone Number" />
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body" >

                            <div class="card-header">
                                <h4 class="mt-0 header-title" > Medical Prescribion Records </h4>
                            </div><br />

                            {el.prescribionRecords.map((rec, j) => (

                                <div key={j}>
                                    <br />
                                    {j + 1 + " ) "}

                                    {el.prescribionRecords.length <= 1 ? (
                                        null
                                    ) : (
                                            <div style={{ textAlign: "right", cursor: "pointer" }}>
                                                <i className="fas fa-times-circle fa-2x" onClick={this.removePrescribionRecord.bind(this, i, j)}></i>
                                            </div>
                                        )}

                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label">  Prescribed Medication  </label>
                                        <div class="col-sm-12">
                                            <input id="prescribedMedication" name="prescribedMedication" class="form-control" type="text" value={rec.prescribedMedication || ''} placeholder="Prescribed Medication" onChange={this.handlePrescribionRecordChange.bind(this, i, j)} />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label">  Dosage </label>
                                        <div class="col-sm-12">
                                            <input id="dosage" name="dosage" class="form-control" type="text" value={rec.dosage || ''} placeholder="Dosage" onChange={this.handlePrescribionRecordChange.bind(this, i, j)} />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label">  Frequency </label>
                                        <div class="col-sm-12">
                                            <input id="frequency" name="frequency" class="form-control" type="text" value={rec.frequency || ''} placeholder="Frequency" onChange={this.handlePrescribionRecordChange.bind(this, i, j)} />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label">  Date Started </label>
                                        <div class="col-sm-12">
                                            {/* <input class="form-control" id="dateStarted" name="dateStarted" type="date" value={el.dateStarted || ''} id="example-date-input" onChange={this.handleTreatmentRecordChange.bind(this, i)} /> */}
                                            <DateTime
                                                className="DateTime"
                                                id="dateStarted"
                                                name="dateStarted"
                                                defaultValue={new Date()}
                                                dateFormat="YYYY-MM-DD"
                                                timeFormat="HH:mm"
                                                value={rec.dateStarted || ''}
                                                onChange={this.handlePrescribionRecordDateChange.bind(this, i, j)}
                                            />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Comments/problems/concerns </label>
                                        <div class="col-sm-12">
                                            <textarea id="comments" name="comments" class="form-control" rows="3" placeholder="Comments" value={rec.comments || ''} onChange={this.handlePrescribionRecordChange.bind(this, i, j)}></textarea>
                                        </div>
                                    </div>

                                    <hr />

                                    <div style={{ textAlign: "right" }}><button className="btn btn-secondary" onClick={this._addMorePrescribionRecord.bind(this, i)} type="button">
                                        Add More Prescribion <i className="fas fa-plus-circle" />
                                    </button>
                                    </div>

                                </div>

                            ))}

                        </div>
                    </div>

                </div>

                <hr />

            </div >

        ))

    }

    _createPastTreatmentRecord() {
        return this.state.medicalPastTreatmentRecords.map((el, i) => (
            <div key={i}>
                <br />

                {this.state.medicalPastTreatmentRecords.length <= 1 ? (
                    null
                ) : (
                        <div style={{ textAlign: "right", cursor: "pointer" }}>
                            <i className="fas fa-times-circle fa-2x" onClick={this.removePastTreatmentRecord.bind(this, i)}></i>
                        </div>
                    )}

                <div class="card-header accordianSubTitle">
                    <h4 class="mt-0 header-title" > General Physician Info </h4>
                </div><br />
                {i + 1 + " ) "}
                <div >
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-12 col-form-label">  Name </label>
                        <div class="col-sm-12">
                            <input id="physicianName" name="physicianName" value={el.physicianName || ''} onChange={this.handlePastTreatmentRecordChange.bind(this, i)} class="form-control" type="text" placeholder="Name" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-12 col-form-label"> Address </label>
                        <div class="col-sm-12">
                            <textarea id="physicianAddress" name="physicianAddress" value={el.physicianAddress || ''} onChange={this.handlePastTreatmentRecordChange.bind(this, i)} class="form-control" rows="3" placeholder="Address"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-12 col-form-label"> Phone Number </label>
                        <div class="col-sm-12">
                            <input id="physicianPhone" name="physicianPhone" value={el.physicianPhone || ''} onChange={this.handlePastTreatmentRecordChange.bind(this, i)} class="form-control" type="number" placeholder="Phone Number" />
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body" >

                            <div class="card-header">
                                <h4 class="mt-0 header-title" > Medical Prescribion Records </h4>
                            </div><br />

                            {el.prescribionRecords.map((rec, j) => (

                                <div key={j}>
                                    <br />
                                    {j + 1 + " ) "}

                                    {el.prescribionRecords.length <= 1 ? (
                                        null
                                    ) : (
                                            <div style={{ textAlign: "right", cursor: "pointer" }}>
                                                <i className="fas fa-times-circle fa-2x" onClick={this.removePastPrescribionRecord.bind(this, i, j)}></i>
                                            </div>
                                        )}

                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label">  Prescribed Medication  </label>
                                        <div class="col-sm-12">
                                            <input id="prescribedMedication" name="prescribedMedication" class="form-control" type="text" value={rec.prescribedMedication || ''} placeholder="Prescribed Medication" onChange={this.handlePastPrescribionRecordChange.bind(this, i, j)} />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label">  Dosage </label>
                                        <div class="col-sm-12">
                                            <input id="dosage" name="dosage" class="form-control" type="text" value={rec.dosage || ''} placeholder="Dosage" onChange={this.handlePastPrescribionRecordChange.bind(this, i, j)} />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label">  Frequency </label>
                                        <div class="col-sm-12">
                                            <input id="frequency" name="frequency" class="form-control" type="text" value={rec.frequency || ''} placeholder="Frequency" onChange={this.handlePastPrescribionRecordChange.bind(this, i, j)} />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label">  Date Started </label>
                                        <div class="col-sm-12">
                                            {/* <input class="form-control" id="dateStarted" name="dateStarted" type="date" value={el.dateStarted || ''} id="example-date-input" onChange={this.handleTreatmentRecordChange.bind(this, i)} /> */}
                                            <DateTime
                                                className="DateTime"
                                                id="dateStarted"
                                                name="dateStarted"
                                                defaultValue={new Date()}
                                                dateFormat="YYYY-MM-DD"
                                                timeFormat="HH:mm"
                                                value={rec.dateStarted || ''}
                                                onChange={this.handlePastPrescribionRecordDateChange.bind(this, i, j)}
                                            />
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Comments/problems/concerns </label>
                                        <div class="col-sm-12">
                                            <textarea id="comments" name="comments" class="form-control" rows="3" placeholder="Comments" value={rec.comments || ''} onChange={this.handlePastPrescribionRecordChange.bind(this, i, j)}></textarea>
                                        </div>
                                    </div>

                                    <hr />

                                    <div style={{ textAlign: "right" }}><button className="btn btn-secondary" onClick={this._addMorePastPrescribionRecord.bind(this, i)} type="button">
                                        Add More Prescribion <i className="fas fa-plus-circle" />
                                    </button>
                                    </div>

                                </div>

                            ))}

                        </div>
                    </div>

                </div>

                <hr />

            </div >

        ))

    }

    _createCounsellerRecord() {
        return this.state.counsellerInformations.map((el, i) => (
            <div key={i}>
                <br />

                {this.state.counsellerInformations.length <= 1 ? (
                    null
                ) : (
                        <div style={{ textAlign: "right", cursor: "pointer" }}>
                            <i className="fas fa-times-circle fa-2x" onClick={this.removeCounsellerRecord.bind(this, i)}></i>
                        </div>
                    )}

                <div class="card-header accordianSubTitle">
                    <h4 class="mt-0 header-title" > Counseller Info </h4>
                </div><br />
                {i + 1 + " ) "}
                <div >

                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-12 col-form-label">  Name </label>
                        <div class="col-sm-12">
                            <input id="counsellerName" name="counsellerName" value={el.counsellerName || ''} onChange={this.handleCounsellerRecordChange.bind(this, i)} class="form-control" type="text" placeholder="Name" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-12 col-form-label"> Address </label>
                        <div class="col-sm-12">
                            <textarea id="counsellerAddress" name="counsellerAddress" value={el.counsellerAddress || ''} onChange={this.handleCounsellerRecordChange.bind(this, i)} class="form-control" rows="3" placeholder="Address"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-12 col-form-label"> Phone Number </label>
                        <div class="col-sm-12">
                            <input id="counsellerPhone" name="counsellerPhone" value={el.counsellerPhone || ''} onChange={this.handleCounsellerRecordChange.bind(this, i)} class="form-control" type="number" placeholder="Phone Number" />
                        </div>
                    </div>
                </div>

                <hr />

            </div >

        ))

    }

    handleCounsellerRecordChange(i, e) {
        const { name, value } = e.target;
        let counsellerInformations = [...this.state.counsellerInformations];
        counsellerInformations[i] = { ...counsellerInformations[i], [name]: value };
        this.setState({ counsellerInformations });
    }

    _addMoreCounsellerRecord() {
        let counsellerInformations = this.state.counsellerInformations;
        counsellerInformations.push({ counsellerName: "", counsellerAddress: "", counsellerPhone: "", });
        this.setState(counsellerInformations)
    }

    removeCounsellerRecord(i) {
        let counsellerInformations = this.state.counsellerInformations;
        counsellerInformations.splice(i, 1);
        this.setState({ counsellerInformations });
    }

    _createReferrelInformation() {
        return this.state.referrelInformations.map((el, i) => (
            <div key={i}>
                <br />

                {i + 1 + " ) "}

                {this.state.referrelInformations.length <= 1 ? (
                    null
                ) : (
                        <div style={{ textAlign: "right", cursor: "pointer" }}>
                            <i className="fas fa-times-circle fa-2x" onClick={this.removeReferrelInformation.bind(this, i)}></i>
                        </div>
                    )}

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label">  Referral Name  </label>
                    <div class="col-sm-12">
                        <input id="referalName" name="referalName" class="form-control" type="text" value={el.referalName || ''} placeholder="Referal Name" onChange={this.handleReferrelInformationChange.bind(this, i)} />
                    </div>
                </div>

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label">  Email </label>
                    <div class="col-sm-12">
                        <input id="email" name="email" class="form-control" type="email" value={el.email || ''} placeholder="Email" onChange={this.handleReferrelInformationChange.bind(this, i)} />
                    </div>
                </div>

                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label">  Phone Number </label>
                    <div class="col-sm-12">
                        <input id="phoneNumber" name="phoneNumber" class="form-control" type="number" value={el.phoneNumber || ''} placeholder="Phone Number" onChange={this.handleReferrelInformationChange.bind(this, i)} />
                    </div>
                </div>


                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-12 col-form-label"> Comments/problems/concerns </label>
                    <div class="col-sm-12">
                        <textarea id="comments" name="comments" class="form-control" rows="3" placeholder="Comments" value={el.comments || ''} onChange={this.handleReferrelInformationChange.bind(this, i)}></textarea>
                    </div>
                </div>

                <hr />

            </div >

        ))

    }


    handleTreatmentRecordChange(i, e) {
        const { name, value } = e.target;
        let medicalTreatmentRecords = [...this.state.medicalTreatmentRecords];
        medicalTreatmentRecords[i] = { ...medicalTreatmentRecords[i], [name]: value };
        this.setState({ medicalTreatmentRecords });
    }

    handlePrescribionRecordChange(i, j, e) {
        const { name, value } = e.target;
        let medicalTreatmentRecords = [...this.state.medicalTreatmentRecords];

        medicalTreatmentRecords[i].prescribionRecords[j] = { ...medicalTreatmentRecords[i].prescribionRecords[j], [name]: value };
        this.setState({ medicalTreatmentRecords });
    }

    handleTreatmentRecordDateChange(i, e) {
        var name = "dateStarted";
        var value = formatDateTime(e);
        let medicalTreatmentRecords = [...this.state.medicalTreatmentRecords];
        medicalTreatmentRecords[i] = { ...medicalTreatmentRecords[i], [name]: value };
        this.setState({ medicalTreatmentRecords });
    }

    handlePrescribionRecordDateChange(i, j, e) {
        var name = "dateStarted";
        var value = formatDateTime(e);
        let medicalTreatmentRecords = [...this.state.medicalTreatmentRecords];
        medicalTreatmentRecords[i].prescribionRecords[j] = { ...medicalTreatmentRecords[i].prescribionRecords[j], [name]: value };
        this.setState({ medicalTreatmentRecords });
    }

    handleTreatmentRecordRadioChange = (i, e) => {
        if (e.target.checked) {
            var name = e.target.name;
            var value = e.target.value;
            console.log(name)
            console.log(value)
            // this.setState({ [name]: value });

            let medicalTreatmentRecords = [...this.state.medicalTreatmentRecords];
            medicalTreatmentRecords[i] = { ...medicalTreatmentRecords[i], [name]: value };
            this.setState({ medicalTreatmentRecords });

        }
    }

    handlePastTreatmentRecordChange(i, e) {
        const { name, value } = e.target;
        let medicalPastTreatmentRecords = [...this.state.medicalPastTreatmentRecords];
        medicalPastTreatmentRecords[i] = { ...medicalPastTreatmentRecords[i], [name]: value };
        this.setState({ medicalPastTreatmentRecords });
    }

    handlePastPrescribionRecordChange(i, j, e) {
        const { name, value } = e.target;
        let medicalPastTreatmentRecords = [...this.state.medicalPastTreatmentRecords];

        medicalPastTreatmentRecords[i].prescribionRecords[j] = { ...medicalPastTreatmentRecords[i].prescribionRecords[j], [name]: value };
        this.setState({ medicalPastTreatmentRecords });
    }

    handlePastPrescribionRecordDateChange(i, j, e) {
        var name = "dateStarted";
        var value = formatDateTime(e);
        let medicalPastTreatmentRecords = [...this.state.medicalPastTreatmentRecords];
        medicalPastTreatmentRecords[i].prescribionRecords[j] = { ...medicalPastTreatmentRecords[i].prescribionRecords[j], [name]: value };
        this.setState({ medicalPastTreatmentRecords });
    }

    handlePastTreatmentRecordDateChange(i, e) {
        var name = "dateStarted";
        var value = formatDateTime(e);
        let medicalPastTreatmentRecords = [...this.state.medicalPastTreatmentRecords];
        medicalPastTreatmentRecords[i] = { ...medicalPastTreatmentRecords[i], [name]: value };
        this.setState({ medicalPastTreatmentRecords });
    }

    handleReferrelInformationChange(i, e) {
        const { name, value } = e.target;
        let referrelInformations = [...this.state.referrelInformations];
        referrelInformations[i] = { ...referrelInformations[i], [name]: value };
        this.setState({ referrelInformations });
    }


    _addMoreTreatmentRecord() {
        let medicalTreatmentRecords = this.state.medicalTreatmentRecords;
        medicalTreatmentRecords.push({ physicianName: "", physicianAddress: "", physicianPhone: "", prescribionRecords: [{ prescribedMedication: "", dosage: "", frequency: "", dateStarted: "", comments: "" }] });
        this.setState(medicalTreatmentRecords)
    }

    _addMorePrescribionRecord(i) {
        let medicalTreatmentRecords = this.state.medicalTreatmentRecords;
        medicalTreatmentRecords[i].prescribionRecords.push({ prescribedMedication: "", dosage: "", frequency: "", dateStarted: "", comments: "" });
        this.setState(medicalTreatmentRecords)
    }

    _addMorePastTreatmentRecord() {
        let medicalPastTreatmentRecords = this.state.medicalPastTreatmentRecords;
        medicalPastTreatmentRecords.push({ physicianName: "", physicianAddress: "", physicianPhone: "", prescribionRecords: [{ prescribedMedication: "", dosage: "", frequency: "", dateStarted: "", comments: "" }] });
        this.setState(medicalPastTreatmentRecords)
    }

    _addMorePastPrescribionRecord(i) {
        let medicalPastTreatmentRecords = this.state.medicalPastTreatmentRecords;
        medicalPastTreatmentRecords[i].prescribionRecords.push({ prescribedMedication: "", dosage: "", frequency: "", dateStarted: "", comments: "" });
        this.setState(medicalPastTreatmentRecords)
    }

    _addMoreReferrelInformation() {
        let referrelInformations = this.state.referrelInformations;
        referrelInformations.push({ referalName: "", email: "", phoneNumber: "", comments: "" });
        this.setState(referrelInformations)
    }

    removeTreatmentRecord(i) {
        let medicalTreatmentRecords = this.state.medicalTreatmentRecords;
        medicalTreatmentRecords.splice(i, 1);
        this.setState({ medicalTreatmentRecords });
    }

    removePrescribionRecord(i, j) {
        let medicalTreatmentRecords = this.state.medicalTreatmentRecords;
        medicalTreatmentRecords[i].prescribionRecords.splice(j, 1);
        this.setState({ medicalTreatmentRecords });
    }

    removePastTreatmentRecord(i) {
        let medicalPastTreatmentRecords = this.state.medicalPastTreatmentRecords;
        medicalPastTreatmentRecords.splice(i, 1);
        this.setState({ medicalPastTreatmentRecords });
    }

    removePastPrescribionRecord(i, j) {
        let medicalPastTreatmentRecords = this.state.medicalPastTreatmentRecords;
        medicalPastTreatmentRecords[i].prescribionRecords.splice(j, 1);
        this.setState({ medicalPastTreatmentRecords });
    }

    removeReferrelInformation(i) {
        let referrelInformations = this.state.referrelInformations;
        referrelInformations.splice(i, 1);
        this.setState({ referrelInformations });
    }

    handleDate = (date, id) => {
        var value = formatDateTime(date._d);
        this.setState({ [id]: value });
    };

    getValidDates = function (currentDate) {
        var yesterday = DateTime.moment().subtract(1, 'day');
        return currentDate.isAfter(yesterday);
    }

    handleClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = this.state
        const newIndex = activeIndex === index ? -1 : index

        this.setState({ activeIndex: newIndex })
    }

    handleRadioOnChange = (e) => {
        console.log("---------------------")
        if (e.target.checked) {
            var name = e.target.name;
            var value = e.target.value;
            console.log(name)
            console.log(value)
            this.setState({ [name]: value });
            console.log(this.state);
        }

    }

    handleMedicalRadioOnChange = (e) => {
        console.log("---------------------")
        if (e.target.checked) {
            var name = "closeupStatus";
            var value = e.target.value;
            console.log(name)
            console.log(value)
            this.setState({ [name]: value });
            console.log(this.state);
        }

    }

    handleSymptomCheckOnChange = (e) => {
        var id = e.target.id;
        var value = e.target.value;

        if (e.target.checked) {
            let counsellingSymptoms = this.state.counsellingSymptoms;
            counsellingSymptoms = { ...counsellingSymptoms, [id]: value };
            this.setState({ counsellingSymptoms });
        } else {
            let counsellingSymptoms = this.state.counsellingSymptoms;
            delete counsellingSymptoms[id];
            this.setState({ counsellingSymptoms });
        }


    }

    handleSymptomChange = (e) => {
        const { id, value } = e.target;
        let counsellingSymptoms = this.state.counsellingSymptoms;
        counsellingSymptoms = { ...counsellingSymptoms, [id]: value };
        this.setState({ counsellingSymptoms });
    }

    render() {

        const { activeIndex, loading, hasMedicalProfileMessage, hasSexualRiskProfileMessage, hasMedicalTreatmentProfileMessage
            , hasCounselingProfileMessage, hasMedicalTreatmentPastProfileMessage, hasCounselingPastProfileMessage,
            hasMedicalReferrelInformationMessage, hasMedicalQualificationProfileMessage, hasClosingMessage } = this.state;

        const { formErrorStatus, formSuccessState } = this.props;

        console.log(this.state.state)

        return (
            <div>
                <br />

                <div class="col-lg-10">

                    <Accordion fluid styled style={{ "box-shadow": "none" }}>
                        <Accordion.Title
                            className="card-header darkHeader"
                            active={activeIndex === 3}
                            index={3}
                            onClick={this.handleClick}
                        >
                            <div class="row">

                                <div class="col-lg-6">
                                    <div class="headerAlign">
                                        <Icon name='dropdown headerRightAlign' />
                                        <div class="">
                                            <div className="accordianLeftTitleText">
                                                <h4 class="mt-0 header-title" > Basic Medical Profile </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="headerRightAlign">
                                        <div className="accordianRightTitleText">
                                            {this.state.basicMedicalHistoryProfiles.length > 0 ? (
                                                <h4> ( {this.state.basicMedicalHistoryProfiles.length} )  previous {this.state.basicMedicalHistoryProfiles.length == 1 ? ("record") : ("records")} available. </h4>
                                            ) : (null)}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </Accordion.Title>

                        <Accordion.Content active={activeIndex === 3}>
                            <br />

                            <div class="col-lg-12">
                                <div class="card HistoryContainer">
                                    <h3 className="HistoryHeading">Hisory Details</h3>

                                    {this.state.basicMedicalHistoryProfiles.length > 0 ? (
                                        this.state.basicMedicalHistoryProfiles.map((profile, index) => {
                                            return [<div className="HistoryGrayHeader"> {index + 1 + ") " + "Created Date : " + formatDate(profile.createdAt)} </div>
                                                , <hr />, <div className="HistoryContainerInner">
                                                <Grid>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Speaking Languages : </strong>

                                                                {
                                                                    profile.ethnicity.length > 0 ? (
                                                                        profile.ethnicity.map((data, index) => {
                                                                            return data + " , "
                                                                        })
                                                                    ) : (null)
                                                                }

                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Sexual Orientation : </strong> {profile.sexualOrientation}  </label>
                                                        </GridColumn>

                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Had Children : </strong>
                                                                {profile.haveChildren == "true" ? ("Yes") : ("No")}
                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Num Of Children : </strong>
                                                                {profile.numOfChildren}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Family History (Illnesses) : </strong>

                                                                {
                                                                    profile.familyHistory.length > 0 ? (
                                                                        profile.familyHistory.map((data, index) => {
                                                                            return data + " , "
                                                                        })
                                                                    ) : (null)
                                                                }

                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Other Major Conditions : </strong> {profile.otherMajorConditions}  </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    <GridRow>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Medical Condition : </strong>
                                                                {profile.currentMedicalCondition}
                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Allergies : </strong>

                                                                {profile.allergies}

                                                            </label>
                                                        </GridColumn>


                                                    </GridRow>

                                                    <GridRow>
                                                        <div className="HistoryMultipleContainer">
                                                            <Grid>
                                                                <GridRow>
                                                                    <h3> Emergency Contacts </h3>
                                                                </GridRow>
                                                                {profile.emergencyContact.length > 0 ? (
                                                                    profile.emergencyContact.map((emergencyContactdetail, index) => {
                                                                        return [<GridRow>
                                                                            {index + 1 + ") "}
                                                                            <GridColumn width={6}>
                                                                                <label> <strong> Contact Person Name : </strong> {emergencyContactdetail.emergencyContactPersonName}  </label>
                                                                                <hr />
                                                                            </GridColumn>
                                                                            <GridColumn width={9}>
                                                                                <label> <strong> Contact Person Number : </strong> {emergencyContactdetail.emergencyContactPersonNum}  </label>
                                                                                <hr />
                                                                            </GridColumn>

                                                                            <GridColumn width={9}>
                                                                                <label> <strong> Relationship : </strong> {emergencyContactdetail.relationship}  </label>
                                                                                <hr />
                                                                            </GridColumn>

                                                                        </GridRow>]

                                                                    })
                                                                ) : (null)}
                                                            </Grid>
                                                        </div>
                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Closeup Status : </strong>
                                                                {profile.closeupStatus == "true" ? ("Yes") : ("No")}
                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Closeup Action : </strong>
                                                                {profile.closeupAction}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                </Grid>

                                            </div>, <hr />]
                                        })
                                    ) : (<h3 style={{ color: "#6c757d" }}>Not any History Records</h3>)}

                                </div>

                            </div>


                            <div class="col-lg-12">

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Speaking Languages  </label>
                                    <div class="col-sm-12">
                                        <Dropdown
                                            placeholder='Select Speaking Languages'
                                            fluid
                                            selection
                                            multiple
                                            id='ethnicity'
                                            name='ethnicity'
                                            value={this.state.ethnicity}
                                            options={this.state.ethnicities}
                                            onChange={this.handleOnChange}
                                        />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="d-block mb-3">Children  </label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="childrenYes" name="haveChildren" checked={this.state.haveChildren == "true" ? true : false} class="custom-control-input" value="true" onChange={this.handleRadioOnChange} />
                                            <label class="custom-control-label" for="childrenYes">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="childrenNo" name="haveChildren" checked={this.state.haveChildren == "false" ? true : false} class="custom-control-input" value="false" onChange={this.handleRadioOnChange} />
                                            <label class="custom-control-label" for="childrenNo">No</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-12 col-form-label"> How Many Children   </label>
                                    <div class="col-sm-12">
                                        <input id="numOfChildren" name="numOfChildren" value={this.state.numOfChildren} onChange={this.handleChange} class="form-control" type="number" placeholder="How Many Children" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Sexual Orientation  </label>
                                    <div class="col-sm-12">
                                        <Dropdown
                                            placeholder='Sexual Orientation'
                                            fluid
                                            selection
                                            id='sexualOrientation'
                                            name='sexualOrientation'
                                            value={this.state.sexualOrientation}
                                            options={this.state.sexualOrientations}
                                            onChange={this.handleOnChange}
                                        />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Family History (Illnesses)  </label>
                                    <div class="col-sm-12">
                                        <Dropdown
                                            placeholder='Family History'
                                            fluid
                                            selection
                                            multiple
                                            id='familyHistory'
                                            name='familyHistory'
                                            value={this.state.familyHistory}
                                            options={this.state.familyHistories}
                                            onChange={this.handleOnChange}
                                        />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Any other major conditions </label>
                                    <div class="col-sm-12">
                                        <textarea id="otherMajorConditions" name="otherMajorConditions" value={this.state.otherMajorConditions} onChange={this.handleChange} class="form-control" rows="3" placeholder="Any other major conditions"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="d-block mb-3">Are you currently being treated for any medical condition  </label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="currentlyTreatedYes" name="isCurrentlyTreatedMedicalCondition" checked={this.state.isCurrentlyTreatedMedicalCondition == "true" ? true : false} value="true" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                            <label class="custom-control-label" for="currentlyTreatedYes">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="currentlyTreatedNo" name="isCurrentlyTreatedMedicalCondition" checked={this.state.isCurrentlyTreatedMedicalCondition == "false" ? true : false} value="false" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                            <label class="custom-control-label" for="currentlyTreatedNo">No</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Details of current medical condition </label>
                                    <div class="col-sm-12">
                                        <textarea id="currentMedicalCondition" name="currentMedicalCondition" value={this.state.currentMedicalCondition} onChange={this.handleChange} class="form-control" rows="3" placeholder="Details of current medical condition"></textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-12 col-form-label"> Do you have any allergies ? </label>
                                    <div class="col-sm-12">
                                        <textarea id="allergies" name="allergies" value={this.state.allergies} onChange={this.handleChange} class="form-control" rows="3" placeholder="Do you have any allergies"></textarea>
                                    </div>
                                </div>

                                <br />
                                <div class="card-header">
                                    <h4 class="mt-0 header-title" > Emergency Contact Informations </h4>
                                </div><br />

                                {this._createEmergencyContact()}
                                <div style={{ textAlign: "right" }}><button className="btn btn-secondary" onClick={this.addMoreEmergencyContact.bind(this)} type="button">
                                    Add More <i className="fas fa-plus-circle" />
                                </button>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12" style={{ textAlign: "right" }}>
                                        <br /><button disabled={loading ? (true) : (false)} onClick={() => this.handleMedicalProfileSubmit()} class="col-sm-2 btn btn-info " type="button" aria-haspopup="true" aria-expanded="false">
                                            {loading ? (
                                                <Loader
                                                    type="Oval"
                                                    color="#2A3F54"
                                                    height={15}
                                                    width={40}
                                                />
                                            ) : ("Submit")}
                                        </button>
                                    </div>
                                </div>

                                <div>

                                    {
                                        (formErrorStatus.status) ? (
                                            hasMedicalProfileMessage ? (
                                                < div class="ui negative message">
                                                    <div class="header">
                                                        Not Submitted.
                                                    </div>
                                                    <p>{formErrorStatus.message}</p>
                                                </div>
                                            ) : (
                                                    null
                                                )

                                        ) : ((formSuccessState.status) ? (
                                            hasMedicalProfileMessage ? (
                                                < div class="ui success message">
                                                    <div class="header">
                                                        Submitted successfully.
                                                    </div>
                                                    <p>{formSuccessState.message}</p>
                                                </ div>
                                            ) : (
                                                    null
                                                )

                                        ) : (''))
                                    }
                                </div>

                            </div>

                        </Accordion.Content>

                        <Accordion.Title
                            className="card-header darkHeader"
                            active={activeIndex === 4}
                            index={4}
                            onClick={this.handleClick}
                        >
                            <div class="row">

                                <div class="col-lg-6">
                                    <div class="headerAlign">
                                        <Icon name='dropdown headerRightAlign' />
                                        <div class="">
                                            <div className="accordianLeftTitleText">
                                                <h4 class="mt-0 header-title" > Basic Social / Sexual Risk Profile </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="headerRightAlign">
                                        <div className="accordianRightTitleText">
                                            {this.state.sexualRiskHistoryProfiles.length > 0 ? (
                                                <h4> ( {this.state.sexualRiskHistoryProfiles.length} )  previous {this.state.sexualRiskHistoryProfiles.length == 1 ? ("record") : ("records")} available. </h4>
                                            ) : (null)}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </Accordion.Title>

                        <Accordion.Content active={activeIndex === 4}>
                            <br />

                            <div class="col-lg-12">
                                <div class="card HistoryContainer">
                                    <h3 className="HistoryHeading">Hisory Details</h3>

                                    {this.state.sexualRiskHistoryProfiles.length > 0 ? (
                                        this.state.sexualRiskHistoryProfiles.map((profile, index) => {
                                            return [<div className="HistoryGrayHeader"> {index + 1 + ") " + "Created Date : " + formatDate(profile.createdAt)} </div>
                                                , <hr />, <div className="HistoryContainerInner">
                                                <Grid>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Did smoke : </strong>
                                                                {profile.isSmoke == "true" ? ("Yes") : ("No")}
                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> How many? : </strong>
                                                                {profile.timesOfSmoke}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Used Alchohol: </strong>
                                                                {profile.isUseAlchohol == "true" ? ("Yes") : ("No")}
                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> How often? : </strong>
                                                                {profile.capacityOfAlchohol}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Partner Used Drugs: </strong>
                                                                {profile.isPartnerUseDrugs == "true" ? ("Yes") : ("No")}
                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> How much / often? : </strong>
                                                                {profile.capacityOfDrugs}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Is Ever Injected Drugs: </strong>
                                                                {profile.isEverInjectedDrugs == "true" ? ("Yes") : ("No")}
                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Is Like Help Drug Problems: </strong>
                                                                {profile.isLikeHelpDrugProblems == "true" ? ("Yes") : ("No")}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                </Grid>

                                            </div>, <hr />]
                                        })
                                    ) : (<h3 style={{ color: "#6c757d" }}>Not any History Records</h3>)}

                                </div>

                            </div>


                            <div class="col-sm-12">


                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="d-block mb-3"> Do you smoke?  </label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="isSmokeYes" name="isSmoke" checked={this.state.isSmoke == "true" ? true : false} value="true" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                            <label class="custom-control-label" for="isSmokeYes">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="isSmokeNo" name="isSmoke" checked={this.state.isSmoke == "false" ? true : false} value="false" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                            <label class="custom-control-label" for="isSmokeNo">No</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-12 col-form-label"> How many? </label>
                                    <div class="col-sm-12">
                                        <input id="timesOfSmoke" name="timesOfSmoke" value={this.state.timesOfSmoke} onChange={this.handleChange} class="form-control" type="text" placeholder="How many" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="d-block mb-3"> Do you use alchohol?  </label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="isAlchoholYes" name="isUseAlchohol" checked={this.state.isUseAlchohol == "true" ? true : false} value="true" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                            <label class="custom-control-label" for="isAlchoholYes">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="isAlchoholNo" name="isUseAlchohol" checked={this.state.isUseAlchohol == "false" ? true : false} value="false" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                            <label class="custom-control-label" for="isAlchoholNo">No</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-12 col-form-label"> How often? </label>
                                    <div class="col-sm-12">
                                        <input id="capacityOfAlchohol" name="capacityOfAlchohol" value={this.state.capacityOfAlchohol} onChange={this.handleChange} class="form-control" type="text" placeholder="How often" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="d-block mb-3">  Do you or your partner use drugs?  </label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="userDrugsYes" name="isPartnerUseDrugs" checked={this.state.isPartnerUseDrugs == "true" ? true : false} value="true" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                            <label class="custom-control-label" for="userDrugsYes">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="userDrugsNo" name="isPartnerUseDrugs" checked={this.state.isPartnerUseDrugs == "false" ? true : false} value="false" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                            <label class="custom-control-label" for="userDrugsNo">No</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-12 col-form-label"> How much / often? </label>
                                    <div class="col-sm-12">
                                        <input id="capacityOfDrugs" name="capacityOfDrugs" value={this.state.capacityOfDrugs} onChange={this.handleChange} class="form-control" type="text" placeholder="How much / often" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="d-block mb-3">  Have you ever injected drugs?  </label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="isInjectedDrugsYes" name="isEverInjectedDrugs" checked={this.state.isEverInjectedDrugs == "true" ? true : false} value="true" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                            <label class="custom-control-label" for="isInjectedDrugsYes">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="isInjectedDrugsNo" name="isEverInjectedDrugs" checked={this.state.isEverInjectedDrugs == "false" ? true : false} value="false" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                            <label class="custom-control-label" for="isInjectedDrugsNo">No</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="d-block mb-3">  Have you ever had or would like help now with an alchohol or drug problem?  </label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="helpDrugProblemsYes" name="isLikeHelpDrugProblems" checked={this.state.isLikeHelpDrugProblems == "true" ? true : false} value="true" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                            <label class="custom-control-label" for="helpDrugProblemsYes">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="helpDrugProblemsNo" name="isLikeHelpDrugProblems" checked={this.state.isLikeHelpDrugProblems == "false" ? true : false} value="false" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                            <label class="custom-control-label" for="helpDrugProblemsNo">No</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="d-block mb-3">  Would you like to discuss problems related to a rape or emotional/physical abuse?  </label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="likeTODiscussPhysicalProblemsYes" name="isLikeDiscussPhysicalAbuse" checked={this.state.isLikeDiscussPhysicalAbuse == "true" ? true : false} value="true" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                            <label class="custom-control-label" for="likeTODiscussPhysicalProblemsYes">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="likeTODiscussPhysicalProblemsNo" name="isLikeDiscussPhysicalAbuse" checked={this.state.isLikeDiscussPhysicalAbuse == "false" ? true : false} value="false" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                            <label class="custom-control-label" for="likeTODiscussPhysicalProblemsNo">No</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="d-block mb-3">  Are you now or ever have been in a relationship where you have been physically hurt or threatned?  </label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="isRelationshipProblemYes" name="hasThreatnedRelationship" checked={this.state.hasThreatnedRelationship == "true" ? true : false} value="true" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                            <label class="custom-control-label" for="isRelationshipProblemYes">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="isRelationshipProblemNo" name="hasThreatnedRelationship" checked={this.state.hasThreatnedRelationship == "false" ? true : false} value="false" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                            <label class="custom-control-label" for="isRelationshipProblemNo">No</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12" style={{ textAlign: "right" }}>
                                        <br /><button disabled={loading ? (true) : (false)} onClick={() => this.handleSexualRiskProfileSubmit()} class="col-sm-2 btn btn-info " type="button" aria-haspopup="true" aria-expanded="false">
                                            {loading ? (
                                                <Loader
                                                    type="Oval"
                                                    color="#2A3F54"
                                                    height={15}
                                                    width={40}
                                                />
                                            ) : ("Submit")}
                                        </button>
                                    </div>
                                </div>


                                <div>

                                    {
                                        (formErrorStatus.status) ? (
                                            hasSexualRiskProfileMessage ? (
                                                < div class="ui negative message">
                                                    <div class="header">
                                                        Not Submitted.
                                                    </div>
                                                    <p>{formErrorStatus.message}</p>
                                                </div>
                                            ) : (
                                                    null
                                                )

                                        ) : ((formSuccessState.status) ? (
                                            hasSexualRiskProfileMessage ? (
                                                < div class="ui success message">
                                                    <div class="header">
                                                        Submitted successfully.
                                                    </div>
                                                    <p>{formSuccessState.message}</p>
                                                </ div>
                                            ) : (
                                                    null
                                                )

                                        ) : (''))
                                    }
                                </div>

                            </div>

                        </Accordion.Content>

                        <Accordion.Title
                            className="card-header darkHeader"
                            active={activeIndex === 5}
                            index={5}
                            onClick={this.handleClick}
                        >
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="headerAlign">
                                        <Icon name='dropdown headerRightAlign' />
                                        <div class="">
                                            <div className="accordianLeftTitleText">
                                                <h4 class="mt-0 header-title" > Medical Treatment Info </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </Accordion.Title>

                        <Accordion.Content active={activeIndex === 5}>
                            <br />

                            <div class="col-sm-12" >
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-12 col-form-label">  Nature Of Illness </label>
                                    <div class="col-sm-12">
                                        <Dropdown
                                            placeholder='Nature Of Illness'
                                            fluid
                                            selection
                                            id='natureOfIllness'
                                            name='natureOfIllness'
                                            value={this.state.natureOfIllness}
                                            options={this.state.illnessList}
                                            onChange={this.handleOnChange}
                                        />
                                    </div>
                                </div>
                                <br />
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-12 col-form-label">  Medical Treatment Category </label>
                                    <div class="col-sm-12">
                                        <Dropdown
                                            placeholder='Medical Treatment Category'
                                            fluid
                                            selection
                                            id='medicalTreatmentCategory'
                                            name='medicalTreatmentCategory'
                                            value={this.state.medicalTreatmentCategory}
                                            options={this.state.medicalTreatmentCategories}
                                            onChange={this.handleOnChange}
                                        />
                                    </div>
                                </div>
                                <br />

                                <div class="card">
                                    <div class="card-body" >

                                        <div class="card-header">
                                            <h4 class="mt-0 header-title" > Medical Treatment Records </h4>
                                        </div><br />


                                        {this._createTreatmentRecord()}
                                        <div style={{ textAlign: "right" }}><button className="btn btn-secondary" onClick={this._addMoreTreatmentRecord.bind(this)} type="button">
                                            Add More <i className="fas fa-plus-circle" />
                                        </button>
                                        </div>

                                    </div>
                                </div>


                                <div class="form-group row">
                                    <div class="col-sm-12" style={{ textAlign: "right" }}>
                                        <br /><button disabled={loading ? (true) : (false)} onClick={() => this.handleMedicalTreatmentProfileSubmit()} class="col-sm-2 btn btn-info " type="button" aria-haspopup="true" aria-expanded="false">
                                            {loading ? (
                                                <Loader
                                                    type="Oval"
                                                    color="#2A3F54"
                                                    height={15}
                                                    width={40}
                                                />
                                            ) : ("Submit")}
                                        </button>
                                    </div>
                                </div>

                                <div>

                                    {
                                        (formErrorStatus.status) ? (
                                            hasMedicalTreatmentProfileMessage ? (
                                                < div class="ui negative message">
                                                    <div class="header">
                                                        Not Submitted.
                                                    </div>
                                                    <p>{formErrorStatus.message}</p>
                                                </div>
                                            ) : (
                                                    null
                                                )

                                        ) : ((formSuccessState.status) ? (
                                            hasMedicalTreatmentProfileMessage ? (
                                                < div class="ui success message">
                                                    <div class="header">
                                                        Submitted successfully.
                                                    </div>
                                                    <p>{formSuccessState.message}</p>
                                                </ div>
                                            ) : (
                                                    null
                                                )

                                        ) : (''))
                                    }
                                </div>

                            </div>

                        </Accordion.Content>

                        <Accordion.Title
                            className="card-header darkHeader"
                            active={activeIndex === 6}
                            index={6}
                            onClick={this.handleClick}
                        >
                            <div class="row">

                                <div class="col-lg-6">
                                    <div class="headerAlign">
                                        <Icon name='dropdown headerRightAlign' />
                                        <div class="">
                                            <div className="accordianLeftTitleText">
                                                <h4 class="mt-0 header-title" > Past Medical Treatment Info </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="headerRightAlign">
                                        <div className="accordianRightTitleText">
                                            {this.state.medicalTreatmentDisplayHistoryProfiles.length > 0 ? (
                                                <h4> ( {this.state.medicalTreatmentDisplayHistoryProfiles.length} )  previous {this.state.medicalTreatmentDisplayHistoryProfiles.length == 1 ? ("record") : ("records")} available. </h4>
                                            ) : (null)}
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </Accordion.Title>

                        <Accordion.Content active={activeIndex === 6}>
                            <br />

                            <div class="col-lg-12">
                                <div class="card HistoryContainer">
                                    <h3 className="HistoryHeading">Hisory Details</h3>

                                    {this.state.medicalTreatmentDisplayHistoryProfiles.length > 0 ? (
                                        this.state.medicalTreatmentDisplayHistoryProfiles.map((profile, index) => {
                                            return [<div className="HistoryGrayHeader"> {index + 1 + ") " + "Created Date : " + formatDate(profile.createdAt)} </div>
                                                , <hr />, <div className="HistoryContainerInner">
                                                <Grid>
                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Nature Of Illness : </strong>
                                                                {profile.pastNatureOfIllness}
                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Treatment Category : </strong>
                                                                {profile.medicalPastTreatmentCategory}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    <GridRow>
                                                        <div className="HistoryMultipleContainer">
                                                            <Grid>
                                                                <GridRow>
                                                                    <h3> Treatment Records </h3>
                                                                </GridRow>
                                                                {profile.medicalPastTreatmentRecords.length > 0 ? (
                                                                    profile.medicalPastTreatmentRecords.map((treatmentdetail, index) => {
                                                                        return [<GridRow>{index + 1 + ") "}</GridRow>, <GridRow>

                                                                            <GridColumn width={6}>
                                                                                <label> <strong> Physician Name : </strong> {treatmentdetail.physicianName}  </label>
                                                                                <hr />
                                                                            </GridColumn>
                                                                            <GridColumn width={9}>
                                                                                <label> <strong> Physician Contact Number : </strong> {treatmentdetail.physicianPhone}  </label>
                                                                                <hr />
                                                                            </GridColumn>
                                                                        </GridRow>,

                                                                        <GridRow>
                                                                            <GridColumn width={16}>
                                                                                <label> <strong> Physician Address : </strong> {treatmentdetail.physicianAddress}  </label>
                                                                                <hr />
                                                                            </GridColumn>
                                                                        </GridRow>,

                                                                        <GridRow>
                                                                            {treatmentdetail.prescribionRecords.length > 0 ? (
                                                                                treatmentdetail.prescribionRecords.map((prescribe, index) => {
                                                                                    return [

                                                                                        index + 1 + " ) ", <GridColumn width={6}>
                                                                                            <label> <strong> Prescribed Medication : </strong> {prescribe.prescribedMedication}  </label>
                                                                                            <hr />
                                                                                        </GridColumn>,
                                                                                        <GridColumn width={9}>
                                                                                            <label> <strong> Date : </strong> {prescribe.dateStarted}  </label>
                                                                                            <hr />
                                                                                        </GridColumn>,

                                                                                        <GridColumn width={6}>
                                                                                            <label> <strong> Dosage : </strong> {prescribe.dosage}  </label>
                                                                                            <hr />
                                                                                        </GridColumn>,
                                                                                        <GridColumn width={9}>
                                                                                            <label> <strong> frequency : </strong> {prescribe.frequency}  </label>
                                                                                            <hr />
                                                                                        </GridColumn>,

                                                                                        <GridColumn width={16}>
                                                                                            <label> <strong> Comments : </strong> {prescribe.comments}  </label>
                                                                                            <hr />
                                                                                        </GridColumn>]

                                                                                })

                                                                            ) : (null)}



                                                                        </GridRow>]
                                                                    })
                                                                ) : (null)}
                                                            </Grid>
                                                        </div>
                                                    </GridRow>

                                                </Grid>

                                            </div>, <hr />]
                                        })
                                    ) : (<h3 style={{ color: "#6c757d" }}>Not any History Records</h3>)}

                                </div>

                            </div>


                            <div class="col-lg-12" >

                                <div class="card-header">
                                    <h4 class="mt-0 header-title" > Past Medical Treatment Record </h4>
                                </div><br />

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-12 col-form-label">  Nature Of Illness </label>
                                    <div class="col-sm-12">
                                        <Dropdown
                                            placeholder='Nature Of Illness'
                                            fluid
                                            selection
                                            id='pastNatureOfIllness'
                                            name='pastNatureOfIllness'
                                            value={this.state.pastNatureOfIllness}
                                            options={this.state.illnessList}
                                            onChange={this.handleOnChange}
                                        />
                                    </div>
                                </div>
                                <br />
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-sm-12 col-form-label">  Medical Treatment Category </label>
                                    <div class="col-sm-12">
                                        <Dropdown
                                            placeholder='Medical Treatment Category'
                                            fluid
                                            selection
                                            id='medicalPastTreatmentCategory'
                                            name='medicalPastTreatmentCategory'
                                            value={this.state.medicalPastTreatmentCategory}
                                            options={this.state.medicalTreatmentCategories}
                                            onChange={this.handleOnChange}
                                        />
                                    </div>
                                </div>
                                <br />

                                {this._createPastTreatmentRecord()}
                                <div style={{ textAlign: "right" }}><button className="btn btn-secondary" onClick={this._addMorePastTreatmentRecord.bind(this)} type="button">
                                    Add More <i className="fas fa-plus-circle" />
                                </button>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12" style={{ textAlign: "right" }}>
                                        <br /><button disabled={loading ? (true) : (false)} onClick={() => this.handleMedicalTreatmentPastProfileSubmit()} class="col-sm-2 btn btn-info " type="button" aria-haspopup="true" aria-expanded="false">
                                            {loading ? (
                                                <Loader
                                                    type="Oval"
                                                    color="#2A3F54"
                                                    height={15}
                                                    width={40}
                                                />
                                            ) : ("Submit")}
                                        </button>
                                    </div>
                                </div>

                                <div>

                                    {
                                        (formErrorStatus.status) ? (
                                            hasMedicalTreatmentPastProfileMessage ? (
                                                < div class="ui negative message">
                                                    <div class="header">
                                                        Not Submitted.
                                                    </div>
                                                    <p>{formErrorStatus.message}</p>
                                                </div>
                                            ) : (
                                                    null
                                                )

                                        ) : ((formSuccessState.status) ? (
                                            hasMedicalTreatmentPastProfileMessage ? (
                                                < div class="ui success message">
                                                    <div class="header">
                                                        Submitted successfully.
                                                    </div>
                                                    <p>{formSuccessState.message}</p>
                                                </ div>
                                            ) : (
                                                    null
                                                )

                                        ) : (''))
                                    }
                                </div>

                            </div>

                        </Accordion.Content>

                        <Accordion.Title
                            className="card-header darkHeader"
                            active={activeIndex === 7}
                            index={7}
                            onClick={this.handleClick}
                        >
                            <div class="row">

                                <div class="col-lg-6">
                                    <div class="headerAlign">
                                        <Icon name='dropdown headerRightAlign' />
                                        <div class="">
                                            <div className="accordianLeftTitleText">
                                                <h4 class="mt-0 header-title" > Counseling Info </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="headerRightAlign">
                                        <div className="accordianRightTitleText">
                                            {this.state.counselingPastRecordDisplayProfiles.length > 0 ? (
                                                <h4> ( {this.state.counselingPastRecordDisplayProfiles.length} )  previous {this.state.counselingPastRecordDisplayProfiles.length == 1 ? ("record") : ("records")} available. </h4>
                                            ) : (null)}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </Accordion.Title>

                        <Accordion.Content active={activeIndex === 7}>

                            <div class="col-lg-12">
                                <div class="card HistoryContainer">
                                    <h3 className="HistoryHeading">Hisory Details</h3>

                                    {this.state.counselingPastRecordDisplayProfiles.length > 0 ? (
                                        this.state.counselingPastRecordDisplayProfiles.map((profile, index) => {
                                            return [<div className="HistoryGrayHeader"> {index + 1 + ") " + "Created Date : " + formatDate(profile.createdAt)} </div>
                                                , <hr />, <div className="HistoryContainerInner">
                                                <Grid>

                                                    <GridRow>
                                                        <div className="HistoryMultipleContainer">
                                                            <Grid>
                                                                <GridRow>
                                                                    <h3> Counseller Informations </h3>
                                                                </GridRow>
                                                                {profile.counsellerInformations.length > 0 ? (
                                                                    profile.counsellerInformations.map((counsellerInformation, index) => {
                                                                        return [<GridRow>
                                                                            {index + 1 + ") "}
                                                                            <GridColumn width={6}>
                                                                                <label> <strong> Counseller Name : </strong> {counsellerInformation.counsellerName}  </label>
                                                                                <hr />
                                                                            </GridColumn>
                                                                            <GridColumn width={9}>
                                                                                <label> <strong> Counseller Contact Number : </strong> {counsellerInformation.counsellerPhone}  </label>
                                                                                <hr />
                                                                            </GridColumn>

                                                                            <GridColumn width={9}>
                                                                                <label> <strong> Counseller Address : </strong> {counsellerInformation.counsellerAddress}  </label>
                                                                                <hr />
                                                                            </GridColumn>

                                                                        </GridRow>]

                                                                    })
                                                                ) : (null)}
                                                            </Grid>
                                                        </div>
                                                    </GridRow>

                                                    <GridRow>

                                                        <GridColumn width={16}>
                                                            <div>
                                                                <h3> Counselling Symptoms </h3><br />

                                                                {
                                                                    profile.counsellingSymptoms.SleepingProblems != undefined ? (
                                                                        <div className="col-lg-12">
                                                                            <Grid>
                                                                                <GridRow width={16}>
                                                                                    <GridColumn width={6}>
                                                                                        <strong> Symptom : </strong> Sleeping  Problem
                                                                                    </GridColumn>
                                                                                    <GridColumn width={9}>
                                                                                        <strong> Expranation : </strong> {profile.counsellingSymptoms.SleepingProblems}
                                                                                    </GridColumn>
                                                                                </GridRow>
                                                                            </Grid>
                                                                        </div>
                                                                    ) : (null)
                                                                }

                                                                {
                                                                    profile.counsellingSymptoms.DestructiveBehaviours != undefined ? (
                                                                        <div className="col-lg-12">
                                                                            <Grid>
                                                                                <GridRow width={16}>
                                                                                    <GridColumn width={6}>
                                                                                        <strong> Symptom : </strong> Destructive Behaviours
                                                                                    </GridColumn>
                                                                                    <GridColumn width={9}>
                                                                                        <strong> Expranation : </strong> {profile.counsellingSymptoms.DestructiveBehaviours}
                                                                                    </GridColumn>
                                                                                </GridRow>
                                                                            </Grid>
                                                                        </div>
                                                                    ) : (null)
                                                                }

                                                                {
                                                                    profile.counsellingSymptoms.EatingProblems != undefined ? (
                                                                        <div className="col-lg-12">
                                                                            <Grid>
                                                                                <GridRow width={16}>
                                                                                    <GridColumn width={6}>
                                                                                        <strong> Symptom : </strong> Eating Problems
                                                                                    </GridColumn>
                                                                                    <GridColumn width={9}>
                                                                                        <strong> Expranation : </strong> {profile.counsellingSymptoms.EatingProblems}
                                                                                    </GridColumn>
                                                                                </GridRow>
                                                                            </Grid>
                                                                        </div>
                                                                    ) : (null)
                                                                }

                                                                {
                                                                    profile.counsellingSymptoms.StrangeThoughts != undefined ? (
                                                                        <div className="col-lg-12">
                                                                            <Grid>
                                                                                <GridRow width={16}>
                                                                                    <GridColumn width={6}>
                                                                                        <strong> Symptom : </strong> Strange Thoughts
                                                                                    </GridColumn>
                                                                                    <GridColumn width={9}>
                                                                                        <strong> Expranation : </strong> {profile.counsellingSymptoms.StrangeThoughts}
                                                                                    </GridColumn>
                                                                                </GridRow>
                                                                            </Grid>
                                                                        </div>
                                                                    ) : (null)
                                                                }

                                                                {
                                                                    profile.counsellingSymptoms.LackOfMotivation != undefined ? (
                                                                        <div className="col-lg-12">
                                                                            <Grid>
                                                                                <GridRow width={16}>
                                                                                    <GridColumn width={6}>
                                                                                        <strong> Symptom : </strong> Lack Of Motivation
                                                                                    </GridColumn>
                                                                                    <GridColumn width={9}>
                                                                                        <strong> Expranation : </strong> {profile.counsellingSymptoms.LackOfMotivation}
                                                                                    </GridColumn>
                                                                                </GridRow>
                                                                            </Grid>
                                                                        </div>
                                                                    ) : (null)
                                                                }

                                                                {
                                                                    profile.counsellingSymptoms.Overactive != undefined ? (
                                                                        <div className="col-lg-12">
                                                                            <Grid>
                                                                                <GridRow width={16}>
                                                                                    <GridColumn width={6}>
                                                                                        <strong> Symptom : </strong> Overactive
                                                                                    </GridColumn>
                                                                                    <GridColumn width={9}>
                                                                                        <strong> Expranation : </strong> {profile.counsellingSymptoms.Overactive}
                                                                                    </GridColumn>
                                                                                </GridRow>
                                                                            </Grid>
                                                                        </div>
                                                                    ) : (null)
                                                                }

                                                                {
                                                                    profile.counsellingSymptoms.SelfHarm != undefined ? (
                                                                        <div className="col-lg-12">
                                                                            <Grid>
                                                                                <GridRow width={16}>
                                                                                    <GridColumn width={6}>
                                                                                        <strong> Symptom : </strong> Self-harm (cutting, burning )
                                                                                    </GridColumn>
                                                                                    <GridColumn width={9}>
                                                                                        <strong> Expranation : </strong> {profile.counsellingSymptoms.SelfHarm}
                                                                                    </GridColumn>
                                                                                </GridRow>
                                                                            </Grid>
                                                                        </div>
                                                                    ) : (null)
                                                                }

                                                                {
                                                                    profile.counsellingSymptoms.LackOfFocus != undefined ? (
                                                                        <div className="col-lg-12">
                                                                            <Grid>
                                                                                <GridRow width={16}>
                                                                                    <GridColumn width={6}>
                                                                                        <strong> Symptom : </strong> Lack of focus, concentration
                                                                                    </GridColumn>
                                                                                    <GridColumn width={9}>
                                                                                        <strong> Expranation : </strong> {profile.counsellingSymptoms.LackOfFocus}
                                                                                    </GridColumn>
                                                                                </GridRow>
                                                                            </Grid>
                                                                        </div>
                                                                    ) : (null)
                                                                }

                                                                {
                                                                    profile.counsellingSymptoms.StrangeBehaviours != undefined ? (
                                                                        <div className="col-lg-12">
                                                                            <Grid>
                                                                                <GridRow width={16}>
                                                                                    <GridColumn width={6}>
                                                                                        <strong> Symptom : </strong> Strange Behaviours
                                                                                    </GridColumn>
                                                                                    <GridColumn width={9}>
                                                                                        <strong> Expranation : </strong> {profile.counsellingSymptoms.StrangeBehaviours}
                                                                                    </GridColumn>
                                                                                </GridRow>
                                                                            </Grid>
                                                                        </div>
                                                                    ) : (null)
                                                                }

                                                                {
                                                                    profile.counsellingSymptoms.Lying != undefined ? (
                                                                        <div className="col-lg-12">
                                                                            <Grid>
                                                                                <GridRow width={16}>
                                                                                    <GridColumn width={6}>
                                                                                        <strong> Symptom : </strong> Lying
                                                                                    </GridColumn>
                                                                                    <GridColumn width={9}>
                                                                                        <strong> Expranation : </strong> {profile.counsellingSymptoms.Lying}
                                                                                    </GridColumn>
                                                                                </GridRow>
                                                                            </Grid>
                                                                        </div>
                                                                    ) : (null)
                                                                }

                                                                {
                                                                    profile.counsellingSymptoms.Withdrawn != undefined ? (
                                                                        <div className="col-lg-12">
                                                                            <Grid>
                                                                                <GridRow width={16}>
                                                                                    <GridColumn width={6}>
                                                                                        <strong> Symptom : </strong> Withdrawn
                                                                                    </GridColumn>
                                                                                    <GridColumn width={9}>
                                                                                        <strong> Expranation : </strong> {profile.counsellingSymptoms.Withdrawn}
                                                                                    </GridColumn>
                                                                                </GridRow>
                                                                            </Grid>
                                                                        </div>
                                                                    ) : (null)
                                                                }

                                                                {
                                                                    profile.counsellingSymptoms.Fearful != undefined ? (
                                                                        <div className="col-lg-12">
                                                                            <Grid>
                                                                                <GridRow width={16}>
                                                                                    <GridColumn width={6}>
                                                                                        <strong> Symptom : </strong> Fearful/anxious/worry
                                                                                    </GridColumn>
                                                                                    <GridColumn width={9}>
                                                                                        <strong> Expranation : </strong> {profile.counsellingSymptoms.Fearful}
                                                                                    </GridColumn>
                                                                                </GridRow>
                                                                            </Grid>
                                                                        </div>
                                                                    ) : (null)
                                                                }

                                                                {
                                                                    profile.counsellingSymptoms.Restlessness != undefined ? (
                                                                        <div className="col-lg-12">
                                                                            <Grid>
                                                                                <GridRow width={16}>
                                                                                    <GridColumn width={6}>
                                                                                        <strong> Symptom : </strong> Restlessness
                                                                                    </GridColumn>
                                                                                    <GridColumn width={9}>
                                                                                        <strong> Expranation : </strong> {profile.counsellingSymptoms.Restlessness}
                                                                                    </GridColumn>
                                                                                </GridRow>
                                                                            </Grid>
                                                                        </div>
                                                                    ) : (null)
                                                                }

                                                                {
                                                                    profile.counsellingSymptoms.SuicidalThoughts != undefined ? (
                                                                        <div className="col-lg-12">
                                                                            <Grid>
                                                                                <GridRow width={16}>
                                                                                    <GridColumn width={6}>
                                                                                        <strong> Symptom : </strong> Suicidal Thoughts
                                                                                    </GridColumn>
                                                                                    <GridColumn width={9}>
                                                                                        <strong> Expranation : </strong> {profile.counsellingSymptoms.SuicidalThoughts}
                                                                                    </GridColumn>
                                                                                </GridRow>
                                                                            </Grid>
                                                                        </div>
                                                                    ) : (null)
                                                                }

                                                                {
                                                                    profile.counsellingSymptoms.SuicidalAttempts != undefined ? (
                                                                        <div className="col-lg-12">
                                                                            <Grid>
                                                                                <GridRow width={16}>
                                                                                    <GridColumn width={6}>
                                                                                        <strong> Symptom : </strong> Suicidal Attempts
                                                                                    </GridColumn>
                                                                                    <GridColumn width={9}>
                                                                                        <strong> Expranation : </strong> {profile.counsellingSymptoms.SuicidalAttempts}
                                                                                    </GridColumn>
                                                                                </GridRow>
                                                                            </Grid>
                                                                        </div>
                                                                    ) : (null)
                                                                }


                                                            </div>
                                                        </GridColumn>

                                                    </GridRow>

                                                    <hr />
                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Any suicidal thoughts or feelings? : </strong>
                                                                {profile.suicidalThoughts == "true" ? ("Yes") : ("No")}
                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Provided details : </strong>
                                                                {profile.suicidalThoughtsDetail}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Previously diagnosed with the mental health condition : </strong>
                                                                {profile.mentalHealthConditions == "true" ? ("Yes") : ("No")}
                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Provided details : </strong>
                                                                {profile.mentalHealthConditionsDetail}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Took medication for mental health conditions : </strong>
                                                                {profile.mentalHealthMedications == "true" ? ("Yes") : ("No")}
                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Provided details : </strong>
                                                                {profile.mentalHealthMedicationsDetail}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Have you been emotionally abused by someone to you : </strong>
                                                                {profile.abuses == "true" ? ("Yes") : ("No")}
                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Provided details : </strong>
                                                                {profile.abusesDetail}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                </Grid>

                                            </div>, <hr />]
                                        })
                                    ) : (<h3 style={{ color: "#6c757d" }}>Not any History Records</h3>)}

                                </div>

                            </div>


                            <br />

                            <div class="col-lg-12" >

                                <div >

                                    {this._createCounsellerRecord()}
                                    <div style={{ textAlign: "right" }}><button className="btn btn-secondary" onClick={this._addMoreCounsellerRecord.bind(this)} type="button">
                                        Add More <i className="fas fa-plus-circle" />
                                    </button>
                                    </div>


                                    <div class="card">
                                        <div class="card-body" >
                                            <div class="card-header" >
                                                <h4 class="mt-0 header-title" > Counseling Info </h4>
                                            </div><br />

                                            <div class="col-sm-12">
                                                <h4 > Select one or more of the symptoms that may apply to the patient </h4>
                                            </div>
                                            <div>
                                                <br />
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <Checkbox id="SleepingProblems" name="SleepingProblems" label='Sleeping Problems' value="" onChange={this.handleSymptomCheckOnChange}
                                                            checked={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.SleepingProblems != undefined ? true : false) : (false)}
                                                        />
                                                    </div>
                                                </div>

                                                {this.state.counsellingSymptoms != undefined ? (
                                                    this.state.counsellingSymptoms.SleepingProblems != undefined ? (
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Further details </label>
                                                            <div class="col-sm-12">
                                                                <textarea id="SleepingProblems" name="SleepingProblemsDetails"
                                                                    value={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.SleepingProblems != "" ? (this.state.counsellingSymptoms.SleepingProblems) : ('')) : ('')} onChange={this.handleSymptomChange} class="form-control" rows="3" placeholder="Further details"></textarea>
                                                            </div>
                                                        </div>
                                                    ) : (null)
                                                ) : (null)}
                                            </div>

                                            <div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <Checkbox id="DestructiveBehaviours" name="DestructiveBehaviours" label='Destructive Behaviours' value="" onChange={this.handleSymptomCheckOnChange}
                                                            checked={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.DestructiveBehaviours != undefined ? true : false) : (false)}
                                                        />
                                                    </div>
                                                </div>

                                                {this.state.counsellingSymptoms != undefined ? (
                                                    this.state.counsellingSymptoms.DestructiveBehaviours != undefined ? (
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Further details </label>
                                                            <div class="col-sm-12">
                                                                <textarea id="DestructiveBehaviours" name="DestructiveBehavioursDetails"
                                                                    value={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.DestructiveBehaviours != "" ? (this.state.counsellingSymptoms.DestructiveBehaviours) : ('')) : ('')} onChange={this.handleSymptomChange} class="form-control" rows="3" placeholder="Further details"></textarea>
                                                            </div>
                                                        </div>
                                                    ) : (null)
                                                ) : (null)}
                                            </div>

                                            <div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <Checkbox id="EatingProblems" name="EatingProblems" label='Eating Problems' value="" onChange={this.handleSymptomCheckOnChange}
                                                            checked={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.EatingProblems != undefined ? true : false) : (false)}
                                                        />
                                                    </div>
                                                </div>

                                                {this.state.counsellingSymptoms != undefined ? (
                                                    this.state.counsellingSymptoms.EatingProblems != undefined ? (
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Further details </label>
                                                            <div class="col-sm-12">
                                                                <textarea id="EatingProblems" name="EatingProblemsDetails"
                                                                    value={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.EatingProblems != "" ? (this.state.counsellingSymptoms.EatingProblems) : ('')) : ('')} onChange={this.handleSymptomChange} class="form-control" rows="3" placeholder="Further details"></textarea>
                                                            </div>
                                                        </div>
                                                    ) : (null)
                                                ) : (null)}
                                            </div>

                                            <div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <Checkbox id="StrangeThoughts" name="StrangeThoughts" label='Strange Thoughts' value="" onChange={this.handleSymptomCheckOnChange}
                                                            checked={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.StrangeThoughts != undefined ? true : false) : (false)}
                                                        />
                                                    </div>
                                                </div>

                                                {this.state.counsellingSymptoms != undefined ? (
                                                    this.state.counsellingSymptoms.StrangeThoughts != undefined ? (
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Further details </label>
                                                            <div class="col-sm-12">
                                                                <textarea id="StrangeThoughts" name="StrangeThoughtsDetails"
                                                                    value={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.StrangeThoughts != "" ? (this.state.counsellingSymptoms.StrangeThoughts) : ('')) : ('')} onChange={this.handleSymptomChange} class="form-control" rows="3" placeholder="Further details"></textarea>
                                                            </div>
                                                        </div>
                                                    ) : (null)
                                                ) : (null)}
                                            </div>

                                            <div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <Checkbox id="LackOfMotivation" name="LackOfMotivation" label='Lack Of Motivation' value="" onChange={this.handleSymptomCheckOnChange}
                                                            checked={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.LackOfMotivation != undefined ? true : false) : (false)}
                                                        />
                                                    </div>
                                                </div>

                                                {this.state.counsellingSymptoms != undefined ? (
                                                    this.state.counsellingSymptoms.LackOfMotivation != undefined ? (
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Further details </label>
                                                            <div class="col-sm-12">
                                                                <textarea id="LackOfMotivation" name="LackOfMotivationDetails"
                                                                    value={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.LackOfMotivation != "" ? (this.state.counsellingSymptoms.LackOfMotivation) : ('')) : ('')} onChange={this.handleSymptomChange} class="form-control" rows="3" placeholder="Further details"></textarea>
                                                            </div>
                                                        </div>
                                                    ) : (null)
                                                ) : (null)}
                                            </div>

                                            <div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <Checkbox id="Overactive" name="Overactive" label='Overactive' value="" onChange={this.handleSymptomCheckOnChange}
                                                            checked={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.Overactive != undefined ? true : false) : (false)}
                                                        />
                                                    </div>
                                                </div>

                                                {this.state.counsellingSymptoms != undefined ? (
                                                    this.state.counsellingSymptoms.Overactive != undefined ? (
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Further details </label>
                                                            <div class="col-sm-12">
                                                                <textarea id="Overactive" name="OveractiveDetails"
                                                                    value={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.Overactive != "" ? (this.state.counsellingSymptoms.Overactive) : ('')) : ('')} onChange={this.handleSymptomChange} class="form-control" rows="3" placeholder="Further details"></textarea>
                                                            </div>
                                                        </div>
                                                    ) : (null)
                                                ) : (null)}
                                            </div>

                                            <div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <Checkbox id="SelfHarm" name="SelfHarm" label='Self-Harm (cutting, burning etc.)' value="" onChange={this.handleSymptomCheckOnChange}
                                                            checked={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.SelfHarm != undefined ? true : false) : (false)}
                                                        />
                                                    </div>
                                                </div>

                                                {this.state.counsellingSymptoms != undefined ? (
                                                    this.state.counsellingSymptoms.SelfHarm != undefined ? (
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Further details </label>
                                                            <div class="col-sm-12">
                                                                <textarea id="SelfHarm" name="SelfHarmDetails"
                                                                    value={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.SelfHarm != "" ? (this.state.counsellingSymptoms.SelfHarm) : ('')) : ('')} onChange={this.handleSymptomChange} class="form-control" rows="3" placeholder="Further details"></textarea>
                                                            </div>
                                                        </div>
                                                    ) : (null)
                                                ) : (null)}
                                            </div>

                                            <div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <Checkbox id="LackOfFocus" name="LackOfFocus" label='Lack of focus, concentration' value="" onChange={this.handleSymptomCheckOnChange}
                                                            checked={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.LackOfFocus != undefined ? true : false) : (false)}
                                                        />
                                                    </div>
                                                </div>

                                                {this.state.counsellingSymptoms != undefined ? (
                                                    this.state.counsellingSymptoms.LackOfFocus != undefined ? (
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Further details </label>
                                                            <div class="col-sm-12">
                                                                <textarea id="LackOfFocus" name="LackOfFocusDetails"
                                                                    value={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.LackOfFocus != "" ? (this.state.counsellingSymptoms.LackOfFocus) : ('')) : ('')} onChange={this.handleSymptomChange} class="form-control" rows="3" placeholder="Further details"></textarea>
                                                            </div>
                                                        </div>
                                                    ) : (null)
                                                ) : (null)}
                                            </div>

                                            <div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <Checkbox id="StrangeBehaviours" name="StrangeBehaviours" label='Strange Behaviours' value="" onChange={this.handleSymptomCheckOnChange}
                                                            checked={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.StrangeBehaviours != undefined ? true : false) : (false)}
                                                        />
                                                    </div>
                                                </div>

                                                {this.state.counsellingSymptoms != undefined ? (
                                                    this.state.counsellingSymptoms.StrangeBehaviours != undefined ? (
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Further details </label>
                                                            <div class="col-sm-12">
                                                                <textarea id="StrangeBehaviours" name="StrangeBehavioursDetails"
                                                                    value={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.StrangeBehaviours != "" ? (this.state.counsellingSymptoms.StrangeBehaviours) : ('')) : ('')} onChange={this.handleSymptomChange} class="form-control" rows="3" placeholder="Further details"></textarea>
                                                            </div>
                                                        </div>
                                                    ) : (null)
                                                ) : (null)}
                                            </div>

                                            <div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <Checkbox id="Lying" name="Lying" label='Lying' value="" onChange={this.handleSymptomCheckOnChange}
                                                            checked={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.Lying != undefined ? true : false) : (false)}
                                                        />
                                                    </div>
                                                </div>

                                                {this.state.counsellingSymptoms != undefined ? (
                                                    this.state.counsellingSymptoms.Lying != undefined ? (
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Further details </label>
                                                            <div class="col-sm-12">
                                                                <textarea id="Lying" name="LyingDetails"
                                                                    value={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.Lying != "" ? (this.state.counsellingSymptoms.Lying) : ('')) : ('')} onChange={this.handleSymptomChange} class="form-control" rows="3" placeholder="Further details"></textarea>
                                                            </div>
                                                        </div>
                                                    ) : (null)
                                                ) : (null)}
                                            </div>

                                            <div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <Checkbox id="Withdrawn" name="Withdrawn" label='Withdrawn' value="" onChange={this.handleSymptomCheckOnChange}
                                                            checked={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.Withdrawn != undefined ? true : false) : (false)}
                                                        />
                                                    </div>
                                                </div>

                                                {this.state.counsellingSymptoms != undefined ? (
                                                    this.state.counsellingSymptoms.Withdrawn != undefined ? (
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Further details </label>
                                                            <div class="col-sm-12">
                                                                <textarea id="Withdrawn" name="WithdrawnDetails"
                                                                    value={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.Withdrawn != "" ? (this.state.counsellingSymptoms.Withdrawn) : ('')) : ('')} onChange={this.handleSymptomChange} class="form-control" rows="3" placeholder="Further details"></textarea>
                                                            </div>
                                                        </div>
                                                    ) : (null)
                                                ) : (null)}
                                            </div>

                                            <div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <Checkbox id="Fearful" name="Fearful" label='Fearful/anxious/worry' value="" onChange={this.handleSymptomCheckOnChange}
                                                            checked={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.Fearful != undefined ? true : false) : (false)}
                                                        />
                                                    </div>
                                                </div>

                                                {this.state.counsellingSymptoms != undefined ? (
                                                    this.state.counsellingSymptoms.Fearful != undefined ? (
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Further details </label>
                                                            <div class="col-sm-12">
                                                                <textarea id="Fearful" name="FearfulDetails"
                                                                    value={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.Fearful != "" ? (this.state.counsellingSymptoms.Fearful) : ('')) : ('')} onChange={this.handleSymptomChange} class="form-control" rows="3" placeholder="Further details"></textarea>
                                                            </div>
                                                        </div>
                                                    ) : (null)
                                                ) : (null)}
                                            </div>

                                            <div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <Checkbox id="Restlessness" name="Restlessness" label='Restlessness' value="" onChange={this.handleSymptomCheckOnChange}
                                                            checked={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.Restlessness != undefined ? true : false) : (false)}
                                                        />
                                                    </div>
                                                </div>

                                                {this.state.counsellingSymptoms != undefined ? (
                                                    this.state.counsellingSymptoms.Restlessness != undefined ? (
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Further details </label>
                                                            <div class="col-sm-12">
                                                                <textarea id="Restlessness" name="RestlessnessDetails"
                                                                    value={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.Restlessness != "" ? (this.state.counsellingSymptoms.Restlessness) : ('')) : ('')} onChange={this.handleSymptomChange} class="form-control" rows="3" placeholder="Further details"></textarea>
                                                            </div>
                                                        </div>
                                                    ) : (null)
                                                ) : (null)}
                                            </div>

                                            <div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <Checkbox id="SuicidalThoughts" name="SuicidalThoughts" label='Suicidal Thoughts' value="" onChange={this.handleSymptomCheckOnChange}
                                                            checked={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.SuicidalThoughts != undefined ? true : false) : (false)}
                                                        />
                                                    </div>
                                                </div>

                                                {this.state.counsellingSymptoms != undefined ? (
                                                    this.state.counsellingSymptoms.SuicidalThoughts != undefined ? (
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Further details </label>
                                                            <div class="col-sm-12">
                                                                <textarea id="SuicidalThoughts" name="SuicidalThoughtsDetails"
                                                                    value={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.SuicidalThoughts != "" ? (this.state.counsellingSymptoms.SuicidalThoughts) : ('')) : ('')} onChange={this.handleSymptomChange} class="form-control" rows="3" placeholder="Further details"></textarea>
                                                            </div>
                                                        </div>
                                                    ) : (null)
                                                ) : (null)}
                                            </div>

                                            <div>
                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <Checkbox id="SuicidalAttempts" name="SuicidalAttempts" label='Suicidal Attempts' value="" onChange={this.handleSymptomCheckOnChange}
                                                            checked={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.SuicidalAttempts != undefined ? true : false) : (false)}
                                                        />
                                                    </div>
                                                </div>

                                                {this.state.counsellingSymptoms != undefined ? (
                                                    this.state.counsellingSymptoms.SuicidalAttempts != undefined ? (
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Further details </label>
                                                            <div class="col-sm-12">
                                                                <textarea id="SuicidalAttempts" name="SuicidalAttemptsDetails"
                                                                    value={this.state.counsellingSymptoms != undefined ? (this.state.counsellingSymptoms.SuicidalAttempts != "" ? (this.state.counsellingSymptoms.SuicidalAttempts) : ('')) : ('')} onChange={this.handleSymptomChange} class="form-control" rows="3" placeholder="Further details"></textarea>
                                                            </div>
                                                        </div>
                                                    ) : (null)
                                                ) : (null)}
                                            </div>


                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <label class="d-block mb-3"> Do you have any current suicidal thoughts or feelings? </label>
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="suicidalThoughtsYes" name="suicidalThoughts" checked={this.state.suicidalThoughts == "true" ? true : false} value="true" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                                        <label class="custom-control-label" for="suicidalThoughtsYes">Yes</label>
                                                    </div>
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="suicidalThoughtsNo" name="suicidalThoughts" checked={this.state.suicidalThoughts == "false" ? true : false} value="false" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                                        <label class="custom-control-label" for="suicidalThoughtsNo">No</label>
                                                    </div>
                                                </div>
                                            </div>

                                            {
                                                this.state.suicidalThoughts == "true" ? (
                                                    <div class="form-group row">
                                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Please provide details  </label>
                                                        <div class="col-sm-12">
                                                            <textarea rows="3" id="suicidalThoughtsDetail" name="suicidalThoughtsDetail" value={this.state.suicidalThoughtsDetail} onChange={this.handleChange} class="form-control" type="text" placeholder="Details" />
                                                        </div>
                                                    </div>
                                                ) : (
                                                        null
                                                    )
                                            }

                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <label class="d-block mb-3"> Has the patient been previously diagnosed with the mental health condition? </label>
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="mentalHealthConditionsYes" name="mentalHealthConditions" checked={this.state.mentalHealthConditions == "true" ? true : false} value="true" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                                        <label class="custom-control-label" for="mentalHealthConditionsYes">Yes</label>
                                                    </div>
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="mentalHealthConditionsNo" name="mentalHealthConditions" checked={this.state.mentalHealthConditions == "false" ? true : false} value="false" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                                        <label class="custom-control-label" for="mentalHealthConditionsNo">No</label>
                                                    </div>
                                                </div>
                                            </div>

                                            {
                                                this.state.mentalHealthConditions == "true" ? (
                                                    <div class="form-group row">
                                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Please provide details  </label>
                                                        <div class="col-sm-12">
                                                            <textarea rows="3" id="mentalHealthConditionsDetail" name="mentalHealthConditionsDetail" value={this.state.mentalHealthConditionsDetail} onChange={this.handleChange} class="form-control" type="text" placeholder="Details" />
                                                        </div>
                                                    </div>
                                                ) : (
                                                        null
                                                    )
                                            }

                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <label class="d-block mb-3"> Are you currently taking any medication for mental health conditions? </label>
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="mentalHealthMedicationsYes" name="mentalHealthMedications" checked={this.state.mentalHealthMedications == "true" ? true : false} value="true" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                                        <label class="custom-control-label" for="mentalHealthMedicationsYes">Yes</label>
                                                    </div>
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="mentalHealthMedicationsNo" name="mentalHealthMedications" checked={this.state.mentalHealthMedications == "false" ? true : false} value="false" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                                        <label class="custom-control-label" for="mentalHealthMedicationsNo">No</label>
                                                    </div>
                                                </div>
                                            </div>

                                            {
                                                this.state.mentalHealthMedications == "true" ? (
                                                    <div class="form-group row">
                                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Please provide details  </label>
                                                        <div class="col-sm-12">
                                                            <textarea rows="3" id="mentalHealthMedicationsDetail" name="mentalHealthMedicationsDetail" value={this.state.mentalHealthMedicationsDetail} onChange={this.handleChange} class="form-control" type="text" placeholder="Details" />
                                                        </div>
                                                    </div>
                                                ) : (
                                                        null
                                                    )
                                            }


                                            <div class="form-group row">
                                                <div class="col-sm-12">
                                                    <label class="d-block mb-3"> Have you been emotionally or physically abused by an employer, partner or someone close to you? </label>
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="abusesYes" name="abuses" checked={this.state.abuses == "true" ? true : false} value="true" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                                        <label class="custom-control-label" for="abusesYes">Yes</label>
                                                    </div>
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="abusesNo" name="abuses" checked={this.state.abuses == "false" ? true : false} value="false" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                                        <label class="custom-control-label" for="abusesNo">No</label>
                                                    </div>
                                                </div>
                                            </div>

                                            {
                                                this.state.abuses == "true" ? (
                                                    <div class="form-group row">
                                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Please provide details  </label>
                                                        <div class="col-sm-12">
                                                            <textarea rows="3" id="abusesDetail" name="abusesDetail" value={this.state.abusesDetail} onChange={this.handleChange} class="form-control" type="text" placeholder="Details" />
                                                        </div>
                                                    </div>
                                                ) : (
                                                        null
                                                    )
                                            }


                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12" style={{ textAlign: "right" }}>
                                        <br /><button disabled={loading ? (true) : (false)} onClick={() => this.handleCounsilingProfileSubmit()} class="col-sm-2 btn btn-info " type="button" aria-haspopup="true" aria-expanded="false">
                                            {loading ? (
                                                <Loader
                                                    type="Oval"
                                                    color="#2A3F54"
                                                    height={15}
                                                    width={40}
                                                />
                                            ) : ("Submit")}
                                        </button>
                                    </div>
                                </div>

                                <div>

                                    {
                                        (formErrorStatus.status) ? (
                                            hasCounselingProfileMessage ? (
                                                < div class="ui negative message">
                                                    <div class="header">
                                                        Not Submitted.
                                                    </div>
                                                    <p>{formErrorStatus.message}</p>
                                                </div>
                                            ) : (
                                                    null
                                                )

                                        ) : ((formSuccessState.status) ? (
                                            hasCounselingProfileMessage ? (
                                                < div class="ui success message">
                                                    <div class="header">
                                                        Submitted successfully.
                                                    </div>
                                                    <p>{formSuccessState.message}</p>
                                                </ div>
                                            ) : (
                                                    null
                                                )

                                        ) : (''))
                                    }
                                </div>
                            </div>


                        </Accordion.Content>

                        <Accordion.Title
                            className="card-header darkHeader"
                            active={activeIndex === 8}
                            index={8}
                            onClick={this.handleClick}
                        >
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="headerAlign">
                                        <Icon name='dropdown headerRightAlign' />
                                        <div class="">
                                            <div className="accordianLeftTitleText">
                                                <h4 class="mt-0 header-title" > Past Counseling Info </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </Accordion.Title>

                        <Accordion.Content active={activeIndex === 8}>
                            <br />
                            <div class="col-lg-12" >
                                <div >
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label class="d-block mb-3">  Have you previously had counselling?  </label>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="isPreviouslyCounselYes" name="hadPreviousCounselling" checked={this.state.hadPreviousCounselling == "true" ? true : false} value="true" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                                <label class="custom-control-label" for="isPreviouslyCounselYes">Yes</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="isPreviouslyCounselNo" name="hadPreviousCounselling" checked={this.state.hadPreviousCounselling == "false" ? true : false} value="false" onChange={this.handleRadioOnChange} class="custom-control-input" />
                                                <label class="custom-control-label" for="isPreviouslyCounselNo">No</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label">  What was the last date of counselling? </label>
                                        <div class="col-sm-12">
                                            <DateTime
                                                className="DateTime"
                                                id="lastDateOfCounselling"
                                                name="lastDateOfCounselling"
                                                defaultValue={new Date()}
                                                dateFormat="YYYY-MM-DD"
                                                timeFormat="HH:mm"
                                                value={this.state.lastDateOfCounselling}
                                                onChange={(e) => this.handleDate(e, "lastDateOfCounselling")}
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Why did you have counselling on the that occasion </label>
                                        <div class="col-sm-12">
                                            <textarea id="counsellingDetail" name="counsellingDetail" value={this.state.counsellingDetail} onChange={this.handleChange} class="form-control" rows="3" placeholder="Why did you have counselling"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12" style={{ textAlign: "right" }}>
                                        <br /><button disabled={loading ? (true) : (false)} onClick={() => this.handleCounsilingPastProfileSubmit()} class="col-sm-2 btn btn-info " type="button" aria-haspopup="true" aria-expanded="false">
                                            {loading ? (
                                                <Loader
                                                    type="Oval"
                                                    color="#2A3F54"
                                                    height={15}
                                                    width={40}
                                                />
                                            ) : ("Submit")}
                                        </button>
                                    </div>
                                </div>

                                <div>

                                    {
                                        (formErrorStatus.status) ? (
                                            hasCounselingPastProfileMessage ? (
                                                < div class="ui negative message">
                                                    <div class="header">
                                                        Not Submitted.
                                                    </div>
                                                    <p>{formErrorStatus.message}</p>
                                                </div>
                                            ) : (
                                                    null
                                                )

                                        ) : ((formSuccessState.status) ? (
                                            hasCounselingPastProfileMessage ? (
                                                < div class="ui success message">
                                                    <div class="header">
                                                        Submitted successfully.
                                                    </div>
                                                    <p>{formSuccessState.message}</p>
                                                </ div>
                                            ) : (
                                                    null
                                                )

                                        ) : (''))
                                    }
                                </div>
                            </div>

                        </Accordion.Content>

                        <Accordion.Title
                            className="card-header darkHeader"
                            active={activeIndex === 9}
                            index={9}
                            onClick={this.handleClick}
                        >
                            <div class="row">

                                <div class="col-lg-6">
                                    <div class="headerAlign">
                                        <Icon name='dropdown headerRightAlign' />
                                        <div class="">
                                            <div className="accordianLeftTitleText">
                                                <h4 class="mt-0 header-title" > Referrel Information </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="headerRightAlign">
                                        <div className="accordianRightTitleText">
                                            {this.state.medicalReferrelHistoryDisplayInformations.length > 0 ? (
                                                <h4> ( {this.state.medicalReferrelHistoryDisplayInformations.length} )  previous {this.state.medicalReferrelHistoryDisplayInformations.length == 1 ? ("record") : ("records")} available. </h4>
                                            ) : (null)}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </Accordion.Title>

                        <Accordion.Content active={activeIndex === 9}>
                            <br />

                            <div class="col-lg-12">
                                <div class="card HistoryContainer">
                                    <h3 className="HistoryHeading">Hisory Details</h3>

                                    {this.state.medicalReferrelHistoryDisplayInformations.length > 0 ? (
                                        this.state.medicalReferrelHistoryDisplayInformations.map((profile, index) => {
                                            return [<div className="HistoryGrayHeader"> {index + 1 + ") " + "Created Date : " + formatDate(profile.createdAt)} </div>
                                                , <hr />, <div className="HistoryContainerInner">
                                                <Grid>


                                                    <GridRow>
                                                        <GridColumn width={8}>
                                                            <label> <strong> Have Referrels : </strong>
                                                                {profile.isReferrel == "true" ? ("Yes") : ("No")}
                                                            </label>
                                                        </GridColumn>

                                                        <GridColumn width={8}>
                                                            <label> <strong> Referrel Type: </strong>
                                                                {profile.referrelType}
                                                            </label>
                                                        </GridColumn>
                                                    </GridRow>

                                                    {profile.referrelType == "MEDICAL" ? (
                                                        <GridRow>
                                                            <GridColumn width={8}>
                                                                <label> <strong> Hospital Type : </strong>
                                                                    {profile.hospitalType}
                                                                </label>
                                                            </GridColumn>

                                                            <GridColumn width={8}>
                                                                <label> <strong> Hospital Name: </strong>
                                                                    {profile.hospitalName}
                                                                </label>
                                                            </GridColumn>
                                                        </GridRow>
                                                    ) : (null)}

                                                    <GridRow>
                                                        <div className="HistoryMultipleContainer">
                                                            <Grid>
                                                                <GridRow>
                                                                    <h3> Referrel Informations </h3>
                                                                </GridRow>
                                                                {profile.referrelInformations.length > 0 ? (
                                                                    profile.referrelInformations.map((referrelInformation, index) => {
                                                                        return [<GridRow>
                                                                            {index + 1 + ") "}
                                                                            <GridColumn width={6}>
                                                                                <label> <strong> Referrel Name : </strong> {referrelInformation.referalName}  </label>
                                                                                <hr />
                                                                            </GridColumn>
                                                                            <GridColumn width={9}>
                                                                                <label> <strong> Email : </strong> {referrelInformation.email}  </label>
                                                                                <hr />
                                                                            </GridColumn>

                                                                            <GridColumn width={6}>
                                                                                <label> <strong> Phone Number : </strong> {referrelInformation.phoneNumber}  </label>
                                                                                <hr />
                                                                            </GridColumn>

                                                                            <GridColumn width={9}>
                                                                                <label> <strong> Comments : </strong> {referrelInformation.comments}  </label>
                                                                                <hr />
                                                                            </GridColumn>

                                                                        </GridRow>]

                                                                    })
                                                                ) : (null)}
                                                            </Grid>
                                                        </div>
                                                    </GridRow>

                                                </Grid>

                                            </div>]
                                        })
                                    ) : (<h3 style={{ color: "#6c757d" }}>Not any History Records</h3>)}

                                </div>

                            </div>



                            <div class="col-lg-12" >

                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label class="d-block mb-3"> Have Referrels ?  </label>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="isReferrelYes" name="isReferrel" checked={this.state.isReferrel == "true" ? true : false} class="custom-control-input" value="true" onChange={this.handleRadioOnChange} />
                                            <label class="custom-control-label" for="isReferrelYes">Yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="isReferrelNo" name="isReferrel" checked={this.state.isReferrel == "false" ? true : false} class="custom-control-input" value="false" onChange={this.handleRadioOnChange} />
                                            <label class="custom-control-label" for="isReferrelNo">No</label>
                                        </div>
                                    </div>
                                </div>

                                <div >

                                    {this.state.isReferrel == "true" ? (

                                        <div class="card">
                                            <div class="card-body" >

                                                <div class="form-group row">
                                                    <div class="col-sm-12">
                                                        <label class="d-block mb-3"> Referrel Type  </label>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="referrelTypeMedical" name="referrelType" checked={this.state.referrelType == "MEDICAL" ? true : false} class="custom-control-input" value={"MEDICAL"} onChange={this.handleRadioOnChange} />
                                                            <label class="custom-control-label" for="referrelTypeMedical">Medical</label>
                                                        </div>
                                                        <div class="custom-control custom-radio custom-control-inline">
                                                            <input type="radio" id="referrelTypeCounceling" name="referrelType" checked={this.state.referrelType == "COUNSELING" ? true : false} class="custom-control-input" value={"COUNSELING"} onChange={this.handleRadioOnChange} />
                                                            <label class="custom-control-label" for="referrelTypeCounceling">Counseling</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                {this.state.referrelType == "MEDICAL" ? (
                                                    <div>
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Hospital Name </label>
                                                            <div class="col-sm-12">
                                                                <input id="hospitalName" name="hospitalName" value={this.state.hospitalName} onChange={this.handleChange} class="form-control" placeholder="Hospital Name" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label">  Hospital Type </label>
                                                            <div class="col-sm-12">

                                                                <Dropdown
                                                                    placeholder='Select Hospital Type'
                                                                    fluid
                                                                    selection
                                                                    id='hospitalType'
                                                                    name='hospitalType'
                                                                    value={this.state.hospitalType}
                                                                    options={this.state.hospitalTypes}
                                                                    onChange={this.handleOnChange}
                                                                />

                                                            </div>
                                                        </div>
                                                    </div>
                                                ) : (null)}

                                                {this._createReferrelInformation()}
                                                <div style={{ textAlign: "right" }}><button className="btn btn-secondary" onClick={this._addMoreReferrelInformation.bind(this)} type="button">
                                                    Add More Record <i className="fas fa-plus-circle" />
                                                </button>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-12" style={{ textAlign: "right" }}>
                                                        <br /><button disabled={loading ? (true) : (false)} onClick={() => this.handleMedicalReferrelInformationSubmit()} class="col-sm-2 btn btn-info " type="button" aria-haspopup="true" aria-expanded="false">
                                                            {loading ? (
                                                                <Loader
                                                                    type="Oval"
                                                                    color="#2A3F54"
                                                                    height={15}
                                                                    width={40}
                                                                />
                                                            ) : ("Submit")}
                                                        </button>
                                                    </div>
                                                </div>

                                                <div>

                                                    {
                                                        (formErrorStatus.status) ? (
                                                            hasMedicalReferrelInformationMessage ? (
                                                                < div class="ui negative message">
                                                                    <div class="header">
                                                                        Not Submitted.
                                                    </div>
                                                                    <p>{formErrorStatus.message}</p>
                                                                </div>
                                                            ) : (
                                                                    null
                                                                )

                                                        ) : ((formSuccessState.status) ? (
                                                            hasMedicalReferrelInformationMessage ? (
                                                                < div class="ui success message">
                                                                    <div class="header">
                                                                        Submitted successfully.
                                                    </div>
                                                                    <p>{formSuccessState.message}</p>
                                                                </ div>
                                                            ) : (
                                                                    null
                                                                )

                                                        ) : (''))
                                                    }
                                                </div>

                                            </div>
                                        </div>

                                    ) : (
                                            null
                                        )}

                                </div>

                            </div>

                        </Accordion.Content>

                        <Accordion.Title
                            className="card-header darkHeader"
                            active={activeIndex === 10}
                            index={10}
                            onClick={this.handleClick}
                        >
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="headerAlign">
                                        <Icon name='dropdown headerRightAlign' />
                                        <div class="">
                                            <div className="accordianLeftTitleText">
                                                <h4 class="mt-0 header-title" > Set Qualification Status </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </Accordion.Title>

                        <Accordion.Content active={activeIndex === 10}>
                            <br />
                            <div class="col-lg-12" >

                                <div >
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Qualification status  </label>
                                        <div class="col-sm-12">
                                            <Dropdown
                                                placeholder='Qualification status'
                                                fluid
                                                selection
                                                id='qualificationStatus'
                                                name='qualificationStatus'
                                                value={this.state.qualificationStatus}
                                                options={this.state.qualificationStatuses}
                                                onChange={this.handleOnChange}
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-sm-12 col-form-label"> Notes </label>
                                        <div class="col-sm-12">
                                            <textarea id="notes" name="notes" value={this.state.notes} onChange={this.handleChange} class="form-control" rows="3" placeholder="Qualification Status Note"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12" style={{ textAlign: "right" }}>
                                        <br /><button disabled={loading ? (true) : (false)} onClick={() => this.handleMedicalQualificationProfileSubmit()} class="col-sm-2 btn btn-info " type="button" aria-haspopup="true" aria-expanded="false">
                                            {loading ? (
                                                <Loader
                                                    type="Oval"
                                                    color="#2A3F54"
                                                    height={15}
                                                    width={40}
                                                />
                                            ) : ("Submit")}
                                        </button>
                                    </div>
                                </div>

                                <div>

                                    {
                                        (formErrorStatus.status) ? (
                                            hasMedicalQualificationProfileMessage ? (
                                                < div class="ui negative message">
                                                    <div class="header">
                                                        Not Submitted.
                                                    </div>
                                                    <p>{formErrorStatus.message}</p>
                                                </div>
                                            ) : (
                                                    null
                                                )

                                        ) : ((formSuccessState.status) ? (
                                            hasMedicalQualificationProfileMessage ? (
                                                < div class="ui success message">
                                                    <div class="header">
                                                        Submitted successfully.
                                                    </div>
                                                    <p>{formSuccessState.message}</p>
                                                </ div>
                                            ) : (
                                                    null
                                                )

                                        ) : (''))
                                    }
                                </div>

                            </div>


                        </Accordion.Content>


                        <Accordion.Title
                            className="card-header darkHeader"
                            active={activeIndex === 16}
                            index={16}
                            onClick={this.handleClick}
                        >
                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="headerAlign">
                                        <Icon name='dropdown headerRightAlign' />
                                        <div class="">
                                            <div className="accordianLeftTitleText">
                                                <h4 class="mt-0 header-title" > Closing Action (Medical) </h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </Accordion.Title>

                        <Accordion.Content active={activeIndex === 16}>
                            <br />

                            <div class="col-lg-12" >

                                <Modal className="ModalDisplay" size="mini" dimmer="blurring" open={this.state.open} onClose={this.close}>
                                    <Modal.Content>
                                        <Modal.Description>
                                            <Header> Closing Medical File </Header>
                                            <p>
                                                Do you want to confirm this closing action?
                                            </p>

                                        </Modal.Description>
                                    </Modal.Content>
                                    <Modal.Actions>
                                        <Button basic color='blue' onClick={() => this.confirmCloseAction()}>
                                            {loading ? (
                                                <Loader
                                                    type="Oval"
                                                    color="#2A3F54"
                                                    height={15}
                                                    width={40}
                                                />
                                            ) : ("Yes")}
                                        </Button>
                                        <Button basic onClick={this.close}>
                                            No
                                        </Button>
                                    </Modal.Actions>
                                </Modal>


                                <div >

                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label class="d-block mb-3"> Close Medical File </label>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="medicalCloseupStatusYes" name="medicalCloseupStatus" checked={this.state.closeupStatus == "true" ? true : false} value="true" onChange={this.handleMedicalRadioOnChange} class="custom-control-input" />
                                                <label class="custom-control-label" for="medicalCloseupStatusYes">Yes</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input
                                                    // disabled={this.state.isMigrantMedicalClosed == "true" ? true : false} 
                                                    type="radio" id="medicalCloseupStatusNo" name="medicalCloseupStatus" checked={this.state.closeupStatus == "false" ? true : false} value="false" onChange={this.handleMedicalRadioOnChange} class="custom-control-input" />
                                                <label class="custom-control-label" for="medicalCloseupStatusNo">No</label>
                                            </div>
                                        </div>
                                    </div>


                                    {
                                        this.state.closeupStatus == "true" ? (
                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-sm-12 col-form-label"> Close File Comment  </label>
                                                <div class="col-sm-12">
                                                    <input id="closeupAction" name="closeupAction" value={this.state.closeupAction} onChange={this.handleChange} class="form-control" type="text" placeholder="Comment" />
                                                </div>
                                            </div>
                                        ) : (
                                                null
                                            )
                                    }


                                    <div class="form-group row">
                                        <div class="col-sm-12" style={{ textAlign: "right" }}>
                                            <br /><button disabled={loading ? (true) : (false)} onClick={() => this.handleCloseupSubmit()} class="col-sm-2 btn btn-info " type="button" aria-haspopup="true" aria-expanded="false">
                                                {loading ? (
                                                    <Loader
                                                        type="Oval"
                                                        color="#2A3F54"
                                                        height={15}
                                                        width={40}
                                                    />
                                                ) : ("Submit")}
                                            </button>
                                        </div>
                                    </div>

                                    <div>

                                        {
                                            (formErrorStatus.status) ? (
                                                hasClosingMessage ? (
                                                    < div class="ui negative message">
                                                        <div class="header">
                                                            Not Executed.
                                                    </div>
                                                        <p>{formErrorStatus.message}</p>
                                                    </div>
                                                ) : (
                                                        null
                                                    )

                                            ) : ((formSuccessState.status) ? (
                                                hasClosingMessage ? (
                                                    < div class="ui success message">
                                                        <div class="header">
                                                            Executed successfully.
                                                    </div>
                                                        <p>{formSuccessState.message}</p>
                                                    </ div>
                                                ) : (
                                                        null
                                                    )

                                            ) : (''))
                                        }
                                    </div>



                                </div>

                            </div>

                        </Accordion.Content>



                    </Accordion>

                </div>


            </div>

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(MedicalActions)));

