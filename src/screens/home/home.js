import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus } from '../../actions/user-actions';

//query
import { LOGIN } from '../../queries/UserQueries';

//components
import Sidemenu from "../../components/sidemenu/sidemenu";
import Header from "../../components/header/header";
import Footer from "../../components/footer/footer";


//routes
import { adminRoutes } from "../../routes/adminRoutes";
import { userRoutes } from "../../routes/userRoutes";
import ahanapiyasaOfficialRoutes from "../../routes/sahanapiyasaOfficialRoutes";
import missionOfficialRoutes from "../../routes/missionOfficialRoutes";
import { Router, Route, Switch, Redirect } from "react-router-dom";

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            sideRoutes: []
        }

    }

    componentDidMount() {
        localStorage.USER_ROLE == "ADMIN" ? (
            this.setState({ loading: false, sideRoutes: adminRoutes })
        ) : (
                localStorage.USER_ROLE == "SLBFEOFFICIAL" ? (
                    this.setState({ loading: false, sideRoutes: userRoutes })
                ) : (
                        localStorage.USER_ROLE == "SAHANAPIYASAOFFICIAL" ? (
                            this.setState({ loading: false, sideRoutes: ahanapiyasaOfficialRoutes })
                        ) : (
                                localStorage.USER_ROLE == "MISSIONOFFICIAL" ? (
                                    this.setState({ loading: false, sideRoutes: missionOfficialRoutes })
                                ) : (
                                        this.setState({ loading: false, sideRoutes: adminRoutes })
                                    )
                            )

                    )
            )

    }

    setRouteFrame(url) {
        this.props.history.push(url)
    }

    render() {
        const { sideRoutes, loading } = this.state;

        console.log(sideRoutes)
        return (

            <div id="wrapper" style={{ backgroundColor: "#f6f6f6" }}>

                <Header />

                {
                    <div>
                        {loading ? (null) : (
                            <div>
                                <Sidemenu sideRoutes={sideRoutes} getClickedUrl={(url) => this.setRouteFrame(url)} />

                                <Switch>
                                    {sideRoutes.map((prop, key) => {
                                        return <Route path={prop.path} component={prop.component} key={key} />;
                                    })}
                                    <Redirect to="/dashboard" />
                                </Switch>
                            </div>
                        )}

                    </div>

                }

                {/* <Footer /> */}
            </div>

        )
    }
}

export default withRouter(withApollo(Home)); 
