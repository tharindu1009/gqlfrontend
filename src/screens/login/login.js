import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus } from '../../actions/user-actions';

//query
import { LOGIN } from '../../queries/UserQueries';
//Spinner
import Loader from 'react-loader-spinner'

const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        }
    }
}

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: {
                form_state: 'default',
                title: '',
                message: '',
            },
            userName: "",
            password: "",
            loading: false
        };

        props.setFormStatus({ status: false, title: '', message: '' });

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange = (event) => {
        this.setState({ [event.target.id]: event.target.value });
    }

    handleSubmit = () => {

        this.props.setFormStatus({ status: false, title: '', message: '' });

        this.setState({ loading: true });
        const { userName, password } = this.state;

    
        if (!userName == '' || !password == '') {
            console.log(this.state);
            this.login().then(result => {

                localStorage.slbfeauthtoken = result.data.login.token;
                localStorage.USER_ID = result.data.login.user.id;
                localStorage.EMAIL = result.data.login.user.email;
                localStorage.USERNAME = result.data.login.user.userName;
                localStorage.NAME = result.data.login.user.name;
                localStorage.USER_ROLE = result.data.login.user.userRole;

                this.setState({ loading: false });

                this.props.history.push("/home")


            })
                .catch(error => {
                    if (error) {
                        console.log(error);
                        console.log('There is an error');
                        this.props.setFormStatus({ status: true, title: 'Oops!', message: 'Your username or password appeared to be wrong!' });
                    }
                    this.setState({ loading: false });

                });
        } else {
            this.props.setFormStatus({ status: true, title: 'Oops!', message: 'Required All Fields!' });
            this.setState({ userName: "", password: "", loading: false });
            return;
        }


    }

    login = async () => {
        const { userName, password } = this.state;
        console.log(this.props.client)
        const result = this.props.client.mutate(
            {
                mutation: LOGIN,
                variables: { userName, password },
            })
        return result;
    }


    render() {
        const { loading } = this.state;
        const { formErrorStatus } = this.props;
        return (
            <div >

                <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                    <div className="container ">
                        <a className="navbar-brand text-white" href="#">Migrant Worker Welfare Management Platform</a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>

                    </div>
                </nav>


                <div class="wrapper-page" >

                    <div class="card overflow-hidden account-card mx-3"  >

                        <form>
                        <div class="account-card-content card"  >
                            <div className="card-header">
                                <h2 style={{ color: "#495057" }}>Login</h2>
                            </div>

                            <div className="card-body" >
                                <div class="form-group">
                                    <label for="email">User Name</label>
                                    <input type="text" class="form-control" id="userName" name="userName" placeholder="Enter User Name" onChange={this.handleChange} required/>
                                </div>

                                <div class="form-group">
                                    <label for="userpassword">Password</label>
                                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter password" onChange={this.handleChange} required/>
                                </div>
                            </div>

                            <div class="form-group row m-t-20">
                                <div class="col-sm-6">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customControlInline" />
                                        <label class="custom-control-label" for="customControlInline">Remember me</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <button disabled={loading ? (true) : (false)} onClick={() => this.handleSubmit()} class="btn btn-primary w-md waves-effect waves-light" type="submit">
                                        {loading ? (
                                            <Loader
                                                type="Oval"
                                                color="#2A3F54"
                                                height={15}
                                                width={40}
                                            />
                                        ) : ("Log In")}
                                    </button>
                                </div>

                            </div>

                            {(formErrorStatus.status) ? (
                                
                                <div class="ui negative message">

                                    <div class="header">
                                        Login Problem.
                                                </div>
                                    <p>{formErrorStatus.message}</p>
                                </div>

                            ) : (null)}

                            <div class="form-group m-t-10 mb-0 row">
                                <div class="col-12 m-t-20">
                                    <a href="pages-recoverpw.html"><i class="mdi mdi-lock"></i> Forgot your password?</a>
                                </div>
                            </div>

                        </div>
                        </form>
                    </div>

                </div>
            </div>


        )
    }
}

export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(Login))); 
