import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../../actions/user-actions'

import { formatDate, formatDateTime } from '../../../middleware/index';

// Queries
import { RETURNED_REASON_REPORT } from '../../../queries/ReportQueries'

import {
    GET_ALL_RETURN_REASONS
} from '../../../queries/CommonQueries';

//sementic ui
import { Grid, Dropdown } from 'semantic-ui-react';

// Loader
import Loader from 'react-loader-spinner';

import {
    DateTimeInput
} from 'semantic-ui-calendar-react';

import DateTime from 'react-datetime';


const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class ReturnedReasonReport extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            pagination: {
                skip: 0,
                first: 50
            },
            reportData: [],
            searchLoading: false,
            returnedReason: "",
            returnedReasonList: [],
            from: "",
            to: "",
        }

    }

    _export_to_pdf = () => {
        var page = '<html>' + document.getElementById("returnedReasonTableDiv").innerHTML + '</html>';
        var w = window.open();
        w.document.write(page);
        w.print();
        w.close();
    }

    _export_to_excel = () => {

        var uri = '';
        if (navigator.appVersion.indexOf("Win") != -1) {
            uri = 'data:application/vnd.ms-excel;base64,';
        }
        if (navigator.appVersion.indexOf("Linux") != -1) {
            uri = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        }
        var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>Worksheet</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>' + document.getElementById("returnedReasonTable").innerHTML + '</table></body></html>',
            a = document.createElement('a');
        a.href = uri + window.btoa(unescape(encodeURIComponent(template)));
        a.setAttribute('download', 'returnedReasonReport.xls');
        a.click();
    }

    componentDidMount() {
        this.loadAllReturnReasons();
    }

    loadAllReturnReasons() {
        var returnReasons = [];
        this.getAllReturnReasons().then(result => {
            console.log(result)
            returnReasons = result.map((returnReason, i) => {
                return { text: returnReason.reason, value: returnReason.reason, id: 'returnedReason' }
            })

            this.setState({ returnedReasonList: returnReasons });
        })
    }

    getAllReturnReasons = async () => {
        const result = await this.props.client.query({
            query: GET_ALL_RETURN_REASONS,
            fetchPolicy: 'network-only'
        });
        return result.data.getAllReturnReasons;
    };

    handleChange = (event) => {
        this.setState({ [event.target.id]: event.target.value });
    }

    handleOnChange = (e, data) => {
        this.setState({ [data.id]: data.value });
    }

    searchData = () => {
        this.getReturnedReasonReport();
    }

    getReturnedReasonReport() {
        var returnedReason = this.state.returnedReason;

        this.setState({ searchLoading: true });
        this.returnedReasonReport().then(result => {
            console.log(result)
            this.setState({ reportData: result.travelDetails, searchLoading: false });
        })

    }


    returnedReasonReport = async () => {
        var returnedReason = this.state.returnedReason;
        var from = this.state.from;
        var to = this.state.to;

        console.log(returnedReason)
        console.log(from)
        console.log(to)

        const result = await this.props.client.query({
            query: RETURNED_REASON_REPORT,
            variables: { returnedReason, from, to },
            fetchPolicy: 'network-only'
        });
        return result.data.returnedReasonReport;
    }

    viewMigrant = (id) => {
        this.props.history.push("/migrantactions?id=" + id)
    }

    handleDate = (date, id) => {
        console.log(id)
        console.log(date._d)
        var value = formatDate(date._d);
        this.setState({ [id]: value });
    };

    render() {

        const { reportData, loading, searchLoading } = this.state;



        return (
            <div class="content-page">
                <div class="content">
                    <div class="container-fluid">

                        <br />

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">

                                        <h4 class="mt-0 header-title"></h4>
                                        <div class="col-sm-12">

                                            <Grid>
                                                <Grid.Row style={{ float: "right" }}>
                                                    <Grid.Column width={4}>
                                                        <br /><br />
                                                        <a class=" exportLink" onClick={() => this._export_to_pdf()}>Export to PDF</a> / 
                                                        <a class=" exportLink" onClick={() => this._export_to_excel()}>Export to Excel</a>
                                                    </Grid.Column>
                                                    <Grid.Column width={3}>
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> From </label>
                                                            <div class="col-sm-12">
                                                                <DateTime
                                                                    placeholder="From Date"
                                                                    className="DateTime"
                                                                    id="from"
                                                                    name="from"
                                                                    dateFormat="YYYY-MM-DD"
                                                                    timeFormat="HH:mm"
                                                                    //value={this.state.from}
                                                                    onChange={(e) => this.handleDate(e, "from")}
                                                                />
                                                            </div>
                                                        </div>
                                                    </Grid.Column>
                                                    <Grid.Column width={3}>
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> To </label>
                                                            <div class="col-sm-12">
                                                                <DateTime
                                                                    placeholder="To Date"
                                                                    className="DateTime"
                                                                    id="to"
                                                                    name="to"
                                                                    dateFormat="YYYY-MM-DD"
                                                                    timeFormat="HH:mm"
                                                                    //value={this.state.to}
                                                                    onChange={(e) => this.handleDate(e, "to")}
                                                                />
                                                            </div>
                                                        </div>
                                                    </Grid.Column>
                                                    <Grid.Column width={4}>
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Returned Reason </label>
                                                            <div class="col-sm-12">
                                                                <Dropdown
                                                                    style={{ float: "right" }}
                                                                    placeholder='Select Returned Reason'
                                                                    fluid
                                                                    selection
                                                                    id='returnedReason'
                                                                    name='returnedReason'
                                                                    value={this.state.returnedReason}
                                                                    options={this.state.returnedReasonList}
                                                                    onChange={this.handleOnChange}
                                                                />
                                                            </div>
                                                        </div>
                                                    </Grid.Column>
                                                    <Grid.Column width={2} >
                                                        <div class="form-group row" className="reportFilter">
                                                            <button onClick={() => this.searchData()} class="btn btn-default searchButton" type="button" aria-haspopup="true" aria-expanded="false">
                                                                Search
                                                            </button>

                                                        </div>
                                                    </Grid.Column>
                                                </Grid.Row>
                                            </Grid>

                                        </div>

                                        <div id="returnedReasonTableDiv">

                                            <div class="page-title-box">
                                                <div class="row align-items-center">

                                                    <div class="col-sm-6">
                                                        <h2>Migrant Returned Reason Analysis Report</h2>
                                                    </div>

                                                </div>
                                            </div><br />

                                            <div class="table-rep-plugin reportTableBottom" >
                                                <div class="table-responsive mb-0" data-pattern="priority-columns">


                                                    <table id="returnedReasonTable" class="table table-bordered" border="1" style={{ borderSpacing: "0", width: "100%" }}>
                                                        <thead>
                                                            <tr>
                                                                <th > Full Name</th>
                                                                <th > Address</th>
                                                                <th > Gender</th>
                                                                <th > Tel No. </th>
                                                                <th > Occupation Country </th>
                                                                <th > Returned Reason </th>
                                                                
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            {reportData.length > 0 ? (
                                                                reportData.map((detail, index) => (
                                                                    <tr key={index}>
                                                                        <td> {detail.migrant.fullName} </td>
                                                                        <td> {detail.migrant.address} </td>
                                                                        <td> {detail.migrant.gender} </td>
                                                                        <td> {detail.migrant.mobileNumber} / {detail.migrant.telNumber}</td>
                                                                        <td>  {detail.travel.lastOccupationCountry} </td>
                                                                        <td>  {detail.travel.returnedReason} </td>
                                                                        {/* <td>
                                                                            {detail.medical.medicalTreatmentRecords.length > 0 ? (
                                                                                detail.medical.medicalTreatmentRecords.map((record, index) => {
                                                                                    return <div>
                                                                                        <strong> Physician Name : </strong> {record.physicianName} <br />
                                                                                        <strong> Physician Phone : </strong> {record.physicianPhone} <br />
                                                                                        <strong> Physician Address : </strong> {record.physicianAddress}
                                                                                    </div>
                                                                                })
                                                                            ) : (null)}

                                                                        </td> */}
                                                                        {/* <td>
                                                                        <button class="btn btn-primary " onClick={() => this.viewMigrant(detail.migrant.id)} type="button" aria-haspopup="true" aria-expanded="false">View More..</button>
                                                                    </td> */}
                                                                    </tr>
                                                                ))
                                                            ) : (null)}

                                                        </tbody>
                                                    </table>



                                                </div>


                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(ReturnedReasonReport)));

