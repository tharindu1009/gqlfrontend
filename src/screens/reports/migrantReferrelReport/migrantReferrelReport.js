import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../../actions/user-actions'
import { formatDate, formatDateTime } from '../../../middleware/index';

// Queries
import { MIGRANT_REFERREL_REPORT } from '../../../queries/ReportQueries'

//sementic ui
import { Grid, Dropdown } from 'semantic-ui-react';

// Loader
import Loader from 'react-loader-spinner';

import {
    DateTimeInput
} from 'semantic-ui-calendar-react';

import DateTime from 'react-datetime';



const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class MigrantSicknessReport extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            pagination: {
                skip: 0,
                first: 50
            },
            reportData: [],
            searchLoading: false,
            from: "",
            to: "",
            referrelType: "",
            referrelTypes: [
                { text: "Select Referrel Type", value: "", id: 'referrelType' },
                { text: "Medical", value: "MEDICAL", id: 'referrelType' },
                { text: "Counseling", value: "COUNSELING", id: 'referrelType' },
            ],
        }

    }

    componentDidMount() {
        this.getMigrantSicknessReport();
    }


    getMigrantSicknessReport() {
        this.setState({ searchLoading: true });
        this.migrantReferrelReport().then(result => {
            console.log(result)
            this.setState({ reportData: result.referrelDetails, searchLoading: false });
        })

    }

    searchData = () => {
        this.getMigrantSicknessReport();
    }


    migrantReferrelReport = async () => {
        const { from, to, referrelType } = this.state;
        const result = await this.props.client.query({
            query: MIGRANT_REFERREL_REPORT,
            variables: { from, to, referrelType },
            fetchPolicy: 'network-only'
        });
        return result.data.migrantReferrelReport;
    }

    viewMigrant = (id) => {
        this.props.history.push("/migrantactions?id=" + id)
    }

    handleDate = (date, id) => {
        console.log(id)
        console.log(date._d)
        var value = formatDate(date._d);
        this.setState({ [id]: value });
    };

    handleOnChange = (e, data) => {
        this.setState({ [data.id]: data.value });
    }

    _export_to_pdf = () => {
        var page = '<html>' + document.getElementById("referrelTableDiv").innerHTML + '</html>';
        var w = window.open();
        w.document.write(page);
        w.print();
        w.close();
    }

    _export_to_excel = () => {

        var uri = '';
        if (navigator.appVersion.indexOf("Win") != -1) {
            uri = 'data:application/vnd.ms-excel;base64,';
        }
        if (navigator.appVersion.indexOf("Linux") != -1) {
            uri = 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,';
        }
        var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>Worksheet</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>' + document.getElementById("referrelTable").innerHTML + '</table></body></html>',
            a = document.createElement('a');
        a.href = uri + window.btoa(unescape(encodeURIComponent(template)));
        a.setAttribute('download', 'referrel.xls');
        a.click();
    }


    render() {

        const { reportData, loading, searchLoading } = this.state;



        return (
            <div class="content-page">
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">

                                        <div class="col-sm-12">

                                            <Grid>
                                                <Grid.Row style={{ float: "right" }}>
                                                    <Grid.Column width={5}>
                                                        <br /><br />
                                                        <a class=" exportLink" onClick={() => this._export_to_pdf()}>Export to PDF</a> /
                                                        <a class=" exportLink" onClick={() => this._export_to_excel()}>Export to Excel</a>
                                                    </Grid.Column>
                                                    <Grid.Column width={3}>
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> From </label>
                                                            <div class="col-sm-12">
                                                                <DateTime
                                                                    className="DateTime"
                                                                    id="from"
                                                                    name="from"
                                                                    dateFormat="YYYY-MM-DD"
                                                                    timeFormat="HH:mm"
                                                                    onChange={(e) => this.handleDate(e, "from")}
                                                                />
                                                            </div>
                                                        </div>
                                                    </Grid.Column>
                                                    <Grid.Column width={3}>
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> To </label>
                                                            <div class="col-sm-12">
                                                                <DateTime
                                                                    className="DateTime"
                                                                    id="to"
                                                                    name="to"
                                                                    dateFormat="YYYY-MM-DD"
                                                                    timeFormat="HH:mm"
                                                                    onChange={(e) => this.handleDate(e, "to")}
                                                                />
                                                            </div>
                                                        </div>
                                                    </Grid.Column>
                                                    <Grid.Column width={3}>
                                                        <div class="form-group row">
                                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Referrel Type </label>
                                                            <div class="col-sm-12">
                                                                <Dropdown
                                                                    style={{ float: "right" }}
                                                                    placeholder='Referrel Type'
                                                                    fluid
                                                                    selection
                                                                    id='referrelType'
                                                                    name='referrelType'
                                                                    value={this.state.referrelType}
                                                                    options={this.state.referrelTypes}
                                                                    onChange={this.handleOnChange}
                                                                />
                                                            </div>
                                                        </div>
                                                    </Grid.Column>
                                                    <Grid.Column width={2} >
                                                        <div class="form-group row" className="reportFilter">
                                                            <button onClick={() => this.searchData()} class="btn btn-default searchButton" type="button" aria-haspopup="true" aria-expanded="false">
                                                                Search
                                                            </button>

                                                        </div>
                                                    </Grid.Column>
                                                </Grid.Row>
                                            </Grid>

                                        </div>

                                        <div id="referrelTableDiv">
                                            <div class="page-title-box">
                                                <div class="row align-items-center">

                                                    <div class="col-sm-6">
                                                        <h2 class="page-title">Migrant Referrel Analysis Report</h2>
                                                    </div>

                                                </div>
                                            </div><br />

                                            <div class="table-rep-plugin reportTableBottom">
                                                <div class="table-responsive mb-0" data-pattern="priority-columns">


                                                    <table id="referrelTable" class="table table-bordered" border="1" style={{ borderSpacing: "0", width: "100%" }}>
                                                        <thead>
                                                            <tr>
                                                                <th > Full Name</th>
                                                                <th > Address</th>
                                                                <th > Gender</th>
                                                                <th > Tel No. </th>
                                                                <th > Referrel Added Date </th>
                                                                <th > Referrel Type </th>
                                                                <th > Informations </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            {reportData.length > 0 ? (
                                                                reportData.map((detail, index) => (
                                                                    <tr key={index}>
                                                                        <td> {detail.migrant.fullName} </td>
                                                                        <td> {detail.migrant.address} </td>
                                                                        <td> {detail.migrant.gender} </td>
                                                                        <td> {detail.migrant.mobileNumber} / {detail.migrant.telNumber}</td>
                                                                        <td>  {formatDate(detail.referrel.createdAt)} </td>
                                                                        <td> {detail.referrel.referrelType} </td>
                                                                        <td>
                                                                            {detail.referrel.referrelInformations.length > 0 ? (
                                                                                detail.referrel.referrelInformations.map((reff, index) => {
                                                                                    return [<label>Referrel Name :  {reff.referalName}</label>, <br />,
                                                                                    <label>Phone Number :  {reff.phoneNumber}</label>, <br />,
                                                                                    <label>Notes :  {reff.comments}</label>, <hr />,
                                                                                    ]
                                                                                })
                                                                            ) : ("")}
                                                                        </td>

                                                                    </tr>
                                                                ))
                                                            ) : (null)}

                                                        </tbody>
                                                    </table>



                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(MigrantSicknessReport)));

