import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../actions/user-actions'

// Queries
import { GET_ALL_MIGRANTS } from '../../queries/CommonQueries'
import { SEARCH_MIGRANTS } from '../../queries/MigrantQueries'

//sementic ui
import { Grid } from 'semantic-ui-react';

// Loader
import Loader from 'react-loader-spinner';


const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class ManageMigrant extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            pagination: {
                skip: 0,
                first: 50
            },
            migrants: [],
            count: 0,
            searchLoading: false,
            searchTerm: "",
        }

    }

    componentDidMount() {
        this.loadAllMigrants();
    }

    handleChange = (event) => {
        this.setState({ [event.target.id]: event.target.value });
    }

    searchEvent() {
        var searchTerm = this.state.searchTerm;
        if (searchTerm == "" || searchTerm == null) {
            return false;
        } else {
            this.setState({ searchLoading: true });
            this.findEvents(searchTerm).then(result => {
                this.setState({ migrants: result.migrants, searchLoading: false });
            })
        }
    }

    findEvents = async (searchTerm) => {
        const result = await this.props.client.query({
            query: SEARCH_MIGRANTS,
            variables: { searchTerm },
            fetchPolicy: 'network-only'
        });
        return result.data.searchMigrants;
    }

    loadAllMigrants() {
        this.setState({ loading: true });
        this.getAllMigrants(this.state.pagination).then(result => {
            this.setState({ migrants: result.data.getAllMigrants.migrants, count: result.data.getAllMigrants.count, loading: false })
        });
    }

    getAllMigrants = async (pagination) => {
        const result = await this.props.client.query({
            query: GET_ALL_MIGRANTS,
            variables: pagination,
            fetchPolicy: 'network-only'
        });
        return result;
    };

    redirectToMigrantActions = () => {
        this.props.history.push("/migrantactions")
    }

    viewMigrant = (id) => {
        this.props.history.push("/viewMigrant?id=" + id)
    }

    editMigrant = (id) => {
        this.props.history.push("/migrantactions?id=" + id)
    }

    render() {

        const { migrants, loading, searchLoading } = this.state;



        return (
            <div class="content-page">
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">


                                <div class="col-sm-6">
                                    <h4 class="page-title">Manage Migrants</h4>
                                </div>
                                <div class="col-sm-6">

                                    <div class="float-right d-none d-md-block">
                                        <div class="dropdown">
                                            <button onClick={() => this.redirectToMigrantActions()} class="btn btn-primary " type="button" aria-haspopup="true" aria-expanded="false">
                                                <i class="mdi mdi-user mr-2"></i> Add New Migrant
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">

                                        <h4 class="mt-0 header-title"></h4>
                                        <div class="col-sm-12">
                                            <Grid>
                                                <Grid.Row style={{ float: "right" }}>
                                                    <Grid.Column width={11}>
                                                        <span class="successDot"></span> Registered (SLBFE) <br />
                                                        <span class="dangerDot"></span> Unregistered (SLBFE)
                                                    </Grid.Column>
                                                    <Grid.Column width={3}>
                                                        <div class="form-group row ">
                                                            <input accept-charset="ISO-8859-1" style={{ float: "right" }} type="text" id="searchTerm" name="searchTerm" value={this.state.searchTerm} placeholder="Search" onChange={this.handleChange} className="form-control" />
                                                        </div>
                                                    </Grid.Column>
                                                    <Grid.Column width={2}>
                                                        <button style={{ float: "left" }} onClick={() => this.searchEvent()} type="button" className="btn btn-default searchButton" disabled={searchLoading ? (true) : (false)}>
                                                            {searchLoading ? (
                                                                <Loader
                                                                    type="Oval"
                                                                    color="#2A3F54"
                                                                    height={15}
                                                                    width={40}
                                                                />
                                                            ) : ("Search")}
                                                        </button>
                                                    </Grid.Column>
                                                </Grid.Row>
                                            </Grid>
                                        </div> <br/>

                                        <div class="table-rep-plugin">
                                            <div class="table-responsive mb-0" data-pattern="priority-columns">

                                                {loading ? (
                                                    <div className="ListLoader">
                                                        <Loader
                                                            type="ThreeDots"
                                                            color="#5A738E"
                                                            height={100}
                                                            width={100}
                                                        />
                                                    </div>
                                                ) : (
                                                        <table id="tech-companies-1" class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th >
                                                                        REG of SLBFE
                                                                    </th>
                                                                    <th >Full Name</th>
                                                                    <th >Address</th>
                                                                    <th >Gender</th>
                                                                    <th >Mobile Number / Tel Number</th>
                                                                    <th >NIC</th>
                                                                    <th >Passport</th>
                                                                    <th ></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                                {
                                                                    migrants.length > 0 ? (
                                                                        migrants.map((migrant, index) => (
                                                                            <tr key={index}>
                                                                                <td style={{ textAlign: "center" }}> {migrant.registeredSLBFE ? (
                                                                                    <span class="successDot"></span>
                                                                                ) : (
                                                                                        <span class="dangerDot"></span>
                                                                                    )} </td>
                                                                                <td> {migrant.fullName} </td>
                                                                                <td> {migrant.address} </td>
                                                                                <td> {migrant.gender} </td>
                                                                                <td> {migrant.mobileNumber} / {migrant.telNumber}</td>
                                                                                <td>  {migrant.nic} </td>
                                                                                <td>  {migrant.passport} </td>

                                                                                <td>
                                                                                    <button class="btn btn-primary " onClick={() => this.editMigrant(migrant.id)} type="button" aria-haspopup="true" aria-expanded="false">Edit</button>
                                                                                </td>
                                                                            </tr>
                                                                        ))
                                                                    ) : (null)

                                                                }

                                                            </tbody>
                                                        </table>
                                                    )}


                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(ManageMigrant)));

