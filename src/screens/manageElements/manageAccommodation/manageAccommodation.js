import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../../actions/user-actions'

// Queries
import { GET_ALL_ACCOMMODATIONS } from '../../../queries/CommonQueries'


// Loader
import Loader from 'react-loader-spinner';


const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class ManageAccommodation extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            accommodations: [],
            count: 0,

        }

    }

    componentDidMount() {
        this.loadAllAccommodations();
    }

    loadAllAccommodations() {
        this.setState({ loading: true });
        this.getAllAccommodations().then(result => {
            this.setState({ accommodations: result.data.getAllAccommodations, loading: false })
        });
    }

    getAllAccommodations = async () => {
        const result = await this.props.client.query({
            query: GET_ALL_ACCOMMODATIONS,
            fetchPolicy: 'network-only'
        });
        return result;
    };

    addAccommodation = () => {
        this.props.history.push("/accommodation")
    }

    editAccommodation = (id) => {
        this.props.history.push("/accommodation?id="+id)
    }

    render() {

        const { accommodations, loading } = this.state;

        

        return (
            <div class="">
                <div class="">
                    <div class="container-fluid">

                        <div class="page-title-box">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <h4 class="page-title">Manage Accommodation Types</h4>
                                </div>
                                <div class="col-sm-6">
                                    <div class="float-right d-none d-md-block">
                                        <div class="dropdown">
                                            <button onClick={() => this.addAccommodation()} class="btn btn-primary " type="button" aria-haspopup="true" aria-expanded="false">
                                                <i class="mdi mdi-user mr-2"></i> Add Accommodation Type
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">


                                        <div class="table-rep-plugin">
                                            <div class="table-responsive mb-0" data-pattern="priority-columns">

                                                {loading ? (
                                                    <div className="ListLoader">
                                                        <Loader
                                                            type="ThreeDots"
                                                            color="#5A738E"
                                                            height={100}
                                                            width={100}
                                                        />
                                                    </div>
                                                ) : (
                                                        <table id="tech-companies-1" class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th > Accommodation Type</th>
                                                                    <th > Status </th>
                                                                    <th ></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                                {
                                                                    accommodations.length > 0 ? (
                                                                        accommodations.map((accommodation, index) => (
                                                                            <tr key={index}>
                                                                                <td> {accommodation.accommodation} </td>
                                                                                <td> {accommodation.status ? ( <span class="badge badge-success">Active</span> ) : ( <span class="badge badge-danger"> Deactive</span> )} </td>
                                
                                                                                <td>
                                                                                    <button class="btn btn-primary " onClick={() => this.editAccommodation(accommodation.id)} type="button" aria-haspopup="true" aria-expanded="false">Edit</button>
                                                                                </td>
                                                                            </tr>
                                                                        ))
                                                                    ) : (null)

                                                                }
                                                               
                                                            </tbody>
                                                        </table>
                                                    )}


                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(ManageAccommodation)));

