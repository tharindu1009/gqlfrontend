import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../../actions/user-actions'

//sementic ui
import { Dropdown } from 'semantic-ui-react';

// Queries
import { GET_SINGLE_ACCOMMODATION, ADD_ACCOMMODATION, EDIT_ACCOMMODATION } from '../../../queries/CommonQueries'

// Loader
import Loader from 'react-loader-spinner';

const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class AddNewAccommodation extends Component {
    constructor(props) {
        super(props);

        const query = new URLSearchParams(this.props.location.search);
        const id = query.get('id');

        var accommodationId = "";

        if (id != null || id != undefined) {
            accommodationId = id;
        }

        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        this.state = {
            accommodationId: id,
            loading: false,
            accommodation: "",
            status: true,
            statusList: [
                { text: "ACTIVE", value: true, id: 'status' },
                { text: "DEACTIVE", value: false, id: 'status' }
            ],
        }

    }

    componentDidMount() {

        if (this.state.accommodationId) {
            this.loadSingleAccommodation();
        }

    }

    loadSingleAccommodation() {
        this.getSingleAccommodation().then(result => {
            this.setState({
                accommodation: result.accommodation,
                status: result.status,
            });

        })
    }

    getSingleAccommodation = async () => {
        const id = this.state.accommodationId;

        const result = await this.props.client.query({
            query: GET_SINGLE_ACCOMMODATION,
            variables: { id },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleAccommodation;
    }


    handleChange = (event) => {
        this.setState({ [event.target.id]: event.target.value });
    }

    handleOnChange = (e, data) => {
        this.setState({ [data.id]: data.value });
    }

    handleSubmit = () => {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        const { accommodation } = this.state;

        if (accommodation == "") {
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "All fields Required."
            });
            return;
        } else {

            this.setState({ loading: true });
            this.props.setFormStatus({ status: false, title: '', message: '' });
            this.props.setSuccessStatus({ status: false, title: '', message: '' });

            if (this.state.accommodationId) {

                this.editAccommodation().then(result => {
                    this.setState({ loading: false })
                    this.loadSingleAccommodation();

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Accommodation Type edited Successfully!"
                    });

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to edit accommodation type."

                        });
                    }
                });



            } else {


                this.addAccommodation().then(result => {

                    this.setState({
                        loading: false,
                        form_state: 'default',
                        title: "",
                        message: "",
                        accommodation: "",
                        status: true
                    });

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "New Accodation type added Successfully!."
                    });

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to add accommodation type."

                        });
                    }
                });
            }
        }



    }

    addAccommodation = async () => {
        const {
            accommodation,
        } = this.state;

        const status = JSON.parse(this.state.status);

        const result = await this.props.client.mutate({
            mutation: ADD_ACCOMMODATION,
            variables: { accommodation, status }
        });
        return result.data.addAccommodation;
    };

    editAccommodation = async () => {
        const {
            accommodation
        } = this.state;

        const id = this.state.accommodationId;
        const status = JSON.parse(this.state.status);

        const result = await this.props.client.mutate({
            mutation: EDIT_ACCOMMODATION,
            variables: { id, accommodation, status }
        });
        return result.data.editAccommodation;
    };


    render() {

        const { loading } = this.state;
        const { formErrorStatus, formSuccessState } = this.props;

        return (
            <div class="content-page">
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">

                                {this.state.accommodationId ? (
                                    <div class="col-sm-6">
                                        <h4 class="page-title">Edit Accommodation Type</h4>
                                    </div>

                                ) : (
                                        <div class="col-sm-6">
                                            <h4 class="page-title">Add Accommodation Type</h4>
                                        </div>
                                    )}


                            </div>
                        </div>


                        <div class="row">
                            <div class="col-8">
                                <div class="card">
                                    <div class="card-body">

                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Accommodation Type </label>
                                            <div class="col-sm-12">
                                                <input id="accommodation" name="accommodation" value={this.state.accommodation} onChange={this.handleChange} class="form-control" type="text" placeholder="Accommodation" />
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Status </label>
                                            <div class="col-sm-12">
                                                <Dropdown
                                                    placeholder='Select Status'
                                                    fluid
                                                    selection
                                                    id='status'
                                                    name='status'
                                                    value={this.state.status}
                                                    options={this.state.statusList}
                                                    onChange={this.handleOnChange}
                                                />
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <div class="col-sm-12" style={{ textAlign: "right" }}>
                                                <br /><button disabled={loading ? (true) : (false)} class="col-sm-2 btn btn-info " onClick={() => this.handleSubmit()} type="button" aria-haspopup="true" aria-expanded="false">
                                                    {loading ? (
                                                        <Loader
                                                            type="Oval"
                                                            color="#2A3F54"
                                                            height={15}
                                                            width={40}
                                                        />
                                                    ) : ("Submit")}

                                                </button>
                                            </div>
                                        </div>

                                        <div>

                                            {
                                                (formErrorStatus.status) ? (

                                                    < div class="ui negative message">
                                                        <div class="header">
                                                            Not Submitted.
                                                    </div>
                                                        <p>{formErrorStatus.message}</p>
                                                    </div>

                                                ) : ((formSuccessState.status) ? (

                                                    < div class="ui success message">
                                                        <div class="header">
                                                            Submitted successfully.
                                                    </div>
                                                        <p>{formSuccessState.message}</p>
                                                    </ div>

                                                ) : (''))
                                            }
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(AddNewAccommodation)));

