import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../../actions/user-actions'

//sementic ui
import { Dropdown } from 'semantic-ui-react';

// Queries
import { GET_SINGLE_RETURN_REASON, ADD_RETURN_REASON, EDIT_RETURN_REASON } from '../../../queries/CommonQueries'

// Loader
import Loader from 'react-loader-spinner';

const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class AddNewReturnReason extends Component {
    constructor(props) {
        super(props);

        const query = new URLSearchParams(this.props.location.search);
        const id = query.get('id');

        var returnReasonId = "";

        if (id != null || id != undefined) {
            returnReasonId = id;
        }

        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        this.state = {
            returnReasonId: id,
            loading: false,
            reason: "",
        }

    }

    componentDidMount() {
        if (this.state.returnReasonId) {
            this.loadSingleReturnReason();
        }
    }

    loadSingleReturnReason() {
        this.getSingleReturnReason().then(result => {
            this.setState({
                reason: result.reason,
            });
        })
    }

    getSingleReturnReason = async () => {
        const id = this.state.returnReasonId;

        const result = await this.props.client.query({
            query: GET_SINGLE_RETURN_REASON,
            variables: { id },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleReturnReason;
    }


    handleChange = (event) => {
        this.setState({ [event.target.id]: event.target.value });
    }

    handleOnChange = (e, data) => {
        this.setState({ [data.id]: data.value });
    }

    handleSubmit = () => {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        const { reason } = this.state;

        if (reason == "") {
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "All fields Required."
            });
            return;
        } else {

            this.setState({ loading: true });
            this.props.setFormStatus({ status: false, title: '', message: '' });
            this.props.setSuccessStatus({ status: false, title: '', message: '' });

            if (this.state.returnReasonId) {

                this.editReturnReason().then(result => {
                    this.setState({ loading: false })
                    this.loadSingleReturnReason();

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Return Reason edited Successfully!"
                    });

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to edit Return Reason ."

                        });
                    }
                });


            } else {

                this.addReturnReason().then(result => {

                    this.setState({
                        loading: false,
                        form_state: 'default',
                        title: "",
                        message: "",
                        reason: "",
                    });

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "New Return Reason added Successfully!."
                    });

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to add return reason."

                        });
                    }
                });

            }
        }



    }

    addReturnReason = async () => {
        const {
            reason,
        } = this.state;

        const result = await this.props.client.mutate({
            mutation: ADD_RETURN_REASON,
            variables: { reason }
        });
        return result.data.addReturnReason;
    };

    editReturnReason = async () => {
        const {
            reason
        } = this.state;

        const id = this.state.returnReasonId;

        const result = await this.props.client.mutate({
            mutation: EDIT_RETURN_REASON,
            variables: { id, reason }
        });
        return result.data.editReturnReason;
    };


    render() {

        const { loading } = this.state;
        const { formErrorStatus, formSuccessState } = this.props;

        return (
            <div class="content-page">
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">

                                {this.state.sicknessId ? (
                                    <div class="col-sm-6">
                                        <h4 class="page-title">Edit Return Reason</h4>
                                    </div>

                                ) : (
                                        <div class="col-sm-6">
                                            <h4 class="page-title">Add Return Reason </h4>
                                        </div>
                                    )}


                            </div>
                        </div>


                        <div class="row">
                            <div class="col-8">
                                <div class="card">
                                    <div class="card-body">

                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Reason </label>
                                            <div class="col-sm-12">
                                                <input id="reason" name="reason" value={this.state.reason} onChange={this.handleChange} class="form-control" type="text" placeholder="Reason" />
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-12" style={{ textAlign: "right" }}>
                                                <br /><button disabled={loading ? (true) : (false)} class="col-sm-2 btn btn-info " onClick={() => this.handleSubmit()} type="button" aria-haspopup="true" aria-expanded="false">
                                                    {loading ? (
                                                        <Loader
                                                            type="Oval"
                                                            color="#2A3F54"
                                                            height={15}
                                                            width={40}
                                                        />
                                                    ) : ("Submit")}

                                                </button>
                                            </div>
                                        </div>

                                        <div>

                                            {
                                                (formErrorStatus.status) ? (

                                                    < div class="ui negative message">
                                                        <div class="header">
                                                            Not Submitted.
                                                    </div>
                                                        <p>{formErrorStatus.message}</p>
                                                    </div>

                                                ) : ((formSuccessState.status) ? (

                                                    < div class="ui success message">
                                                        <div class="header">
                                                            Submitted successfully.
                                                    </div>
                                                        <p>{formSuccessState.message}</p>
                                                    </ div>

                                                ) : (''))
                                            }
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(AddNewReturnReason)));

