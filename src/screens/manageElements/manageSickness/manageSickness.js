import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../../actions/user-actions'

// Queries
import { GET_ALL_SICKNESSES  } from '../../../queries/CommonQueries'


// Loader
import Loader from 'react-loader-spinner';


const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class ManageSickness extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            sicknesses: [],
            count: 0,

        }

    }

    componentDidMount() {
        this.loadAllSicknesses();
    }

    loadAllSicknesses() {
        this.setState({ loading: true });
        this.getAllSicknesses().then(result => {
            this.setState({ sicknesses: result.data.getAllSicknesses, loading: false })
        });
    }

    getAllSicknesses = async () => {
        const result = await this.props.client.query({
            query: GET_ALL_SICKNESSES,
            fetchPolicy: 'network-only'
        });
        return result;
    };

    addSickness = () => {
        this.props.history.push("/sickness")
    }

    editSickness = (id) => {
        this.props.history.push("/sickness?id="+id)
    }

    render() {

        const { sicknesses, loading } = this.state;

        

        return (
            <div class="">
                <div class="">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">

                                <div class="col-sm-6">
                                    <h4 class="page-title">Manage Sicknesses </h4>
                                </div>
                                <div class="col-sm-6">

                                    <div class="float-right d-none d-md-block">
                                        <div class="dropdown">
                                            <button onClick={() => this.addSickness()} class="btn btn-primary " type="button" aria-haspopup="true" aria-expanded="false">
                                                <i class="mdi mdi-user mr-2"></i> Add Sickness
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">

                                        <div class="table-rep-plugin">
                                            <div class="table-responsive mb-0" data-pattern="priority-columns">

                                                {loading ? (
                                                    <div className="ListLoader">
                                                        <Loader
                                                            type="ThreeDots"
                                                            color="#5A738E"
                                                            height={100}
                                                            width={100}
                                                        />
                                                    </div>
                                                ) : (
                                                        <table id="tech-companies-1" class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th > Sickness</th>
                                                                    <th > Status </th>
                                                                    <th ></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                                {
                                                                    sicknesses.length > 0 ? (
                                                                        sicknesses.map((sick, index) => (
                                                                            <tr key={index}>
                                                                                <td> {sick.sicknessName} </td>
                                                                                <td> {sick.status ? ( <span class="badge badge-success">Active</span> ) : ( <span class="badge badge-danger"> Deactive</span> )} </td>
                                
                                                                                <td>
                                                                                    <button class="btn btn-primary " onClick={() => this.editSickness(sick.id)} type="button" aria-haspopup="true" aria-expanded="false">Edit</button>
                                                                                </td>
                                                                            </tr>
                                                                        ))
                                                                    ) : (null)

                                                                }
                                                               
                                                            </tbody>
                                                        </table>
                                                    )}


                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(ManageSickness)));

