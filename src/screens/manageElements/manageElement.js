import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../actions/user-actions'

//components
import ManageAccommodation  from './manageAccommodation/manageAccommodation';
import ManageSickness  from './manageSickness/manageSickness';
import ManageReturnReason  from './manageReturnReason/manageReturnReason';
import ManageExpenseType  from './manageExpenseType/manageExpenseType';




const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class ManageElement extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
           
        }

    }


    render() {

        return (
            <div class="content-page" >

                <div class="content">
                    <div class="container-fluid" >
                        <div >
                            <div class="page-title-box">
                                <div class="row align-items-center">
                                    <div class="col-sm-6">
                                        <h4 class="page-title">Manage System Elements</h4>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-12">

                                    <div class="col-lg-12">


                                        <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#action1" role="tab">
                                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                                    <span class="d-none d-sm-block">Accommodations</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#action2" role="tab">
                                                    <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                                    <span class="d-none d-sm-block">Sicknesses</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#action3" role="tab">
                                                    <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                                    <span class="d-none d-sm-block">Return Reasons</span>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#action4" role="tab">
                                                    <span class="d-block d-sm-none"><i class="far fa-user"></i></span>
                                                    <span class="d-none d-sm-block">Expense Types</span>
                                                </a>
                                            </li>
                                            
                                        </ul>


                                        <div class="tab-content">
                                            <div class="tab-pane active p-3" id="action1" role="tabpanel">

                                                <ManageAccommodation/>

                                            </div>
                                            <div class="tab-pane p-3" id="action2" role="tabpanel">
                                                
                                                <ManageSickness/>
                                            </div>
                                            <div class="tab-pane p-3" id="action3" role="tabpanel">
                                                
                                                <ManageReturnReason/>
                                            </div>
                                            <div class="tab-pane p-3" id="action4" role="tabpanel">
                                                
                                                <ManageExpenseType/>
                                            </div>
                                            
                                        </div>


                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            </div>

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(ManageElement)));

