import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../../actions/user-actions'

//sementic ui
import { Dropdown } from 'semantic-ui-react';

// Queries
import { GET_SINGLE_EXPENSE_TYPE, ADD_EXPENSE_TYPE, EDIT_EXPENSE_TYPE } from '../../../queries/CommonQueries'

// Loader
import Loader from 'react-loader-spinner';

const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class AddExpenseType extends Component {
    constructor(props) {
        super(props);

        const query = new URLSearchParams(this.props.location.search);
        const id = query.get('id');

        var expenceTypeId = "";

        if (id != null || id != undefined) {
            expenceTypeId = id;
        }

        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        this.state = {
            expenceTypeId: id,
            loading: false,
            expenceType: "",
        }

    }

    componentDidMount() {
        if (this.state.expenceTypeId) {
            this.loadSingleExpense();
        }
    }

    loadSingleExpense() {
        this.getSingleExpense().then(result => {
            this.setState({
                expenceType: result.expenceType,
            });
        })
    }

    getSingleExpense = async () => {
        const id = this.state.expenceTypeId;
        const result = await this.props.client.query({
            query: GET_SINGLE_EXPENSE_TYPE,
            variables: { id },
            fetchPolicy: 'network-only'
        });
        return result.data.getSingleExpense;
    }


    handleChange = (event) => {
        this.setState({ [event.target.id]: event.target.value });
    }

    handleOnChange = (e, data) => {
        this.setState({ [data.id]: data.value });
    }

    handleSubmit = () => {
        this.props.setFormStatus({ status: false, title: '', message: '' });
        this.props.setSuccessStatus({ status: false, title: '', message: '' });

        const { expenceType } = this.state;

        if (expenceType == "") {
            this.props.setFormStatus({
                status: true,
                title: "Oops!",
                message: "All fields Required."
            });
            return;
        } else {

            this.setState({ loading: true });
            this.props.setFormStatus({ status: false, title: '', message: '' });
            this.props.setSuccessStatus({ status: false, title: '', message: '' });

            if (this.state.expenceTypeId) {

                this.editExpenseType().then(result => {
                    this.setState({ loading: false })
                    this.loadSingleExpense();

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "Expense Type edited Successfully!"
                    });

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to edit Expense Type."

                        });
                    }
                });


            } else {

                this.addExpenseType().then(result => {

                    this.setState({
                        loading: false,
                        form_state: 'default',
                        title: "",
                        message: "",
                        expenceType: "",
                    });

                    this.props.setSuccessStatus({
                        status: true,
                        title: "",
                        message:
                            "New Expense Type added Successfully!."
                    });

                }).catch(error => {
                    console.log(error);
                    if (error) {
                        this.setState({ loading: false });
                        this.props.setFormStatus({
                            status: true,
                            title: "Oops!",
                            message:
                                "There was an error while trying to add expense type."

                        });
                    }
                });

            }
        }



    }

    addExpenseType = async () => {
        const {
            expenceType,
        } = this.state;

        const result = await this.props.client.mutate({
            mutation: ADD_EXPENSE_TYPE,
            variables: { expenceType }
        });
        return result.data.addExpenseType;
    };

    editExpenseType = async () => {
        const {
            expenceType
        } = this.state;

        const id = this.state.expenceTypeId;

        const result = await this.props.client.mutate({
            mutation: EDIT_EXPENSE_TYPE,
            variables: { id, expenceType }
        });
        return result.data.editExpenseType;
    };


    render() {

        const { loading } = this.state;
        const { formErrorStatus, formSuccessState } = this.props;

        return (
            <div class="content-page">
                <div class="content">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">

                                {this.state.sicknessId ? (
                                    <div class="col-sm-6">
                                        <h4 class="page-title">Edit Expense Type</h4>
                                    </div>

                                ) : (
                                        <div class="col-sm-6">
                                            <h4 class="page-title">Add Expense Type </h4>
                                        </div>
                                    )}


                            </div>
                        </div>


                        <div class="row">
                            <div class="col-8">
                                <div class="card">
                                    <div class="card-body">

                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-sm-12 col-form-label"> Expense Type </label>
                                            <div class="col-sm-12">
                                                <input id="expenceType" name="expenceType" value={this.state.expenceType} onChange={this.handleChange} class="form-control" type="text" placeholder="Expense Type" />
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-sm-12" style={{ textAlign: "right" }}>
                                                <br /><button disabled={loading ? (true) : (false)} class="col-sm-2 btn btn-info " onClick={() => this.handleSubmit()} type="button" aria-haspopup="true" aria-expanded="false">
                                                    {loading ? (
                                                        <Loader
                                                            type="Oval"
                                                            color="#2A3F54"
                                                            height={15}
                                                            width={40}
                                                        />
                                                    ) : ("Submit")}

                                                </button>
                                            </div>
                                        </div>

                                        <div>

                                            {
                                                (formErrorStatus.status) ? (

                                                    < div class="ui negative message">
                                                        <div class="header">
                                                            Not Submitted.
                                                    </div>
                                                        <p>{formErrorStatus.message}</p>
                                                    </div>

                                                ) : ((formSuccessState.status) ? (

                                                    < div class="ui success message">
                                                        <div class="header">
                                                            Submitted successfully.
                                                    </div>
                                                        <p>{formSuccessState.message}</p>
                                                    </ div>

                                                ) : (''))
                                            }
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(AddExpenseType)));

