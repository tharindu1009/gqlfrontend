import React, { Component } from 'react'

import { connect } from 'react-redux';
import { withApollo } from 'react-apollo';
import { withRouter } from 'react-router-dom';
import { setFormStatus, setSuccessStatus } from '../../../actions/user-actions'

// Queries
import { GET_ALL_EXPENSE_TYPES  } from '../../../queries/CommonQueries'


// Loader
import Loader from 'react-loader-spinner';


const mapStateToProps = (state, ownProps) => {
    return {
        formErrorStatus: state.formErrorStatus,
        formSuccessState: state.formSuccessState,
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        setFormStatus: (status) => {
            dispatch(setFormStatus(status))
        },
        setSuccessStatus: (status) => {
            dispatch(setSuccessStatus(status))
        }
    }
}


class ManageExpenseType extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            expenceTypes: [],
            count: 0,

        }

    }

    componentDidMount() {
        this.loadAllExpenseTypes();
    }

    loadAllExpenseTypes() {
        this.setState({ loading: true });
        this.getAllExpenseTypes().then(result => {
            this.setState({ expenceTypes: result.data.getAllExpenseTypes, loading: false })
        });
    }

    getAllExpenseTypes = async () => {
        const result = await this.props.client.query({
            query: GET_ALL_EXPENSE_TYPES,
            fetchPolicy: 'network-only'
        });
        return result;
    };

    addExpenseType = () => {
        this.props.history.push("/addexpensetype")
    }

    editExpenseType = (id) => {
        this.props.history.push("/editexpensetype?id="+id)
    }

    render() {

        const { expenceTypes, loading } = this.state;

        return (
            <div class="">
                <div class="">
                    <div class="container-fluid">
                        <div class="page-title-box">
                            <div class="row align-items-center">

                                <div class="col-sm-6">
                                    <h4 class="page-title">Manage Expense Types </h4>
                                </div>
                                <div class="col-sm-6">

                                    <div class="float-right d-none d-md-block">
                                        <div class="dropdown">
                                            <button onClick={() => this.addExpenseType()} class="btn btn-primary " type="button" aria-haspopup="true" aria-expanded="false">
                                                <i class="mdi mdi-user mr-2"></i> Add Expense Type
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">

                                        <div class="table-rep-plugin">
                                            <div class="table-responsive mb-0" data-pattern="priority-columns">

                                                {loading ? (
                                                    <div className="ListLoader">
                                                        <Loader
                                                            type="ThreeDots"
                                                            color="#5A738E"
                                                            height={100}
                                                            width={100}
                                                        />
                                                    </div>
                                                ) : (
                                                        <table id="tech-companies-1" class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th > Expense Type</th>
                                                                    <th ></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                                {
                                                                    expenceTypes.length > 0 ? (
                                                                        expenceTypes.map((expenceType, index) => (
                                                                            <tr key={index}>
                                                                                <td> {expenceType.expenceType} </td>
                                                                               
                                                                                <td>
                                                                                    <button class="btn btn-primary " onClick={() => this.editExpenseType(expenceType.id)} type="button" aria-haspopup="true" aria-expanded="false">Edit</button>
                                                                                </td>
                                                                            </tr>
                                                                        ))
                                                                    ) : (null)

                                                                }
                                                               
                                                            </tbody>
                                                        </table>
                                                    )}


                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

        )
    }
}
export default withRouter(withApollo(connect(mapStateToProps, mapDispatchToProps)(ManageExpenseType)));

